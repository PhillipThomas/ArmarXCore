/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::BaseComponent
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/BaseComponent.h>
#include <ArmarXCore/interface/Core/Log.h>

#include <iostream>

class DummyComponent : public armarx::BaseComponent
{
public:
    virtual void setup(int, char*[])
    {
        counter += 1;
    }

    virtual int exec()
    {
        counter += 2;

        return 23;
    }

    static int counter;
};
int DummyComponent::counter;


BOOST_AUTO_TEST_CASE(testBaseComponentRun)
{
    DummyComponent::counter = 0;

    DummyComponent dummy;

    std::vector<std::string> args;
    args.push_back("--Ice.Default.Locator=IceGrid/Locator:tcp -h localhost -p 4061");
    args.push_back("dummy");

    int status = dummy.main(args);

    BOOST_CHECK_EQUAL(status, 23);
    BOOST_CHECK_EQUAL(DummyComponent::counter, 3);
}

class DummyLogComponent : public armarx::BaseComponent, public armarx::Log
{
public:
    virtual void setup(int, char*[]) {}
    virtual int exec()
    {
        armarx::LogPrx l = getProxy<armarx::LogPrx>("Logger");
        l->write(std::string("foo"), armarx::eINFO, std::string("bar"), "", 0, "");

        // the write() function will call shutdown
        waitForShutdown();
        return 23;
    }

    void write(
        const std::string& who,
        armarx::MessageType t,
        const std::string& what,
        const std::string& file,
        int line,
        const std::string& function,
        const Ice::Current& c)
    {
        BOOST_CHECK_EQUAL(who, std::string("foo"));
        BOOST_CHECK_EQUAL(t, armarx::eINFO);
        BOOST_CHECK_EQUAL(what, std::string("bar"));

        shutdown();
    }
};

BOOST_AUTO_TEST_CASE(testBaseComponentAdapterAndProxy)
{
    Ice::ObjectPtr logger(new DummyLogComponent);

    std::vector<std::string> args;
    args.push_back("--Ice.Default.Locator=IceGrid/Locator:tcp -h localhost -p 4061");
    args.push_back("logger");

    dynamic_cast<armarx::BaseComponent*>(&*logger)->setName("Logger");
    int status = dynamic_cast<armarx::BaseComponent*>(&*logger)->main(args);

    BOOST_CHECK_EQUAL(status, 23);
}

class DummyStormLogComponent : public armarx::BaseComponent, public armarx::Log
{
public:
    int counter;
    virtual void setup(int, char*[]) {}
    virtual int exec()
    {
        counter = 3;
        stormSubscribeTopic("Log");

        armarx::LogPrx l = stormGetTopic<armarx::LogPrx>("Log");
        l->write(std::string("Logger"), armarx::eINFO, std::string("bar"), "", 0, "");

        logf(armarx::eINFO, "%s", "bar");

        ARMARX_LOG << "bar" << armarx::flush;

        // the write() function will call shutdown
        waitForShutdown();
        return 23;
    }

    void write(
        const std::string& who,
        armarx::MessageType t,
        const std::string& what,
        const std::string& file,
        int line,
        const std::string& function,
        const Ice::Current& c)
    {
        BOOST_CHECK_EQUAL(who, std::string("Logger"));
        BOOST_CHECK_EQUAL(t, armarx::eINFO);
        BOOST_CHECK_EQUAL(what, std::string("bar"));

        if (--counter == 0)
        {
            shutdown();
        }
    }
};

BOOST_AUTO_TEST_CASE(testBaseComponentIceStorm)
{
    Ice::ObjectPtr logger(new DummyStormLogComponent);

    std::vector<std::string> args;
    args.push_back("--Ice.Default.Locator=IceGrid/Locator:tcp -h localhost -p 4061");
    args.push_back("logger");

    dynamic_cast<armarx::BaseComponent*>(&*logger)->setName("Logger");
    int status = dynamic_cast<armarx::BaseComponent*>(&*logger)->main(args);

    BOOST_CHECK_EQUAL(status, 23);
}
