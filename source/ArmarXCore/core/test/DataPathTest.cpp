/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::DataPath
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>

#include <iostream>

using namespace armarx;



BOOST_AUTO_TEST_CASE(testDataPathCleanUp)
{
    std::string path = "/root/user/projects/armarx/trunk/Core/../";
    std::string cleanedPath = ArmarXDataPath::cleanPath(path);
    BOOST_CHECK_EQUAL(cleanedPath, "/root/user/projects/armarx/trunk/");

    path = "../bla/../blub/google/../web/com/../../";
    cleanedPath = ArmarXDataPath::cleanPath(path);
    BOOST_CHECK_EQUAL(cleanedPath, "../blub/");
    BOOST_CHECK_EQUAL(ArmarXDataPath::cleanPath("../blub/"), "../blub/");
    BOOST_CHECK_EQUAL(ArmarXDataPath::cleanPath("../blub"), "../blub");


}

BOOST_AUTO_TEST_CASE(testReplaceEnvVars)
{
    std::string path = "${HOME}/root/user/projects/armarx/trunk/Core/../";

    //    std::cout << "replaced path: " << ArmarXDataPath::ReplaceEnvVars(path) << std::endl;
    if (ArmarXDataPath::ReplaceEnvVars(path))
    {
        BOOST_CHECK(path.find("${HOME}") == std::string::npos);
    }
    else
    {
        std::cout << "aborted test" << std::endl;
    }


}

class TestCMakePackageFinder : public CMakePackageFinder
{
public:
    TestCMakePackageFinder(const std::string& packageName) : CMakePackageFinder(packageName)
    {}
    bool test()
    {
        bool result = true;
        std::string var, content;
        result &= _ParseString("-- VAR_NAME:/home/git/project.code/blabla/", var, content);
        BOOST_CHECK_EQUAL(var, "VAR_NAME");
        BOOST_CHECK_EQUAL(content, "/home/git/project.code/blabla/");
        return result;
    }
};


BOOST_AUTO_TEST_CASE(testFindPackage)
{


    ARMARX_INFO_S << CMakePackageFinder::FindPackageIncludePaths("ArmarXCore");
    //    ARMARX_INFO_S << CMakePackageFinder::FindPackageIncludePaths("ArmarXCore2");
    CMakePackageFinder package("ArmarXCore");
    CMakePackageFinder nopackage("ArmarXCoreXXXXX");
    BOOST_CHECK_EQUAL(xstr(DEPENDENCIES), "ArmarXCore");
    BOOST_CHECK_EQUAL(package.packageFound(), true);
    BOOST_CHECK_EQUAL(nopackage.packageFound(), false);
    //    ARMARX_INFO_S << package.getVars();
    std::string var = package.getVar("EXECUTABLE");
    //    ARMARX_INFO_S << var;
    BOOST_CHECK(var.find("DataPathTest") != std::string::npos);

    CMakePackageFinder core("ArmarXCore");
    core.getCMakeDir();
    BOOST_CHECK(!core.getBinaryDir().empty());
    BOOST_CHECK(!core.getDataDir().empty());
    BOOST_CHECK(!core.getCMakeDir().empty());
    BOOST_CHECK(!core.getExecutables().empty());
    BOOST_CHECK(!core.getIncludePaths().empty());
    BOOST_CHECK(!core.getInterfacePaths().empty());
    BOOST_CHECK(!core.getLibraryPaths().empty());
    BOOST_CHECK(!core.getLibs().empty());
    ArmarXPackageToolInterface tool;
    TestCMakePackageFinder testFinder("ArmarXCore");
    BOOST_CHECK(testFinder.test());
    ARMARX_INFO_S << core.getCMakeDir();

    if (!core.getBuildDir().empty())
    {
        CMakePackageFinder coreWithPath("ArmarXCore", core.getBuildDir());
        BOOST_CHECK(coreWithPath.packageFound());
        BOOST_CHECK_EQUAL(coreWithPath.getBuildDir(), core.getBuildDir());
    }

}





