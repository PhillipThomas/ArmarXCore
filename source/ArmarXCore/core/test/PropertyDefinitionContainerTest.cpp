
/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionContainer
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<std::string>("i-am-optional:string", "default text", "Desc: a default text");
        defineOptionalProperty<int>("i-am-optional:int", 42, "Desc: ...");
        defineOptionalProperty<float>("i-am-optional:float", 42, "Desc: ...");
        defineOptionalProperty<TestTypes::EnumType>("i-am-optional:EnumType", TestTypes::One, "Desc: ...");
        defineOptionalProperty<TestTypes::StructType>("i-am-optional:StructType", TestTypes::StructType(), "Desc: ...");
        defineOptionalProperty<TestTypes::ClassType>("i-am-optional:ClassType", TestTypes::ClassType(), "Desc: ...");

        defineRequiredProperty<std::string>("i-am-required:string", "Desc: ...");
        defineRequiredProperty<int>("i-am-required:int", "Desc: ...");
        defineRequiredProperty<float>("i-am-required:float", "Desc: ...");
        defineRequiredProperty<TestTypes::EnumType>("i-am-required:EnumType", "Desc: ...");
        defineRequiredProperty<TestTypes::StructType>("i-am-required:StructType", "Desc: ...");
        defineRequiredProperty<TestTypes::ClassType>("i-am-required:ClassType", "Desc: ...");
    }
};



PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)

BOOST_AUTO_TEST_CASE(DefinitionRequirementTest)
{
    ComponentPrototype component;

    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<std::string>("i-am-optional:string").isRequired());
    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<int>("i-am-optional:int").isRequired());
    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<float>("i-am-optional:float").isRequired());
    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<TestTypes::EnumType>("i-am-optional:EnumType").isRequired());
    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<TestTypes::StructType>("i-am-optional:StructType").isRequired());
    BOOST_CHECK(!component.getPropertyDefinitions()->getDefintion<TestTypes::ClassType>("i-am-optional:ClassType").isRequired());

    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<std::string>("i-am-required:string").isRequired());
    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<int>("i-am-required:int").isRequired());
    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<float>("i-am-required:float").isRequired());
    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<TestTypes::EnumType>("i-am-required:EnumType").isRequired());
    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<TestTypes::StructType>("i-am-required:StructType").isRequired());
    BOOST_CHECK(component.getPropertyDefinitions()->getDefintion<TestTypes::ClassType>("i-am-required:ClassType").isRequired());
}

BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)

