/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nils Adermann (naderman at naderman dot de)
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2010
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IceManager.h"
#include "IceGridAdmin.h"
#include "Component.h"

#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;

IceManager::IceManager(Ice::CommunicatorPtr communicator, std::string name) :
    communicator(communicator),
    name(name),
    forceShutdown(false)
{
    setTag("IceManager");
}

IceManager::~IceManager()
{
    destroy();
}


ObjectHandles IceManager::registerObject(Ice::ObjectPtr object,
        const std::string& objectName,
        const Ice::ObjectAdapterPtr& adapterToAddTo)
{
    boost::mutex::scoped_lock lock(objectRegistryMutex);

    if (isObjectReachable(objectName))
    {
        throw Ice::AlreadyRegisteredException(__FILE__, __LINE__, object->ice_id(), objectName);
    }

    ObjectEntryPtr objectEntry = getOrCreateObjectEntry(objectName);

    objectEntry->id = getCommunicator()->stringToIdentity(objectName);

    if (adapterToAddTo)
    {
        objectEntry->adapter = adapterToAddTo;
        objectEntry->ownAdapter = false;
    }
    else
    {
        objectEntry->adapter =
            getCommunicator()
            ->createObjectAdapterWithEndpoints(objectName, "tcp");
        objectEntry->adapter->activate();
        objectEntry->ownAdapter = true;
    }

    objectEntry->adapter->add(object, objectEntry->id);
    objectEntry->proxy = objectEntry->adapter->createProxy(objectEntry->id);




    ARMARX_VERBOSE
            << objectEntry->name
            << " registered"
            << flush;

    return ObjectHandles(objectEntry->proxy, objectEntry->adapter);
}


void IceManager::removeObject(const std::string& objectName)
{
    IceGrid::AdminPrx admin = getIceGridSession()->getAdmin();

    boost::mutex::scoped_lock lock(objectRegistryMutex);
    {
        ObjectRegistry::iterator objectIt = objectRegistry.find(objectName);

        if (objectIt != objectRegistry.end())
        {
            try
            {
                ObjectEntryPtr objectEntry = objectIt->second;
                Ice::ObjectAdapterPtr adapter = objectIt->second->adapter;

                for (auto & topic : objectEntry->usedTopics)
                {
                    unsubscribeTopic(objectEntry->proxy, topic);
                }

                if (adapter)
                {
                    if (objectEntry->ownAdapter)
                    {
                        adapter->destroy();
                    }
                    else if (!adapter->isDeactivated())
                    {
                        adapter->remove(objectEntry->id);
                    }
                }


                //                objectIt->second->adapter->remove(objectIt->second->id); // deactivate object adapter
                //ARMARX_VERBOSE << "removing object from ice: " << objectName << " with id: " <<  objectIt->second->id.name;
                admin->removeObject(objectIt->second->id);
            }
            catch (IceGrid::ObjectNotRegisteredException& notRegisteredException)
            {
                // removing an unregistered object
                //                 //!!!
                //                ARMARX_ERROR << "*** ARMARX_ERROR: IceManager >> removing "
                //                          << objectName
                //                          << " object failed due to ObjectNotRegisteredException"
                //                          << flush;

            }
            catch (IceGrid::DeploymentException& deploymentException)
            {
                // cannot remove object due to deployment
                ARMARX_ERROR << "*** removing "
                             << objectName
                             << " object failed due to DeploymentException"
                             << flush;
            }
            catch (Ice::ObjectAdapterDeactivatedException& e)
            {
                ARMARX_INFO << "ObjectAdapterDeactivatedException for " << objectIt->second->id.name;
            }

            objectRegistry.erase_return_void(objectIt);
        }
    }
}


IceStorm::TopicManagerPrx IceManager::getTopicManager()
{
    boost::mutex::scoped_lock lock(topicManagerMutex);

    if (!topicManagerProxy)
    {
        Ice::ObjectPrx obj = getCommunicator()
                             ->stringToProxy("IceStorm/TopicManager");

        topicManagerProxy = IceStorm::TopicManagerPrx::checkedCast(obj);
    }

    return topicManagerProxy;
}


void IceManager::subscribeTopic(Ice::ObjectPrx subscriberProxy,
                                const std::string& topicName)
{
    IceStorm::TopicPrx topic = retrieveTopic(topicName);

    IceStorm::QoS qos;
    // ensure ordered. If CPU is heavily used, the messages might arrive out of sequence even with one publisher if not enabled
    // might be a performance issue. But as long as this is not proved leave ordered in order to prevent unexpected behavior (as publishers would assume blocking calls)
    qos["reliability"] = "ordered";

    try
    {
        topic->subscribeAndGetPublisher(qos, subscriberProxy);
    }
    catch (IceStorm::AlreadySubscribed e)
    {
        try
        {
            unsubscribeTopic(subscriberProxy, topicName);
            topic->subscribeAndGetPublisher(qos, subscriberProxy);
        }
        catch (IceStorm::AlreadySubscribed e)
        {
            ARMARX_INFO
                    << topicName
                    << " already subscribed"
                    << flush;
        }
    }

    ARMARX_INFO << "Subscribed to topic " << topicName;
    boost::mutex::scoped_lock lock(topicSubscriptionMutex);
    {
        subscriptions.push_back(std::make_pair(topicName, subscriberProxy));
    }
}

void IceManager::unsubscribeTopic(Ice::ObjectPrx subscriberProxy,
                                  const std::string& topicName)
{
    IceStorm::TopicPrx topic = retrieveTopic(topicName);

    topic->unsubscribe(subscriberProxy);
    ARMARX_INFO << "Unsubscribed from topic " << topicName;
    boost::mutex::scoped_lock lock(topicSubscriptionMutex);
    {
        std::vector<std::pair<std::string, Ice::ObjectPrx> >::iterator toDelete = subscriptions.end();
        std::vector<std::pair<std::string, Ice::ObjectPrx> >::iterator it;

        for (it = subscriptions.begin(); it != subscriptions.end(); ++it)
        {
            if (it->first == topicName && it->second == subscriberProxy)
            {
                toDelete = it;
            }
        }

        // we have to check because an old component which is not in our list might still be subscribed
        if (toDelete != subscriptions.end())
        {
            subscriptions.erase(toDelete);
        }
    }
}

IceStorm::TopicPrx IceManager::retrieveTopic(const std::string& topicName)
{
    IceStorm::TopicPrx topic;

    boost::mutex::scoped_lock lock(topicRetrievalMutex);
    {
        std::map<std::string, IceStorm::TopicPrx>::iterator topicIterator =
            topics.find(topicName);

        if (topicIterator == topics.end())
        {
            while (!isShutdown() && !topic)
            {
                try
                {
                    topic = getTopicManager()->retrieve(topicName);
                }
                catch (const IceStorm::NoSuchTopic&)
                {
                    try
                    {
                        topic = getTopicManager()->create(topicName);
                        ARMARX_INFO << "Topic " << topicName << " created " << flush;
                    }
                    catch (const IceStorm::TopicExists&)
                    {
                        // if the topic has been created in the meanwhile
                        // retry the retrieval.
                    }
                }
            }

            topics[topicName] = topic;
        }
        else
        {
            topic = topicIterator->second;
        }
    }

    return topic;
}


void IceManager::shutdown()
{
    cleanUp();

    getCommunicator()->shutdown();
}


void IceManager::waitForShutdown()
{
    getCommunicator()->waitForShutdown();
}


bool IceManager::isShutdown()
{
    return getCommunicator()->isShutdown();
}


void IceManager::destroy()
{
    if (iceGridAdmin)
    {
        iceGridAdmin->stop();
    }

    getCommunicator()->destroy();
}


void IceManager::cleanUp()
{
    ARMARX_INFO << " *** CLEAN UP ***" << flush;

    // unsubscribe all and remove all objects
    if (getCommunicator())
    {
        IceGrid::AdminPrx admin = getIceGridSession()->getAdmin();
        std::vector<std::pair<std::string, Ice::ObjectPrx> >::iterator it;

        {
            /* topicSubscription lock scope */
            boost::mutex::scoped_lock lock(topicSubscriptionMutex);
            {
                for (it = subscriptions.begin(); it != subscriptions.end(); ++it)
                {
                    retrieveTopic(it->first)->unsubscribe(it->second);
                }

                subscriptions.clear();
            }
        }

        boost::mutex::scoped_lock lock(objectRegistryMutex);
        {
            ObjectRegistry::iterator objListIt = objectRegistry.begin();

            for (; objListIt != objectRegistry.end(); ++objListIt)
            {
                try
                {
                    objListIt->second->adapter->deactivate();
                    //                    objListIt->second->adapter->remove(objListIt->second->id);  // deactivate object adapter
                    admin->removeObject(objListIt->second->id);
                }
                catch (...)
                {
                }
            }
        }

        if (iceGridAdmin)
        {
            iceGridAdmin->removeObservers();
        }
    }
}


void IceManager::registerObjectDependency(
    const std::string& registrantName,
    const std::string& dependencyObjectName)
{
    boost::mutex::scoped_lock lock(objectRegistryMutex);
    {
        ObjectEntryPtr objectEntry = getOrCreateObjectEntry(registrantName);

        objectEntry->dependencies.push_back
        (
            new DependencyObjectEntry
            (
                dependencyObjectName,
                getCommunicator()->stringToProxy(dependencyObjectName)
            )
        );
    }
}


void IceManager::resolveObjectDependencies()
{
    ObjectRegistry::iterator objectIt = objectRegistry.begin();

    for (; objectIt != objectRegistry.end(); ++objectIt)
    {
        std::string missingObjects;
        ObjectEntryPtr objectEntry = objectIt->second;

        if (!objectEntry->active && objectEntry->proxy)
        {
            objectEntry->dependenciesResolved = true;
            DependencyList::iterator depIt = objectEntry->dependencies.begin();

            for (; depIt != objectEntry->dependencies.end(); ++depIt)
            {
                DependencyObjectEntryPtr dependencyEntry = *depIt;

                if (!dependencyEntry->resolved)
                {
                    try
                    {
                        dependencyEntry->proxy->ice_timeout(2000)->ice_ping();

                        ARMARX_INFO
                                << objectEntry->name
                                << " found "
                                << dependencyEntry->name
                                << flush;

                        dependencyEntry->resolved = true;

                        objectEntry->updated = true;
                    }
                    catch (...)
                    {
                        objectEntry->dependenciesResolved = false;

                        missingObjects += "\t" + dependencyEntry->name + "\n";
                    }

                    IceUtil::ThreadControl::sleep(IceUtil::Time::milliSeconds(10));
                }
            }
        }

        if (objectEntry->updated)
        {
            if (missingObjects.length() > 0)
            {
                ARMARX_INFO
                        << objectEntry->name
                        << " still waiting for:\n"
                        << missingObjects
                        << flush;
            }

            objectEntry->updated = false;

            if (objectEntry->dependenciesResolved)
            {
                ARMARX_INFO << " all "
                            << objectEntry->name
                            << " dependencies resolved"
                            << flush;
            }
        }
    }
}


void IceManager::registerDelayedTopicSubscription(
    const std::string& registrantName,
    const std::string& topicName)
{
    boost::mutex::scoped_lock lock(objectRegistryMutex);

    ObjectEntryPtr objectEntry = getOrCreateObjectEntry(registrantName);

    objectEntry->usedTopics.push_back(topicName);
}


void IceManager::registerDelayedTopicRetrieval(
    const std::string& registrantName,
    const std::string& topicName)
{
    boost::mutex::scoped_lock lock(objectRegistryMutex);

    ObjectEntryPtr objectEntry = getOrCreateObjectEntry(registrantName);

    objectEntry->offeredTopics.push_back(topicName);
}


void IceManager::subscribeTopics(Ice::ObjectPrx subscriber, const TopicList& topics)
{
    TopicList::const_iterator it = topics.begin();

    for (; it != topics.end(); ++it)
    {
        subscribeTopic(subscriber, *it);
    }

}


void IceManager::retrieveTopics(const TopicList& topics)
{
    TopicList::const_iterator it = topics.begin();

    for (; it != topics.end(); ++it)
    {
        retrieveTopic(*it);
    }
}


void IceManager::setName(std::string name)
{
    this->name = name;
}


Ice::CommunicatorPtr IceManager::getCommunicator()
{
    return communicator;
}


IceGridAdminPtr IceManager::getIceGridSession()
{
    boost::mutex::scoped_lock lock(iceGridAdminMutex);

    if (!iceGridAdmin)
    {
        iceGridAdmin = new IceGridAdmin(getCommunicator(), name);
    }

    return iceGridAdmin;
}


IceGrid::RegistryPrx IceManager::getIceGridRegistry()
{
    return getIceGridSession()->registry();
}


IceManager::ObjectEntryPtr
IceManager::getOrCreateObjectEntry(const std::string& objectName)
{
    ObjectRegistry::iterator objIt = objectRegistry.find(objectName);

    if (objIt == objectRegistry.end() || objIt->second->name.empty())
    {
        ObjectEntryPtr objectEntry = new ObjectEntry();
        objectEntry->name = objectName;

        objectRegistry[objectName] = objectEntry;

        return objectEntry;
    }

    return objIt->second;
}



bool IceManager::isObjectReachable(std::string objectName)
{
    try
    {
        Ice::ObjectPrx prx = getProxy<Ice::ObjectPrx>(objectName);
        prx->ice_timeout(500)->ice_ping();
        return true; // if ping'able, object is already registered
        //        throw Ice::AlreadyRegisteredException(__FILE__,__LINE__, object->ice_id(), object->getName());
    }
    catch (...)
    {

        return false;
    }
}
