/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXManager.h"
#include "IceGridAdmin.h"
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <Ice/Initialize.h>

#include <thread>
#include <chrono>

using namespace armarx;

#define MANAGEROBJNAME applicationName + "Manager"
#define OBJOBSNAME std::string("ArmarXObjectObserver_") + applicationName

// *******************************************************
// construction
// *******************************************************
ArmarXManager::ArmarXManager(std::string applicationName, int port, std::string host, std::string locatorName, Ice::StringSeq args) :
    applicationName(applicationName),
    managerState(eCreated)
{
    // initialize communicator
    std::stringstream defaultLocator;
    defaultLocator << "--Ice.Default.Locator=" << locatorName << ":tcp -p " << port << " -h " << host;
    args.push_back(defaultLocator.str());
    Ice::CommunicatorPtr communicator = Ice::initialize(args);

    // init members
    init(applicationName, communicator);
}

ArmarXManager::ArmarXManager(std::string applicationName, Ice::CommunicatorPtr communicator) :
    applicationName(applicationName),
    managerState(eCreated)
{
    // init members
    init(applicationName, communicator);
}


ArmarXManager::~ArmarXManager()
{
    shutdown();
    waitForShutdown();
}

// *******************************************************
// ArmarXManager property setters
// *******************************************************
void ArmarXManager::enableLogging(bool enable)
{
    LogSender::SetLoggingActivated(enable);
}


void ArmarXManager::enableProfiling(bool enable)
{
    ThreadList::getApplicationThreadList()->enableProfiler(enable);
}


void ArmarXManager::setGlobalMinimumLoggingLevel(MessageType minimumLoggingLevel)
{
    LogSender::SetGlobalMinimumLoggingLevel(minimumLoggingLevel);
}

void ArmarXManager::setDataPaths(std::string dataPaths)
{
    ArmarXDataPath::initDataPaths(dataPaths);
}

// *******************************************************
// adding / removing ManagedIceObjects
// *******************************************************


void ArmarXManager::addObject(ManagedIceObjectPtr object, bool addWithOwnAdapter)
{
    addObject(object,
              addWithOwnAdapter ? Ice::ObjectAdapterPtr() : getAdapter());

}

void ArmarXManager::addObject(ManagedIceObjectPtr object, Ice::ObjectAdapterPtr objectAdapterToAddTo)
{
    if (!object)
    {
        throw LocalException("Cannot add NULL object");
    }

    if (object->getName().empty())
    {
        throw LocalException("Object name must not be empty");
    }

    if (!acquireManagedObjectsMutex())
    {
        return;
    }

    bool reachable = false;

    try
    {
        reachable = getIceManager()->isObjectReachable(object->getName());
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
        return;
    }

    if (reachable)
    {
        releaseManagedObjectsMutex();
        throw Ice::AlreadyRegisteredException(__FILE__, __LINE__, object->ice_id(), object->getName());
    }

    try
    {
        ArmarXObjectSchedulerPtr objectScheduler = new ArmarXObjectScheduler(this, iceManager, object, objectAdapterToAddTo);
        managedObjects.insert(std::make_pair(object->getName(), objectScheduler));
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

void ArmarXManager::removeObjectBlocking(const std::string& objectName)
{
    if (!acquireManagedObjectsMutex())
    {
        return;
    }

    try
    {
        removeObject(managedObjects.find(objectName), true);
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

void ArmarXManager::removeObjectNonBlocking(const std::string& objectName)
{
    if (!acquireManagedObjectsMutex())
    {
        return;
    }

    try
    {
        removeObject(managedObjects.find(objectName), false);
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

void ArmarXManager::removeObjectBlocking(ManagedIceObjectPtr object)
{
    removeObjectBlocking(object->getName());
}

void ArmarXManager::removeObjectNonBlocking(ManagedIceObjectPtr object)
{
    removeObjectNonBlocking(object->getName());
}

// *******************************************************
// shutdown handling
// *******************************************************
void ArmarXManager::waitForShutdown()
{
    // assure no state changed
    managerStateMutex.lock();

    if (managerState == eShutdown)
    {
        managerStateMutex.unlock();
        return;
    }

    // wait for shutdown
    boost::unique_lock<boost::mutex> lock(shutdownMutex);
    managerStateMutex.unlock();
    shutdownCondition.wait(lock);
}

void ArmarXManager::shutdown()
{
    try
    {
        // check state
        {
            boost::mutex::scoped_lock lock(managerStateMutex);

            // do not shutdown if shutdown is in progress
            if (managerState >= eShutdownInProgress)
            {
                return;
            }

            managerState = eShutdownInProgress;

            // locking managedObjects ne done before state mutex is released
            managedObjectsMutex.lock();
        }

        ARMARX_VERBOSE << "Shutting down ArmarXManager" << std::endl;

        // stop cleanup task. All objects will be removed manually in shutdown
        cleanupSchedulersTask->stop();

        if (checkDependencyStatusTask)
        {
            checkDependencyStatusTask->stop();
        }


        disconnectAllObjects();

        // shutdown log sender
        LogSender::SetSendLoggingActivated(false);



        // deactivate all object adapters (and do internal iceManager shutdown)
        iceManager->shutdown();

        // wait until all adapters have been deactivated
        iceManager->waitForShutdown();

        iceManager->removeObject(MANAGEROBJNAME);
        iceManager->removeObject(OBJOBSNAME);

        // remove all managed objects
        removeAllObjects(true);

        // destroy manager and communicator
        iceManager->destroy();

        // set to NULL to avoid cycle in pointers ArmarXManager <-> ArmarXObjectObserver
        objObserver = NULL;

        // set state to shutdown and notify waitForShutdown
        {
            boost::mutex::scoped_lock lock(managerStateMutex);

            // inform waiting threads of shutdown
            {
                boost::unique_lock<boost::mutex> lock(shutdownMutex);
                shutdownCondition.notify_all();
            }

            managerState = eShutdown;
        }

        managedObjectsMutex.unlock();

    }
    catch (...)
    {
        handleExceptions();
    }

    ARMARX_INFO << "Shutdown of ArmarXManager finished" << std::endl;
}

void ArmarXManager::asyncShutdown(std::size_t timeoutMs)
{
    std::thread {[this, timeoutMs]{
            std::this_thread::sleep_for(std::chrono::seconds{timeoutMs});
            shutdown();
        }
    } .detach();
}

bool ArmarXManager::isShutdown()
{
    boost::mutex::scoped_lock lock(managerStateMutex);
    return (managerState == eShutdown);
}


// *******************************************************
// getters
// *******************************************************

IceManagerPtr ArmarXManager::getIceManager() const
{
    return iceManager;
}

std::vector<ManagedIceObjectPtr> ArmarXManager::getManagedObjects()
{
    std::vector<ManagedIceObjectPtr> objects;

    if (!acquireManagedObjectsMutex())
    {
        return objects;
    }

    ObjectSchedulerMap::iterator iter = managedObjects.begin();

    while (iter != managedObjects.end())
    {
        objects.push_back(iter->second->getObject());
        iter++;
    }

    releaseManagedObjectsMutex();

    return objects;
}

// *******************************************************
// Slice ArmarXManagerInterface implementation
// *******************************************************
ManagedIceObjectState ArmarXManager::getObjectState(const std::string& objectName, const Ice::Current& c)
{
    if (!acquireManagedObjectsMutex())
    {
        return eManagedIceObjectExiting;
    }

    ObjectSchedulerMap::iterator iter = managedObjects.find(objectName);

    if (iter == managedObjects.end())
    {
        return eManagedIceObjectExited;
    }

    ManagedIceObjectState state = iter->second->getObject()->getState();

    releaseManagedObjectsMutex();

    return state;
}

ManagedIceObjectConnectivity ArmarXManager::getObjectConnectivity(const std::string& objectName, const Ice::Current& c)
{
    if (!acquireManagedObjectsMutex())
    {
        return ManagedIceObjectConnectivity();
    }

    ObjectSchedulerMap::iterator iter = managedObjects.find(objectName);

    if (iter == managedObjects.end())
    {
        return ManagedIceObjectConnectivity();
    }

    ManagedIceObjectConnectivity con = iter->second->getObject()->getConnectivity();

    releaseManagedObjectsMutex();

    return con;
}


StringStringDictionary ArmarXManager::getObjectProperties(const ::std::string& objectName, const ::Ice::Current&)
{
    StringStringDictionary propertyMap;
    ObjectSchedulerMap::iterator iter = managedObjects.find(objectName);

    if (iter == managedObjects.end())
    {
        return propertyMap;
    }

    ComponentPtr component = ComponentPtr::dynamicCast(iter->second->getObject());

    if (!component)
    {
        return propertyMap;
    }

    return component->getIceProperties()->getPropertiesForPrefix("");
}

StringSequence ArmarXManager::getManagedObjectNames(const Ice::Current& c)
{
    StringSequence objectNames;

    if (!acquireManagedObjectsMutex())
    {
        return objectNames;
    }

    ObjectSchedulerMap::iterator iter = managedObjects.begin();

    while (iter != managedObjects.end())
    {
        objectNames.push_back(iter->first);
        iter++;
    }

    releaseManagedObjectsMutex();

    return objectNames;
}

// *******************************************************
// private methods
// *******************************************************
void ArmarXManager::init(std::string applicationName, Ice::CommunicatorPtr communicator)
{
    // create ice manager
    iceManager = new IceManager(communicator, applicationName);

    // init logging
    LogSender::setProxy(applicationName,
                        iceManager->getTopic<LogPrx>("Log"));

    setTag("ArmarXManager");

    RegisterKnownObjectFactoriesWithIce(communicator);

    // register obj observer for onDisconnect
    objObserver = new ArmarXObjectObserver(this);
    Ice::ObjectAdapterPtr observerAdapter;
    Ice::ObjectPrx oPrx = iceManager->getIceGridSession()->registerObjectWithNewAdapter(objObserver, OBJOBSNAME, observerAdapter);
    IceGrid::ObjectObserverPrx objObsPrx =  IceGrid::ObjectObserverPrx::checkedCast(oPrx);
    iceManager->getIceGridSession()->setObjectObserver(objObsPrx);

    // register manager to ice
    iceManager->getIceGridSession()->registerObjectWithNewAdapter(this, MANAGEROBJNAME, armarxManagerAdapter);

    // create periodic task for starter cleanup
    cleanupSchedulersTask = new PeriodicTask<ArmarXManager>(this, &ArmarXManager::cleanupSchedulers, 500, false, "ArmarXManager::cleanupSchedulers");
    cleanupSchedulersTask->start();

    checkDependencyStatusTask = new PeriodicTask<ArmarXManager>(this, &ArmarXManager::checkDependencies, 1000, false, "ArmarXManager::DependenciesChecker");
    checkDependencyStatusTask->start();


    // set manager state to running
    {
        boost::mutex::scoped_lock lock(managerStateMutex);
        managerState = eRunning;
    }

    // create ThreadList for this process
    ThreadList::getApplicationThreadList()->setApplicationThreadListName(applicationName + "ThreadList");

    try
    {
        addObject(ThreadList::getApplicationThreadList());
    }
    catch (...) {}
}

void ArmarXManager::cleanupSchedulers()
{
    boost::mutex::scoped_lock lock(terminatingObjectsMutex);

    ObjectSchedulerList::iterator iter = terminatingObjects.begin();

    while (iter != terminatingObjects.end())
    {
        // remove terminated starters
        if ((*iter)->isTerminated())
        {
            ARMARX_VERBOSE << "Delayed Removal of ManagedIceObject " << (*iter)->getObject()->getName() << " finished";
            iter = terminatingObjects.erase(iter);
        }
        else
        {
            iter++;
        }
    }
}

void ArmarXManager::disconnectDependees(const std::string& object)
{
    try
    {
        std::vector<std::string> dependees = getDependendees(object);

        if (!acquireManagedObjectsMutex())
        {
            return;
        }

        for (unsigned int i = 0; i < dependees.size(); i++)
        {
            ArmarXManager::ObjectSchedulerMap::iterator it = managedObjects.find(dependees.at(i));
            ARMARX_INFO << dependees.at(i) << " disconnected because of " << object;

            if (it != managedObjects.end())
            {
                it->second->disconnected(true);
            }
        }
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

void ArmarXManager::disconnectAllObjects()
{
    ObjectSchedulerMap::iterator iter = managedObjects.begin();

    for (; iter != managedObjects.end(); iter++)
    {
        iter->second->disconnected(false);
    }
}

std::vector<std::string> ArmarXManager::getDependendees(const std::string& removedObject)
{
    std::vector<std::string> result;

    if (!acquireManagedObjectsMutex())
    {
        return result;
    }

    try
    {

        ObjectSchedulerMap::const_iterator it = managedObjects.begin();

        for (; it != managedObjects.end(); it++)
        {
            ArmarXObjectSchedulerPtr scheduler = it->second;

            if (scheduler->dependsOn(removedObject))
            {
                result.push_back(it->first);
            }
        }
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
    return result;
}

void ArmarXManager::wakeupWaitingSchedulers()
{
    if (!acquireManagedObjectsMutex())
    {
        return;
    }

    try
    {

        ObjectSchedulerMap::const_iterator it = managedObjects.begin();

        for (; it != managedObjects.end(); it++)
        {
            ArmarXObjectSchedulerPtr scheduler = it->second;
            scheduler->wakeupDependencyCheck();
        }
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

void ArmarXManager::removeAllObjects(bool blocking)
{
    auto tempMap = managedObjects;

    for (auto it = tempMap.begin(); it != tempMap.end(); it++)
    {
        removeObject(it->second, false);
    }

    if (blocking)
    {
        for (auto it = terminatingObjects.begin(); it != terminatingObjects.end(); it++)
        {
            ArmarXObjectSchedulerPtr objectScheduler = *it;
            objectScheduler->waitForTermination();

        }

        terminatingObjects.clear();
    }

}

bool ArmarXManager::removeObject(const ArmarXObjectSchedulerPtr& objectScheduler, bool blocking)
{
    if (!objectScheduler)
    {
        return true;
    }

    const std::string objName = objectScheduler->getObject()->getName();

    try
    {
        iceManager->removeObject(objName);
        managedObjects.erase(objectScheduler->getObject()->getName());
        // terminate
        objectScheduler->terminate();

        if (blocking)
        {
            objectScheduler->waitForTermination();
            ARMARX_VERBOSE << "Blocking removal of ManagedIceObject " <<  objName << " finished";
        }
        else // only move to terminating list if delayed removal
        {
            ARMARX_VERBOSE << "Inserting ManagedIceObject into delayed removal list: " << objName;
            boost::mutex::scoped_lock lockTerm(terminatingObjectsMutex);
            terminatingObjects.push_back(objectScheduler);
        }

        return true;
    }
    catch (...)
    {
        ARMARX_ERROR << "Removing of object '" << objName << "' failed with an exception!";
        handleExceptions();
    }

    return false;
}

bool ArmarXManager::removeObject(const ObjectSchedulerMap::iterator& iter, bool blocking)
{
    if (iter != managedObjects.end())
    {
        const std::string objName = iter->second->getObject()->getName();

        try
        {

            ARMARX_VERBOSE << "Removing ManagedIceObject " << objName << " - blocking = " << blocking;


            ArmarXObjectSchedulerPtr objectScheduler = iter->second;


            //            managedObjects.erase(iter);
            return removeObject(objectScheduler, blocking);
        }
        catch (...)
        {
            ARMARX_ERROR << "Removing of object '" << objName << "' failed with an exception!";
            handleExceptions();
        }
    }

    return false;
}

void ArmarXManager::checkDependencies()
{


    if (!acquireManagedObjectsMutex())
    {
        return;
    }

    try
    {

        ObjectSchedulerMap::const_iterator it = managedObjects.begin();

        for (; it != managedObjects.end(); it++)
        {
            ArmarXObjectSchedulerPtr scheduler = it->second;

            if (scheduler->getObject()->getState() == eManagedIceObjectStarted && !scheduler->checkDependenciesStatus())
            {
                scheduler->disconnected(true);
            }
        }
    }
    catch (...)
    {
        releaseManagedObjectsMutex();
        throw;
    }

    releaseManagedObjectsMutex();
}

bool ArmarXManager::acquireManagedObjectsMutex()
{
    // assure no state change until lock of managedObjectsMutex
    boost::mutex::scoped_lock lock(managerStateMutex);

    if (managerState >= eShutdownInProgress)
    {
        return false;
    }

    // lock access to managed objects
    managedObjectsMutex.lock();

    return true;
}

void ArmarXManager::releaseManagedObjectsMutex()
{
    // lock access to managed objects
    managedObjectsMutex.unlock();
}

void ArmarXManager::RegisterKnownObjectFactoriesWithIce(Ice::CommunicatorPtr ic)
{
    ScopedLock lock(FactoryCollectionBase::RegistrationListMutex());
    unsigned int size = FactoryCollectionBase::PreregistrationList().size();

    for (unsigned int i = 0; i < size; i++)
    {
        ObjectFactoryMap objFacMap = FactoryCollectionBase::PreregistrationList().at(i)->getFactories();
        ObjectFactoryMap::iterator it = objFacMap.begin();

        for (; it != objFacMap.end(); it++)
        {
            if (!ic->findObjectFactory(it->first))
            {
                // use this for debugging:
                // ARMARX_LOG_S << "Adding factory for " << it->first;
                ic->addObjectFactory(it->second, it->first);
            }
        }
    }

}

void ArmarXManager::registerKnownObjectFactoriesWithIce()
{
    RegisterKnownObjectFactoriesWithIce(getIceManager()->getCommunicator());
}



