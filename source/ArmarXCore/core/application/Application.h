/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nils Adermann (naderman at naderman dot de)
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmail dot com)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_APPLICATION_H
#define _ARMARX_CORE_APPLICATION_H

#include "properties/PropertyUser.h"
#include "../system/ImportExport.h"
#include "../ArmarXManager.h"
#include "../logging/Logging.h"
#include "../system/cmake/CMakePackageFinder.h"
#include "../system/ArmarXDataPath.h"
#include "ApplicationNetworkStats.h"

#include <Ice/Application.h>

#include <boost/algorithm/string.hpp>

#define xstr(s) stringify(s)
#define stringify(s) #s

/**
    \defgroup Application ArmarX Application
    \ingroup DistributedProcessingGrp
 */

namespace armarx
{
    // forward declarations
    class ManagedIceObjectRegistryInterface;
    /**
     * \typedef ManagedIceObjectRegistryInterfacePtr shared pointer for armarx::ManagedIceObjectRegistryInterface
     */
    typedef IceUtil::Handle<ManagedIceObjectRegistryInterface> ManagedIceObjectRegistryInterfacePtr;

    /**
     * armarx::Application smart pointer
     */
    class Application;
    /**
     * \typedef ApplicationPtr shared pointer for armarx::Application
     */
    typedef IceUtil::Handle<Application> ApplicationPtr;

    /**
      \class ApplicationPropertyDefinitions
      \brief Application property definition container.
      \ingroup Application
      \see properties
     */
    class ARMARXCORE_IMPORT_EXPORT ApplicationPropertyDefinitions:
        public PropertyDefinitionContainer
    {
    public:
        /**
         * @see armarx::PropertyDefinitionContainer::PropertyDefinitionContainer()
         */
        ApplicationPropertyDefinitions(std::string prefix);
    };

    /**
      \class Application
      \brief Baseclass for all ArmarX applications.
      \ingroup Application

      \li helper class for ArmarX main programs
      \li creates an instance of ArmarXManager
      \li generates properties from commandline parameters
      \li provides help and automatic generation of properties

      Implements the run method from the Ice::Application interface
      which is called by the automatically generated main.cpp.
      In order to start writing an ArmarX application, inherit from armarx::Application
      and implement the setup method to setup and add the required ManagedIceObjects.

      \code
        class ExampleApp :
            virtual public armarx::Application
        {
            void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                       Ice::PropertiesPtr properties)
            {
                registry->addObject(Component::create<Example>(properties));
            };
        }
      \endcode

      The application automatically loads a default config file
      (usually ~/.armarx/default.cfg), which is needed to connect to IceGrid etc.
      This function is also available as a static version: LoadDefaultConfig()

      Application specific configuration files can be passed to the application
      by using the Ice.Config commandline parameter.
      The used configuration file format is those of regular Ice configuration files.

      Each entry in Ice configuration files is prefixed with a "DomainName"
      which is "ArmarX" by default.
      This "DomainName" can be changed by overriding armarx::Application::getDomainName()
      to return a different string.

      Sometimes it is not possible to pass configuration files via the Ice.Config parameter
      (e.g. when using IceGrid XML deployment files where this paramter is overriden by IceGrid).
      In this case, the commandline parameter ArmarX.Config can be used.
      All files specified in this parameter will be merged into the standard properties
      available inside the armarx::Application.

      Special properties:

      \li ArmarX.Config
      \li ArmarX.NetworkStats: set to "1" to enable logging of network statistics (use armarx::ProfilerObserver to collect the data)
      \li ArmarX.DependenciesConfig
      \li ArmarX.ProjectDatapath
      \li ArmarX.ProjectName
      \li ArmarX.ProjectDependencies
      \li <DomainName>.DisableLogging
      \li <DomainName>.Verbosity
      \li <DomainName>.DataPath
     */
    class ARMARXCORE_IMPORT_EXPORT Application :
        public Ice::Application,
        public PropertyUser
    {
    public:
        /**
         * @brief Application initalizes the Ice::Application base class.
         */
        Application();
        /**
         * Creates the one application instance of type T. T is usually the
         * type of a sublass as defined in a specific component. This method is
         * called from the automatically generated main.cpp.
         *
         * @return shared pointer to the application
         */
        template <class T>
        static ApplicationPtr createInstance()
        {
            ScopedLock lock(instanceMutex);

            if (instance)
            {
                throw LocalException("Application instance already created");
            }

            instance = ApplicationPtr(new T());

            return instance;
        }

        /**
         * Retrieve shared pointer to the application object. Use this in order
         * to access is called from the automatically generated main.cpp.
         *
         * @return shared pointer to the application
         */
        static ApplicationPtr getInstance();


        /**
         * Ice::Application replacement for the main function.
         */
        virtual int run(int argc, char* argv[]);

        /**
         * Set name of the application. Called from main.cpp.
         * This method has an effect only if it is called before Application::main()
         *
         * @param application name
         */
        void setName(const std::string& name);

        /**
         * Retrieve name of the application
         *
         * @return application name
         */
        std::string getName();

        /**
         * Cleans up connections with IceStorm before terminating the app
         *
         * @param signal The signal send to the application
         */
        virtual void interruptCallback(int signal);

        void registerDataPathsFromDependencies(std::string dependencies);

        /**
         * @brief getDefaultPackageNames returns the value of the ArmarX.DefaultPackages property
         * It splits the string by , and returns the elements as a vector.
         * @return
         */
        std::vector<std::string> getDefaultPackageNames();

        static StringList GetDefaultsPaths();
        static void LoadDefaultConfig(Ice::PropertiesPtr properties);
        static const std::string& GetProjectName();
        static const StringList& GetProjectDependencies();

    protected:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        /**
         * Setup method to be implemented by user applications
         *
         * Use this method to add all required ManagedIceObject to the application
         * @see void ManagedIceObjectRegistryInterfacePtr::addObject(ManagedIceObjectPtr object)
         */
        virtual void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) = 0;

        /**
         * Exec method is the main process of the application. The default
         * implementation only calls
         *
         * <code>
         *     armarXManager->waitForShutdown();
         * </code>
         *
         * If you need to do something in the main process (e.g. processing QT
         * guis) overwrite this method.
         *
         * @return program exit status
         */
        virtual int exec(const ArmarXManagerPtr& armarXManager);

        /**
         * Retrieve the domain name used for property parsing. Overwrite this in your subclass in order to
         * use another domain name then "ArmarX".
         * The domain name is the first part of the property identifier where each part is separated via a "." (e.g. ArmarX.some.property).
         *
         * @return domain name. Defaults to "ArmarX".
         */
        virtual std::string getDomainName();

        /**
         * @return an instance of armarx::ArmarXManagerPtr
         */
        ArmarXManagerPtr getArmarXManager();


        /**
         * @brief installProcessFacet creates a new Ice::Process facet to handle graceful application shutdown when run via IceGrid
         * @param armarXManager the ApplicationProcessFacet requires an ArmarXManager instance.
         *
         * This requires the following Ice property to be set to prevent raceconditions
         * \code Ice.Admin.DelayCreation=1 \endcode
         *
         * for further detail take a look at: https://doc.zeroc.com/display/Ice34/The+Process+Facet
         */
        void installProcessFacet(const ArmarXManagerPtr& armarXManager);


        /**
         * @brief handlerInterrupt handles interrupt signals sent to the application (Linux)
         * @param sig
         */
        static void HandlerInterrupt(int sig);


        /**
         * @brief handlerFault handles signals sendt to the application such as SIGSEGF or SIGABRT (Linux)
         * @param sig
         */
        static void HandlerFault(int sig);

    private:

        /**
         * Application singleton instance
         */
        static ApplicationPtr instance;

        /**
         * Application instance instantiation and access mutex
         */
        static Mutex instanceMutex;

        /**
         * Application instance name
         */
        std::string applicationName;

        /**
         * the ArmarXManager
         */
        ArmarXManagerPtr armarXManager;

        ApplicationNetworkStatsPtr applicationNetworkStats;

        static std::string ProjectName;
        static StringList ProjectDependendencies;

    protected:
        /**
         * @brief Ice::Application::doMain() is called by Ice::Application::main() and does setup of Ice::Communicator before calling the virtual Ice::Application::run() method
         * @param argc
         * @param argv
         * @param initData
         * @return
         */
        virtual int doMain(int argc, char* argv[], const Ice::InitializationData& initData);
        void loadDefaultConfig(int argc, char* argv[], const Ice::InitializationData& initData);
        void loadDependentProjectDatapaths();
    };

    class DummyApplication : public Application
    {
        // Application interface
    protected:
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties)
        {
        }
    };


}

#endif
