/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nils Adermann (naderman at naderman dot de)
 * @author     Kai Welke (welke at kit dot edu)
 * @author     Jan Issac (jan dot issac at gmail dot com)
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2010
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Application.h"
#include "ApplicationOptions.h"
#include "ApplicationProcessFacet.h"
#include "properties/IceProperties.h"
#include "../ManagedIceObject.h"
#include "../services/tasks/ThreadList.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string/split.hpp>

#ifndef WIN32
#include <signal.h>

#endif

using namespace armarx;

// static members for the one application instance (maybe not required anymore???)
Mutex Application::instanceMutex;
ApplicationPtr Application::instance;
StringList Application::ProjectDependendencies;
std::string Application::ProjectName;


void Application::HandlerFault(int sig)
{
#ifndef WIN32
    // print out all the frames to stderr
    std::stringstream str;

    if (sig == SIGSEGV)
    {
        str << "Error: Segmentation Fault\nBacktrace:\n";
    }
    else if (sig == SIGABRT)
    {
        str << "Error: Abort\nBacktrace:\n";
    }
    else
    {
        str << "Error: signal " << sig;
    }

    ARMARX_FATAL_S << str.str() << "\nBacktrace:\n" << LogSender::CreateBackTrace();
    exit(EXIT_FAILURE);
#endif
}


void Application::HandlerInterrupt(int sig)
{
#ifndef WIN32
    ARMARX_INFO_S << "Caught interrupt: " << sig;

    if (Application::getInstance())
    {
        Application::getInstance()->interruptCallback(sig);
    }

#endif
}


// main entry point of Ice::Application
Application::Application()
#ifndef WIN32
    : Ice::Application(Ice::NoSignalHandling)
#endif
{
}


ApplicationPtr Application::getInstance()
{
    ScopedLock lock(instanceMutex);

    if (!instance)
    {
        throw LocalException("Application instance not yet created");
    }

    return instance;
}


int Application::run(int argc, char* argv[])
{
#ifndef WIN32
    // register signal handler
    signal(SIGSEGV, Application::HandlerFault);
    signal(SIGABRT, Application::HandlerFault);

    signal(SIGHUP, Application::HandlerInterrupt);
    signal(SIGINT, Application::HandlerInterrupt);
    signal(SIGTERM, Application::HandlerInterrupt);
#endif


    // parse options and merge these with properties passed via Ice.Config

    // Here, use armarx::IceProperties instead of Ice::Properties to support property inheritance.
    properties = IceProperties::create(ApplicationOptions::mergeProperties(communicator()->getProperties()->clone(), argc, argv));
    // Load config files passed via ArmarX.Config
    std::string configFiles = communicator()->getProperties()->getProperty("ArmarX.Config");

    if (!configFiles.empty() && configFiles.compare("1") != 0)
        // if not set, size is zero
        // if set with out value (== "1" as TRUE), size is one
    {
        std::vector<std::string> configFileList;
        boost::split(configFileList,
                     configFiles,
                     boost::is_any_of(", "),
                     boost::token_compress_on);

        for (std::string configFile : configFileList)
        {
            if (!configFile.empty())
            {
                properties->load(configFile);
            }
        }
    }

    // parse help options
    ApplicationOptions::Options options = ApplicationOptions::parseHelpOptions(properties, argc, argv);

    if (options.error)
    {
        // error in options
        return 0;
    }

    if (options.showHelp) // display help
    {
        // perform dummy setup to register ManagedIceObjects
        ArmarXDummyManagerPtr dummyManager = new ArmarXDummyManager();

        LogSender::SetLoggingActivated(false, false);

        setup(dummyManager, properties);

        showHelp(this, dummyManager, options, NULL);

        return 0;
    }

    loadDependentProjectDatapaths();

    // create the ArmarXManager and set properties
    if (getProperty<std::string>("ApplicationName").isSet())
    {
        applicationName = getProperty<std::string>("ApplicationName").getValue();
    }

    armarXManager = new ArmarXManager(applicationName, communicator());


    if (getProperty<bool>("DisableLogging").isSet())
    {
        armarXManager->enableLogging(!getProperty<bool>("DisableLogging").getValue());
    }

    armarXManager->enableProfiling(getProperty<bool>("EnableProfiling").getValue());
    armarXManager->setGlobalMinimumLoggingLevel(getProperty<MessageType>("Verbosity").getValue());
    armarXManager->setDataPaths(getProperty<std::string>("DataPath").getValue());


#ifdef WIN32
    // register interrupt handler
    callbackOnInterrupt();
#endif
    installProcessFacet(armarXManager);

    if (applicationNetworkStats)
    {
        ProfilerListenerPrx profilerTopic = armarXManager->getIceManager()->getTopic<ProfilerListenerPrx>(armarx::Profiler::PROFILER_TOPIC_NAME);
        applicationNetworkStats->setProfilerTopic(profilerTopic);
        applicationNetworkStats->setApplicationName(applicationName);
    }

    try
    {
        // calls virtual setup in order to allow subclass to add components to the
        // application
        setup(armarXManager, properties);
    }
    catch (...)
    {
        handleExceptions();
        armarXManager->shutdown();
    }


    // calls exec implementation
    int result = exec(armarXManager);
    LogSender::SetSendLoggingActivated(false);
    return result;
}

/*
 * the default exec implementation. Exec is always blocking and waits for
 * shutdown
 */
int Application::exec(const ArmarXManagerPtr& armarXManager)
{
    armarXManager->waitForShutdown();

    return 0;
}

std::string Application::getDomainName()
{
    return "ArmarX";
}

void Application::setName(const std::string& name)
{
    this->applicationName = name;
}

std::string Application::getName()
{
    return applicationName;
}

void Application::interruptCallback(int signal)
{
    ARMARX_INFO_S << "Interrupt received: " << signal;
    armarXManager->shutdown();
}

void Application::registerDataPathsFromDependencies(std::string dependencies)
{
    IceUtil::Time start = IceUtil::Time::now();
    StringList resultList;
    ARMARX_INFO_S << "Deps: " << dependencies;
    boost::replace_all(dependencies, "\"", "");
    boost::split(resultList,
                 dependencies,
                 boost::is_any_of("/"),
                 boost::token_compress_on);

    for (size_t i = 0; i < resultList.size(); i++)
    {
        CMakePackageFinder pack(resultList[i]);

        if (pack.packageFound())
        {
            ArmarXDataPath::addDataPaths(pack.getIncludePaths());
        }
    }

    ARMARX_INFO_S << "loading took " << (IceUtil::Time::now() - start).toMilliSeconds();
}

std::vector<std::string> Application::getDefaultPackageNames()
{

    std::string packagesStr = getProperty<std::string>("DefaultPackages").getValue();
    std::vector<std::string> packageNames = Split(packagesStr, ",");

    for (auto & name : packageNames)
    {
        boost::trim(name);
    }

    return packageNames;
}

ArmarXManagerPtr Application::getArmarXManager()
{
    return armarXManager;
}


void Application::installProcessFacet(const ArmarXManagerPtr& armarXManager)
{
    return;
    Ice::CommunicatorPtr applicationCommunicator = communicator();
    // remove default Ice::Process facet
    applicationCommunicator->removeAdminFacet("Process");
    // create and register new Ice::Process facet
    Ice::ProcessPtr applicationProcessFacet = new ApplicationProcessFacet(armarXManager);
    applicationCommunicator->addAdminFacet(applicationProcessFacet, "Process");
    // activate new Ice::Process facet
    applicationCommunicator->getAdmin();
}


int Application::doMain(int argc, char* argv[], const Ice::InitializationData& initData)
{
    // create copy of initData so the stats object can be added
    // using const_cast leads to unexpected errors
    Ice::InitializationData id(initData);
    loadDefaultConfig(argc, argv, id);

    if (initData.properties->getProperty("ArmarX.NetworkStats") == "1")
    {
        applicationNetworkStats = new ::armarx::ApplicationNetworkStats();
        id.stats = applicationNetworkStats;
    }

    return Ice::Application::doMain(argc, argv, id);
}

StringList Application::GetDefaultsPaths()
{
    const std::string configFileName = "default.cfg";
    StringList defaultsPaths;
    char* defaultsPathVar = getenv("ARMARX_USER_CONFIG_DIR");

    if (defaultsPathVar)
    {
        boost::filesystem::path path(defaultsPathVar);
        path /= configFileName;
        defaultsPaths.push_back(path.string()) ;
    }
    else
    {
        char* home = getenv("HOME");

        if (home)
        {
            boost::filesystem::path fullpath(home);
            fullpath /= std::string(".armarx");
            fullpath /= configFileName;
            defaultsPaths.push_back(fullpath.string());
        }
    }

    return defaultsPaths;
}


void Application::loadDefaultConfig(int argc, char* argv[], const Ice::InitializationData& initData)
{
    LoadDefaultConfig(initData.properties);
}

void Application::LoadDefaultConfig(Ice::PropertiesPtr properties)
{
    const StringList defaultsPath = GetDefaultsPaths();
    Ice::PropertiesPtr p = IceProperties::create();

    for (std::string path : defaultsPath)
    {
        try
        {
            p->load(path);

            Ice::PropertyDict defaultCfg = p->getPropertiesForPrefix("");

            // copy default config into current config (if values are not already there)
            for (auto e : defaultCfg)
            {
                if (properties->getProperty(e.first).empty())
                {
                    properties->setProperty(e.first, e.second);
                }
            }

        }
        catch (Ice::FileException& e)
        {
            ARMARX_WARNING_S << "Loading default config failed: " << e.what();
        }
    }
}


const std::string& Application::GetProjectName()
{
    return ProjectName;
}


const StringList& Application::GetProjectDependencies()
{
    return ProjectDependendencies;
}


PropertyDefinitionsPtr Application::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ApplicationPropertyDefinitions(getDomainName()));
}


void Application::loadDependentProjectDatapaths()
{
    std::string dependenciesConfig = getIceProperties()->getProperty("ArmarX.DependenciesConfig");

    if (!dependenciesConfig.empty())
    {
        if (boost::filesystem::exists(dependenciesConfig))
        {
            Ice::PropertiesPtr prop = IceProperties::create();
            prop->load(dependenciesConfig);
            ArmarXDataPath::addDataPaths(prop->getProperty("ArmarX.ProjectDatapath"));
            ProjectName = prop->getProperty("ArmarX.ProjectName");
            std::string dependencies = prop->getProperty("ArmarX.ProjectDependencies");
            boost::split(ProjectDependendencies, dependencies, boost::is_any_of(";"), boost::token_compress_on);
        }
        else
        {
            /*
            ARMARX_WARNING_S << "The given project datapath config file '" << datapathConfig << "', that this app depends on, could not be found. \
                                Relative paths to subprojects are not available. Set the property ArmarX.ProjectDatapath to empty, if you do not need the paths.";
            */
        }
    }
}


ApplicationPropertyDefinitions::ApplicationPropertyDefinitions(std::string prefix):
    PropertyDefinitionContainer(prefix)
{
    defineOptionalProperty<std::string>("Config", "", "Comma-separated list of configuration files ");
    defineOptionalProperty<std::string>("DependenciesConfig", "./config/dependencies.cfg", "Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.");
    defineOptionalProperty<std::string>("DefaultPackages", "ArmarXCore, ArmarXGui, MemoryX, RobotAPI, RobotComponents, RobotSkillTemplates, ArmarXSimulation, VisionX, SpeechX, Armar3, Armar4", "List of ArmarX packages which are accessible by default");

    defineOptionalProperty<std::string>("ApplicationName", "", "Application name");

    defineOptionalProperty<bool>("DisableLogging", false, "Turn logging off in whole application")
    .setCaseInsensitive(true)
    .map("true",    true)
    .map("yes",     true)
    .map("1",       true)
    .map("false",   false)
    .map("no",      false)
    .map("0",       false);

    defineOptionalProperty<MessageType>("Verbosity", eVERBOSE, "Global logging level for whole application")
    .setCaseInsensitive(true)
    .map("Debug", eDEBUG)
    .map("Verbose", eVERBOSE)
    .map("Info", eINFO)
    .map("Important", eIMPORTANT)
    .map("Warning", eWARN)
    .map("Error", eERROR)
    .map("Fatal", eFATAL)
    .map("Undefined", eUNDEFINED);

    defineOptionalProperty<std::string>("DataPath", "", "Semicolon-separated search list for data files");

    defineOptionalProperty<std::string>("CachePath", "${HOME}/.armarx/mongo/.cache", "Path for cache files");

    defineOptionalProperty<bool>("EnableProfiling", false, "Enable profiling of CPU load produced by this application")
    .setCaseInsensitive(true)
    .map("true",    true)
    .map("yes",     true)
    .map("1",       true)
    .map("false",   false)
    .map("no",      false)
    .map("0",       false);
}
