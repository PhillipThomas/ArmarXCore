/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARXCORE_PROPERTYDEFINITIONBASE_H_
#define _ARMARXCORE_PROPERTYDEFINITIONBASE_H_

#include "PropertyDefinitionFormatter.h"

#include <Ice/Properties.h>

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionBase
     * @brief Common interface of any property definition
     *
     * This abstract class is part of the internal implementation
     */
    class PropertyDefinitionBase
    {
    public:
        /**
         * Converts the property definition into a string using a specified
         * formatter.
         *
         * @param formatter Custom definition formatter
         */
        virtual std::string toString(PropertyDefinitionFormatter& formatter, const std::string& value) = 0;

        virtual ~PropertyDefinitionBase() {}

    private:
        friend class PropertyDefinitionContainer;

        /**
         * Sets the typeid name of the property value type
         *
         * @param typeIdName    Value type name
         */
        void setTypeIdName(std::string typeIdName)
        {
            this->typeIdName = typeIdName;
        }

        /**
         * Returns the value type name
         */
        std::string getTypeIdName() const
        {
            return typeIdName;
        }

        /**
         * Value type name
         */
        std::string typeIdName;
    };
}

#endif
