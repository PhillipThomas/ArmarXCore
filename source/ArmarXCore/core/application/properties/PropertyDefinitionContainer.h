/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_PROPERTYDEFINITIONCONTAINER_H
#define _ARMARX_PROPERTYDEFINITIONCONTAINER_H


#include "PropertyDefinitionInterface.h"
#include "PropertyDefinition.h"
#include "PropertyDefinitionFormatter.h"

#include "ArmarXCore/core/exceptions/Exception.h"
#include "ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h"
#include "ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h"
#include "ArmarXCore/core/exceptions/local/UnmappedValueException.h"
#include "ArmarXCore/core/exceptions/local/ValueRangeExceededException.h"

#include <Ice/Properties.h>

#include <string>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
//#include <boost/lexical_cast.hpp>
#endif


namespace armarx
{
    class PropertyDefinitionBase;

    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainer
     * @brief PropertyDefinitionContainer
     */
    class PropertyDefinitionContainer: public virtual IceUtil::Shared
    {
    public:
        typedef boost::unordered_map<std::string, PropertyDefinitionBase*>
        DefinitionContainer;

        PropertyDefinitionContainer(const std::string& prefix)
        {
            setDescription(prefix + " properties");

            this->prefix = prefix + ".";
        }

        ~PropertyDefinitionContainer()
        {
            DefinitionContainer::iterator it = definitions.begin();

            while (it != definitions.end())
            {
                delete it->second;

                it++;
            }

            definitions.clear();
        }

        void setProperties(Ice::PropertiesPtr properties)
        {
            this->properties  = properties;
        }

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineRequiredProperty(
            const std::string& name,
            const std::string& description = "");

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineOptionalProperty(
            const std::string& name,
            PropertyType defaultValue,
            const std::string& description = "");

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& getDefintion(const std::string& name);

        std::string toString(PropertyDefinitionFormatter& formatter)
        {
            formatter.setPrefix(prefix);
            std::string definitionStr;

            DefinitionContainer::iterator it = definitions.begin();
            Ice::PropertyDict propertiesMap;

            if (properties)
            {
                propertiesMap = properties->getPropertiesForPrefix(getPrefix());
            }

            while (it != definitions.end())
            {
                definitionStr += it->second->toString(formatter, getValue(it->first));
                propertiesMap.erase(it->first);
                it++;
            }

            if (properties)
            {
                formatter.setPrefix("");
            }

            std::vector<std::string> values;

            for (Ice::PropertyDict::iterator property = propertiesMap.begin(); property != propertiesMap.end(); property++)
            {
                definitionStr += formatter.formatDefinition(property->first,
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 values,
                                 property->second);
            }

            return definitionStr;
        }

        /**
         * Returns the detailed description of the property user
         *
         * @return detailed description text
         */
        std::string getDescription() const
        {
            return description;
        }

        /**
         * Sets the detailed description of the property user
         *
         * @param description detailed description text
         */
        void setDescription(const std::string& description)
        {
            this->description = description;
        }

        std::string getPrefix()
        {
            return prefix;
        }

        bool isPropertySet(const std::string& name)
        {
            Ice::PropertyDict selectedProperties = properties->getPropertiesForPrefix(getPrefix());
            return (selectedProperties.count(name) > 0);
        }

        std::string getValue(const std::string& name)
        {
            if (properties)
            {
                return properties->getPropertiesForPrefix(getPrefix())[name];
            }
            else
            {
                return "";
            }
        }

    protected:
        /**
         * Property definitions container
         */
        DefinitionContainer definitions;

        /**
         * Prefix of the properties such as namespace, domain, component name,
         * etc.
         */
        std::string prefix;

        /**
         * Property User description
         */
        std::string description;

        /**
         * PropertyUser brief description
         */
        std::string briefDescription;

        Ice::PropertiesPtr properties;
    };


    /**
     * PropertyDefinitions smart pointer type
     */
    typedef IceUtil::Handle<PropertyDefinitionContainer> PropertyDefinitionsPtr;

    /* ====================================================================== */
    /* === Property Definition Container Implementation ===================== */
    /* ====================================================================== */

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineRequiredProperty(
        const std::string& name,
        const std::string& description)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name, description);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        PropertyType defaultValue,
        const std::string& description)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name,
                    defaultValue,
                    description);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::getDefintion(const std::string& name)
    {
        // check definition existance
        if (definitions.find(name) == definitions.end())
        {
            throw armarx::LocalException(name + " property is not defined.");
        }

        PropertyDefinitionBase* def = definitions[name];

        // check definition type
        if (def->getTypeIdName().compare(typeid(PropertyType).name()) != 0)
        {
            throw armarx::LocalException(
                std::string("Calling getProperty<T>() for the property '")
                + name
                + "' with wrong property type [note: "
                + typeid(PropertyType).name()
                + " instead of "
                + def->getTypeIdName()
                + "]");
        }

        // retrieve and down cast the requested definition
        return *dynamic_cast< PropertyDefinition<PropertyType>* >(def);
    }
}

#endif
