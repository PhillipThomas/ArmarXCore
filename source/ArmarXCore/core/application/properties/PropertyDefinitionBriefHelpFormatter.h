/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMARXCORE_PROPERTYDEFINITIONBRIEFHELPFORMATTER_H
#define ARMARXCORE_PROPERTYDEFINITIONBRIEFHELPFORMATTER_H

#include "PropertyDefinitionFormatter.h"

#include <string>
#include <vector>
#include <list>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/program_options.hpp>
#endif

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionBriefHelpFormatter
     * @brief PropertyDefinitionBriefHelpFormatter
     */
    class PropertyDefinitionBriefHelpFormatter:
        public PropertyDefinitionFormatter
    {
    public:
        PropertyDefinitionBriefHelpFormatter(
            boost::program_options::options_description* optionsDescription);

        virtual std::string formatDefinition(std::string name,
                                             std::string description,
                                             std::string min,
                                             std::string max,
                                             std::string default_,
                                             std::string casesensitivity,
                                             std::string requirement,
                                             std::string reged,
                                             std::vector<std::string> values, std::string value);

        virtual std::string formatName(std::string name)
        {
            return std::string();
        }
        virtual std::string formatDescription(std::string description)
        {
            return std::string();
        }
        virtual std::string formatBounds(std::string min, std::string max)
        {
            return std::string();
        }
        virtual std::string formatDefault(std::string default_)
        {
            return std::string();
        }
        virtual std::string formatCaseSensitivity(std::string caseSensitivity)
        {
            return std::string();
        }
        virtual std::string formatRequirement(std::string requirement)
        {
            return std::string();
        }
        virtual std::string formatRegex(std::string regex)
        {
            return std::string();
        }
        virtual std::string formatValue(std::string value)
        {
            return std::string();
        }
        virtual std::string formatValues(std::vector<std::string> values)
        {
            return std::string();
        }
        virtual std::string formatAttribute(std::string name, std::string details)
        {
            return std::string();
        }

        virtual std::string formatHeader(std::string headerText);

        virtual std::string getFormat()
        {
            return std::string();
        }

    protected:
        boost::program_options::options_description* optionsDescription;
    };
}

#endif
