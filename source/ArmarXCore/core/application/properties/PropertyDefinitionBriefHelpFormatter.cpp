/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionBriefHelpFormatter.h"

#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>

using namespace armarx;


PropertyDefinitionBriefHelpFormatter::PropertyDefinitionBriefHelpFormatter(
    boost::program_options::options_description* optionsDescription)
{
    this->optionsDescription = optionsDescription;
}

std::string PropertyDefinitionBriefHelpFormatter::formatDefinition(
    std::string name,
    std::string description,
    std::string min,
    std::string max,
    std::string default_,
    std::string casesensitivity,
    std::string requirement,
    std::string regex,
    std::vector<std::string> values,
    std::string value)
{
    optionsDescription->add_options()(
        (getPrefix() + name + " =").c_str(),
        boost::program_options::value<std::string>(),
        description.c_str());

    return std::string();
}

std::string PropertyDefinitionBriefHelpFormatter::formatHeader(std::string headerText)
{
    return   "==================================================================\n"
             +  headerText + "\n"
             + "==================================================================\n\n";
}

