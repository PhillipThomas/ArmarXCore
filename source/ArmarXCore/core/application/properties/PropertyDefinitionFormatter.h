/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARXCORE_PROPERTYDEFINITIONFORMATTER_H
#define _ARMARXCORE_PROPERTYDEFINITIONFORMATTER_H

#include <string>
#include <vector>

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionFormatter
     * @brief PropertyDefinitionFormatter is the base class for all formatters of PropertyDefinitions
     */
    class PropertyDefinitionFormatter
    {
    public:
        virtual ~PropertyDefinitionFormatter() {}

        virtual std::string formatDefinition(
            std::string name,
            std::string description,
            std::string min,
            std::string max,
            std::string default_,
            std::string casesensitivity,
            std::string requirement,
            std::string reged,
            std::vector<std::string> values,
            std::string value) = 0;

        virtual std::string formatName(std::string name) = 0;
        virtual std::string formatDescription(std::string description) = 0;
        virtual std::string formatBounds(std::string min, std::string max) = 0;
        virtual std::string formatDefault(std::string default_) = 0;
        virtual std::string formatCaseSensitivity(std::string caseSensitivity) = 0;
        virtual std::string formatRequirement(std::string requirement) = 0;
        virtual std::string formatRegex(std::string regex) = 0;
        virtual std::string formatValue(std::string value) = 0;
        virtual std::string formatValues(std::vector<std::string> values) = 0;
        virtual std::string formatAttribute(std::string name, std::string details) = 0;

        virtual std::string formatHeader(std::string headerText) = 0;

        virtual std::string getFormat() = 0;

        virtual void setPrefix(std::string prefix)
        {
            this->prefix = prefix;
        }

        virtual std::string getPrefix() const
        {
            return prefix;
        }

    protected:
        std::string prefix;
    };
}

#endif
