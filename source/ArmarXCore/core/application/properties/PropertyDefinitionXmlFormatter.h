/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMARXCORE_PROPERTYDEFINITIONXMLFORMATTER_H
#define ARMARXCORE_PROPERTYDEFINITIONXMLFORMATTER_H

#include "PropertyDefinitionFormatter.h"

#include <string>
#include <vector>

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionXmlFormatter
     * @brief PropertyDefinitionXmlFormatter
     */
    class PropertyDefinitionXmlFormatter:
        public PropertyDefinitionFormatter
    {
    public:
        virtual std::string formatDefinition(std::string name,
                                             std::string description,
                                             std::string min,
                                             std::string max,
                                             std::string default_,
                                             std::string casesensitivity,
                                             std::string requirement,
                                             std::string reged,
                                             std::vector<std::string> values, std::string value);

        virtual std::string formatName(std::string name);
        virtual std::string formatDescription(std::string description);
        virtual std::string formatBounds(std::string min, std::string max);
        virtual std::string formatDefault(std::string default_);
        virtual std::string formatCaseSensitivity(std::string caseSensitivity);
        virtual std::string formatRequirement(std::string requirement);
        virtual std::string formatRegex(std::string regex);
        virtual std::string formatValue(std::string value);
        virtual std::string formatValues(std::vector<std::string> values);
        virtual std::string formatAttribute(std::string name, std::string details);

        virtual std::string formatHeader(std::string headerText);

        virtual std::string getFormat();
    };
}

#endif
