/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionConfigFormatter.h"

#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>

using namespace armarx;

std::string PropertyDefinitionConfigFormatter::formatDefinition(
    std::string name,
    std::string description,
    std::string min,
    std::string max,
    std::string default_,
    std::string casesensitivity,
    std::string requirement,
    std::string regex,
    std::vector<std::string> values,
    std::string value)
{
    std::string output = getFormat();

    name = getPrefix() + name;

    boost::replace_first(output, "%name%",          formatName(name));
    boost::replace_first(output, "%description%",   formatDescription(description));
    boost::replace_first(output, "%bounds%",        formatBounds(min, max));
    boost::replace_first(output, "%default%",       formatDefault(default_));
    boost::replace_first(output, "%casesensitive%", formatCaseSensitivity(casesensitivity));
    boost::replace_first(output, "%required%",      formatRequirement(requirement));
    boost::replace_first(output, "%regex%",         formatRegex(regex));
    boost::replace_first(output, "%values%",        formatValues(values));

    std::string valueEntry;

    if (!value.empty())
    {
        valueEntry = value;
    }
    else
    {
        if (default_.empty())
        {
            valueEntry = "<set value!>";
        }
        else
        {
            valueEntry = default_;
        }
    }

    if (requirement.compare("no") == 0 && value.empty())
    {
        // comment the property if it is not required
        name = "# " + name;
    }

    boost::replace_first(output, "%name%",        name);
    boost::replace_first(output, "%value%",       valueEntry);

    return output + "\n";
}

std::string PropertyDefinitionConfigFormatter::getFormat()
{
    return std::string("%name%:")
           + "  %description%\n"
           + "#  Attributes:\n"
           + "%default%"
           + "%bounds%"
           + "%casesensitive%"
           + "%required%"
           + "%regex%"
           + "%values%"
           + "%name% = %value%\n\n";
}

std::string PropertyDefinitionConfigFormatter::formatName(std::string name)
{
    return "# " + name;
}

std::string PropertyDefinitionConfigFormatter::formatDescription(std::string description)
{
    std::string commentDescription = description;
    boost::replace_all(commentDescription, "\n", "\n# ");

    return commentDescription;
}

std::string PropertyDefinitionConfigFormatter::formatBounds(std::string min, std::string max)
{
    std::string bounds;

    if (!min.empty() && max.empty())
    {
        bounds = formatAttribute("Min:", min);
    }
    else if (min.empty() && !max.empty())
    {
        bounds = formatAttribute("Max:", max);
    }
    else if (!min.empty() && !max.empty())
    {
        bounds = formatAttribute("Bounds:", "[" + min + "; " + max + "]");
    }

    return bounds;
}

std::string PropertyDefinitionConfigFormatter::formatDefault(std::string default_)
{
    return formatAttribute("Default:", default_);
}

std::string PropertyDefinitionConfigFormatter::formatCaseSensitivity(std::string caseSensitivity)
{
    return formatAttribute("Case sensitivity:", caseSensitivity);
}

std::string PropertyDefinitionConfigFormatter::formatRequirement(std::string requirement)
{
    return formatAttribute("Required:", requirement);
}

std::string PropertyDefinitionConfigFormatter::formatRegex(std::string regex)
{
    return formatAttribute("Format:", regex);
}

std::string PropertyDefinitionConfigFormatter::formatValues(std::vector<std::string> mapValues)
{
    std::string valueStrings;

    if (mapValues.size() > 0)
    {
        valueStrings += "#  - Possible values: {";

        std::vector<std::string>::iterator it = mapValues.begin();

        while (it != mapValues.end())
        {
            if (!it->empty())
            {
                valueStrings += formatValue(*it);
            }

            ++it;

            valueStrings += (it != mapValues.end() ? ", " : "}\n");
        }
    }

    return valueStrings ;
}

std::string PropertyDefinitionConfigFormatter::formatValue(std::string value)
{
    return  value;
}

std::string PropertyDefinitionConfigFormatter::formatAttribute(std::string name, std::string details)
{
    if (!details.empty())
    {
        std::stringstream strStream;
        strStream << std::setfill(' ') << std::left << std::setw(20) << name;
        strStream << details;

        return "#  - " + strStream.str() + "\n";
    }

    return std::string();
}

std::string PropertyDefinitionConfigFormatter::formatHeader(std::string headerText)
{
    std::string formattedHeaderText = "# " + headerText;
    boost::replace_all(formattedHeaderText, "\n", "\n# ");

    return   "# ==================================================================\n"
             +    formattedHeaderText + "\n"
             + "# ==================================================================\n\n";
}
