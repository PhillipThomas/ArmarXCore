/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApplicationNetworkStats.h"


armarx::ApplicationNetworkStats::ApplicationNetworkStats() :
    networkBytesIn(0),
    networkBytesOut(0),
    REPORT_TIME_MS(1000)
{
    reportNetworkTrafficTask = new PeriodicTask<ApplicationNetworkStats>(this, &ApplicationNetworkStats::reportNetworkTraffic, REPORT_TIME_MS);
    reportNetworkTrafficTask->start();
}

armarx::ApplicationNetworkStats::~ApplicationNetworkStats()
{
    reportNetworkTrafficTask->stop();
}


void armarx::ApplicationNetworkStats::bytesSent(const std::string& protocol, Ice::Int numberBytes)
{
    boost::mutex::scoped_lock l(protocolTrafficMapMutex);
    auto it = protocolTrafficMap.find(protocol);

    if (it == protocolTrafficMap.end())
    {
        protocolTrafficMap[protocol].outBytes = numberBytes;
    }
    else
    {
        it->second.outBytes += numberBytes;
    }
}


void armarx::ApplicationNetworkStats::bytesReceived(const std::string& protocol, Ice::Int numberBytes)
{
    boost::mutex::scoped_lock l(protocolTrafficMapMutex);
    auto it = protocolTrafficMap.find(protocol);

    if (it == protocolTrafficMap.end())
    {
        protocolTrafficMap[protocol].inBytes = numberBytes;
    }
    else
    {
        it->second.inBytes += numberBytes;
    }
}

void armarx::ApplicationNetworkStats::setProfilerTopic(ProfilerListenerPrx profiler)
{
    this->profiler = profiler;
}

void armarx::ApplicationNetworkStats::setApplicationName(const std::string& appName)
{
    applicationName = appName;
}


void armarx::ApplicationNetworkStats::reportNetworkTraffic()
{
    if (!profiler || applicationName.empty())
    {
        return;
    }

    std::map<std::string, NetworkTraffic> protocolTrafficMapCopy;
    {
        boost::mutex::scoped_lock l(protocolTrafficMapMutex);
        protocolTrafficMapCopy.swap(protocolTrafficMap);
    }

    for (auto & traffic : protocolTrafficMapCopy)
    {
        profiler->reportNetworkTraffic(applicationName, traffic.first, traffic.second.inBytes, traffic.second.outBytes);
    }
}
