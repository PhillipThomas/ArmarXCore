/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApplicationOptions.h"
#include "Application.h"
#include "properties/Properties.h"

using namespace armarx;
using namespace boost;
using namespace boost::program_options;


Ice::PropertiesPtr ApplicationOptions::mergeProperties(Ice::PropertiesPtr properties, int argc, char* argv[])
{
    // override and merge new properties given by the command line
    Ice::StringSeq args = Ice::argsToStringSeq(argc, argv);
    args = properties->parseCommandLineOptions("", args);

    return properties;
}

ApplicationOptions::Options ApplicationOptions::parseHelpOptions(Ice::PropertiesPtr properties, int argc, char* argv[])
{
    Options options;
    options.error = false;

    // create program options
    options.description.reset(new options_description(std::string(
                                  "This executable is an application within the ArmarX framework.\n")
                              + "==================================================================\n"
                              + " Starting an ArmarX application\n"
                              + "==================================================================\n"
                              + "In order to start the application execute it normally and pass optional config"
                              + " parameters to it:\n\n"

                              + "  > " + argv[0] + " [options]\n\n"

                              + "Make sure that Ice is started using the ice-start.sh script.\n"
                              + "Options for the component can be provided either in the command line\n"
                              + "or in a separate config file. In order to provide config files use \n\n"

                              + "  > " + argv[0] + " --Ice.Config=cfg1,cfg2\n\n"

                              + "where multiple config files are allowed.\n\n"

                              + "Help options"));

    options.description->add_options()("help,h", "Help option");

    options.description->add_options()("print-options,p", "Print detailed application options description");

    options.description->add_options()("options-format,f", value<std::string>(), "Options print format: { help, config, doxygen, doxygen_component_pages, xml }");

    variables_map vm;
    store(command_line_parser(argc, argv).options(*options.description).allow_unregistered().run(), vm);
    notify(vm);

    if (vm.count("print-options"))
    {
        std::map<std::string, OptionsFormat> optionsFormats;
        optionsFormats["help"] = eOptionsDetailed;
        optionsFormats["doxygen"] = eDoxygen;
        optionsFormats["doxygen_component_pages"] = eDoxygenComponentPages;
        optionsFormats["config"] = eConfig;
        optionsFormats["xml"] = eXml;

        if (vm.count("options-format"))
        {
            std::string optionsFormatName = vm["options-format"].as<std::string>();
            optionsFormatName = boost::algorithm::to_lower_copy(optionsFormatName);

            if (optionsFormats.find(optionsFormatName) != optionsFormats.end())
            {
                options.format = optionsFormats[optionsFormatName];
            }
            else
            {
                std::cout << "Unknown options-format:" << optionsFormatName << std::endl;

                options.showHelp = false;
                options.error = true;
                return options;
            }
        }
        else
        {
            options.format = eOptionsDetailed;
        }
    }
    else if (vm.count("help") || properties->getProperty("Ice.Default.Locator").empty())
    {
        options.format = eHelpBrief;
    }
    else
    {
        options.showHelp = false;
        return options;
    }

    options.showHelp = true;

    return options;
}


void ApplicationOptions::showHelp(ApplicationPtr application, ArmarXDummyManagerPtr dummyManager, ApplicationOptions::Options options, Ice::PropertiesPtr properties)
{
    OptionsFormat optionsFormat = options.format;
    program_options::options_description& desc = *options.description;

    PropertyDefinitionFormatter* defFormatter;
    PropertyDefinitionContainerFormatter* pdcFormatter;
    PropertyUserList puList;

    switch (optionsFormat)
    {
        case eHelpBrief:
        {
            PropertyDefinitionBriefHelpFormatter* definitionFormatter;
            PropertyDefinitionContainerBriefHelpFormatter* containerFormatter;

            // help options
            std::cout << desc << std::endl;

            std::cout << "By default the following config files are loaded:\n ";
            StringList paths = application->GetDefaultsPaths();

            for (auto p : paths)
            {
                std::cout << "\t" << p << std::endl;
            }

            std::cout << std::endl;
            // application options
            options_description applicationDesc("Application properties");

            definitionFormatter = new PropertyDefinitionBriefHelpFormatter(
                &applicationDesc);
            containerFormatter = new PropertyDefinitionContainerBriefHelpFormatter(
                *definitionFormatter, &applicationDesc);

            puList.push_front(application);
            std::cout << containerFormatter->formatPropertyUsers(puList) << std::endl;

            // component options
            options_description componentDesc("Components properties");

            delete definitionFormatter;
            delete containerFormatter;

            definitionFormatter = new PropertyDefinitionBriefHelpFormatter(&componentDesc);
            containerFormatter = new PropertyDefinitionContainerBriefHelpFormatter(*definitionFormatter, &componentDesc);

            puList = getPropertyUsers(dummyManager);
            std::cout << containerFormatter->formatPropertyUsers(puList) << std::endl;

            delete definitionFormatter;
            delete containerFormatter;
        }

        return;

        case eOptionsDetailed:
            defFormatter = new PropertyDefinitionHelpFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eXml:
            defFormatter = new PropertyDefinitionXmlFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eDoxygen:
            defFormatter = new PropertyDefinitionDoxygenFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eDoxygenComponentPages:
            defFormatter = new PropertyDefinitionDoxygenComponentPagesFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eConfig:
            defFormatter = new PropertyDefinitionConfigFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;
    }

    pdcFormatter->setProperties(properties);
    // add this application to the property user list
    puList = getPropertyUsers(dummyManager);
    puList.push_front(application);

    std::cout << pdcFormatter->formatPropertyUsers(puList) << std::endl;

    delete defFormatter;
    delete pdcFormatter;
}


PropertyUserList ApplicationOptions::getPropertyUsers(ArmarXDummyManagerPtr dummyManager)
{
    PropertyUserList propertyUsers;

    std::vector<ManagedIceObjectPtr> objects = dummyManager->getManagedObjects();
    std::vector<ManagedIceObjectPtr>::iterator it = objects.begin();

    while (it != objects.end())
    {
        PropertyUserPtr user = PropertyUserPtr::dynamicCast(*it);

        if (user)
        {
            propertyUsers.push_back(user);
        }

        ++it;
    }

    return propertyUsers;
}
