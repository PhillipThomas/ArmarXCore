/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _H_ARMARX_CPULOADWATCHER
#define _H_ARMARX_CPULOADWATCHER

#include "../services/profiler/Profiler.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <boost/unordered/unordered_set.hpp>
#include <boost/functional/hash.hpp>
#include <functional>

namespace armarx
{
    struct ThreadUsage
    {
        int threadId;
        int processId;
        IceUtil::Time lastUpdate;
        int lastJiffies;
        double load;
        bool operator <(const ThreadUsage& rhs) const;
        bool operator ==(const ThreadUsage& rhs) const;
    };

    struct CpuUsage
    {
        long processId;
        int lastUtime;
        int lastStime;
        int lastCUtime;
        int lastCStime;
        IceUtil::Time lastUpdate;
        double proc_total_time;
    };
}

namespace boost
{
    template <>
    struct hash<armarx::ThreadUsage>
    {
        typedef armarx::ThreadUsage      argument_type;
        typedef std::size_t  result_type;

        result_type operator()(const armarx::ThreadUsage& t) const
        {
            std::size_t val = {0};

            boost::hash_combine(val, t.processId);
            boost::hash_combine(val, t.threadId);
            return val;
        }
    };
}

namespace armarx
{
    class CPULoadWatcher
    {
    public:

        enum PROC_INDEX
        {
            eUTime = 13,
            eSTime = 14,
            eCUTIME = 15,
            eCSTIME = 16
        };

        CPULoadWatcher(Profiler::ProfilerPtr profiler);
        void start();
        void stop();


        void addThread(int processId, int threadId);
        void addThread(int threadId);
        void addAllChildThreads(int processId, int threadId);


        void removeAllThreads();
        void removeThread(int processId, int threadId);
        void removeThread(int threadId);

        void reportCpuUsage();

        double getThreadLoad(int processId, int threadId);
        double getThreadLoad(int threadId);

        armarx::CpuUsage getProcessCpuUsage();

        static int GetThreadJiffies(int processId, int threadId);
        static std::map<int, int> GetThreadListJiffies(int processId, std::vector<int> threadIds);
        static int GetHertz();
    private:
        void watch();
        void runThreadWatchList();
        void cpuUsageFromProcFile();

        long Hertz;
        std::string processCpuDataFilename;
        std::string processCpuUptimeDataFilename;

        PeriodicTask<CPULoadWatcher>::pointer_type watcherTask;

        Mutex processCpuUsageMutex;
        armarx::CpuUsage processCpuUsage;

        Mutex threadWatchListMutex;
        boost::unordered_set<ThreadUsage> threadWatchList;

        /**
         * Holds an instance of armarx::Profiler wich is set in the constructor of CPULoadWatcher.
         */
        Profiler::ProfilerPtr profiler;

    };
}



#endif
