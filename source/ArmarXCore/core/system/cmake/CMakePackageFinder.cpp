/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "CMakePackageFinder.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <stdio.h>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>

#include <boost/shared_ptr.hpp>
#ifndef Q_MOC_RUN
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>
#endif

#define SCRIPT_PATH "ArmarXCore/core/system/cmake/FindPackageX.cmake"


namespace armarx
{

    boost::interprocess::interprocess_upgradable_mutex* memoryMutex = NULL;
    boost::shared_ptr<boost::interprocess::managed_shared_memory> sharedMemorySegment;
    Mutex cmakePackageMutex;
    //    const std::vector<std::string> armarxcoreIncludePaths = CMakePackageFinder::FindPackageIncludePathList("ArmarXCore");

    CMakePackageFinder::CMakePackageFinder(const std::string& packageName, const boost::filesystem::path& packagePath) :
        found(false),
        packageName(packageName)
    {
        _CreateSharedMutex();
        static std::string scriptPath;
        {

            ScopedLock lock(cmakePackageMutex);

            if (scriptPath.empty())
            {
                // Somehow this call fails sometimes if many components are started seperatly
                // So try on fail again with another path
                auto start = IceUtil::Time::now();
                auto list  = FindPackageIncludePathList("ArmarXCore");
                auto dura = (IceUtil::Time::now() - start);

                if (dura.toMilliSeconds() > 1000)
                {
                    ARMARX_ERROR_S << "Cmakefinder for core path took long  - Duration: " << dura.toMilliSeconds();
                }

                if (!ArmarXDataPath::getAbsolutePath(SCRIPT_PATH, scriptPath, list, false))
                {
                    ARMARX_WARNING_S << "Finding FindPackageX.cmake failed - trying again with different path";

                    if (!ArmarXDataPath::getAbsolutePath(std::string("../source/") + SCRIPT_PATH, scriptPath))
                    {
                        return;
                    }
                }
            }
        }
        //        ARMARX_INFO_S << scriptPath;
        int result;
        bool ownLock = false;
        //        if(memoryMutex)
        //            ownLock = memoryMutex->timed_lock(boost::get_system_time() +
        //                                                    boost::posix_time::seconds(1));
        //        else
        //            ARMARX_ERROR_S << "Mutex NULL";
        auto start = IceUtil::Time::now();
        std::string resultStr;

        try
        {
            if (packagePath.empty())
            {
                resultStr = ExecCommand("cmake -DPACKAGE=" + packageName + " -P " + scriptPath, result);
            }
            else
            {
                resultStr = ExecCommand("cmake -DPACKAGE=" + packageName + " -DPACKAGEBUILDPATH=" + packagePath.string() + " -P " + scriptPath, result);
            }

            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }
        }
        catch (...)
        {
            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }

        }


        auto dura = (IceUtil::Time::now() - start);

        if (dura.toMilliSeconds() > 1000)
        {
            ARMARX_INFO_S << "Cmakefinder took long for package " << packagePath << " - Duration: " << dura.toMilliSeconds();
        }

        std::vector<std::string> resultList;
        resultList = extractVariables(resultStr);

        //        ARMARX_INFO_S << resultList;
    }

    std::vector<std::string> CMakePackageFinder::FindPackageIncludePathList(const std::string& packageName)
    {
        _CreateSharedMutex();
        std::string output = FindPackageIncludePaths(packageName);
        std::vector<std::string> result;
        boost::split_regex(result,
                           output,
                           boost::regex("-I")
                          );

        for (size_t i = 0; i < result.size(); i++)
        {
            boost::algorithm::trim(result[i]);

            if (result[i].empty())
            {
                result.erase(result.begin() + i);
                i--;
            }
        }

        return result;
    }

    std::string CMakePackageFinder::FindPackageLibs(const std::string& packageName)
    {
        _CreateSharedMutex();
        bool ownLock = false;

        if (memoryMutex)
            ownLock = memoryMutex->timed_lock(boost::get_system_time() +
                                              boost::posix_time::seconds(1));
        else
        {
            ARMARX_ERROR_S << "Mutex NULL";
        }

        try
        {

            std::stringstream str;
            str << "cmake --find-package -DNAME=" << packageName << " -DLANGUAGE=CXX -DCOMPILER_ID=GNU -DMODE=LINK";
            int result;
            std::string output = ExecCommand(str.str(), result);

            if (result != 0)
            {
                return "";
            }

            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }

            return output;

        }
        catch (...)
        {
            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }

            return "";
        }

    }


    std::string CMakePackageFinder::FindPackageIncludePaths(const std::string& packageName)
    {
        bool ownLock = false;
        _CreateSharedMutex();

        if (memoryMutex)
            ownLock = memoryMutex->timed_lock(boost::get_system_time() +
                                              boost::posix_time::seconds(1));
        else
        {
            ARMARX_ERROR_S << "Mutex NULL";
        }

        if (!ownLock)
        {
            ARMARX_ERROR_S << "Failed to get lock";
        }

        try
        {
            std::stringstream str;
            str << "cmake --find-package -DNAME=" << packageName << " -DLANGUAGE=CXX -DCOMPILER_ID=GNU -DMODE=COMPILE";
            int result;
            std::string output = ExecCommand(str.str(), result);

            //        ARMARX_IMPORTANT_S << "output: " << output;
            if (result != 0)
            {
                return "";
            }

            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }

            return output;
        }
        catch (...)
        {
            if (memoryMutex && ownLock)
            {
                memoryMutex->unlock();
            }

            return "";
        }
    }

    std::string CMakePackageFinder::ExecCommand(const std::string& command, int& result)
    {
        FILE* fp = popen(command.c_str(), "r");
        char line [50];
        std::string output;

        while (fgets(line, sizeof line, fp))
        {
            output += line;
        }

        result = pclose(fp) / 256;
        return output;
    }

    std::string CMakePackageFinder::getName() const
    {
        return packageName;
    }

    bool CMakePackageFinder::packageFound() const
    {
        return found;
    }

    const std::map<std::string, std::string>& CMakePackageFinder::getVars() const
    {
        return vars;
    }

    std::string CMakePackageFinder::getVar(const std::string& varName) const
    {
        std::map<std::string, std::string>::const_iterator it =  vars.find(varName);

        if (it != vars.end())
        {
            return it->second;
        }

        return "";
    }

    std::vector<std::string> CMakePackageFinder::getDependencies() const
    {
        auto depListString = getVar("DEPENDENCIES");
        std::vector<std::string> resultList;
        boost::split(resultList,
                     depListString,
                     boost::is_any_of(";"),
                     boost::token_compress_on);
        return resultList;
    }

    std::map<std::string, std::string> CMakePackageFinder::getDependencyPaths() const
    {
        // is of type "package1:package1Path;package2:packagePath2"
        auto depListString = getVar("PACKAGE_DEPENDENCY_PATHS");
        std::vector<std::string> resultList;
        boost::split(resultList,
                     depListString,
                     boost::is_any_of(";"),
                     boost::token_compress_on);
        std::map<std::string, std::string> resultMap;

        for (auto & depPairString : resultList)
        {
            std::vector<std::string> depPair;
            boost::split(depPair,
                         depPairString,
                         boost::is_any_of(":"),
                         boost::token_compress_on);

            if (depPair.size() < 2)
            {
                continue;
            }

            resultMap[depPair.at(0)] = depPair.at(1);
        }

        return resultMap;
    }

    bool CMakePackageFinder::_ParseString(const std::string& input, std::string& varName, std::string& content)
    {
        //        ARMARX_INFO_S << "input: " << input;
        const boost::regex e("\\-\\- ([a-zA-Z0-9_]+):(.+)");
        boost::match_results<std::string::const_iterator> what;

        bool found = boost::regex_search(input, what, e);

        for (size_t i = 1; i < what.size(); i++)
        {

            if (i == 1)
            {
                varName =  what[i];
            }
            else if (i == 2)
            {
                content = what[i];
            }

            //            ARMARX_INFO_S << VAROUT(varName);
        }

        boost::algorithm::trim(varName);
        boost::algorithm::trim(content);
        return found;

    }

    void CMakePackageFinder::_CreateSharedMutex()
    {
        if (sharedMemorySegment && memoryMutex)
        {
            return;
        }

        std::size_t shmSize = sizeof(boost::interprocess::interprocess_mutex)
                              + 1024;
        memoryMutex = NULL;
        auto userName = getenv("USER");
        std::string memoryName = "CMakeSharedMemory";

        if (userName)
        {
            memoryName += userName;
        }

        std::string mutexName = "SharedCMakeMutex";

        try
        {
            //            if(!boost::interprocess::shared_memory_object::remove(memoryName.c_str()))
            //                ARMARX_ERROR_S << "Removing shared memory failed";
            if (!sharedMemorySegment)
                sharedMemorySegment.reset(new boost::interprocess::managed_shared_memory(boost::interprocess::open_or_create,
                                          memoryName.c_str(),
                                          shmSize));
        }
        catch (std::exception& e)
        {
            sharedMemorySegment.reset();
            throw exceptions::local::SharedMemoryException(memoryName, "Error creating shared memory segment. Still a consumer running? - reason: ") << + e.what();
        }
        catch (...)
        {
            sharedMemorySegment.reset();
            throw exceptions::local::SharedMemoryException(memoryName, "Error creating shared memory segment. Still a consumer running?");
        }

        //        sharedMemorySegment->destroy<boost::interprocess::interprocess_upgradable_mutex>(mutexName.c_str());
        memoryMutex = sharedMemorySegment->find_or_construct<boost::interprocess::interprocess_upgradable_mutex>(mutexName.c_str())();

        if (!memoryMutex)
        {
            throw exceptions::local::SharedMemoryException(memoryName, "Error constructing " + mutexName + " in shared memory segment.");
        }

        //        ARMARX_WARNING_S << "Created shared memory and mutex";
    }


    std::vector<std::string> CMakePackageFinder::extractVariables(const std::string& cmakeVarString)
    {
        std::vector<std::string> resultList;
        boost::split(resultList,
                     cmakeVarString,
                     boost::is_any_of("\n"),
                     boost::token_compress_on);

        for (size_t i = 0; i < resultList.size(); i++)
        {
            boost::algorithm::trim(resultList[i]);

            if (resultList[i].empty())
            {
                resultList.erase(resultList.begin() + i);
                i--;
            }
            else
            {
                std::string var;
                std::string content;

                if (_ParseString(resultList[i], var, content))
                {
                    found = true;
                    vars[var] = content;
                }
            }
        }

        return resultList;
    }

    std::vector<std::string> armarx::CMakePackageFinder::getIncludePathList() const
    {
        auto depListString = getIncludePaths();
        std::vector<std::string> resultList;
        boost::split(resultList,
                     depListString,
                     boost::is_any_of(";"),
                     boost::token_compress_on);
        return resultList;
    }

}
