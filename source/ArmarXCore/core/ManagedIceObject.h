/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_MANAGEDICEOBJECT_H
#define _ARMARX_CORE_MANAGEDICEOBJECT_H

#include "ArmarXFwd.h"
#include "IceManager.h"

#include "system/ImportExport.h"
#include "logging/Logging.h"
#include "services/profiler/Profiler.h"

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.h>


namespace armarx
{
    /**
     * ManagedIceObject shared pointer for convenience
     */
    class ManagedIceObject;
    typedef IceInternal::Handle<ManagedIceObject> ManagedIceObjectPtr;

    /**
     * @class ManagedIceObject
     * @ingroup DistributedProcessingGrp
     * @brief The ManagedIceObject is the base class for all ArmarX objects.
     *
     * ManagedIceObject are the ArmarX equivalent to well-known Ice objects.
     * A defined lifecycle is guaranteed which and state dependent interface
     * is provided by the following framework hooks:
     * @li ManagedIceObjects::onInitComponent()
     * @li ManagedIceObjects::onConnectComponent()
     * @li ManagedIceObjects::onExitComponent()
     *
     * @image html Lifecycle-UML.svg Lifecyle of a ManagedIceObject managed by the ArmarXObjectScheduler
     * Every subclass of ManagedIceObject can specify dependent proxies and topics
     * as well as offered topics in ManagedIceObjects::onInitComponent().
     * These connectivity parameters are specified through the following methods:
     * @li MangedIceObject::usingProxy()
     * @li MangedIceObject::usingTopic()
     * @li MangedIceObject::offeringTopic()
     *
     * These dependencies are resolved at runtime and can be visualized.
     * Additionally it is possible to handle the case of crashed dependencies.
     \code
     MyManagedIceObject::onInitComponent()
     {
         // component depends on proxy "MyDataProvider"
         usingProxy("MyDataProvider");

         // component wants to subscribe to topic "News"
         usingTopic("News");

         // component will offer topic "FilteredNews"
         offeringTopic("FilteredNews");
     }
     \endcode

     @see ArmarXObjectScheduler
     */
    class ARMARXCORE_IMPORT_EXPORT ManagedIceObject :
        virtual public Ice::Object,
        virtual public Logging

    {
        friend class ArmarXObjectScheduler;

    public:
        ManagedIceObject(ManagedIceObject const& other);

        /**
         * Factory method for ManagedIceObjects.
         *
         * @param name name of the Ice well-known object used for registration to IceGrid.
         */
        template <class T>
        static IceInternal::Handle<T> create(const std::string& name = "")
        {
            IceInternal::Handle<T> ptr = new T();

            if (name.empty())
            {
                ptr->setName(ptr->getDefaultName());
            }
            else
            {
                ptr->setName(name);
            }

            return IceInternal::Handle<T>::dynamicCast(ptr);
        }

        /**
        * Retrieve name of object. Corresponds to well-known object name.
        *
        * @return name
        */
        std::string getName() const
        {
            if (name.empty())
            {
                return getDefaultName();
            }

            return name;
        }

        /**
         * Returns the proxy of this object
         *
         * @return object's proxy
         */
        Ice::ObjectPrx getProxy() const
        {
            return proxy;
        }

        /**
        * Retrieve current state of the ManagedIceObject
        *
        * @return state of the ManagedIceObject
        */
        ManagedIceObjectState getState() const;



        ArmarXObjectSchedulerPtr getObjectScheduler();

        /**
        * Retrieve connectivity of the object (topcis as well as proxies)
        *
        * @return connectivity
        */
        ManagedIceObjectConnectivity getConnectivity() const;

        /**
         * Returns the ArmarX manager used to add and remove components
         *
         * @retrun pointer to the armarXManager
         */
        ArmarXManagerPtr getArmarXManager() const;

        /**
         * Returns the IceManager
         *
         * @return pointer to IceManager
         */
        IceManagerPtr getIceManager() const;

        /**
         * Registers a proxy for retrieval after initialization and adds it to the dependency list.
         * This ManagedIceObject won't start (onConnectComponent()) until this proxy is avaiable.
         *
         * @param name          Proxy name
         * @param endpoints     Specific endpoints, e.g. tcp ‑p 10002
         * @return returns true if new dependency, else false
         */
        bool usingProxy(const std::string& name,
                        const std::string& endpoints = std::string());

        /**
         * Retrieves a proxy object.
         *
         * @param name         Proxy name, e.g. Log
         * @param endpoints    The endpoints, e.g. tcp ‑p 10002
         * @param addToDependencies If true, this proxy is added to the dependency list and this function won't return until the proxy becomes available.
         * @throw LocalException Throws if proxy is not available, this object is not yet starting or the proxyName is empty.
         * @return A proxy of the remote instance.
         */
        template <class ProxyType>
        ProxyType getProxy(const std::string& name,
                           bool addToDependencies = false,
                           const std::string& endpoints = std::string())
        {
            if (name.empty())
            {
                throw LocalException("The proxy name must not be empty.");
            }

            if (getState() < eManagedIceObjectStarting)
            {
                throw LocalException("Calling getProxy before component has been started");
            }

            if (addToDependencies)
            {
                usingProxy(name);

                waitForObjectScheduler();
            }

            // return proxy
            return iceManager->getProxy<ProxyType>(name, endpoints);
        }

        /**
         * Assigns a proxy object. Convenience function for getProxy<>().
         *
         * @param  var          The var to assign the proxy to
         * @param  name         Proxy name, e.g. Log
         * @param  endpoints    The endpoints, e.g. tcp ‑p 10002
         *
         * @see getProxy<>()
         */
        template <class ProxyType>
        void assignProxy(ProxyType& var,
                         const std::string& name,
                         bool addToDependencies = false,
                         const std::string& endpoints = std::string())
        {
            var = getProxy<ProxyType>(name, addToDependencies, endpoints);
        }

        /**
         * @brief returns the names of all unresolved dependencies
         */
        std::vector<std::string> getUnresolvedDependencies() const;

        static std::string GetObjectStateAsString(ManagedIceObjectState state);

        /**
         * @brief getProfiler returns an instance of armarx::Profiler
         */
        Profiler::ProfilerPtr getProfiler() const;

        /**
         * @brief setProfiler allows setting ManagedIceObject::profiler to a new instance (if the new instance is actually not a null pointer)
         */
        void enableProfiler(bool enable);

    protected:
        /**
         * Protected default constructor. Used for virtual inheritance. Use createManagedIceObject() instead.
         */
        ManagedIceObject();
        virtual ~ManagedIceObject();

        /**
         * Returns object's Ice adapter
         *
         * @return object's adapter
         */
        Ice::ObjectAdapterPtr getObjectAdapter() const
        {
            return objectAdapter;
        }

        /**
         * Registers a proxy for subscription after initialization
         *
         * @param name          Topic name
         */
        void usingTopic(const std::string& name);

        /**
         * Registers a topic for retrival after initialization
         *
         * @param name          Topic name
         */
        void offeringTopic(const std::string& name);

        /**
         * Returns a proxy of the spcified topic.
         *
         * @param name          Topic name
         *
         * @return Topic proxy of the type \e TopicProxyType
         */
        template <class TopicProxyType>
        TopicProxyType getTopic(const std::string& name)
        {
            if (getState() < eManagedIceObjectStarting)
            {
                throw LocalException("Calling getTopic before component has been started");
            }

            // make sure proxy is on the dependency list
            offeringTopic(name);

            // retrieve topic
            return iceManager->getTopic<TopicProxyType>(name);
        }

        /**
         * @brief Waits until the ObjectScheduler could resolve all dependencies.
         */
        void waitForObjectScheduler();

        /**
         * @brief This function removes the dependency of this object
         * on the in parameter name specified object.
         * @param name name of the depedency proxy
         * @return returns true if depedency was found and removed
         * false if dependency does not exist
         */
        bool removeProxyDependency(const std::string& name);


        /**
         * Initiates termination of this IceManagedObject. Returns immediately.
         */
        void terminate();

        /**
         * Override name of well-known object
         */
        void setName(std::string name)
        {
            this->name = name;
        }

        /**
         * Pure virtual hook for the subclass. Is called once initialization of the ManagedIceObject is done.
         * This hook is called in the implenting class once and never again during the lifecyle of the object. This function is called
         * as soon as the ManagedIceObject was added to the ArmarXManager. Called in an own thread and not the thread it was created in.
         *
         */
        virtual void onInitComponent() = 0;

        /**
         * Pure virtual hook for the subclass. Is called once all dependencies of the object have been resolved and Ice connection is established.
         * This hook is called whenever the depedencies are found. That means if the a depedency crashes or shuts down, the ManagedIceObject goes
         * into disconnected state. When the dependencies are found again, this hook is called again.
         *
         *
         * @see onDisconnectComponent()
         */
        virtual void onConnectComponent() = 0;

        /**
         * Hook for subclass. Is called if a dependency of the object got lost (crash, network error, stopped, ...).
         * This hook should be the inverse to onConnectComponent(). So that onDisconnectComponent() and onConnectComponent() can be called alternatingly
         * and the ManagedIceObject remains in valid states.         *
         *
         * @see onConnectComponent
         */
        virtual void onDisconnectComponent() {}

        /**
         * Hook for subclass. Is called once the component terminates. Use for cleanup. Only called once.
         */
        virtual void onExitComponent() {}

        /**
         * Retrieve default name of component
         *
         * Implement this method in each IceManagedObject. The default name of a
         * is used if no name is specified in the factory method.
         *
         * @return default name of the component (e.g. "KinematicUnit")
         */
        virtual std::string getDefaultName() const = 0;

    private:
        // init sets members and calls onInitComponent hook
        void init(IceManagerPtr iceManager);
        // start sets members and calls onConnectComponent hook
        void start(Ice::ObjectPrx& proxy, const Ice::ObjectAdapterPtr& objectAdapter);
        void disconnect();
        // exit sets members and calls onExitComponent hook
        void exit();

        void setObjectState(ManagedIceObjectState newState);

    private:
        /**
         * Name of object. Corresponds to name of Ice well-known object.
         */
        std::string name;

        /**
         * Component manager instance. The manager can be used to add and remove
         * components dynamically.
         */
        ArmarXManagerPtr armarXManager;

        /**
         * The Ice manager used for adapter registration, proxy retrieval topic
         * and session management.
         */
        IceManagerPtr iceManager;

        /**
         * The Ice manager used for adapter registration, proxy retrieval topic
         * and session management.
         */
        ArmarXObjectSchedulerPtr objectScheduler;

        /**
         * State of the ManagedIceObject.
         */
        ManagedIceObjectState objectState;
        mutable Mutex objectStateMutex;

        /**
         * Proxy of this object needed for storm topic subscriptions.
         */
        Ice::ObjectPrx proxy;

        /**
         * Component object adapter.
         */
        Ice::ObjectAdapterPtr objectAdapter;

        /**
         * connectivity.
         */
        ManagedIceObjectConnectivity connectivity;

        /**
         * Mutex for connectivity.
         */
        mutable Mutex connectivityMutex;

        /**
         * @brief startedCondition is used to signal waiting threads, that
         * this object is started (i.e. onConnectComponent() was called)
         *
         * @see onConnectComponent(), waitForObjectStart()
         */
        boost::condition_variable stateCondition;

        /**
         * Holds an instance of armarx::Profiler wich can be accessed via ManagedIceObject::getProfiler().
         * Enable or disable the profiler via ManagedIceObject::enable().
         */
        Profiler::ProfilerPtr profiler;

        /**
         * @brief enableProfilerFunction is used to defer the creation of the IceLoggingStrategy topic proxy
         *
         * This function pointer normally executes MangedIceObject::Noop() and is called in ManagedIceObject::init() after the IceManager is set.
         *
         * In case ManagedIceObject::enableProfiler() is called before ManagedIceObject::init() the creation of the the IceLoggingStrategy
         * is put into this function object and executed when Ice is available.
         */
        void (*enableProfilerFunction)(ManagedIceObject* object);

        /**
         * @brief Noop function which does nothing (Only to be used as default value for enableProfilerFunction function pointer)
         * @param object
         */
        static void Noop(ManagedIceObject* object);

        /**
         * @brief EnableProfilerOn creates an instance of armarx::IceLoggingStrategy and sets it on object->profiler
         * @param object
         */
        static void EnableProfilerOn(ManagedIceObject* object);
    };
}

#endif
