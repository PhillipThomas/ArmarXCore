/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_LOGSENDER_H
#define _ARMARX_CORE_LOGSENDER_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/core/system/Synchronization.h>
#include <ArmarXCore/interface/core/Log.h>

#include <ArmarXCore/core/exceptions/Exception.h>


#include <boost/unordered_map.hpp>
#include <boost/logic/tribool.hpp>







namespace armarx
{

    class LogSender;
    /**
    * Typedef of boost::shared_ptr for convenience.
    */
    typedef boost::shared_ptr<LogSender> LogSenderPtr;


    struct LogTag
    {
        LogTag() {}
        LogTag(const std::string& tagName)
        {
            this->tagName = tagName;
        }
        std::string tagName;
    };

    typedef boost::unordered_map<std::string, boost::unordered_map<std::string, IceUtil::Time> > SpamFilterMap;
    typedef boost::shared_ptr<SpamFilterMap> SpamFilterMapPtr;
    struct SpamFilterData
    {
        SpamFilterMapPtr filterMap;
        boost::shared_ptr<Mutex> mutex;
        std::string identifier;
        float durationSec;
        SpamFilterData()
        {
            filterMap.reset(new SpamFilterMap());
            mutex.reset(new Mutex());
            durationSec = 10.0f;
        }
    };
    typedef boost::shared_ptr<SpamFilterData> SpamFilterDataPtr;

    /**
      \class LogSender
      \brief Wrapper for the Log IceStorm topic with convenience methods for logging.
      \ingroup Logging
    */
    class ARMARXCORE_IMPORT_EXPORT LogSender : public boost::enable_shared_from_this<LogSender>
    {
    public:
        enum ConsoleColor
        {
            eReset = -1,
            eBlack = 0,
            eRed = 1,
            eGreen = 2,
            eYellow = 3,
            eBlue = 4
        };
        typedef void (LogSender::*manipulator)();

        // this is the type of std::cout
        typedef std::basic_ostream<char, std::char_traits<char> > CoutType;

        // this is the function signature of std::endl
        typedef CoutType& (*StandardEndLine)(CoutType&);





        static LogSenderPtr createLogSender();

        LogSender();
        ~LogSender();

        /**
          * Constructor taking a LogPrx pointer to the IceStorm topic.
          *
          * \param componentName All log messages sent through this instance
          *                      will show as having originated from this
          *                      component.
          * \param logProxy      Ice proxy to send messages to.
          */
        LogSender(const std::string& componentName, LogPrx logProxy);

        static void setProxy(const std::string& componentName, LogPrx logProxy);

        /**
          Appends a variable to the current message stringstream

          \param message
          \return This object for further streaming
          */
        template<typename T>
        LogSender& operator<<(const T& message)
        {
            currentMessage << message;
            return *this;
        }



        LogSender& operator<< (const StandardEndLine& manipulator); // overloading for std::endl, but cannot get it to work with template specialization (Mirko 2012)
        /**
          Sends the current message to the log and resets the content.
          */
        void flush();

        static ConsoleColor GetColorCode(MessageType type);
        static std::string GetColorCodeString(MessageType verbosityLevel);
        static std::string GetColorCodeString(ConsoleColor colorCode);

        static std::string CreateBackTrace(int linesToSkip = 1);
        /**
          Retrieves the current message severity

          \return Current message severity
          */
        MessageType getSeverity();

        /**
          Set the source code filename associated with this message.

          \param filename The file this message was sent from
          \return a pointer to this object
          */
        LogSenderPtr setFile(const std::string& file);

        /**
          Set the source code line associated with this message.

          \param line The line this message was sent from
          \return a pointer to this object
          */
        LogSenderPtr setLine(int line);

        /**
          Set the function name associated with this message.

          \param line The function this message was sent from
          \return a pointer to this object
        */
        LogSenderPtr setFunction(const std::string& function);
        LogSenderPtr setLocalMinimumLoggingLevel(MessageType level)
        {
            minimumLoggingLevel = level;
            return shared_from_this();
        }
        LogSenderPtr setBacktrace(bool printBackTrace)
        {
            this->printBackTrace = printBackTrace;
            return shared_from_this();
        }

        LogSenderPtr setTag(const LogTag& tag);

        //        LogSenderPtr setSpamFilter(SpamFilterMapPtr spamFilter);

        static std::string levelToString(MessageType type);

        /**
         * \brief stringToLevel converts a string into a LoggingLevel,
         * if possible.
         *
         * <b>Case-Insensitive</b>
         * \param typeStr string that is to be converted
         * \return The LoggingLevel or Undefined if conversion not possible.
         */
        static MessageType StringToLevel(const std::string& typeStr);

        /**
         * \brief With setGlobalMinimumLoggingLevel the minimum verbosity-level of
         * log-messages can be set for the whole application.<br/>
         * the flag 'minimumLoggingLevel' overrides this setting.
         * \param level The minimum logging level
         */
        static void SetGlobalMinimumLoggingLevel(MessageType level);

        /**
         * \brief setLoggingActivated() is used to activate or disable
         * the logging facilities in the whole application
         * \param activated
         */
        static void SetLoggingActivated(bool activated = true, bool showMessage = true);
        static void SetSendLoggingActivated(bool activated = true);

        static void SetColoredConsoleActivated(bool activated = true);
        static long getThreadId();
        static long getProcessId();
        template <typename T>
        static std::string GetTypeString();
        static std::string CropFunctionName(std::string originalFunctionName);
    protected:
        /**
         * Sends a message to the logging component.
         *
         * \param severity eVERBOSE / eINFO / eWARN / eERROR
         * \param message
         */
        void log(MessageType severity, const std::string& message);

    private:
        void initConsole();

        /**
          Resets information about associated file, line and function.
         */
        void resetLocation();
        //! Flag to specify a minimum logging level for this application <br/>
        //! Is overidden by minimumLoggingLevel (if set)
        static MessageType GlobalMinimumLoggingLevel;
        //! Flag to activate or disable the logging facilities for this application <br/>
        static bool LoggingActivated;

        static bool ColoringActivated;
        static bool SendLogging;

        //! Flag to specify a minimum logging level for this LogSender<br/>
        //! usually is set by Logging-Class
        MessageType minimumLoggingLevel;
        static std::string componentName;
        static LogPrx logProxy;


        SpamFilterMapPtr spamFilter;
        bool cancelNextMessage;
        std::stringstream currentMessage;
        MessageType currentSeverity;

        std::string currentFile;
        int currentLine;
        std::string currentFunction;
        LogTag currentTag;
        boost::logic::tribool printBackTrace;
        static std::vector<LogMessage> buffer;
        static Mutex mutex;
    } ;

    const LogSender::manipulator flush = &LogSender::flush;

    /**
    * Changes the current message severity for streamed messages
    *
    * \param  severity
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <MessageType>(const MessageType& severity);

    /**
    * Changes the current tag for streamed messages
    *
    * \param  tag
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogTag>(const LogTag& tag);

    /**
    * Executes a manipulator like flush on the stream
    *
    * \param  manipulator
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogSender::manipulator>(const manipulator& manipulator);

    /**
        * Changes the current message color
        *
        * \param  colorCode Color of the Text
        * \return This object for further streaming
        */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogSender::ConsoleColor>(const LogSender::ConsoleColor& colorCode);

    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <bool>(const bool& duality);

    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <SpamFilterDataPtr>(const SpamFilterDataPtr& spamFilterData);




}

#endif
