/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Mirko Wächter 2012 (mirko dot waechter @ kit dot edu)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef _ARMARX_CORE_LOGGING_H
#define _ARMARX_CORE_LOGGING_H

#include <ArmarXCore/core/logging/LogSender.h>

#define BOOST_ENABLE_ASSERT_HANDLER


/**
  *  \page LoggingDoc ArmarX Logging
  *
  *  Every output in ArmarX should be done with the built-in logging features.
  *  The ArmarX logging facilities are
  *  available anytime and anywhere in the code and should be used
  *  instead of std::cout or printf().<br/>
  *  ArmarX Logging provides the following main features:
  *  \li usable like std::cout
  *  \li stores meta data like file, line, time, who etc.
  *  \li offers several different log levels (from verbose to fatal)
  *  \li can log backtraces
  *  \li provides logs over network (Ice)
  *  \li LogViewer (GUI) which works through Ice
  *  \li thread safety
  *  \li console coloring
  *
  *  \section Usage How to use the ArmarX logging facilities
  *
  *  For logging there are a few macros available (e.g. "ARMARX_INFO"). They all
  *  begin with "ARMARX_". After that the log level follows (e.g. "ARMARX_INFO"),
  *  followed by an "_S", if the static version should be used.<br/>
  *
  *  An example looks like this:<br/>
  *  \code
  *     ARMARX_INFO_S << "cycle time dropped below " << 1 << " fps in " << "Capturer";
  *  \endcode
  *
  *  After the c++-line has ended (i.e. after the semi-colon), the data is written
  *  as <b>one entry</b> into the log.
  *
  *  To be able use the logging, one has to include the file <ArmarXCore/core/logging/Logging.h>.<p/>
  *
  *  There are 2 ways for logging:
  *  \li <b>Logging with static functions</B>
  *      <br/>Logging with the static functions is available from anywhere at anytime.
  *      Use the logging macros ending on *_S* for this, e.g. ARMARX_INFO_S
  *  \li <b>Deriving from the \ref armarx::Logging class</b><br/>
  *      Derive your own class from \ref armarx::Logging to have additional features
  *      like a log-tag or setting a minimum logging-level for the whole class
  *      (Outputs below this level will be ignored). In this case use the non static macro versions: ARMARX_INFO
  *
  *  The main features in detail:
  *  \li <b>usable like std::cout</b><br/>
  *      The usage of the works like std::cout. That means after the macro (e.g.
  *      "ARMARX_INFO") follows the streaming operator (<<). There are conversion
  *      for most of the standard types available like with std::cout and some
  *      additional types (e.g. std::vector, QString in Gui). These can be easily
  *      extended by overloading/specializing the armarx::LogSender::operator<< <T>().
  *  \li <b>stores meta data like file, line, time, who etc.</b><br/>
  *      At the moment the logging store the following meta data: time, tag, log-level,
  *      Ice-component, file, line, function, threadId and backtrace (for log-level
  *      eWARN and above).
  *  \li <b>offers several different log levels (from debug to fatal)</b><br>
  *      There are 5 log levels available. In order of increasing severity:
  *      debug, verbose, info, warning, error, fatal (The corresponding macros are:
  *      ARMARX_DEBUG, ARMARX_VERBOSE, ARMARX_INFO, ARMARX_WARNING, ARMARX_ERROR, ARMARX_FATAL).
  *  \li <b>can log backtraces</b><br>
  *      For the log levels warning and above a backtrace is stored in the meta data.
  *      For lower levels the backtrace is empty to reduce the network traffic.<br/>
  *      The backtrace is not shown in the console.
  *  \li <b>provides logs over network (Ice)</b><br/>
  *      All log output are automatically streamed via Ice, if an Ice connection is available.<br/>
  *      If the connection is not established yet, all log outputs are buffered locally
  *      until the connection is ready.<br/>
  *      The logging facilties are using the publish-subscribe-methodolgy of Ice for this,
  *      so potential log-viewers can just subscribe to the topic "Log"(Type: LogPrx).
  *  \li <b>LogViewer (GUI) which works through Ice</b><br/>
  *      The LogViewer is an ArmarX Gui Widget, that provides a comfortable
  *      access to the logs. It offers the possibility to define custom filters to split
  *      the data in several logs, to search quickly through the log file and much more.
  *  \li <b>thread safety</b><br/>
  *      As opposed to std::cout the ArmarX logging facilities are thread safe.
  *      So there is no mixing of log entries of different threads.
  *  \li <b>console coloring</b><br/>
  *      If activated (default) the output on the console of different log levels
  *      is colorized. Additional the user can just use the streaming operator \<\<
  *      and the LogSender color enum (e.g. LogSender::eGreen) to set the color
  *      for the current output message.
  *
  *
  *
  *
  *
 */


#if defined ( WIN32 )
#define ARMARX_FUNCTION __FUNCSIG__
#else
#define ARMARX_FUNCTION __PRETTY_FUNCTION__
#endif

/**
 * \defgroup Logging
 * \ingroup core-utility
 * \copydoc LoggingDoc

 * \def ARMARX_LOG_S
 * \ingroup Logging
 * This macro creates a new temporary instance which
 * can then be used to log data using the << operator.
 * This macro can be used by everybody.
 * S stands for static.

 * example:
 * \code
 * ARMARX_LOG_S << eWARN << "cycle time dropped below " << 1 << "fps in " << "Capturer" << endl;
 * \endcode
 *
 */
#define ARMARX_LOG_S (*(armarx::LogSender::createLogSender()->setFile(__FILE__)->setLine(__LINE__)->setFunction(ARMARX_FUNCTION)))

/**
 * \def ARMARX_LOG
 * \ingroup Logging
 * This macro retrieves the armarx::LogSender instance of this class which
 * can then be used to log data using the << operator.
 * This macro can be used by classes that inherit from Logging.
 *
 * example:
 * \code
 * ARMARX_LOG << eWARN << "cycle time dropped below " << 1 << "fps in " << "Capturer" << endl;
 * \endcode
 *
 */
#define ARMARX_LOG (*ARMARX_LOG_S.setLocalMinimumLoggingLevel(this->Logging::minimumLoggingLevel)) << this->Logging::tag

//! \ingroup Logging
//! The normal logging level
#define ARMARX_INFO ARMARX_LOG << armarx::eINFO
//! \ingroup Logging
//! The logging level for output that is only interesting while debugging
#define ARMARX_DEBUG ARMARX_LOG << armarx::eDEBUG
//! \ingroup Logging
//! The logging level for verbose information
#define ARMARX_VERBOSE ARMARX_LOG << armarx::eVERBOSE
//! \ingroup Logging
//! The logging level for always important information, but expected behaviour (in contrast to ARMARX_WARNING)
#define ARMARX_IMPORTANT ARMARX_LOG << armarx::eIMPORTANT
//! \ingroup Logging
//! The logging level for unexpected behaviour, but not a serious problem
#define ARMARX_WARNING ARMARX_LOG << armarx::eWARN
//! \ingroup Logging
//! The logging level for unexpected behaviour, that must be fixed
#define ARMARX_ERROR ARMARX_LOG << armarx::eERROR
//! \ingroup Logging
//! The logging level for unexpected behaviour, that will lead to a seriously malfunctioning program and probably to program exit
#define ARMARX_FATAL ARMARX_LOG << armarx::eFATAL

//! \ingroup Logging
#define ARMARX_INFO_S ARMARX_LOG_S << armarx::eINFO
//! \ingroup Logging
//! The logging level for output that is only interesting while debugging
#define ARMARX_DEBUG_S ARMARX_LOG_S << armarx::eDEBUG
//! \ingroup Logging
#define ARMARX_VERBOSE_S ARMARX_LOG_S << armarx::eVERBOSE
//! \ingroup Logging
//! The logging level for always important information, but expected behaviour (in contrast to ARMARX_WARNING)
#define ARMARX_IMPORTANT_S ARMARX_LOG_S << armarx::eIMPORTANT
//! \ingroup Logging
#define ARMARX_WARNING_S ARMARX_LOG_S << armarx::eWARN
//! \ingroup Logging
#define ARMARX_ERROR_S ARMARX_LOG_S << armarx::eERROR
//! \ingroup Logging
#define ARMARX_FATAL_S ARMARX_LOG_S << armarx::eFATAL


namespace armarx
{
    class LogSender;
    /**
    * Typedef of boost::shared_ptr for convenience.
    */
    typedef boost::shared_ptr<LogSender> LogSenderPtr;

    /**
      \class Logging
      \brief Base Class for all Logging classes.
      \ingroup Logging

      Inherit from this class in order to use ArmarX Logging facitities.

      You should call setTag() in the derived class's constructor,
      otherwise the category string will be empty.
    */
    class ARMARXCORE_IMPORT_EXPORT Logging
    {
    public:


        Logging();
        virtual ~Logging();
        void setTag(const LogTag& tag)
        {
            this->tag = tag;
        }
        void setTag(const std::string& tagName)
        {
            this->tag = LogTag(tagName);
        }
        /*!
         * \brief With setLocalMinimumLoggingLevel the minimum verbosity-level of
         * log-messages can be set.
         * \param level The minimum logging level
         */
        void setLocalMinimumLoggingLevel(MessageType level);

        SpamFilterDataPtr deactivateSpam(float deactivationDurationSec = 10.0f, const std::string& identifier = "", bool deactivate = true);
    protected:
        /** Retrieve log sender.
             *
             * This method is usually called by using one of the loggin macro ARMARX_LOG.
             *
             * @return pointer to the logSender
             */
        LogSenderPtr getLogSender() const;
        LogTag tag;
        MessageType minimumLoggingLevel;
        SpamFilterDataPtr spamFilter;

    private:
        LogSenderPtr logSender;

    };

}

#endif
