/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::core
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Logging.h"


#include <ArmarXCore/core/util/StringHelpers.h>

#include <ArmarXCore/core/exceptions/Exception.h>



using namespace armarx;

Logging::Logging()
    :
    minimumLoggingLevel(eUNDEFINED),
    spamFilter(new SpamFilterData()),
    logSender(new LogSender())
{

}

Logging::~Logging()
{
}

void Logging::setLocalMinimumLoggingLevel(MessageType level)
{
    *logSender << minimumLoggingLevel << "setting logging level to " << LogSender::levelToString(level);
    logSender->flush();
    minimumLoggingLevel = level;
}

SpamFilterDataPtr Logging::deactivateSpam(float deactivationDurationSec, const std::string& identifier, bool deactivate)
{
    if (deactivate)
    {
        ScopedLock lock(*spamFilter->mutex);
        spamFilter->durationSec = deactivationDurationSec;
        spamFilter->identifier = identifier;

        if (spamFilter->filterMap->count(identifier) == 0)
        {
            (*spamFilter->filterMap)[identifier] = boost::unordered_map<std::string, IceUtil::Time>();
        }

        return spamFilter;
    }


    return SpamFilterDataPtr();
}

LogSenderPtr Logging::getLogSender() const
{
    return logSender;
}




namespace boost
{
    void assertion_failed(char const* expr,
                          char const* function, char const* file, long line)
    {
        std::stringstream str;
        str << "At " << file << ":" << line << " in function " << function << " the following expression evaluted to false: " << expr;
        throw armarx::exceptions::local::BoostAssertException(str.str());
    }

    void assertion_failed_msg(char const* expr, char const* msg,
                              char const* function, char const* file, long line)
    {
        std::stringstream str;
        str << "At " << file << ":" << line << " in function " << function << " the following expression evaluted to false:" << expr  << "\n" << msg;
        throw armarx::exceptions::local::BoostAssertException(str.str());

    }

}
