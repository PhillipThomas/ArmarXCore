/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_NOTIMPLEMNTEDYETEXCEPTION_H
#define _ARMARX_CORE_NOTIMPLEMNTEDYETEXCEPTION_H


#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace user
        {
            /**
              \class NotImplementedYetException
              \brief Throw this exception to indicate missing functionality.
              \ingroup Exceptions
             */
            class NotImplementedYetException: public armarx::NotImplementedYetException
            {
            public:

                NotImplementedYetException(std::string hint)
                {
                    reason = "The following is not implemented yet: " + hint;
                }

                ~NotImplementedYetException() throw()
                {
                }

                virtual std::string ice_name() const
                {
                    return "armarx::exceptions::user::NotImplementedYetException";
                }
            };
        }
    }
}

#endif
