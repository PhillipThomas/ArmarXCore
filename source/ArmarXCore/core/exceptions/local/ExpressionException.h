/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef H_ARMARX_EXPRESSIONEXCEPTION
#define H_ARMARX_EXPRESSIONEXCEPTION

#include "../Exception.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class ExpressionException
              \brief This exception is thrown if the macro \ref ARMARX_CHECK_EXPRESSION
              is used.
              \ingroup Exceptions
             */
            class ExpressionException: public armarx::LocalException
            {
                std::string expression;
            public:
                ExpressionException(std::string expression) throw() :
                    armarx::LocalException("The following expression is not true, but needs to be: '" + expression + "'"),
                    expression(expression)
                { }

                ~ExpressionException() throw()
                { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::ExpressionException";
                }
                std::string getExpression() const
                {
                    return expression;
                }
            };
        }
    }
}
/**
\ingroup Exceptions
\brief This macro is evaluates the expression and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_EXPRESSION(expression) do { \
        if( !(expression) ) \
            throw armarx::exceptions::local::ExpressionException(#expression); \
    } while(0);

/**
\ingroup Exceptions
\brief This macro is evaluates the expression and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_EXPRESSION_W_HINT(expression, hint) do { \
        if( !(expression) ) \
            throw armarx::exceptions::local::ExpressionException(#expression) << " Error hint: " << hint; \
    } while(0);


/**
\ingroup Exceptions
\brief This macro is evaluates the expression and if it turns out to be
false it will throw an exception of the given type.
  **/
#define ARMARX_CHECK_AND_THROW(expression, ExceptionType) do { \
        if( !(expression) ) \
            throw ExceptionType(#expression); \
    } while(0);

#undef eigen_assert
#define eigen_assert(expression) ARMARX_CHECK_EXPRESSION(expression)


#endif
