/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_DYNAMICLIBRARYEXCEPTION_H
#define _ARMARX_DYNAMICLIBRARYEXCEPTION_H

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>


namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class DynamicLibraryException
              \brief This exception is thrown if an invalid value was specified for a property.
              \ingroup Exceptions
             */
            class DynamicLibraryException: public armarx::LocalException
            {
            public:
                std::string propertyName;
                std::string invalidPropertyValue;

                DynamicLibraryException(std::string reason) :
                    armarx::LocalException("Error in DynamicLibrary: " + reason)
                {}

                ~DynamicLibraryException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::DynamicLibraryException";
                }
            };
        }
    }
}
#endif
