/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_PROPERTYINHERITANCECYCLEEXCEPTION_H_
#define _ARMARX_PROPERTYINHERITANCECYCLEEXCEPTION_H_

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>
#include <vector>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class PropertyInheritanceCycleException
              \brief This exception is thrown if a property inheritance cycle has been encountered.
              \ingroup Exceptions
             */
            class PropertyInheritanceCycleException:
                public armarx::LocalException
            {
            public:
                PropertyInheritanceCycleException(const std::vector<std::string>& cycle) :
                    armarx::LocalException()
                {
                    std::stringstream sstream;
                    std::vector<std::string>::const_iterator cycleIter = cycle.begin();

                    sstream << "Property inheritance cycle encountered: " << std::endl;

                    while (cycleIter != cycle.end())
                    {
                        if (cycleIter != cycle.begin())
                        {
                            sstream << " --> ";
                        }
                        else
                        {
                            sstream << "     ";
                        }

                        sstream << *cycleIter << std::endl;

                        ++cycleIter;
                    }

                    setReason(sstream.str());
                }

                ~PropertyInheritanceCycleException() throw()
                { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::PropertyInheritanceCycleException";
                }
            };
        }
    }
}

#endif
