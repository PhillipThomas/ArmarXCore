/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_INVALIDPROPERTYVALUEEXCEPTION_H_
#define _ARMARX_INVALIDPROPERTYVALUEEXCEPTION_H_

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class InvalidPropertyValueException
              \brief This exception is thrown if an invalid value was specified for a property.
              \ingroup Exceptions
             */
            class InvalidPropertyValueException: public armarx::LocalException
            {
            public:
                std::string propertyName;
                std::string invalidPropertyValue;

                InvalidPropertyValueException(std::string propertyName, std::string invalidPropertyValue) :
                    armarx::LocalException(" Invalid value '" + invalidPropertyValue + "'" + " for property <" + propertyName + ">"),
                    propertyName(propertyName), invalidPropertyValue(invalidPropertyValue)
                { }

                ~InvalidPropertyValueException() throw()
                { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::InvalidPropertyValueException";
                }
            };
        }
    }
}

#endif
