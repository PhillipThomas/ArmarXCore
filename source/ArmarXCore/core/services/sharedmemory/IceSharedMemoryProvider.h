/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ICE_SHARED_MEMORY_PROVIDER_H_
#define _ARMARX_ICE_SHARED_MEMORY_PROVIDER_H_

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/services/sharedmemory/SharedMemoryProvider.h>
#include <ArmarXCore/core/services/sharedmemory/HardwareIdentifierProvider.h>
#include <ArmarXCore/interface/core/SharedMemory.h>
#include <ArmarXCore/core/IceGridAdmin.h>


// exceptions
#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>

namespace armarx
{
    /**
    * \class IceSharedMemoryProvider
    * \ingroup SharedMemory
    * The IceSharedMemoryProvider provides data via Ice or shared memory. Whether Ice or shared memory is used for data
    * transfer is determined based on the hardware id of the machine. The provider should be constructed in the onInit method
    * and started in the onStart method. <br/>
    * Use the getScopedWriteLock() or lock() / unlock() methods before writing to the memory.
    */
    template <class MemoryObject, class MemoryObjectMetaInfo = MetaInfoSizeBase>
    class IceSharedMemoryProvider :
        virtual public HardwareIdentifierProvider,
        virtual public SharedMemoryProviderInterface
    {
    public:
        /**
         * Creates an ice shared memory provider which transparentely communicates using shared memory
         * on local machines and using Ice on remote machines.
         *
         * @param component pointer to component
         * @param numberElements number of elements of type MemoryObject stored in the memory
         *
         * @throw SharedMemoryException
         */
        IceSharedMemoryProvider(ManagedIceObject* object, int size, int capacity = 0, std::string nameSuffix = "")
        {

            memoryName = object->getName() + "Memory" + nameSuffix;

            this->object = object;

            typename MemoryObjectMetaInfo::PointerType info = new MemoryObjectMetaInfo();
            info->size = size;

            if (capacity > size)
            {
                info->capacity = capacity;
            }
            else
            {
                info->capacity = size;
            }

            // init shared memory
            initSharedMemory(object, info);
        }


        IceSharedMemoryProvider(ManagedIceObject* object, typename MemoryObjectMetaInfo::PointerType info, std::string nameSuffix = "")
        {
            memoryName = object->getName() + "Memory" + nameSuffix;

            this->object = object;

            // init shared memory

            initSharedMemory(object, info);
        }


        /**
         * Starts the memory provider. The component needs to be connected, otherwise  method will fail.
         *
         * @throw SharedMemoryException
         */
        void start()
        {
            if (object->getState() < eManagedIceObjectStarting)
            {
                throw exceptions::local::SharedMemoryException(memoryName, "Started IceSharedMemoryProvider before component is in connected state.");
            }

            initIce(object);
        }

        /**
         * Retrieve pointer to buffer. This buffer should only be used for writing.
         * Before writing do not forget to lock the provider using either lock() and unlock()
         * or getScopedLock()
         *
         * @return pointer to the shared memory segment
         */
        MemoryObject* getBuffer()
        {
            return sharedMemoryProvider->getMemory();
        }



        /**
         * Retrieve scoped lock for writing to the memory.
         *
         * @return the scoped lock
         */
        SharedMemoryScopedWriteLockPtr getScopedWriteLock()
        {
            return sharedMemoryProvider->getScopedWriteLock();
        }

        /**
         * lock memory for writing
         */
        void lock()
        {
            sharedMemoryProvider->lock();
        }

        /**
         * unlock memory after writing
         */
        void unlock()
        {
            sharedMemoryProvider->unlock();
        }

        /**
        * return data via ice.
        */
        Blob getData(MetaInfoSizeBasePtr& info, const Ice::Current& c = ::Ice::Current())
        {
            // todo downgrade to ReadLock
            SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();

            info = new MemoryObjectMetaInfo(*sharedMemoryProvider->getMetaInfo());

            unsigned char* dst = &buffer[0];

            memcpy(dst, getBuffer(), info->size);

            return buffer;
        }

        Blob getData()
        {
            // todo downgrade to ReadLock
            SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();

            unsigned char* dst = &buffer[0];

            memcpy(dst, getBuffer(), sharedMemoryProvider->getMetaInfo()->size);

            return buffer;
        }


        void setMetaInfo(const typename MemoryObjectMetaInfo::PointerType& info)
        {
            sharedMemoryProvider->setMetaInfo(info);

            buffer.resize(info->size);
        }

        /**
         * @brief getMetaInfo returns a copy of the memory object information
         */
        MemoryObjectMetaInfo* getMetaInfo()
        {
            return new MemoryObjectMetaInfo(*sharedMemoryProvider->getMetaInfo());
        }


        /**
        * return memory size via ice.
        */
        int getSize(const Ice::Current& c = ::Ice::Current())
        {
            return sharedMemoryProvider->getSize();
        }


        /**
        * pointer type for convenience.
        */
        typedef IceUtil::Handle<IceSharedMemoryProvider<MemoryObject, MemoryObjectMetaInfo> > pointer_type;

    private:
        // register shared memory object to ice and init ice buffer
        void initIce(ManagedIceObject* object)
        {
            Ice::ObjectAdapterPtr memoryAdapter;
            Ice::ObjectPrx thisProxy = object->getIceManager()->getIceGridSession()->registerObjectWithNewAdapter(this, memoryName, memoryAdapter);
            buffer.resize(sharedMemoryProvider->getSize());
        }

        // register shared memory provider shared memory and init shared memory buffer
        void initSharedMemory(ManagedIceObject* object, typename MemoryObjectMetaInfo::PointerType info)
        {
            // might throw
            sharedMemoryProvider.reset(new SharedMemoryProvider<MemoryObject, MemoryObjectMetaInfo>(memoryName, info));
        }

        std::string memoryName;

        // data
        Blob buffer;

        // component
        ManagedIceObject* object;

        // pointer type
        typename SharedMemoryProvider<MemoryObject, MemoryObjectMetaInfo>::pointer_type sharedMemoryProvider;
    };
}

#endif
