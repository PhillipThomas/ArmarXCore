#ifndef ARMARX_PROFILER_H
#define ARMARX_PROFILER_H

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <IceUtil/Time.h>

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <string>
#include <map>

namespace armarx
{
    namespace Profiler
    {
        class Profiler;
        typedef boost::shared_ptr<Profiler> ProfilerPtr;

        class LoggingStrategy;
        typedef boost::shared_ptr<LoggingStrategy> LoggingStrategyPtr;

        /**
         * \page armarx-profiling-doc ArmarX Profiling
         *
         * \section armarx-profiling-overview ArmarX Profiling Overview
         *
         * ArmarXCore provides a basic armarx::Profiler::Profiler facility for profiling applications.
         *
         * The API documentation can be found here: \ref Profiling
         *
         * The armarx::Profiler::Profiler can be activated during application startup via the following commandline parameters
         *
         * \li ArmarX.NetworkStats : sends out information about amount of data sent and received by the enabled application
         * \li ArmarX.EnableProfiling : sends out information about CPU utilization of the enabled application
         * \li ArmarX.$COMPONENT_NAME.EnableProfiling : sends out profiling events and state transitions (in case of statecharts) of the enabled application
         *
         * To display the profiled data via ArmarXGui, the armarx::ProfilerObserver component can be executed and its output visualized using the Plotter gui.
         *
         * \subsection armarx-profiling-api ArmarX Profiling API
         *
         * The profiler is built into each armarx::ManagedIceObject and can be accessed via armarx::ManagedIceObject::getProfiler().
         * armarx::Component instances provide an "EnableProfiling" property which activates the profiler via armarx::ManagedIceObject::enableProfiler().
         *
         * After obtaining an instance of armarx::Profiler::Profiler the following methods can be called
         *
         * \li armarx::Profiler::Profiler::logEvent()
         * \li armarx::Profiler::Profiler::logStatechartTransition() (this is automatically called in any statechart if the property "EnableProfiling" and "ProfilingDepth" are set)
         *
         * \defgroup Profiling
         * \ingroup core-utility
         */


        /**
         * \class Profiler
         * \ingroup Profiling
         * \brief The armarx::Profiler::Profiler class can be used for timing executions within the ArmarX framework
         *
         * The armarx::Profiler::Profiler class provides methods for logging events and statechart transitions.
         * Changing the logging behavior requires calling armarx::Profiler::Profiler::setLoggingStrategy() with a specialized implementation
         * of armarx::Profiler::LoggingStrategy.
         * By default, all calls to log functions are ignored.
         */
        class Profiler
        {
        public:
            /**
             * @brief The EventType enum provides symbolic names for the different events which can be logged via armarx::Profiler::Profiler::logEvent()
             */
            enum EventType
            {
                eFunctionStart = 0,
                eFunctionReturn,
                eFunctionBreak,
                eNumberEventTypes
            };

            typedef std::map<Profiler::EventType, std::string> EventTypeMap;

            /**
             * @brief getEventName maps enum values from armarx::Profiler::Profiler::EventType to strings
             * @param eventType
             * @return string representation of the enum value
             */
            static std::string GetEventName(Profiler::EventType eventType);

            Profiler();

            virtual ~Profiler();

            void setName(const std::string& profilerName);

            void setLoggingStrategy(LoggingStrategyPtr loggingStrategy);

            virtual void logProcessCpuUsage(const float cpuUsage);
            virtual void logEvent(Profiler::EventType eventType, const std::string& parentName, const std::string& functionName);
            virtual void logStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName);

            /**
             * @brief reset reinitializes armarx::Profiler::Profiler::startTime with the current time.
             */
            void reset();
        protected:
            std::string profilerName;
        private:
            /**
             * @brief GetEventTypeMap ensures that elements are only put once into armarx::Profiler::Profiler::evenTypeNameMap
             * @return reference to armarx::Profiler::Profiler::evenTypeNameMap
             */
            static EventTypeMap& GetEventTypeMap();

            /**
             * @brief logger holds an armarx::Profiler::LoggingStrategy subclass that defines what armarx::Profiler::Profiler should do with the collected data
             */
            LoggingStrategyPtr logger;

            /**
             * @brief loggerMutex is used to prevent race conditions when armarx::Profiler::Profiler::logger is exchanged.
             */
            boost::mutex loggerMutex;
            /**
             * @brief startTime is initialized with the current time during armarx::Profiler::Profiler creation and defines the zero point from which timestamps are meassured.
             */
            IceUtil::Time startTime;

            /**
             * @brief evenTypeNameMap stores a mapping between armarx::Profiler::Profiler::EventType enums and human readable names.
             */
            static EventTypeMap evenTypeNameMap;
        };
    }
}

#endif
