/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::RobotAPI
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @copyright  2014
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_EXAMPLEUNIT_SLICE_
#define _ARMARX_CORE_EXAMPLEUNIT_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

/*
 * The ExampleUnit is a unit that is only implemented for documentation
 * purposes. It is designed to be a minimum working example and has no real use.
 */
module armarx
{
    interface ExampleUnitInterface
    {
        void setPeriodicValue(VariantBase value);
    };

    interface ExampleUnitListener
    {
        void reportPeriodicValue(VariantBase value);
    };
};

#endif
