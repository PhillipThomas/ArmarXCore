/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Observers
* @author     Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARX_CORE_SERIALIZER_SLICE
#define ARMARX_CORE_SERIALIZER_SLICE

#include <ArmarXCore/interface/core/UserException.ice>

module armarx
{

	/*
	  * Basic serializer/deserializer which has following functions:
	  * 
	  * 1. Convert object to string representation
	  * 2. Load object from string representation
	  * 3. Access object fields (get/set)
	  * 
	  * Specific implementation may use e.g. JSON or XML as output format
	  */	
	class ObjectSerializerBase
	{
		["cpp:const"]
		string toString();
		void fromString(string objectString);
	};
	
	/*
	  * Interface to be implemented by all entities which
	  * can be serialized
	  * 
	  */	
	interface Serializable 
	{
		["cpp:const"]
		void serialize(ObjectSerializerBase serializer);
		void deserialize(ObjectSerializerBase serializer);
	};
	
};
	
#endif
