/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Mirko Waechter <waechter@kit.edu>
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_OBSERVERS_VARIANTCONTAINERS_SLICE_
#define _ARMARX_OBSERVERS_VARIANTCONTAINERS_SLICE_

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/Serialization.ice>
#include <Ice/Communicator.ice>

module armarx
{
    exception KeyAlreadyExistsException extends UserException
    {
    };

    exception UnknownTypeException extends UserException
    {
        int typeId;
    };

    exception UnsupportedTypeException extends UserException
    {
    };

    class ContainerType{
        ContainerType subType;
        ["cpp:const"]
        idempotent ContainerType clone();
        string typeId;
    };


    class VariantContainerBase implements Serializable
    {

        ["cpp:const"]
        idempotent int getSize();
        ["cpp:const"]
        idempotent ContainerType getContainerType();
        void setContainerType(ContainerType typeContainer);

        void clear();
        ["cpp:const"]
        idempotent VariantContainerBase cloneContainer();
        idempotent bool validateElements();

        string toString();

        /**
         * @brief Function to read a paramater from a XML-string
         * @param xmlData string with XML--Data, <b>not</b>  a path
         * @return ErrorCode, 1 on success
         */
        int readFromXML(string xmlData);

//        /**
//         * @brief writeAsXML returns the xml-representation of this Variant in
//         * a string.
//         *
//         */
//        string writeAsXML();

        ["protected"]
        ContainerType typeContainer;
    };


    dictionary<string, VariantContainerBase> StringVariantContainerBaseMap;
    sequence<VariantContainerBase> VariantContainerBaseList;


    ["cpp:virtual"]
    class SingleTypeVariantListBase extends VariantContainerBase
    {
        void addElement(VariantContainerBase variantContainer) throws InvalidTypeException;

        ["cpp:const"]
        idempotent VariantContainerBase getElementBase(int index) throws IndexOutOfBoundsException;

        ["protected"]
        VariantContainerBaseList elements;

    };

    ["cpp:virtual"]
    class StringValueMapBase extends VariantContainerBase
    {
        void addElement(string key, VariantContainerBase variantContainer) throws InvalidTypeException;
        void setElement(string key, VariantContainerBase variantContainer) throws InvalidTypeException, KeyAlreadyExistsException;

        ["cpp:const"]
        idempotent VariantContainerBase getElementBase(string key) throws IndexOutOfBoundsException;

        ["protected"]
        StringVariantContainerBaseMap elements;

    };
};

#endif
