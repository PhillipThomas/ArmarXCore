/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_CONDITION_BASE_SLICE_
#define _ARMARX_CORE_CONDITION_BASE_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.ice>
#include <ArmarXCore/interface/observers/Event.ice>

module armarx
{
    /**
     * Interfaces for conditions
     */
    sequence<VariantBase> ParameterList;
	
    class LiteralImplBase;
	struct CheckConfiguration
	{
        string checkName;
        ParameterList checkParameters;
        DataFieldIdentifierBase dataFieldIdentifier;
        LiteralImplBase* listener;
        bool reportAlways = false;
	};
    
    enum TermType
    {
        eUndefinedTerm,
        eConditionRoot,
        eOperation,
        eLiteral
    };

    enum OperationType
    {
        eUndefinedOperation,
        eOperationAnd,
        eOperationOr,
        eOperationNot
    };
    
	/**
     * Superclass for all terms in the expression tree
     * holds the current state (value) and the type of the term
     * (either eConditionRoot or eOperation or eLiteral)
     */
    class TermImplBase;
    
    sequence<TermImplBase> TermImplSequence;
    class TermImplBase
    {
        void setParent(TermImplBase parent);
    	TermImplBase getParent();
    	void resetParent();
        void addChild(TermImplBase child);
        TermImplSequence getChilds();

        void update();

    	["cpp:const"]    
    	idempotent bool getValue();
    	["cpp:const"]    
    	idempotent TermType getType();
    	
    	["protected"] bool value = false;
    	["protected"] TermType type;
        ["protected"] TermImplBase parent;
        ["protected"] TermImplSequence childs;
    };
    
	/**
     * A literal is a leaf in the expression tree. Each literal
     * represents a condition check and therefore contains
     * its configuration.
     */
    ["cpp:virtual"]
    class LiteralImplBase extends TermImplBase
    {  
    	void setValue(bool value);      
        CheckConfiguration getCheckConfiguration();
        
        CheckConfiguration checkConfig;
    };
    
    ["cpp:virtual"]
    class OperationBase extends TermImplBase
    {
    	["cpp:const"]    
        OperationType getOperationType();
        string getOperationString();
        
        ["protected"] OperationType opType;
    };
    
	/**
     * The and operation as used in the expression tree
     */
    ["cpp:virtual"]
    class OperationAndBase extends OperationBase
    {  
    };

	/**
     * The or operation as used in the expression tree
     */
    ["cpp:virtual"]
    class OperationOrBase extends OperationBase
    {  
    };

	/**
     * The not operation as used in the expression tree
     */
    ["cpp:virtual"]
    class OperationNotBase extends OperationBase
    {  
    };
    

	/**
     * A ConditionRoot is the root node of the expression tree.
     * It is only used by the ConditionHandler in order to 
     * generate an event in the case the complete expression
     * represented by the tree is true.
     */
    ["cpp:virtual"]
    class ConditionRootBase extends TermImplBase
    {
    	EventListenerInterface* listener;
    	EventBase event;
        string description;
    	bool fired = false;
        bool onlyFireOnce = true;
    };


    exception InvalidConditionException extends UserException
    {
    };
    
    struct ConditionIdentifier
    {
    	int uniqueId;
    };
};

#endif
