/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_PARAMETER_SLICE_
#define _ARMARX_CORE_PARAMETER_SLICE_

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/VariantContainers.ice>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.ice>

module armarx
{
  sequence<VariantBase> VariantBaseSequence;

  enum ParameterType
  {
	eInvalidParam,
	eVariantParam,
	eVariantListParam,
	eDataFieldIdentifierParam
  };
  
  class ParameterBase
  {
  	void setVariant(VariantBase variant) throws InvalidTypeException;
  	void setVariantList(SingleTypeVariantListBase variantList) throws InvalidTypeException;
  	void setDataFieldIdentifier(DataFieldIdentifierBase dataFieldIdentifier) throws InvalidTypeException;


    ["cpp:const"]
    idempotent ParameterBase clone();

    ["cpp:const"]
    idempotent ParameterType getParameterType() throws InvalidTypeException;
    ["cpp:const"]
    idempotent int getVariantType() throws InvalidTypeException;
    ["cpp:const"]
  	idempotent VariantBase getVariant() throws InvalidTypeException;
  	["cpp:const"]    
  	idempotent SingleTypeVariantListBase getVariantList() throws InvalidTypeException;
  	["cpp:const"]    
  	idempotent DataFieldIdentifierBase getDataFieldIdentifier() throws InvalidTypeException;
  	
    ["cpp:const"]
    idempotent bool validate();


  	["protected"]
  	ParameterType type = eInvalidParam;
  };
  
  dictionary<string, ParameterBase> StringParameterBaseMap;

  ["cpp:virtual"]
  class VariantParameterBase extends ParameterBase
  {
  	["protected"]
  	VariantBase variant;
  };

  ["cpp:virtual"]
  class VariantListParameterBase extends ParameterBase
  {
  	["protected"]
  	SingleTypeVariantListBase variantList;
  };

  ["cpp:virtual"]
  class DataFieldIdentifierParameterBase extends ParameterBase
  {
  	["protected"]
  	DataFieldIdentifierBase dataFieldIdentifier;
  };
};

#endif
