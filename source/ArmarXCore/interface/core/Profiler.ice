/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Manfred Kroehnert <manfred dot kroehnert at kit dot edu>
* @copyright  2015
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_PROFILER_SLICE
#define _ARMARX_CORE_PROFILER_SLICE

module armarx
{
    module Profiler
    {
        const string PROFILER_TOPIC_NAME = "ArmarXProfilerListenerTopic";
    };

    struct ProfilerEvent
    {
        int processId;
        string executableName;
        long timestamp;
        string timestampUnit;
        string eventName;
        string parentName;
        string functionName;
    };

    sequence<ProfilerEvent> ProfilerEventList;

    struct ProfilerTransition
    {
        string parentStateName;
        string sourceStateName;
        string targetStateName;
    };

    sequence<ProfilerTransition> ProfilerTransitionList;

    struct ProfilerProcessCpuUsage
    {
        int processId;
        string processName;
        long timestamp;
        float cpuUsage;
    };

    sequence<ProfilerProcessCpuUsage> ProfilerProcessCpuUsageList;

    interface ProfilerListener
    {
        void reportNetworkTraffic(string id, string protocol, int inBytes, int outBytes);
        void reportEvent(ProfilerEvent e);
        void reportStatechartTransition(string parentStateName, string sourceStateName, string targetStateName);
        void reportProcessCpuUsage(ProfilerProcessCpuUsage process);

        void reportEventList(ProfilerEventList events);
        void reportStatechartTransitionList(ProfilerTransitionList transitions);
        void reportProcessCpuUsageList(ProfilerProcessCpuUsageList processes);
    };
};

#endif
