/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke at kit dot edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_SHAREDMEMORY_INTERFACE_SLICE
#define _ARMARX_CORE_SHAREDMEMORY_INTERFACE_SLICE

module armarx
{
	sequence<byte> Blob;

    class MetaInfoSizeBase
    {
        int size;
        int capacity;
    };
    
    enum TransferMode
    {
		eSharedMem,
		eIce
    };

	// classes that are identifiable via hardware id
    interface HardwareIdentifierProviderInterface
    {
		string getHardwareId();
    };
    
	// provides data via ice or shared memory
    interface SharedMemoryProviderInterface extends HardwareIdentifierProviderInterface
    {
        Blob getData(out MetaInfoSizeBase info);
		int getSize();
    };

	// consumes data via ice or shared memory
    interface SharedMemoryConsumerInterface extends HardwareIdentifierProviderInterface
    {
    };
};

#endif
