/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_BASIC_TYPES_SLICE_
#define _ARMARX_CORE_BASIC_TYPES_SLICE_

#include <Ice/BuiltinSequences.ice>

module armarx
{
    struct FloatRange
    {
         float min;
         float max;
    };

    struct IntRange
    {
         int min;
         int max;
    };

    //Two dimensional lists
    sequence<Ice::StringSeq> StringSeqList;

    ///TODO replace all occurrences with basic ice sequences from Ice/BuiltinSequences.ice
    sequence<string> StringList; //130 occurrences
    //sequences for all built-in basic types
    sequence<float> FloatSequence;//131 occurrences
    sequence<double> DoubleSequence;//2 occurrences
    sequence<string> StringSequence;//49 occurrences

    dictionary<string,string> StringStringDictionary;
};

#endif
