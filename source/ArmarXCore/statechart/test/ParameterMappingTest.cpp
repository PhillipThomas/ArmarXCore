/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE ArmarX::Statechart::ParameterMappingTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>

using namespace armarx;

//BOOST_AUTO_TEST_CASE(testHasMappingEntry1)
//{
//    Mapping mapping;
//    PMPtr pm = PM::createMapping();
//    pm->mapFromParent("State2.angle", "State1.angle");


//    mapping.insert(std::pair<std::string, std::string>("State2.angle", "State1.angle"));
//    mapping.insert(std::pair<std::string, std::string>("State2.head.*", "State1.head.*"));


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string keyDestination = "State2.angle";
//    it =  PM::hasMappingEntry(mapping, keyDestination, mapSource);
//    BOOST_CHECK(it != mapSource.end());





//}

//BOOST_AUTO_TEST_CASE(testHasMappingEntry2)
//{
//  Mapping mapping;
//  mapping.insert(std::pair<std::string, std::string>("State2.angle", "State1.angle"));
//  mapping.insert(std::pair<std::string, std::string>("State2.head.*", "State1.head.*"));


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.direction";
//    it =  PM::hasMappingEntry(mapping, keyDestination, mapSource);
//    BOOST_CHECK(it == mapSource.end());
//}


//BOOST_AUTO_TEST_CASE(testHasMappingEntry3)
//{
//  Mapping mapping;
//  mapping.insert(std::pair<std::string, std::string>("State2.angle", "State1.angle"));
//  mapping.insert(std::pair<std::string, std::string>("State2.head.*", "State1.head.*"));


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.angle";
//    it =  PM::hasMappingEntry(mapping, keyDestination, mapSource);
//    BOOST_CHECK(it != mapSource.end());
//}

//BOOST_AUTO_TEST_CASE(testHasMappingEntry4)
//{
//    ParameterMappingPtr mapping = createMapping();
//    mapping->addTuple("State1.head.in.*", "State2.head.in.*");


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.in.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.in.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.in.angle";
//    it =  PM::hasMappingEntry(mapping->paramMapping, keyDestination, mapSource);
//    BOOST_CHECK(it != mapSource.end());
//}


//BOOST_AUTO_TEST_CASE(testApplyMapping)
//{
//    ParameterMappingPtr mapping = createMapping();
//    mapping->addTuple("*", "State2.in.*");


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("angle",new Variant(5.5f)));
////    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.in.angle",new Variant(5.5f)));

//    StateParameterMap mapDest;
//    StateParameter param;
//    param.value = new Variant(1.5f);
//    param.defaultValue = new Variant(1.5f);
//    param.optionalParam = false;
//    param.set = false;
//    mapDest.insert(std::pair<std::string, StateParameter>("State2.in.angle",param));

//    mapping->applyMapping(&mapSource, NULL, "", NULL, mapDest, false);
//    BOOST_CHECK(mapDest.size()>0);
//}

//BOOST_AUTO_TEST_CASE(testaddTuple)
//{
//    ParameterMappingPtr mapping = createMapping();
//    BOOST_CHECK_NO_THROW(
//                mapping->addTuple( "State2.head.*", "State1.head.*")

//                );
//    BOOST_CHECK_THROW(
//                mapping->addTuple("State2.head.*", "State1.head.direction"), armarx::LocalException
//                );
//}
