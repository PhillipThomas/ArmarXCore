/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE ArmarX::Statechart::StatechartTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

using namespace armarx;


// Define Events before the first state they are used in
DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
DEFINEEVENT(EvNext)

struct StateResult;
struct StateRun;
struct Statechart_StateRunFunctionTest : StateTemplate<Statechart_StateRunFunctionTest>
{



    void defineParameters()
    {
        addToLocal("quit", VariantType::Bool);
    }

    void defineSubstates()
    {
        //add substates

        StatePtr stateRun = addState<StateRun>("Running");
        setInitState(stateRun, createMapping()->mapFromParent("*"));
        StatePtr stateResult = addState<StateResult>("Result");
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalStateBase, that sends triggers a transition on the upper state



        // add transitions
        addTransition<EvNext>(stateRun, stateResult);
        addTransition<EvNext>(stateResult, stateRun, createMapping()->mapFromEvent("*"));
        addTransition<Success>(stateRun, stateSuccess);


        // ...add more transitions
    }
    void onEnter()
    {
        setLocal("quit", false);
    }


};

struct StateRun : StateTemplate<StateRun>
{

    void defineParameters()
    {
        addToInput("quit", VariantType::Bool, true);
    }

    void run()
    {
        if (getInput<bool>("quit"))
        {
            sendEvent<Success>();
        }
        else
        {
            sendEvent<EvNext>();
        }
    }

};

struct StateResult : StateTemplate<StateResult>
{
    void run()
    {
        EventPtr event = createEvent<EvNext>();
        event->add("quit", true);
        sendEvent(event);
    }

};

BOOST_AUTO_TEST_CASE(StatechartRunFunctionTest)
{
    {
        StatePtr statechart;
        StatechartManager manager;
        //    try{
        statechart = Statechart_StateRunFunctionTest::createInstance();
        statechart->init(NULL, &manager);

        BOOST_CHECK(statechart->isInitialized());
        manager.setToplevelState(statechart);
        manager.start();
        statechart->waitForStateToFinish();
        //    }
        //    catch(...){
        //        handleExceptions();
        //    }
    }
    std::cout << "test DONE" << std::endl;
    BOOST_CHECK(true);
}




