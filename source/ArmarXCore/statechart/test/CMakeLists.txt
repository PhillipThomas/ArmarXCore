
SET(LIBS ${LIBS} ArmarXCore ArmarXCoreStatechart)
#armarx_add_test(ParameterMappingTest ParameterMappingTest.cpp "${LIBS}")
armarx_add_test(ParameterLoadingTest ParameterLoadingTest.cpp "${LIBS}")
armarx_add_test(StatechartTest StatechartTest.cpp "${LIBS}")
armarx_add_test(StatechartIceTest StatechartIceTest.cpp "${LIBS}")
armarx_add_test(StatechartUnbreakableStateTest StatechartUnbreakableStateTest.cpp "${LIBS}")

armarx_add_test(StatechartRunFunctionTest StatechartRunFunctionTest.cpp "${LIBS}")
armarx_add_test(XMLStatechartTest XMLStatechartTest.cpp "${LIBS}")
