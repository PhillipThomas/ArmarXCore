/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef INPUTPARAMETERLOADER_H
#define INPUTPARAMETERLOADER_H

#include "ParameterMapping.h"

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/filesystem.hpp>

// c++ includes
#include <sstream>
#include <string>



namespace armarx
{

    /**
     * @brief The InputParameterLoader class creates an interface to load
     * state config files (in XML-format) in to state parameter dictionaries.
     *
     * There can be configs for several states in one config file. Their<s parameters
     * just need to be in a xml-node, that is named like the state.<br/>
     * In this node there need to be a node named 'StateParameters' and in this
     * node the state parameters need to be located.<br/>
     * The type of the parameters must fit to the parameters of the map.
     * Examplefile:
     *@code
    <NameOfYourState>
    <StateParameters>
    <timeout>2</timeout>
    <x>1</x>
    <y>2</y>
    <Position>
      <x>1</x>
      <y>2</y>
      <z>1</z>
      <frame>testframe</frame>
    </Position>
    <ObjectList>
      <Item1>Blue Cup</Item1>
      <Item2>Vitalis Cereals</Item2>
    </ObjectList>
    <ObjectPositionList>
      <Item0>
          <x>1</x>
          <y>2</y>
          <z>1</z>
          <frame>testframe</frame>
      </Item0>
      <Item1>
          <x>1</x>
          <y>2</y>
          <z>1</z>
          <frame>testframe2</frame>
      </Item1>
    </ObjectList>
    </StateParameters>
    </NameOfYourState>
    @endcode
     *
     */
    class InputParameterLoader :
        virtual public Logging
    {

    public:
        InputParameterLoader();
        InputParameterLoader(const InputParameterLoader& source);
        void setIceCommunicator(Ice::CommunicatorPtr ic);
        /**
         * @brief Loads a XML-configfile and stores the fitting data into the given StateParamterMap
         * @param configFileName path to the XML-configfile
         * @param stateName state of the name for which the function should look in the configfile
         * @param map reference to the map, in which the data should be set
         */
        void load(const boost::filesystem::path& configFile, const std::string stateName, StateParameterMap& map);
        void load(const std::string& xmlContent, const std::string stateName, StateParameterMap& map);
        void load(const boost::property_tree::ptree& pt, const std::string stateName, StateParameterMap& map);
        void apply(StateParameterMap& map);

    protected:
        void _createEmptyXMLConfigFile(const boost::filesystem::path& configFile, const std::string& stateName, const StateParameterMap& map);

    private:
        template <class Type>
        void addElementstoList(SingleTypeVariantListBasePtr& list, const boost::property_tree::ptree& tree, const std::string& xml_key)
        {
            std::stringstream sstr;
            int i = 0;
            sstr << xml_key << ".Item" << i;
            boost::optional<Type> value;

            while ((value = tree.get_optional<Type>(sstr.str())), value.is_initialized())
            {
                list->addElement(new SingleVariant(value.get()));
                i++;
                sstr.seekp(0);
                sstr << xml_key << ".Item" << i;
            }
        }

        template <class Type>
        void addEmptyElementstoPTree(const Variant& element, boost::property_tree::ptree& tree, const std::string& xml_key)
        {
            std::stringstream sstr;
            int i = 0;
            sstr << xml_key << ".Item" << i;
            Type t;
            tree.add<Type>(sstr.str(), t);
        }


        StringVariantContainerBaseMap paramBufferMap;
        Ice::CommunicatorPtr ic;
    };
}
#endif // INPUTPARAMETERLOADER_H
