/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_REMOTESTATEWRAPPER_H
#define _ARMARX_REMOTESTATEWRAPPER_H


// Slice Includes
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>

// ArmarX Includes
#include <ArmarXCore/core/Component.h>

// Statechart Includes
#include "State.h"
#include "RemoteState.h"

namespace armarx
{

    class RemoteStateWrapper;
    typedef IceInternal::Handle<RemoteStateWrapper> RemoteStateWrapperPtr;

    /**
      \class RemoteStateWrapper
      \ingroup StatechartGrp
      This class functions as a pseudo parent state. Every instance of a
      remoteaccessable state has an instance of this class as parent.
      It overrides all the functions that a normal state can call on his
      parent state and forwards the call over Ice to the "real" parent
      state.

      @see RemoteState, DynamicRemoteState, RemoteStateOfferer
      */
    class RemoteStateWrapper:
        virtual public State
    {
    protected:
        RemoteStateIceBasePrx  callerStatePrx;
        StatePtr realState;
        RemoteStateOffererIceBasePtr handlerPtr;
        void __propagateUpdate(bool directionUp = true);

    public:

        RemoteStateWrapper(StatePtr realState, RemoteStateIceBasePrx  callerStatePrx, RemoteStateOffererIceBasePtr handlerPtr);

        RemoteStateWrapper(const RemoteStateWrapper& source);
        // inherited from StateBase
        void __processBufferedEvents();
        void __processEvent(const EventPtr event, bool buffered = false);
        void __enqueueEvent(const EventPtr event);
        virtual unsigned int __getUnbreakableBufferSize() const;
        bool __getUnbreakableBufferStati() const;
        void __finalize(const EventPtr event);
        StateBasePtr clone() const;
        StateBasePtr createEmptyCopy() const;
        template <typename ContextType>
        friend class RemoteStateOfferer;
    };
}

#endif
