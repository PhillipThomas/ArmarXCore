/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef _ARMARX_CORE_REMOTESTATEOFFERER_H
#define _ARMARX_CORE_REMOTESTATEOFFERER_H

// Slice Includes
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>

// ArmarX Includes
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>

// Statechart Includes
#include "State.h"
#include "RemoteState.h"
#include "RemoteStateWrapper.h"
#include "StatechartManager.h"


namespace armarx
{
    /**
      \class RemoteStateOfferer
      \brief Class that holds states, which offer functionality for other states over Ice.
      \ingroup StatechartGrp
      To offer such functionality one must derive from this class and implement
      the function armarx::RemoteStateOfferer::onInitRemoteStateOfferer() and add the states offering remote functionality there.<br/>
      For every incoming call to a state a copy of the base instance is created, so that concurrent calls dont interfere with each other.<br/>
      The implemented, remoteaccessable states must call setRemoteaccessable().<br/>
      An Example can be found in applications/StateChartExamples/RemoteAccessableStateExample.

      \tparam ContextType The template parameter must be a class that derives from
             StatechartContext

      \see StatechartContext, RemoteState, DynamicRemoteState, RemoteStateWrapper

      */
    template <typename ContextType = StatechartContext>
    class RemoteStateOfferer :
        virtual public RemoteStateOffererIceBase,
        virtual public State,
        virtual public ContextType
    {
    public:
        RemoteStateOfferer();

        //! \brief Implement this function to specify the RemoteStateOfferer prefix.
        //! \note Do not override getDefaultName()!
        virtual std::string getStateOffererName() const = 0;
        //! \brief Pure virtual function, in which the states must be added, that should be remote-accessable.
        virtual void onInitRemoteStateOfferer() = 0;
        //! \brief Virtual function, in which the user can fetch some proxies.
        virtual void onConnectRemoteStateOfferer() {}
        //! \brief Virtual function, in which the user can implement some clean up.
        virtual void onExitRemoteStateOfferer() {}


        // inherited from RemoteStateOffererInterface
        int createRemoteStateInstance(const ::std::string& stateName,
                                      const RemoteStateIceBasePrx& remoteStatePrx, const std::string& parentStateItentifierStr, const std::string& instanceName,
                                      const Ice::Current& context = ::Ice::Current());
        void callRemoteState(int stateId,
                             const StringVariantContainerBaseMap& properties, const Ice::Current& context = ::Ice::Current());
        void exitRemoteState(int stateId,
                             const ::Ice::Current& context = ::Ice::Current());
        bool breakRemoteState(int stateId,
                              const EventBasePtr& evt,
                              const ::Ice::Current& context = ::Ice::Current());
        bool breakActiveSubstateRemotely(int stateId,
                                         const EventBasePtr& evt,
                                         const ::Ice::Current& context = ::Ice::Current());
        void notifyEventBufferedDueToUnbreakableStateRemote(int stateId, bool eventBuffered, const ::Ice::Current& context = ::Ice::Current());
        StateIceBasePtr refetchRemoteSubstates(int stateId, const ::Ice::Current& context = ::Ice::Current());
        StateParameterMap getRemoteInputParameters(const std::string& stateName, const ::Ice::Current& context = ::Ice::Current());
        StateParameterMap getRemoteOutputParameters(const std::string& stateName, const ::Ice::Current& context = ::Ice::Current());
        StateParameterMap getRemoteInputParametersById(int stateId, const ::Ice::Current& context = ::Ice::Current());
        StateParameterMap getRemoteOutputParametersById(int stateId, const ::Ice::Current& context = ::Ice::Current());
        bool hasSubstatesRemote(const std::string& stateName, const ::Ice::Current& context = ::Ice::Current()) const;
        bool hasActiveSubstateRemote(int stateId, const ::Ice::Current& context = ::Ice::Current());
        StringList getAvailableStates(const ::Ice::Current& context = ::Ice::Current());
        StateIdNameMap getAvailableStateInstances(const ::Ice::Current& context = ::Ice::Current());
        StateIceBasePtr getStatechart(const std::string& stateName, const ::Ice::Current& context = ::Ice::Current());
        StateIceBasePtr getStatechartInstance(int stateId, const Ice::Current& = ::Ice::Current());
        StateIceBasePtr getStatechartInstanceByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current& = ::Ice::Current());
        bool registerForUpdates(const std::string& stateName, const StateListenerPrx& listener, const Ice::Current& = ::Ice::Current());
        void unregisterForUpdates(const StateListenerPrx& listener, const Ice::Current& = ::Ice::Current());
        void removeInstance(int stateId, const Ice::Current& = ::Ice::Current());
        void issueEvent(int receivingStateId, const EventBasePtr& evt, const Ice::Current& = ::Ice::Current());
        void issueEventWithGlobalIdStr(const std::string& globalStateIdStr, const EventBasePtr& evt, const Ice::Current& = ::Ice::Current());

    protected:
        /** \struct RemoteStateData
          A struct that holds meta data for a remoteaccessable state instances.
          */
        struct RemoteStateData
        {
            //! Local id of this RemoteStateOfferer, that identifies the state instance in the stateInstanceList
            int id;
            //! Not used yet.
            std::string callerIceName;
            //! Proxy to the state, that called this state
            RemoteStateIceBasePrx  callerStatePrx;
            //! Pointer to a Pseudo parent state, that contains the real state instance.
            RemoteStateWrapperPtr remoteWrappedState;

        };
        RemoteStateData getInstance(int stateId);
        StateBasePtr getGlobalInstancePtr(int globalId) const;
        virtual StateBasePtr getStatePtr(const std::string& stateName) const;
        std::map<int, StateBasePtr> getChildStatesByName(int parentId, std::string stateName);

    private:
        //TODO: It should be possible to force a state to be a single instance and all incoming calls are buffered
        BOOST_STATIC_ASSERT_MSG((boost::is_base_of<StatechartContext, ContextType>::value), "The template parameter of RemoteStateOfferer, must be a class that derives from StatechartContext or StatechartContext itself");


        std::string getDefaultName() const
        {
            return getStateOffererName() + "StateOfferer";
        }
        virtual void onInitStatechart();
        virtual void onConnectStatechart();
        virtual void onExitStatechart();
        void run() {}
        //! \brief Overridden so that the user cannot use it
        RemoteStatePtr addRemoteState(std::string stateName, std::string proxyName, std::string instanceName)
        {
            throw exceptions::local::eStatechartLogicError("You cannot add remoteStates directly to the RemoteStateOfferer. You can only add them to substates.");
        }
        //! \brief Overridden so that the user cannot use it
        RemoteStatePtr addDynamicRemoteState(std::string instanceName)
        {
            throw exceptions::local::eStatechartLogicError("You cannot add dynamicRemoteStates directly to the RemoteStateOfferer. You can only add them to substates.");
        }
        StateIceBasePtr getStatechartInstanceByGlobalIdStrRecursive(const std::string& globalStateIdStr, StateBasePtr state, int& stateCounter);
        /**
         * @brief notifySubscribers calls all registered components (with registerForUpdates()) and notifies them about the update
         * @param updatedState contains the current state
         */
        void notifySubscribers(const StateIceBasePtr& updatedState, const Ice::Current& context = ::Ice::Current());

        HiddenTimedMutex stateInstanceListMutex;
        //! Holds the instances that where requested from remotely located states
        std::map<int, RemoteStateData> stateInstanceList;
        std::string componentName;

        friend class RemoteStateWrapper;
    };
    //    typedef IceInternal::Handle<RemoteStateOfferer> RemoteStateOffererPtr;




    /////////////////////////////////////////////////////////////
    ////////////// Implementation
    /////////////////////////////////////////////////////////////

    template <typename ContextType>
    RemoteStateOfferer<ContextType>::RemoteStateOfferer()
    {
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::onInitStatechart()
    {
        stateName = getDefaultName();
        ContextType::setToplevelState(this);

        StatePhase oldPhase = getStatePhase();
        setStatePhase(eSubstatesDefinitions);

        try
        {
            onInitRemoteStateOfferer();
        }
        catch (...)
        {
            setStatePhase(oldPhase);
            Component::terminate();
            throw;
        }

        for (StateIceBasePtr state : subStateList)
        {
            StateControllerPtr stateController = StateControllerPtr::dynamicCast(state);

            if (stateController)
            {
                int numberLogLevels = Component::getProperty<int>("ProfilingDepth").getValue();
                stateController->setProfilerRecursive(Component::getProfiler(), numberLogLevels);
            }
        }

        ContextType::setAutoEnterToplevelState(false);
    }


    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::onConnectStatechart()
    {
        ARMARX_DEBUG << "Starting RemoteStateOfferer " << flush;
        onConnectRemoteStateOfferer();

    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::onExitStatechart()
    {
        onExitRemoteStateOfferer();

        for (size_t i = 0; i < subStateList.size(); i++)
        {
            StateControllerPtr c = StateControllerPtr::dynamicCast(subStateList.at(i));

            if (c)
            {
                c->disableRunFunction();
            }
        }

        subStateList.clear();

        for (size_t i = 0; i < stateInstanceList.size(); i++)
        {
            StateControllerPtr c = StateControllerPtr::dynamicCast(stateInstanceList.at(i).remoteWrappedState->realState);

            if (c)
            {
                c->disableRunFunction();
            }
        }

        stateInstanceList.clear();
        //        // remove all remote state instances
        //        std::queue<StateBasePtr> stateBuffer;
        //        StateBase* currentState = this;
        //        do{
        //            cout << "Testing " << currentState->stateName << endl;
        //            for(unsigned int i = 0; i < subStateList.size(); i++)
        //            {
        //                RemoteStatePtr remoteStatePtr = RemoteStatePtr::dynamicCast(subStateList.at(i));
        //                if(remoteStatePtr
        //                        && remoteStatePtr->remoteStateId != -1)
        //                {
        //                    int id = remoteStatePtr->remoteStateId;
        //                    remoteStatePtr-> stateOffererPrx->removeInstance(id);
        //                }
        //                else
        //                {
        //                    stateBuffer.push(StateBasePtr::dynamicCast(subStateList.at(i)));
        //                }
        //            }
        //            if(stateBuffer.size() > 0)
        //                currentState = stateBuffer.pop()._ptr;
        //            else currentState = NULL;
        //        }while(currentState);
    }




    template <typename ContextType>
    int
    RemoteStateOfferer<ContextType>::createRemoteStateInstance(const ::std::string& stateName, const RemoteStateIceBasePrx& remoteStatePrx, const std::string& parentStateItentifierStr, const ::std::string& instanceName, const Ice::Current& context)
    {

        if (!ContextType::getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted, 5000))
        {
            throw LocalException() << "Cannot create a remote state instance because the RemoteStateOfferer '" << ContextType::getName() << "' is still waiting for dependencies: " << boost::algorithm::join(ContextType::getUnresolvedDependencies(), ", ");
        }

        //look for state with stateName
        StateBasePtr statePtr = getStatePtr(stateName);

        if (!statePtr)
        {
            std::stringstream msg;
            msg << "Could not find (public) state: " << stateName;
            HiddenTimedMutex::ScopedLock lock(*__StateInstancesMutex);

            for (const auto & entry : (*stateInstancesPtr))
            {
                StateBase* state = entry.second;

                if (stateName == state->getStateName())
                {
                    msg << "- but found a state instance with same name - maybe you forgot to make the state public?";
                    break;
                }
            }

            throw LocalException(msg.str());
        }

        //        if(!RemoteAccessableStatePtr::dynamicCast(statePtr))
        //            throw throw UserException("The state "+ statePtr->stateName + " must be derived of armarx::RemoteAccessableState");
        ARMARX_DEBUG << "entering createRemoteStateInstance() with " << parentStateItentifierStr << " stateclass: " << statePtr->stateClassName << " statename: " << statePtr->stateName << flush;
        StatePtr newState = StatePtr::dynamicCast(statePtr->clone());

        if (!newState)
        {
            throw exceptions::local::eNullPointerException("Could not cast from StateBasePtr to StatePtr");
        }

        if (!instanceName.empty())
        {
            newState->setStateName(instanceName);
        }

        ARMARX_DEBUG << "RemoteStateInstanceName: " << newState->getStateName() << " instanceName=" << instanceName;
        RemoteStateWrapperPtr newWrappedState = new RemoteStateWrapper(newState, remoteStatePrx, this);
        RemoteStateData remoteStateData;
        remoteStateData.callerIceName = "";
        remoteStateData.callerStatePrx = remoteStatePrx;
        remoteStateData.remoteWrappedState = newWrappedState;
        remoteStateData.id = newState->localUniqueId;
        {
            HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
            stateInstanceList[remoteStateData.id] = remoteStateData;
        }

        newWrappedState->globalStateIdentifier = parentStateItentifierStr;
        newState->__setParentState(newWrappedState._ptr);
        //        ARMARX_LOG << "gl id: " << newState->globalStateIdentifier;

        return remoteStateData.id;
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::callRemoteState(int stateId, const StringVariantContainerBaseMap& properties, const Ice::Current& context)
    {
        StatePtr state = getInstance(stateId).remoteWrappedState->realState;

        if (state->getStatePhase() >= eExited)
        {
            state->setStatePhase(eDefined);
        }

        //        ARMARX_LOG << "calling remote State "<< stateId<< "\n" << flush;
        state->enter(properties);
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::exitRemoteState(int stateId, const Ice::Current& context)
    {
        getInstance(stateId).remoteWrappedState->realState->_baseOnExit();
    }


    template <typename ContextType>
    bool
    RemoteStateOfferer<ContextType>::breakRemoteState(int stateId,
            const EventBasePtr& evt,
            const ::Ice::Current& context)
    {
        ARMARX_DEBUG << "breaking remote state id " << stateId << "\n" << flush;
        bool result = true;
        RemoteStateData stateData;

        try
        {
            stateData = getInstance(stateId);
            result =  stateData.remoteWrappedState->realState->_baseOnBreak(EventPtr::dynamicCast(evt));
        }
        catch (const LocalException&)
        {
            ARMARX_ERROR << "Could not find state with id " << stateId << "  - thus cannot break it" << flush;
            return true;
        }

        return result;

    }

    template <typename ContextType>
    bool RemoteStateOfferer<ContextType>::breakActiveSubstateRemotely(int stateId, const EventBasePtr& evt, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->__breakActiveSubstate(EventPtr::dynamicCast(evt));
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::notifyEventBufferedDueToUnbreakableStateRemote(int stateId, bool eventBuffered, const Ice::Current& context)
    {
        StateControllerPtr statePtr = StateControllerPtr::dynamicCast(getGlobalInstancePtr(stateId));

        statePtr->__notifyEventBufferedDueToUnbreakableState(eventBuffered);
    }

    template <typename ContextType>
    StateIceBasePtr RemoteStateOfferer<ContextType>::refetchRemoteSubstates(int stateId, const Ice::Current& context)
    {
        auto state = getInstance(stateId).remoteWrappedState->realState;
        state->refetchSubstates();
        return state->clone();
    }

    template <typename ContextType>
    StateParameterMap RemoteStateOfferer<ContextType>::getRemoteInputParameters(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr statePtr = getStatePtr(stateName);
        //        ARMARX_INFO << StateUtilFunctions::getDictionaryString(statePtr->inputParameters);
        return statePtr->inputParameters;
    }

    template <typename ContextType>
    StateParameterMap RemoteStateOfferer<ContextType>::getRemoteOutputParameters(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr statePtr = getStatePtr(stateName);
        return statePtr->outputParameters;
    }

    template <typename ContextType>
    StateParameterMap RemoteStateOfferer<ContextType>::getRemoteInputParametersById(int stateId, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->inputParameters;
    }

    template <typename ContextType>
    StateParameterMap RemoteStateOfferer<ContextType>::getRemoteOutputParametersById(int stateId, const Ice::Current& context)
    {
        RemoteStateData entry = getInstance(stateId);
        //        ARMARX_LOG << "output Dict: " << StateUtilFunctions::getDictionaryString(entry.remoteWrappedState->realState->outputParameters);
        return entry.remoteWrappedState->realState->outputParameters;
    }



    template <typename ContextType>
    bool RemoteStateOfferer<ContextType>::hasSubstatesRemote(const std::string& stateName, const Ice::Current& context) const
    {
        //        ARMARX_LOG << "checking for substates..." << flush;
        StateBasePtr statePtr = getStatePtr(stateName);

        if (statePtr->subStateList.size() > 0)
        {
            return true;
        }

        return false;
    }

    template <typename ContextType>
    bool RemoteStateOfferer<ContextType>::hasActiveSubstateRemote(int stateId, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->__hasActiveSubstate();
    }

    template <typename ContextType>
    StringList RemoteStateOfferer<ContextType>::getAvailableStates(const Ice::Current& context)
    {
        StringList result;

        for (unsigned int i = 0; i < subStateList.size(); ++i)
        {
            result.push_back(StateBasePtr::dynamicCast(subStateList.at(i))->stateName);
        }

        return result;
    }

    template <typename ContextType>
    StateIdNameMap RemoteStateOfferer<ContextType>::getAvailableStateInstances(const Ice::Current& context)
    {
        StateIdNameMap result;
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.begin();

        for (; it != stateInstanceList.end(); ++it)
        {
            result[it->second.remoteWrappedState->realState->localUniqueId] = it->second.remoteWrappedState->realState->getLocalHierarchyString();
        }

        return result;
    }

    template <typename ContextType>
    StateIceBasePtr RemoteStateOfferer<ContextType>::getStatechart(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr state = getStatePtr(stateName);
        HiddenTimedMutex::ScopedLock lock(__stateMutex);
        return state;
    }

    template <typename ContextType>
    StateIceBasePtr RemoteStateOfferer<ContextType>::getStatechartInstance(int stateId, const Ice::Current&)
    {
        StateBasePtr state = getInstance(stateId).remoteWrappedState->realState;
        state->refetchSubstates();
        return state;
    }

    template <typename ContextType>
    StateIceBasePtr RemoteStateOfferer<ContextType>::getStatechartInstanceByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current&)
    {

        StateIceBasePtr result;
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.begin();
        int stateCount = 0;

        for (; it != stateInstanceList.end(); ++it)
        {
            StateBasePtr curState = StateBasePtr::dynamicCast(it->second.remoteWrappedState->realState);

            if (curState->globalStateIdentifier == globalStateIdStr)
            {

                stateCount++;

                if (stateCount == 1)
                {
                    result = curState;
                }
            }

            if (stateCount == 0)
            {
                result = getStatechartInstanceByGlobalIdStrRecursive(globalStateIdStr, curState, stateCount);
            }
        }

        if (stateCount > 1)
        {
            ARMARX_WARNING << "Found more than one state with globalStateIdStr '" << globalStateIdStr << "'. Returning first found occurence." << flush;
        }

        StateBasePtr state = StateBasePtr::dynamicCast(result);
        state->refetchSubstates();
        return state;
    }


    template <typename ContextType>
    StateIceBasePtr RemoteStateOfferer<ContextType>::getStatechartInstanceByGlobalIdStrRecursive(const std::string& globalStateIdStr, StateBasePtr state, int& stateCounter)
    {
        StateIceBasePtr result;
        StateList::iterator it = state->subStateList.begin();

        for (; it != state->subStateList.end(); ++it)
        {
            StateBasePtr curState = StateBasePtr::dynamicCast(*it);

            if (curState->globalStateIdentifier == globalStateIdStr)
            {
                stateCounter++;

                if (stateCounter == 1)
                {
                    result = curState;
                }
            }

            if (stateCounter == 0)
            {
                result = getStatechartInstanceByGlobalIdStrRecursive(globalStateIdStr, curState, stateCounter);
            }
        }

        return result;
    }


    template <typename ContextType>
    bool RemoteStateOfferer<ContextType>::registerForUpdates(const std::string& stateName, const StateListenerPrx& listener, const Ice::Current&)
    {
        StateControllerPtr stateController = StateControllerPtr::dynamicCast(getStatechartInstanceByGlobalIdStr(stateName));

        if (stateController)
        {
            stateController->registerStateListener(listener);
            return true;
        }
        else
        {
            ARMARX_WARNING << "no StateController found with name " << stateName << flush;
            return false;
        }
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::unregisterForUpdates(const StateListenerPrx& listener, const Ice::Current&)
    {
        unregisterStateListenerRecursive(listener);
    }


    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::notifySubscribers(const StateIceBasePtr& updatedState, const Ice::Current& context)
    {
        notifyActiveStateChangedRecursive(updatedState);
    }






    template <typename ContextType>
    void
    RemoteStateOfferer<ContextType>::removeInstance(int stateId, const Ice::Current&)
    {
        try
        {
            RemoteStateData entry = getInstance(stateId);
            ARMARX_DEBUG << "removing instance of state '" << entry.remoteWrappedState->realState->stateName << "'  \n" << flush;
            entry.remoteWrappedState->realState->clearSelfPointer();
            entry.remoteWrappedState->realState->disableRunFunction();
            HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
            stateInstanceList.erase(stateId);
        }
        catch (LocalException& e)
        {
            ARMARX_LOG << eWARN << "Couldn't find state with id " << stateId << ". Hence, it could not be removed.\n" << flush;
        }


    }



    template <typename ContextType>
    StateBasePtr
    RemoteStateOfferer<ContextType>::getStatePtr(const std::string& stateName) const
    {

        bool found = false;
        StateBasePtr statePtr = NULL;

        for (unsigned int i = 0; i < subStateList.size(); i++)
        {
            if (subStateList.at(i)->stateName == stateName)
            {
                statePtr = StateBasePtr::dynamicCast(subStateList.at(i));
                found = true;
                break;
            }
        }

        if (!found)
        {
            ARMARX_ERROR << "Could not find state with name '" << stateName << "'" << flush;
            //            throw LocalException("Could not find state with name '" + stateName + "'");
        }

        return statePtr;
    }

    template <typename ContextType>
    std::map<int, StateBasePtr> RemoteStateOfferer<ContextType>::getChildStatesByName(int parentId, std::string stateName)
    {
        StateBasePtr statePtr = getInstance(parentId).remoteWrappedState->realState;
        std::map<int, StateBasePtr> stateList;
        std::map<int, StateBasePtr> result;

        do
        {
            //first add all subchildren to list
            for (unsigned int i = 0; i < statePtr->subStateList.size(); i++)
            {
                stateList.insert(std::pair<int, StateBasePtr> (StateBasePtr::dynamicCast(statePtr->subStateList.at(i))->localUniqueId, StateBasePtr::dynamicCast(statePtr->subStateList.at(i))));
            }

            if (parentId != statePtr->localUniqueId)// dont add parentstate
            {
                if (statePtr->stateName == stateName)
                {
                    result.insert(std::pair<int, StateBasePtr> (statePtr->localUniqueId, statePtr));
                }

                stateList.erase(statePtr->localUniqueId);
            }

            if (stateList.size() > 0)
            {
                statePtr = stateList.begin()->second;
            }
        }
        while (stateList.size() > 0);

        return result;
    }




    template <typename ContextType>
    typename RemoteStateOfferer<ContextType>::RemoteStateData RemoteStateOfferer<ContextType>::getInstance(int stateId)
    {
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.find(stateId);

        if (it == stateInstanceList.end())
        {
            std::stringstream str;
            str << "Could not find state with id '" << stateId << "'\n";
            str << "Known states:\n";

            for (it = stateInstanceList.begin(); it != stateInstanceList.end(); ++it)
            {
                RemoteStateData& data = it->second;
                str << "\t" << data.remoteWrappedState->stateName << " id: " << data.remoteWrappedState->localUniqueId << flush;
            }

            throw LocalException(str.str());
        }

        return it->second;
    }

    template <typename ContextType>
    StateBasePtr
    RemoteStateOfferer<ContextType>::getGlobalInstancePtr(int globalId) const
    {
        HiddenTimedMutex::ScopedLock lock(*StateBase::__StateInstancesMutex);
        typename std::map<int, StateBase*>::iterator it = stateInstancesPtr->find(globalId);

        if (it != stateInstancesPtr->end())
        {
            return it->second;
        }

        std::stringstream str;
        str << "Could not find state with id '" << globalId << "'";
        throw LocalException(str.str());

        return NULL;
    }

    template <typename ContextType>
    void
    RemoteStateOfferer<ContextType>::issueEvent(int receivingStateId, const EventBasePtr& event,  const Ice::Current&)
    {
        ARMARX_VERBOSE << "received external event '" << event->eventName << "' for state '" << event->eventReceiverName << "'" << flush;
        ARMARX_VERBOSE << "Event Dict Size: " << event->properties.size() << "\n" << StateUtilFunctions::getDictionaryString(event->properties) << flush;
        StateControllerPtr statePtr = StateControllerPtr::dynamicCast(getGlobalInstancePtr(receivingStateId));

        try
        {
            if (manager)
            {
                manager->addEvent(EventPtr::dynamicCast(event), statePtr);
            }
        }
        catch (LocalException& local)
        {
            ARMARX_LOG << eERROR << "Caught Exception in issueEvent: " << local.what() << flush;
        }
        catch (const IceUtil::Exception& exception)
        {
            // Handle local (run-time) exceptions
            ARMARX_LOG << armarx::eERROR
                       << "caught ice exception: " << exception.what()
                       << "\nBacktrace: " << exception.ice_stackTrace()
                       << flush;
        }
    }

    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::issueEventWithGlobalIdStr(const std::string& globalStateIdStr, const EventBasePtr& evt, const Ice::Current&)
    {
        try
        {
            int id = StateBasePtr::dynamicCast(getStatechartInstanceByGlobalIdStr(globalStateIdStr))->localUniqueId;
            issueEvent(id, evt);
        }
        catch (Ice::Exception& e)
        {
            ARMARX_ERROR << "Caught Ice::exception: " << e.what() << std::endl << e.ice_stackTrace();
        }
    }


}
#endif
