
#ifndef GROUPXMLREADER_H
#define GROUPXMLREADER_H

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include "../StateBase.h"
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

namespace armarx
{
    class StatechartGroupXmlReader
    {
    public:
        enum StateVisibility
        {
            ePrivate,
            ePublic
        };

        StatechartGroupXmlReader(const StatechartProfiles::StatechartProfilePtr& selectedProfile);

        void readXml(const boost::filesystem::path& groupDefinitionFile);
        //void readXml(const std::string &groupDefinitionXMLString);
        void readXml(RapidXmlReaderPtr reader);

        boost::filesystem::path getGroupDefinitionFilePath() const;
        std::string getGroupName() const
        {
            return groupName;
        }
        std::vector<std::string> getStateFilepaths() const
        {
            return allstateFilePaths;
        }
        std::string getDescription() const;
        std::string getPackageName() const;
        bool contextGenerationEnabled() const;
        StateVisibility getStateVisibility(const std::string& filepath) const;
        int getStateNestingLevel(std::string filepath) const;
        std::vector<std::string> getProxies() const;

        StatechartProfiles::StatechartProfilePtr getSelectedProfile()
        {
            return selectedProfile;
        }

    private:
        void ReadChildren(RapidXmlReaderNode xmlNode, const boost::filesystem::path& path, int nesting);
        std::vector<std::string> allstateFilePaths;
        boost::filesystem::path basePath;
        boost::filesystem::path groupDefinitionFile;
        std::string groupName;
        std::string packageName;
        std::string description;
        bool generateContext;
        std::vector<std::string> proxies;
        std::map<std::string, StateVisibility> stateVisibilityMap;
        std::map<std::string, int> stateNestingMap;
        StatechartProfiles::StatechartProfilePtr selectedProfile;

    };
    typedef boost::shared_ptr<StatechartGroupXmlReader> StatechartGroupXmlReaderPtr;
}

#endif
