#ifndef CPPBLOCK_H
#define CPPBLOCK_H

#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include "CppWriter.h"
#include <boost/format.hpp>

namespace armarx
{
    class CppBlock;
    typedef boost::shared_ptr<CppBlock> CppBlockPtr;

    class CppBlock
    {
    public:
        CppBlock();

        void writeCpp(CppWriterPtr writer);
        std::string getAsSingleLine();
        void addLine(const std::string& line);
        void addLine(const boost::basic_format<char>& line);

    private:
        std::vector<std::string> lines;
    };
}

#endif // CPPBLOCK_H
