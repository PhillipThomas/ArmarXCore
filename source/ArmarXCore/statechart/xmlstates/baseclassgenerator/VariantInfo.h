#ifndef VARIANTINFO_H
#define VARIANTINFO_H

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <vector>
#include <string>
#include <set>
#include <boost/shared_ptr.hpp>

namespace armarx
{
    class VariantInfo;
    typedef boost::shared_ptr<VariantInfo> VariantInfoPtr;

    class VariantInfo
    {
    public:
        class VariantEntry;
        typedef boost::shared_ptr<VariantEntry> VariantEntryPtr;

        class VariantEntry
        {
            friend class VariantInfo;

        public:
            VariantEntry(RapidXmlReaderNode node);
            std::string getHumanName();
        private:
            std::string baseTypeName;
            std::string dataTypeName;
            std::string humanName;
            bool basic;
        };

        enum ProxyType { SingleProxy, Topic };

        class ProxyEntry;
        typedef boost::shared_ptr<ProxyEntry> ProxyEntryPtr;

        class ProxyEntry
        {
            friend class VariantInfo;

        private:
            std::string includePath;
            std::string humanName;
            std::string typeName;
            std::string memberName;
            std::string getterName;
            std::string propertyName;
            bool propertyIsOptional;
            std::string propertyDefaultValue;
            ProxyType proxyType;

            std::vector<std::string> includes;
            std::vector<std::pair<std::string, std::string>> methods;
            std::vector<std::string> members;
            std::vector<std::string> onInit;
            std::vector<std::string> onConnect;
            std::vector<std::pair<std::string, std::string>> stateMethods;

            void readVector(RapidXmlReaderNode node, const char* name, std::vector<std::string>& vec);

        public:
            ProxyEntry(RapidXmlReaderNode node);
            std::string getIncludePath()
            {
                return includePath;
            }
            std::string getHumanName()
            {
                return humanName;
            }
            std::string getTypeName()
            {
                return typeName;
            }
            std::string getMemberName()
            {
                return memberName;
            }
            std::string getGetterName()
            {
                return getterName;
            }
            std::string getPropertyName()
            {
                return propertyName;
            }
            bool getPropertyIsOptional()
            {
                return propertyIsOptional;
            }
            std::string getPropertyDefaultValue()
            {
                return propertyDefaultValue;
            }
            ProxyType getProxyType()
            {
                return proxyType;
            }
            std::string getProxyTypeAsString()
            {
                return proxyType == Topic ? "Topic" : "Proxy";
            }

            std::vector<std::string> getIncludes()
            {
                return includes;
            }
            std::vector<std::pair<std::string, std::string>> getMethods()
            {
                return methods;
            }
            std::vector<std::string> getMembers()
            {
                return members;
            }
            std::vector<std::string> getOnInit()
            {
                return onInit;
            }
            std::vector<std::string> getOnConnect()
            {
                return onConnect;
            }
            std::vector<std::pair<std::string, std::string>> getStateMethods()
            {
                return stateMethods;
            }
        };

        class LibEntry;
        typedef boost::shared_ptr<LibEntry> LibEntryPtr;

        class LibEntry
        {
            friend class VariantInfo;

        public:
            LibEntry(RapidXmlReaderNode node);
            std::vector<std::string> getFactoryIncludes() const;
            std::string getName();
            std::vector<ProxyEntryPtr> getProxies();

        private:
            std::vector<std::string> factoryIncludes;
            std::vector<VariantEntryPtr> variants;
            std::vector<ProxyEntryPtr> proxies;
            std::string name;
        };

    public:
        VariantInfo();
        void readVariantInfo(RapidXmlReaderPtr reader, const std::string& packagePath);
        LibEntryPtr findLibByVariant(std::string variantTypeName);
        std::string getDataTypeName(std::string variantBaseTypeName);
        std::string getReturnTypeName(std::string variantBaseTypeName);
        bool isBasic(std::string variantBaseTypeName);
        std::vector<LibEntryPtr> getLibs();
        ProxyEntryPtr getProxyEntry(std::string proxyId);
        VariantEntryPtr getVariantByName(std::string variantBaseTypeName);
        VariantEntryPtr getVariantByHumanName(std::string humanName);
        std::string getNestedHumanNameFromBaseName(std::string variantBaseTypeName);
        std::string getNestedBaseNameFromHumanName(std::string humanName);
        const std::set<std::string>& getPackagePaths() const;
        std::string getDebugInfo() const;

        static VariantInfoPtr ReadInfoFilesRecursive(const std::string& rootPackageName, const std::string& rootPackagePath, bool showErrors, VariantInfoPtr variantInfo = VariantInfoPtr());
        static VariantInfoPtr ReadInfoFiles(const std::vector<std::string>& packages, bool showErrors = true);

    private:
        std::vector<LibEntryPtr> libs;
        std::map<std::string, LibEntryPtr> variantToLibMap;
        std::map<std::string, VariantEntryPtr> variantMap;
        std::map<std::string, VariantEntryPtr> humanNameToVariantMap;
        std::map<std::string, ProxyEntryPtr> proxyMap;
        std::set<std::string> packagePaths;
    };
}

#endif // VARIANTINFO_H
