#include "XmlContextBaseClassGenerator.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

using namespace armarx;
using namespace boost;

XmlContextBaseClassGenerator::XmlContextBaseClassGenerator()
{

}


std::string XmlContextBaseClassGenerator::GenerateCpp(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo)
{
    CppClassPtr cppClass = BuildClass(namespaces, proxies, groupName, variantInfo);
    CppWriterPtr writer(new CppWriter());
    cppClass->writeCpp(writer);
    return writer->getString();
}

CppClassPtr XmlContextBaseClassGenerator::BuildClass(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo)
{
    std::string className = groupName + "StatechartContext";

    for (std::string & ns : namespaces)
    {
        ns = boost::replace_all_copy(ns, "-", "_");
    }

    CppClassPtr cppClass(new CppClass(namespaces, className));

    std::stringstream ss;
    ss << "ARMARX_COMPONENT_";

    for (auto ns : namespaces)
    {
        ss << boost::to_upper_copy(ns) << "_";
    }

    ss << boost::to_upper_copy(className) << "_H";
    cppClass->setIncludeGuard(ss.str());
    cppClass->addInherit("virtual public StatechartContext");

    cppClass->addInclude("<ArmarXCore/core/Component.h>");
    cppClass->addInclude("<ArmarXCore/core/system/ImportExportComponent.h>");
    cppClass->addInclude("<ArmarXCore/statechart/StatechartContext.h>");

    std::vector<VariantInfo::ProxyEntryPtr> proxyEntries;

    for (std::string proxyId : proxies)
    {
        VariantInfo::ProxyEntryPtr p = variantInfo->getProxyEntry(proxyId);

        if (p)
        {
            proxyEntries.push_back(p);
        }
    }

    CppClassPtr propertiesClass = cppClass->addInnerClass("PropertyDefinitions");
    propertiesClass->addInherit("public StatechartContextPropertyDefinitions");

    CppCtorPtr propertiesCtor = propertiesClass->addCtor("std::string prefix");
    propertiesCtor->addInitListEntry("StatechartContextPropertyDefinitions", "prefix");

    CppMethodPtr getDefaultName = cppClass->addMethod("std::string getDefaultName() const");
    CppMethodPtr onInit = cppClass->addMethod("virtual void onInitStatechartContext()");
    CppMethodPtr onConnect = cppClass->addMethod("virtual void onConnectStatechartContext()");

    getDefaultName->addLine(fmt("return \"%s\";", className));

    for (VariantInfo::ProxyEntryPtr p : proxyEntries)
    {
        std::string description = fmt("Name of the %s that should be used", boost::to_lower_copy(magicNameSplit(p->getPropertyName())));

        if (p->getPropertyIsOptional())
        {
            propertiesCtor->addLine(fmt("defineOptionalProperty<std::string>(\"%s\", \"%s\", \"%s\");", p->getPropertyName(), p->getPropertyDefaultValue(), description));
        }
        else
        {
            propertiesCtor->addLine(fmt("defineRequiredProperty<std::string>(\"%s\", \"%s\");", p->getPropertyName(), description));
        }


        if (p->getProxyType() == VariantInfo::SingleProxy)
        {
            onInit->addLine(fmt("usingProxy(getProperty<std::string>(\"%s\").getValue());", p->getPropertyName()));
            onConnect->addLine(fmt("%s = getProxy<%s>(getProperty<std::string>(\"%s\").getValue());", p->getMemberName(), p->getTypeName(), p->getPropertyName()));
        }
        else if (p->getProxyType() == VariantInfo::Topic)
        {
            onInit->addLine(fmt("offeringTopic(getProperty<std::string>(\"%s\").getValue());", p->getPropertyName()));
            onConnect->addLine(fmt("%s = getTopic<%s>(getProperty<std::string>(\"%s\").getValue());", p->getMemberName(), p->getTypeName(), p->getPropertyName()));
        }
        else
        {
            throw LocalException("Not supported ProxyType");
        }

        cppClass->addPrivateField(fmt("%s %s;", p->getTypeName(), p->getMemberName()));
        CppMethodPtr getProxy = cppClass->addMethod(fmt("%s %s() const", p->getTypeName(), p->getGetterName()));
        getProxy->addLine(fmt("return %s;", p->getMemberName()));
        getProxy->setCompact(true);
        cppClass->addInclude(fmt("<%s>", p->getIncludePath()));

    }

    // special custom proxy code
    for (VariantInfo::ProxyEntryPtr p : proxyEntries)
    {
        for (std::string member : p->getMembers())
        {
            cppClass->addPrivateField(member);
        }

        for (std::string include : p->getIncludes())
        {
            cppClass->addInclude(fmt("<%s>", include));
        }

        for (std::string oni : p->getOnInit())
        {
            onInit->addLine(oni);
        }

        for (std::string onc : p->getOnConnect())
        {
            onConnect->addLine(onc);
        }

        for (std::pair<std::string, std::string> method : p->getMethods())
        {
            cppClass->addMethod(method.first)->addLine(method.second);
        }
    }

    CppMethodPtr createPropertyDefinitions = cppClass->addMethod("virtual PropertyDefinitionsPtr createPropertyDefinitions()");
    createPropertyDefinitions->addLine(fmt("return PropertyDefinitionsPtr(new %s::PropertyDefinitions(getConfigIdentifier()));", className));


    return cppClass;
}

std::string XmlContextBaseClassGenerator::magicNameSplit(std::string name)
{
    if (boost::algorithm::ends_with(name, "Name"))
    {
        name = name.substr(0, name.size() - 4);
    }

    boost::regex re("([A-Z][a-z])");
    name = boost::regex_replace(name, re, " \\1");
    boost::to_lower(name);
    boost::trim(name);
    return name;
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1)
{
    return str(format(fmt) % arg1);
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2)
{
    return str(format(fmt) % arg1 % arg2);
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3)
{
    return str(format(fmt) % arg1 % arg2 % arg3);
}
