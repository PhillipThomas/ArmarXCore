#include "CppMethod.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace armarx;

CppMethod::CppMethod(const std::string& header, const std::string& doc)
{
    this->compact = false;
    this->header = header;
    this->doc = doc;
    this->block.reset(new CppBlock());
}

void CppMethod::writeCpp(CppWriterPtr writer)
{
    if (!doc.empty())
    {
        std::string delimiters = "\n";
        std::vector<std::string> doclines;
        boost::split(doclines, doc, boost::is_any_of(delimiters));
        writer->line("/**");

        for (auto & line : doclines)
        {
            writer->line(" * " + line);
        }

        writer->line(" */");
    }

    if (compact)
    {
        writer->line(header + " " + block->getAsSingleLine());
    }
    else
    {
        writer->line(header);
        block->writeCpp(writer);
    }
}

void CppMethod::addLine(const std::string& line)
{
    block->addLine(line);
}

void CppMethod::addLine(const boost::basic_format<char>& line)
{
    block->addLine(line);
}

void CppMethod::setCompact(bool compact)
{
    this->compact = compact;
}
