#ifndef CPPMETHOD_H
#define CPPMETHOD_H

#include "CppBlock.h"
#include "DoxDoc.h"


namespace armarx
{
    class CppMethod;
    typedef boost::shared_ptr<CppMethod> CppMethodPtr;

    class CppMethod
    {
    public:
        CppMethod(const std::string& header, const std::string& doc = "");

        void writeCpp(CppWriterPtr writer);
        void addLine(const std::string& line);
        void addLine(const boost::basic_format<char>& line);
        void setCompact(bool compact);

    private:
        std::string header, doc;
        CppBlockPtr block;
        bool compact;

    };

}
#endif // CPPMETHOD_H
