#ifndef DOXDOC_H
#define DOXDOC_H

#include <boost/shared_ptr.hpp>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <Ice/Communicator.h>
#include "DoxEntry.h"
#include "CppWriter.h"
#include "VariantInfo.h"


namespace armarx
{

    class DoxDoc;
    typedef boost::shared_ptr<DoxDoc> DoxDocPtr;

    class DoxDoc
    {
    public:
        DoxDoc();
        void addEntry(DoxEntryPtr entry);
        void addLine();
        void addLine(const std::string& line);
        void writeDoc(CppWriterPtr writer);
        void addParameterTable(const RapidXmlReaderNode& parameters, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator);
        void addTransitionGraph(const RapidXmlReaderNode& transitions);
    private:
        std::vector<DoxEntryPtr> entries;
    };
}


#endif // DOXDOC_H
