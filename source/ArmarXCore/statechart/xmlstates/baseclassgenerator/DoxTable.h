#ifndef DOXTABLE_H
#define DOXTABLE_H

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include "DoxEntry.h"


namespace armarx
{
    class DoxTable;
    typedef boost::shared_ptr<DoxTable> DoxTablePtr;


    class DoxTable : public DoxEntry
    {
    public:
        DoxTable(const std::vector<std::string>& header);
        void addRow(const std::vector<std::string>& row);
        int rows();
        std::string getDoxString();
    private:
        std::vector<std::string> header;
        std::vector<std::vector<std::string>> cells;
    };
}

#endif
