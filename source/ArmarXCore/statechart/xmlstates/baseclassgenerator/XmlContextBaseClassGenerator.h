#ifndef XMLRSOBASECLASSGENERATOR_H
#define XMLRSOBASECLASSGENERATOR_H

#include "CppClass.h"
#include "VariantInfo.h"

#include <set>
#include <string>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

namespace armarx
{
    class XmlContextBaseClassGenerator
    {
    public:
        XmlContextBaseClassGenerator();

        static std::string GenerateCpp(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo);

    private:
        static CppClassPtr BuildClass(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo);
        static std::string magicNameSplit(std::string name);
        static std::string fmt(const std::string& fmt, const std::string& arg1);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3);
    };
}

#endif // XMLRSOBASECLASSGENERATOR_H
