#ifndef DOXENTRY_H
#define DOXENTRY_H

#include <boost/shared_ptr.hpp>
#include <string>

namespace armarx
{
    class DoxEntry;
    typedef boost::shared_ptr<DoxEntry> DoxEntryPtr;

    class DoxEntry
    {
    public:
        virtual std::string getDoxString() = 0;
    };
}


#endif // DOXENTRY_H
