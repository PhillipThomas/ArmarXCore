#include "VariantInfo.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

using namespace armarx;

VariantInfo::VariantInfo()
{
}

void VariantInfo::readVariantInfo(RapidXmlReaderPtr reader, const std::string& packagePath)
{
    if (packagePaths.find(packagePath) != packagePaths.end())
    {
	ARMARX_INFO_S << "path " << packagePath << " already in there.";
        return;
    }

    packagePaths.insert(packagePath);
    RapidXmlReaderNode node = reader->getRoot("VariantInfo");
    for (RapidXmlReaderNode libNode = node.first_node("Lib"); libNode.is_valid(); libNode = libNode.next_sibling("Lib"))
    {
        LibEntryPtr lib(new LibEntry(libNode));
        libs.push_back(lib);

        for (VariantEntryPtr e : lib->variants)
        {
            variantToLibMap.insert(std::make_pair(e->baseTypeName, lib));
            variantMap.insert(std::make_pair(e->baseTypeName, e));
            humanNameToVariantMap.insert(std::make_pair(e->humanName, e));
        }

        for (ProxyEntryPtr p : lib->proxies)
        {
            proxyMap.insert(std::make_pair(lib->name + "." + p->memberName, p));
        }
    }
}

VariantInfo::LibEntryPtr VariantInfo::findLibByVariant(std::string variantTypeName)
{
    std::map<std::string, LibEntryPtr>::const_iterator it = variantToLibMap.find(variantTypeName);

    if (it == variantToLibMap.end())
    {
        return LibEntryPtr();
    }
    else
    {
        return it->second;
    }
}

std::string VariantInfo::getDataTypeName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return "NOT_FOUND";
    }
    else
    {
        return it->second->dataTypeName;
    }
}

std::string VariantInfo::getReturnTypeName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return "NOT_FOUND";
    }
    else
    {
        // NOTE: Here a typedef in the form of "typedef IceInternal::Handle<[VariantDataType]> [VariantDataType]Ptr;" is assumed.
        // If this typedef is missing, the statechart code generator WILL GENERATE INVALID CODE.
        VariantEntryPtr e = it->second;
        return e->basic ? e->dataTypeName : e->dataTypeName + "Ptr";
    }
}

bool VariantInfo::isBasic(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return false;
    }
    else
    {
        VariantEntryPtr e = it->second;
        return e->basic;
    }
}

std::vector<VariantInfo::LibEntryPtr> VariantInfo::getLibs()
{
    return libs;
}

VariantInfo::ProxyEntryPtr VariantInfo::getProxyEntry(std::string proxyId)
{
    std::map<std::string, ProxyEntryPtr>::const_iterator it = proxyMap.find(proxyId);

    if (it == proxyMap.end())
    {
        return ProxyEntryPtr();
    }
    else
    {
        return it->second;
    }
}

VariantInfo::VariantEntryPtr VariantInfo::getVariantByName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return VariantEntryPtr();
    }
    else
    {
        return it->second;
    }
}

VariantInfo::VariantEntryPtr VariantInfo::getVariantByHumanName(std::string humanName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = humanNameToVariantMap.find(humanName);

    if (it == humanNameToVariantMap.end())
    {
        return VariantEntryPtr();
    }
    else
    {
        return it->second;
    }
}

std::string VariantInfo::getNestedHumanNameFromBaseName(std::string variantBaseTypeName)
{
    ContainerTypePtr containerInfo = VariantContainerType::FromString(variantBaseTypeName);
    std::stringstream containers;
    std::stringstream parenthesis;

    while (containerInfo->subType)
    {
        if (containerInfo->typeId == SingleTypeVariantList::getTypePrefix())
        {
            containers << "List(";
        }
        else if (containerInfo->typeId == StringValueMap::getTypePrefix())
        {
            containers << "Map(";
        }
        else
        {
            return "";
        }

        parenthesis << ")";
        containerInfo = containerInfo->subType;
    }

    VariantEntryPtr entry = getVariantByName(containerInfo->typeId);

    if (!entry)
    {
        return "";
    }

    return containers.str() + entry->humanName + parenthesis.str();
}

std::string VariantInfo::getNestedBaseNameFromHumanName(std::string humanName)
{
    ContainerTypePtr containerInfo = VariantContainerType::FromString(humanName);
    std::stringstream containers;
    std::stringstream parenthesis;

    while (containerInfo->subType)
    {
        if (containerInfo->typeId == "List")
        {
            containers << SingleTypeVariantList::getTypePrefix() << "(";
        }
        else if (containerInfo->typeId == "Map")
        {
            containers << StringValueMap::getTypePrefix() << "(";
        }
        else
        {
            return "";
        }

        parenthesis << ")";
        containerInfo = containerInfo->subType;
    }

    VariantEntryPtr entry = getVariantByHumanName(containerInfo->typeId);

    if (!entry)
    {
        return "";
    }

    return containers.str() + entry->baseTypeName + parenthesis.str();
}

const std::set<std::string>& VariantInfo::getPackagePaths() const
{
    return packagePaths;
}

std::string VariantInfo::getDebugInfo() const
{
    std::stringstream ss;

    for (const LibEntryPtr & e : libs)
    {
        ss << e->getName() << "\n";

        for (const VariantEntryPtr & v : e->variants)
        {
            ss << "  " << v->getHumanName() << ": " << v->dataTypeName << "\n";
        }

        for (const ProxyEntryPtr & p : e->getProxies())
        {
            ss << "  " << p->getHumanName() << ": " << p->getIncludePath() << "\n";
        }
    }

    return ss.str();
}

VariantInfoPtr VariantInfo::ReadInfoFilesRecursive(const std::string& rootPackageName, const std::string& rootPackagePath, bool showErrors,  VariantInfoPtr variantInfo)
{
    if (!variantInfo)
    {
        variantInfo.reset(new VariantInfo());
    }

    auto finder = CMakePackageFinder(rootPackageName, rootPackagePath);
    boost::filesystem::path variantInfoFile(finder.getDataDir().c_str());
    variantInfoFile /= rootPackageName;
    variantInfoFile /= "VariantInfo-" + rootPackageName + ".xml";

    if (boost::filesystem::exists(variantInfoFile))
    {
        try
        {
            RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(variantInfoFile.string());
            variantInfo->readVariantInfo(xmlReader, rootPackagePath);
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR_S << "Reading " << variantInfoFile.string() << " failed: " << e.what();
        }
    }
    else if (showErrors)
    {
        ARMARX_ERROR_S << "VariantInfo File not found for project " << rootPackageName << ": " << variantInfoFile.string();
    }

    if (finder.packageFound())
    {
        auto depPaths = finder.getDependencyPaths();

        for (auto & depPath : depPaths)
        {
            if (variantInfo->getPackagePaths().find(depPath.second) != variantInfo->getPackagePaths().end())
            {
                continue;
            }

            if (!depPath.first.empty() && !depPath.second.empty())
            {
                std::string packagePath;

                if (!depPath.second.empty() && boost::filesystem::exists(depPath.second))
                {
                    packagePath = depPath.second;
                }

                variantInfo = ReadInfoFilesRecursive(depPath.first, packagePath, showErrors, variantInfo);
            }
        }
    }

    return variantInfo;
}

VariantInfoPtr VariantInfo::ReadInfoFiles(const std::vector<std::string>& packages, bool showErrors)
{
    VariantInfoPtr variantInfo(new VariantInfo());

    for (std::string package : packages)
    {
        auto finder = CMakePackageFinder(package);
        boost::filesystem::path variantInfoFile(finder.getDataDir().c_str());
        variantInfoFile /= package;
        variantInfoFile /= "VariantInfo-" + package + ".xml";

        if (boost::filesystem::exists(variantInfoFile))
        {
            try
            {
                RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(variantInfoFile.string());
                variantInfo->readVariantInfo(xmlReader, finder.getDataDir());
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR_S << "Reading " << variantInfoFile.string() << " failed: " << e.what();
            }
        }
        else if (showErrors)
        {
            ARMARX_ERROR_S << "VariantInfo File not found for project " << package << ": " << variantInfoFile.string();
        }
    }

    return variantInfo;
}


VariantInfo::LibEntry::LibEntry(RapidXmlReaderNode node)
{
    this->name = node.attribute_value("name");

    for (RapidXmlReaderNode includeNode = node.first_node("VariantFactory"); includeNode.is_valid(); includeNode = includeNode.next_sibling("VariantFactory"))
    {
        factoryIncludes.push_back(includeNode.attribute_value("include"));
    }

    for (RapidXmlReaderNode variantNode = node.first_node("Variant"); variantNode.is_valid(); variantNode = variantNode.next_sibling("Variant"))
    {
        VariantEntryPtr entry(new VariantEntry(variantNode));
        variants.push_back(entry);
    }

    for (RapidXmlReaderNode proxyNode : node.nodes())
    {
        if (proxyNode.name() == "Proxy" || proxyNode.name() == "Topic")
        {
            ProxyEntryPtr proxy(new ProxyEntry(proxyNode));
            proxies.push_back(proxy);
        }
    }
}

std::vector<std::string> VariantInfo::LibEntry::getFactoryIncludes() const
{
    return factoryIncludes;
}

std::string VariantInfo::LibEntry::getName()
{
    return name;
}

std::vector<VariantInfo::ProxyEntryPtr> VariantInfo::LibEntry::getProxies()
{
    return proxies;
}

VariantInfo::VariantEntry::VariantEntry(RapidXmlReaderNode node)
{
    baseTypeName = node.attribute_value("baseType");
    dataTypeName = node.attribute_value("dataType");
    humanName = node.attribute_value("humanName");
    basic = node.attribute_as_optional_bool("basic", "true", "false", false);
}

std::string VariantInfo::VariantEntry::getHumanName()
{
    return humanName;
}


void VariantInfo::ProxyEntry::readVector(RapidXmlReaderNode node, const char* name, std::vector<std::string>& vec)
{
    for (RapidXmlReaderNode n : node.nodes(name))
    {
        vec.push_back(n.value());
    }
}

VariantInfo::ProxyEntry::ProxyEntry(RapidXmlReaderNode node)
{
    includePath = node.attribute_value("include");
    humanName = node.attribute_value("humanName");
    typeName = node.attribute_value("typeName");
    memberName = node.attribute_value("memberName");
    getterName = node.attribute_value("getterName");
    propertyName = node.attribute_value("propertyName");
    propertyIsOptional = node.attribute_as_optional_bool("propertyIsOptional", "true", "false", false);
    propertyDefaultValue = node.attribute_value_or_default("propertyDefaultValue", "");
    proxyType = node.name() == "Topic" ? Topic : SingleProxy;

    readVector(node, "include", includes);
    readVector(node, "member", members);
    readVector(node, "onInit", onInit);
    readVector(node, "onConnect", onConnect);

    for (RapidXmlReaderNode n : node.nodes("method"))
    {
        methods.push_back(std::make_pair(n.attribute_value("header"), n.value()));
    }

    for (RapidXmlReaderNode n : node.nodes("stateMethod"))
    {
        stateMethods.push_back(std::make_pair(n.attribute_value("header"), n.value()));
    }
}