/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_XMLSTATE_H
#define _ARMARX_XMLSTATE_H

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <ArmarXCore/statechart/State.h>
#include <ArmarXCore/statechart/standardstates/FinalState.h>
#include <ArmarXCore/statechart/RemoteState.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>

#include <fstream>


#include <boost/shared_ptr.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/tuple/tuple.hpp>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

namespace armarx
{
    typedef std::map<std::string, RapidXmlReaderPtr> StringXMLNodeMap;
    typedef boost::shared_ptr<StringXMLNodeMap> StringXMLNodeMapPtr;
    struct XMLStateConstructorParams
    {
        boost::filesystem::path xmlFilepath;
        RapidXmlReaderPtr reader;
        StatechartProfiles::StatechartProfilePtr selectedProfile;
        StringXMLNodeMapPtr uuidToXMLMap;
        Ice::CommunicatorPtr ic;
        XMLStateConstructorParams(boost::filesystem::path xmlFilepath,
                                  RapidXmlReaderPtr reader,
                                  StatechartProfiles::StatechartProfilePtr selectedProfile,
                                  StringXMLNodeMapPtr uuidToXMLMap,
                                  Ice::CommunicatorPtr ic)
            : xmlFilepath(xmlFilepath),
              reader(reader),
              selectedProfile(selectedProfile),
              uuidToXMLMap(uuidToXMLMap),
              ic(ic)
        {}
    };

    //typedef boost::tuple<boost::filesystem::path, RapidXmlReaderPtr, StatechartProfiles::StatechartProfilePtr, StringXMLNodeMapPtr, Ice::CommunicatorPtr> XMLStateConstructorParams;
    struct XMLStateFactoryBase;
    typedef  IceInternal::Handle<XMLStateFactoryBase > XMLStateFactoryBasePtr;
    struct XMLStateFactoryBase : virtual Ice::Object,
            virtual public AbstractFactoryMethod<XMLStateFactoryBase, XMLStateConstructorParams, XMLStateFactoryBasePtr >
    {

    };

    class StateParameterDeserialization
    {
    public:
        StateParameterDeserialization(RapidXmlReaderNode parameterNode, Ice::CommunicatorPtr ic, StatechartProfiles::StatechartProfilePtr selectedProfile);

        std::string getName()
        {
            return name;
        }
        bool getOptional()
        {
            return optional;
        }
        std::string getTypeStr()
        {
            return typeStr;
        }
        ContainerTypePtr getTypePtr()
        {
            return typePtr;
        }
        VariantContainerBasePtr getContainer()
        {
            return container;
        }

    protected:
        std::string name;
        bool optional;
        std::string typeStr;
        ContainerTypePtr typePtr;
        VariantContainerBasePtr container;
    };

    StateParameterMap deserializeStateParameter(RapidXmlReaderNode parametersNode);


    template <typename StateType>
    class XMLStateTemplate :
        virtual public StateTemplate<StateType>
    {
    public:


        XMLStateTemplate(const XMLStateConstructorParams& stateData);

        ~XMLStateTemplate();

    protected:

        StateParameterMap getParameters(RapidXmlReaderNode  parametersNode);

        StatePtr addState(StatePtr state);
        void addXMLSubstates(RapidXmlReaderNode  substatesNode, const std::string& parentStateName);
        StateBasePtr addXMLSubstate(RapidXmlReaderNode  stateNode, const std::string& parentStateName);

        void defineParameters();
        void defineSubstates();
    private:


        void addTransitions(const RapidXmlReaderNode& transitionsNode);
        void setStartState(const RapidXmlReaderNode& startNode);
        ParameterMappingPtr getMapping(const RapidXmlReaderNode& mappingNode);
        boost::filesystem::path xmlFilepath;
        RapidXmlReaderPtr stateReader;
        RapidXmlReaderNode stateNode;
        StatechartProfiles::StatechartProfilePtr selectedProfile;
        StringXMLNodeMapPtr uuidToXMLMap;
        Ice::CommunicatorPtr ic;


    };

    struct NoUserCodeState :
            virtual public XMLStateTemplate<NoUserCodeState>,
        public virtual XMLStateFactoryBase
    {
        NoUserCodeState(XMLStateConstructorParams stateData);
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static std::string GetName()
        {
            return "NoUserCodeState";
        }
        static SubClassRegistry Registry;

    };



    ///////////////////////////////////////////////////
    ///// Implemetation
    ///////////////////////////////////////////////////

    template <typename StateType>
    XMLStateTemplate<StateType>::XMLStateTemplate(const XMLStateConstructorParams& stateData) :
        xmlFilepath(stateData.xmlFilepath),
        stateReader(stateData.reader),
        stateNode(RapidXmlReaderNode::NullNode()),
        selectedProfile(stateData.selectedProfile),
        uuidToXMLMap(stateData.uuidToXMLMap)
    {
        ARMARX_IMPORTANT_S << "selectedProfile: " << selectedProfile->getName();

        if (!stateReader && xmlFilepath.empty())
        {
            throw LocalException("Either a xml node or a filepath must be given.");
        }
        else if (!stateReader)
        {
            stateNode = RapidXmlReader::FromFile(xmlFilepath.string())->getRoot("State");
        }
        else
        {
            stateNode = stateReader->getRoot("State");
            xmlFilepath.clear();
        }

    }



    template <typename StateType>
    XMLStateTemplate<StateType>::~XMLStateTemplate()
    {
    }


    template <typename StateType>
    void XMLStateTemplate<StateType>::defineParameters()
    {
        //        ARMARX_INFO_S << "defineSubstates of " << State::getStateName();
        if (!stateNode.is_valid())
        {
            return;
        }

        if (!ic && StateBase::context && StateBase::context->getIceManager()->getCommunicator())
        {
            ic = StateBase::context->getIceManager()->getCommunicator();
        }

        this->StateBase::inputParameters = getParameters(stateNode.first_node("InputParameters"));
        this->StateBase::outputParameters = getParameters(stateNode.first_node("OutputParameters"));
        this->StateBase::localParameters = getParameters(stateNode.first_node("LocalParameters"));


    }

    template <typename StateType>
    StateParameterMap XMLStateTemplate<StateType>::getParameters(RapidXmlReaderNode  parametersNode)
    {
        StateParameterMap result;

        if (!parametersNode.is_valid())
        {
            return result;
        }



        for (RapidXmlReaderNode curParameterNode : parametersNode.nodes("Parameter"))
        {
            StateParameterDeserialization deserialization(curParameterNode, ic, selectedProfile);

            try
            {
                StateBase::addParameterContainer(result, deserialization.getName(), *deserialization.getTypePtr(), deserialization.getOptional(), deserialization.getContainer());
                //                ARMARX_VERBOSE << "Adding param: " << name << ", " << VariantContainerType::allTypesToString(typePtr) << ", optional: " << optional;
                //                ARMARX_VERBOSE << "JSON: " << jsonString;
            }
            catch (exceptions::user::UnknownTypeException& e)
            {
                ARMARX_WARNING << "The type '" << deserialization.getTypeStr() << "' is unknown, Parameter '" << deserialization.getName() << "' not added";
            }
        }

        return result;
    }


    template <typename StateType>
    void XMLStateTemplate<StateType>::defineSubstates()
    {
        //        ARMARX_INFO_S << "defineSubstates of " << State::getStateName();
        if (!stateNode.is_valid())
        {
            return;
        }

        const std::string stateName = stateNode.attribute_value("name");

        addXMLSubstates(stateNode.first_node("Substates"), stateName);

        addTransitions(stateNode.first_node("Transitions"));

        setStartState(stateNode.first_node("StartState"));

    }


    template <typename StateType>
    void XMLStateTemplate<StateType>::addXMLSubstates(RapidXmlReaderNode substatesNode, const std::string& parentStateName)
    {
        if (!substatesNode.is_valid())
        {
            return;
        }

        for (RapidXmlReaderNode curSubstateNode = substatesNode.first_node();
             curSubstateNode.is_valid();
             curSubstateNode = curSubstateNode.next_sibling())
        {
            addXMLSubstate(curSubstateNode, parentStateName);


        }

    }
    template <typename StateType>
    StateBasePtr XMLStateTemplate<StateType>::addXMLSubstate(RapidXmlReaderNode stateNode, const std::string& parentStateName)
    {
        ARMARX_CHECK_EXPRESSION(uuidToXMLMap);

        const std::string stateType = stateNode.name();



        StateBasePtr state;

        if (boost::iequals(stateType, "LocalState"))
        {
            const std::string refuuid = stateNode.attribute_value("refuuid");
            const std::string instanceName = stateNode.attribute_value("name");

            auto it = uuidToXMLMap->find(refuuid);

            if (it == uuidToXMLMap->end())
            {
                throw LocalException("Could not find local state with UUID ") << refuuid;
            }

            RapidXmlReaderPtr substateReader = it->second;
            const std::string stateName = substateReader->getRoot("State").attribute_value("name");
            state = StateBasePtr::dynamicCast(XMLStateFactoryBase::fromName(stateName, XMLStateConstructorParams(boost::filesystem::path(), substateReader, selectedProfile, uuidToXMLMap, ic)));

            if (!state)
            {
                ARMARX_DEBUG << "Using state with no code for " << stateName << " refuuid: " << refuuid << " instanceName: " << instanceName;
                state = StateBasePtr::dynamicCast(NoUserCodeState::CreateInstance(XMLStateConstructorParams(boost::filesystem::path(), substateReader, selectedProfile, uuidToXMLMap, ic)));
            }

            state->stateName = instanceName;
            state = addState(StatePtr::dynamicCast(state));
            ARMARX_DEBUG << "Added " << stateName << " with instanceName " << instanceName;

        }
        else if (boost::iequals(stateType, "RemoteState"))
        {
            const std::string instanceName = stateNode.attribute_value("name");
            const std::string refuuid = stateNode.attribute_value("refuuid");
            const std::string proxyName = stateNode.attribute_value("proxyName");
            ARMARX_DEBUG << "Adding remote state with refuuid " << refuuid << " and instance name: " << instanceName;
            state = State::addRemoteState(refuuid, proxyName, instanceName);
        }

        else if (boost::iequals(stateType, "DynamicRemoteState"))
        {
            const std::string instanceName = stateNode.attribute_value("name");
            ARMARX_DEBUG << "Adding dynamic remote state with instance name: " << instanceName;
            state = State::addDynamicRemoteState(instanceName);
        }
        else if (boost::iequals(stateType, "EndState"))
        {
            const std::string eventName = stateNode.attribute_value("name");
            ARMARX_DEBUG << "Adding end state with event " << eventName;
            EventPtr evt = StateUtility::createEvent(eventName);
            StatePtr  state = FinalState<>::createState(evt);
            state = addState(state);
        }

        else
        {
            throw LocalException("Unknown state type in XML - found state type: ") << stateType;
        }

        return state;
    }

    template <typename StateType>
    StatePtr XMLStateTemplate<StateType>::addState(StatePtr state)
    {


        StateBase::__checkPhase(StateBase::eSubstatesDefinitions, __PRETTY_FUNCTION__);

        if (State::findSubstateByName(state->stateName))
        {
            throw exceptions::local::eStatechartLogicError("There exists already a substate with name '" + state->StateBase::stateName + "' in this hierarchy level. In one hierarchy level (aka one substatelist) the names must be unique.");
        }

        if (state->stateName.empty())
        {
            throw exceptions::local::eStatechartLogicError("The statename must not be empty");
        }


        state->__setParentState(this);
        this->StateBase::subStateList.push_back(state);
        state->init(this->StateBase::context, this->StateBase::manager);

        return state;
    }





    template <typename StateType>
    void XMLStateTemplate<StateType>::addTransitions(const RapidXmlReaderNode& transitionsNode)
    {

        if (!transitionsNode.is_valid())
        {
            return;
        }

        for (RapidXmlReaderNode curTransitionNode = transitionsNode.first_node("Transition");
             curTransitionNode.is_valid();
             curTransitionNode = curTransitionNode.next_sibling("Transition"))
        {
            const std::string eventName = curTransitionNode.attribute_value("eventName");

            const std::string sourceStateName = curTransitionNode.attribute_value("from");

            if (!curTransitionNode.has_attribute("to"))
            {
                ARMARX_WARNING << "Skipping detached transition " << eventName;
                continue;
            }

            const std::string destinationStateName = curTransitionNode.attribute_value("to");


            StateBasePtr source = State::findSubstateByName(sourceStateName);
            StateBasePtr destination = State::findSubstateByName(destinationStateName);

            if (!source)
            {
                throw LocalException("Could not find source state with name :") << sourceStateName;
            }

            if (!destination)
            {
                throw LocalException("Could not find source state with name :") << destinationStateName;
            }

            if (eventName.empty())
            {
                throw LocalException("Event name must not bet empty");
            }

            EventPtr event = State::createEvent(eventName);
            ARMARX_DEBUG << "Adding Transition on event " << eventName;
            ParameterMappingPtr mappingToNextStateInput = getMapping(curTransitionNode.first_node("ParameterMappings"));
            ParameterMappingPtr mappingsToParentsLocal = getMapping(curTransitionNode.first_node("ParameterMappingsToParentsLocal"));
            ParameterMappingPtr mappingsToParentsOutput = getMapping(curTransitionNode.first_node("ParameterMappingsToParentsOutput"));

            State::addTransition(event, source, destination,
                                 mappingToNextStateInput, mappingsToParentsLocal, mappingsToParentsOutput);
        }
    }

    template <typename StateType>
    void XMLStateTemplate<StateType>::setStartState(const RapidXmlReaderNode& startNode)
    {
        if (!startNode.is_valid())
        {
            return;
        }

        PMPtr mapping = getMapping(startNode.first_node("ParameterMappings"));
        this->setInitState(State::findSubstateByName(startNode.attribute_value("substateName")),
                           mapping);

    }

    template <typename StateType>
    ParameterMappingPtr XMLStateTemplate<StateType>::getMapping(const RapidXmlReaderNode& mappingNode)
    {
        if (!mappingNode.is_valid())
        {
            return 0;
        }

        PMPtr mapping = PM::createMapping();

        for (RapidXmlReaderNode curMappingNode = mappingNode.first_node("ParameterMapping");
             curMappingNode.is_valid();
             curMappingNode = curMappingNode.next_sibling("ParameterMapping"))
        {
            MappingSource sourceType = PM::StringToMappingSource(curMappingNode.attribute_value("sourceType"));
            const std::string fromParamName = curMappingNode.attribute_value("from");
            const std::string targetParamName = curMappingNode.attribute_value("to");
            mapping->addMappingEntry(sourceType, fromParamName, targetParamName);
        }

        return mapping;
    }




}

#endif
