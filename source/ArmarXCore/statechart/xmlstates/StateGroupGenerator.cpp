#include "StateGroupGenerator.h"

#include <fstream>

#include "baseclassgenerator/XmlStateBaseClassGenerator.h"
#include <ArmarXCore/core/exceptions/local/FileIOException.h>

#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.h>

using namespace armarx;

void StatechartGroupGenerator::generateStatechartFiles(const std::string& statechartsPath)
{
    std::vector<std::string> groups;

    for (boost::filesystem::recursive_directory_iterator end, dir(statechartsPath); dir != end; ++dir)
    {
        // search for all statechart group xml files
        if (dir->path().extension() == ".scgxml" && dir->path().string().find("deprecated") == std::string::npos)
        {
            groups.push_back(dir->path().string());
            ARMARX_INFO_S << dir->path().string();
        }
    }
}

void StatechartGroupGenerator::generateStateFile(const std::string& statechartGroupXmlFilePath, const std::string& statePath, const std::string& packagePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfiles::StatechartProfilePtr()));
    reader->readXml(boost::filesystem::path(statechartGroupXmlFilePath));
    //ARMARX_INFO_S << reader->getPackageName();
    boost::filesystem::path buildDir = CMakePackageFinder(reader->getPackageName(), packagePath).getBuildDir();

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    boost::filesystem::path boostStatePath(statePath);
    generateStateFile(boostStatePath.filename().replace_extension().string(),
                      RapidXmlReader::FromFile(statePath),
                      buildDir,
                      reader->getPackageName(),
                      reader->getGroupName(),
                      reader->getProxies(),
                      reader->contextGenerationEnabled(),
                      variantInfo,
                      false);
}

bool StatechartGroupGenerator::generateStateFile(const std::string& stateName, RapidXmlReaderPtr reader, boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, bool contextGenerationEnabled, const VariantInfoPtr& variantInfo, bool forceRewrite)
{
    boost::filesystem::path outputPath = buildDir / "source" / packageName / "statecharts" / groupName / (stateName + ".generated.h");

    boost::filesystem::path dir = outputPath;
    dir.remove_filename();
    boost::filesystem::create_directories(dir);
    //ARMARX_INFO_S << "generating into " << outputPath.string();

    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp = XmlStateBaseClassGenerator::GenerateCpp(namespaces,
                      reader,
                      proxies,
                      contextGenerationEnabled,
                      groupName,
                      variantInfo);

    writeFileContents(outputPath.string() + ".touch", "");

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        return true;
    }
    else
    {
        return writeFileContentsIfChanged(outputPath.string(), cpp);
    }

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

void StatechartGroupGenerator::generateStatechartContextFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfiles::StatechartProfilePtr()));
    reader->readXml(boost::filesystem::path(statechartGroupXmlFilePath));

    if (!reader->contextGenerationEnabled())
    {
        throw LocalException("Will not generate context for ") << statechartGroupXmlFilePath << ". Context generation is not enabled for this group.";
    }

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    boost::filesystem::path buildDir = CMakePackageFinder(reader->getPackageName(), packagePath).getBuildDir();

    generateStatechartContextFile(buildDir,
                                  reader->getPackageName(),
                                  reader->getGroupName(),
                                  reader->getProxies(),
                                  variantInfo,
                                  false);
}

bool StatechartGroupGenerator::generateStatechartContextFile(boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, const VariantInfoPtr& variantInfo, bool forceRewrite)
{
    boost::filesystem::path outputPath = buildDir / "source" / packageName / "statecharts" / groupName / (groupName + "StatechartContext.generated.h");

    boost::filesystem::path dir = outputPath;
    dir.remove_filename();
    boost::filesystem::create_directories(dir);

    //ARMARX_INFO_S << "generating into " << outputPath.string();
    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp = XmlContextBaseClassGenerator::GenerateCpp(namespaces,
                      proxies,
                      groupName,
                      variantInfo);

    writeFileContents(outputPath.string() + ".touch", "");

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        return true;
    }
    else
    {
        return writeFileContentsIfChanged(outputPath.string(), cpp);
    }

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

bool StatechartGroupGenerator::writeFileContentsIfChanged(const std::string& path, const std::string& contents)
{
    if (boost::filesystem::exists(boost::filesystem::path(path))
        && RapidXmlReader::ReadFileContents(path) == contents)
    {
        return false;
    }

    writeFileContents(path, contents);
    return true;
}

void StatechartGroupGenerator::writeFileContents(const std::string& path, const std::string& contents)
{
    std::ofstream file;
    file.open(path.c_str());

    if (!file.is_open())
    {
        throw armarx::exceptions::local::FileOpenException(path);
    }

    file << contents;
    file.flush();
    file.close();
}

