#ifndef ARMARX_STATEGROUPDOCGENERATOR_H
#define ARMARX_STATEGROUPDOCGENERATOR_H

#include "GroupXmlReader.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/DoxTable.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/DoxDoc.h>

namespace armarx
{

    class StatechartGroupDocGenerator
    {
        struct StateInfo
        {
            StateInfo(std::string package, std::string group, std::string state)
                : package(package), group(group), state(state) {}
            std::string package, group, state;
            std::string toString() const
            {
                return package + "-" + group + "-" + state;
            }
        };

    public:
        StatechartGroupDocGenerator();
        std::string generateDocString(const StatechartGroupXmlReaderPtr& reader) const;
        void generateDoxygenFile(const std::string& groupDefinitionFilePath);
        void generateDoxygenFiles(const std::vector<std::string>& groups);

        void setOutputpath(const std::string& outputPath);

        static std::vector<std::string> FindAllStatechartGroupDefinitions(const boost::filesystem::path& path);

        CMakePackageFinder getFinder(const StatechartGroupXmlReaderPtr& reader);

        std::set<std::string> getUsedProfiles(const RapidXmlReaderNode& stateNode) const;
        void buildReferenceIndex(const std::vector<std::string>& groups);
    protected:
        //std::map<std::string, StatechartGroupXmlReaderPtr> readers;
        std::map<std::string, CMakePackageFinder> finders;
        std::string outputPath;
        std::map<std::string, StateInfo> uuidToStateInfoMap;
        std::map<std::string, std::set<std::string>> usageMap;
    private:
        DoxTablePtr buildParameterTable(RapidXmlReaderNode parametersNode) const;
        void addParameterTable(const DoxDocPtr& doc, const RapidXmlReaderNode& stateNode, std::string name, const char* nodeName) const;
        std::string unescapeString(std::string str) const;
        std::string nToBr(std::string str) const;
        std::string fmt(const std::string& fmt, const std::string& arg1) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3, const std::string& arg4) const;
        std::string getNodeAttrsFromStateType(const std::string& nodeType) const;
    };

} // namespace armarx

#endif // ARMARX_STATEGROUPDOCGENERATOR_H
