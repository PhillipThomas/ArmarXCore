/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core::Statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartManager.h"

#include "StateController.h"
#include "State.h"

using namespace  armarx;

StatechartManager::StatechartManager() :
    managerState(eConstructed),
    //    runningTaskMutex(10000, "runningTaskMutex")
    managerStateMutex(10000, "managerStateMutex")
{
    setTag("StatechartManager");
}

StatechartManager::~StatechartManager()
{
    shutdown();
}

bool StatechartManager::setToplevelState(const StatePtr& newToplevelState, StringVariantContainerBaseMap startParameters)
{
    if (isRunning())
    {
        return false;
    }

    toplevelState = newToplevelState;
    this->startParameters = startParameters;
    return true;
}

StatePtr StatechartManager::getToplevelState() const
{
    return toplevelState;
}

void StatechartManager::start(bool enterToplevelState)
{
    HiddenTimedMutex::ScopedLock lock(managerStateMutex);
    //    HiddenTimedMutex::scoped_lock lock(runningTaskMutex);
    ARMARX_VERBOSE << "starting StatechartManager";


    if (!runningTask && managerState != eShutDown)
    {
        if (toplevelState)
        {
            ARMARX_VERBOSE << "starting StatechartManagerTask";

            runningTask = new RunningTask<StatechartManager>(this, &StatechartManager::processEvents, "StatechartManager");
            runningTask->start();
            managerState = eRunning;
            lock.unlock();

            if (enterToplevelState)
            {
                toplevelState->enter(startParameters);
            }

            //            toplevelState = NULL;
        }
        else
        {
            throw LocalException("No toplevel state was set in StatechartManager!");
        }

    }
}

void
StatechartManager::shutdown()
{
    if (isShutdown())
    {
        return;
    }

    {
        //        HiddenTimedMutex::scoped_lock lock(runningTaskMutex);
        HiddenTimedMutex::ScopedLock lock2(managerStateMutex);
        managerState = eShutDown;
    }

    //    ARMARX_INFO << "RefCount: " << toplevelState->IceUtil::Shared::__getRef();
    if (toplevelState)
    {
        toplevelState->disableRunFunction();
    }

    toplevelState = NULL; // delete so that there are not ring pointers


    // first issue stop, then wake up, then wait
    {
        ScopedLock lock(eventQueueMutex); // necessary?

        if (runningTask)
        {
            runningTask->stop(false);
        }

        idleCondition.notify_one();
    }

    if (runningTask)
    {
        runningTask->waitForFinished();
    }


    ARMARX_VERBOSE << "StatechartManager is shutdown!";
}

bool StatechartManager::isRunning() const
{
    HiddenTimedMutex::ScopedLock lock(managerStateMutex);

    if (managerState == eRunning)
    {
        return true;
    }

    return false;
}

bool StatechartManager::isShutdown() const
{
    HiddenTimedMutex::ScopedLock lock(managerStateMutex);

    if (managerState == eShutDown)
    {
        return true;
    }

    return false;
}

StatechartManager::StatechartManagerState StatechartManager::getManagerState() const
{
    HiddenTimedMutex::ScopedLock lock(managerStateMutex);
    return managerState;
}

void StatechartManager::wakeUp()
{
    idleCondition.notify_all();
}

bool StatechartManager::addEvent(const EventPtr& newEvent, const StateControllerPtr& receivingState)
{
    //    HiddenTimedMutex::scoped_lock lock2(runningTaskMutex);
    {
        HiddenTimedMutex::ScopedLock lock2(managerStateMutex);

        if (!runningTask || !runningTask->isRunning())
        {
            return false;
        }

        if (!newEvent || !receivingState)
        {
            return false;
        }

    }
    EventProcessingData data;
    data.event = newEvent;
    data.receivingState = receivingState;
    /*    if(!checkEvent(data))
        {
            ARMARX_INFO << "event skipped: " << newEvent->eventName;
    //        return true;
        }*/

    ScopedLock lock(eventQueueMutex);
    eventQueue.push_back(data);
    //    ARMARX_INFO << "New event: " << newEvent->eventName << " for " << receivingState->getStateName();
    idleCondition.notify_one();
    return true;
}

void
StatechartManager::processEvents()
{

    while (!runningTask->isStopped())
    {
        EventProcessingData nextEvent;
        {
            // just retrieve the event and release the lock
            ScopedLock lock(eventQueueMutex);

            if (eventQueue.size() > 0)
            {
                nextEvent = eventQueue.front();
                eventQueue.pop_front();
            }
        }

        try
        {
            if (nextEvent.receivingState)
            {
                nextEvent.receivingState->__processEvent(nextEvent.event);
            }
        }
        catch (...)
        {
            handleExceptions();
        }

        ScopedLock lock(eventQueueMutex); // lock so that no events can be added

        // between size checking and going to sleep AND that shutdown cannot be issued after isStopped() was checked
        if (runningTask->isStopped())
        {
            break;
        }

        if (eventQueue.size() == 0)
        {
            // wait in idle mode until new events are available
            //            ARMARX_VERBOSE << "Waiting...";
            idleCondition.wait(lock);
        }
    }
}

bool StatechartManager::checkEvent(const StatechartManager::EventProcessingData& eventData) const
{
    ScopedLock lock(eventQueueMutex);
    std::list<EventProcessingData>::const_iterator it = eventQueue.begin();

    for (; it != eventQueue.end(); it++)
    {
        if (eventData.receivingState == it->receivingState)
        {
            return false;
        }
    }

    return true;
}
