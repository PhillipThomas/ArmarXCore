/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_STATETEMPLATE_H
#define _ARMARX_CORE_STATETEMPLATE_H

#include "State.h"

#include <cxxabi.h>

namespace armarx
{

    /**
    @class StateTemplate
    @ingroup StatechartGrp
    Template class from which all states with additional functionality like an overriden onEnter() function must be derived.
    */
    template <class StateType>
    class StateTemplate :
        virtual public State
    {

    protected:
        StateTemplate();
        ~StateTemplate() {}
    public:
        // this typedef is for later checking if the inheritance is correct (e.g. in addState)
        typedef StateType Type;
        /*!
        * @brief Creates a new state instance of the type of the template parameter.
        * @param The name the new state should have.
        * @return New state instance
        */
        static IceInternal::Handle<StateType> createInstance(std::string stateName = "");

        /*!
        * @brief Creates a copy of this state.
        *
        * All substates and parameters are cloned as well.
        * @return New state instance
        */
        StateBasePtr clone() const;

        /*!
        * @brief Creates a copy of this state.
        * @return New state instance
        */
        StateBasePtr createEmptyCopy();
    };



    ///////////////////////////////////////////////////////////////
    //////     Implementations of StateTemplate
    ///////////////////////////////////////////////////////////////

    template <class StateType>
    StateTemplate<StateType>::StateTemplate(): State()
    {
        BOOST_STATIC_ASSERT_MSG((boost::is_base_of<StateTemplate, StateType>::value), "The template parameter of StateTemplate, must be a class that derives from StateTemplate");

        // get the class name of this state
        static Mutex classNameMutex;
        ScopedLock lock(classNameMutex);
        static std::string className;

        if (className.empty())
        {
            // only get the name once for each state class (each template version creates it own version of this function and so it's own static variable)
            StateType* state;
            char* demangled = NULL;
            int status = -1;
            demangled = abi::__cxa_demangle(typeid(state).name(), NULL, 0, &status);
            className = demangled;

            if (className.size() > 1)
            {
                className = className.substr(0, className.size() - 1);
            }

            free(demangled);
        }

        boost::replace_all(className, ":", "_");
        stateClassName = className;

    }

    template <class StateType>
    IceInternal::Handle<StateType> StateTemplate<StateType>::createInstance(std::string stateName)
    {
        IceInternal::Handle<StateType> ptr = new StateType();

        if (stateName.empty())
        {
            if (ptr->stateClassName.empty())
            {
                throw LocalException("StateName and StateClassName are empty - cannot create state instance");
            }

            stateName = ptr->stateClassName;
        }

        ptr->setTag(stateName);
        ptr->stateName = stateName;
        return ptr;
    }

    template <class StateType>
    StateBasePtr StateTemplate<StateType>::clone() const
    {
        StatePtr result = new StateType(*dynamic_cast<const StateType*>(this));
        return result;
    }

    template <class StateType>
    StateBasePtr StateTemplate<StateType>::createEmptyCopy()
    {
        return new StateType();
    }

}
#endif
