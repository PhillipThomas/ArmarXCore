/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateParameter.h"

using namespace armarx;
StateParameter::StateParameter()
{
}

StateParameter::StateParameter(const StateParameter& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    StateParameterIceBase(source)
{
    *this = source;
}

StateParameter& StateParameter::operator =(const StateParameter& rhs)
{
    optionalParam = rhs.optionalParam;
    this->set = rhs.set;

    if (rhs.value)
    {
        value = rhs.value->cloneContainer();
    }

    if (rhs.defaultValue)
    {
        defaultValue = rhs.defaultValue->cloneContainer();
    }

    return *this;
}

StateParameterPtr StateParameter::create()
{
    return new StateParameter();
}

StateParameterIceBasePtr StateParameter::clone(const Ice::Current&)
{
    return new StateParameter(*this);
}
