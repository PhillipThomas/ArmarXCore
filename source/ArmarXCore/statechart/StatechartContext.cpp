/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "StatechartContext.h"
#include "State.h"
#include "StatechartManager.h"
#include "ParameterMapping.h"

#include "../core/application/Application.h"
#include "../statechart/StatechartObjectFactories.h"
#include "../observers/ObserverObjectFactories.h"
#include "../observers/exceptions/local/InvalidChannelException.h"
#include "../observers/variant/DatafieldRef.h"

namespace armarx
{
    StatechartContextPropertyDefinitions::StatechartContextPropertyDefinitions(std::string prefix) :
        ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<int>("ProfilingDepth", -1, "-1 to profile all hierarchy levels; any positive number to stop profiling after a certain depth in the state hierarchy");
    }



    StatechartContext::StatechartContext()
    {
        autoEnterToplevelState = true;
        statechartManager = new StatechartManager();
        setTag("StatechartContext");
    }

    PropertyDefinitionsPtr StatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new StatechartContextPropertyDefinitions(getConfigIdentifier()));
    }


    void StatechartContext::onInitComponent()
    {
        getProfiler()->setName(getDefaultName());
        usingProxy("ConditionHandler");
        usingProxy("SystemObserver");
        eventDistributor = ManagedIceObject::create<StatechartEventDistributor>();
        eventDistributor->setStatechartName(getDefaultName());
        eventDistributor->setStatechartManager(statechartManager);

        getArmarXManager()->addObject(ManagedIceObjectPtr::dynamicCast(eventDistributor), false);

        PM::_setStatechartContext(this);

        onInitStatechartContext();
        onInitStatechart();
    }

    void StatechartContext::onConnectComponent()
    {
        ARMARX_VERBOSE << "Starting StatechartContext\n" << flush;

        conditionHandlerPrx = getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");
        systemObserverPrx = getProxy<SystemObserverInterfacePrx>("SystemObserver");

        onConnectStatechartContext();
        onConnectStatechart();
        startStatechart();
    }

    void StatechartContext::onDisconnectComponent()
    {
        eventDistributor->clearEventMap();
    }

    void StatechartContext::onExitComponent()
    {

        statechartManager->shutdown();
        onExitStatechart();
        ARMARX_VERBOSE << "shutting statechart down" << flush;
    }


    VariantBaseList
    StatechartContext::getDataListFromObserver(std::string observerName, const DataFieldIdentifierBaseList& identifierList)
    {
        if (observerMap.find(observerName) == observerMap.end())
        {
            observerMap[observerName] = getProxy<ObserverInterfacePrx>(observerName);
        }

        return observerMap[observerName]->getDataFields(identifierList);
    }

    VariantBasePtr StatechartContext::getDataFromObserver(const DataFieldIdentifierBasePtr& identifier)
    {
        if (observerMap.find(identifier->observerName) == observerMap.end())
        {
            observerMap[identifier->observerName] = getProxy<ObserverInterfacePrx>(identifier->observerName);
        }

        return observerMap[identifier->observerName]->getDataField(identifier);
    }

    ChannelRefPtr StatechartContext::getChannelRef(const std::string& observerName, const std::string& channelName)
    {
        ObserverInterfacePrx obs;
        boost::unordered_map<std::string, ObserverInterfacePrx>::iterator it = observerMap.find(observerName);

        if (it == observerMap.end())
        {
            try
            {
                obs = getProxy<ObserverInterfacePrx>(observerName);
                ObserverInterfacePrx::checkedCast(obs);
            }
            catch (...)
            {
                throw exceptions::local::InvalidChannelException(observerName + "." + channelName);
            }

            observerMap[observerName] = obs;
        }
        else
        {
            obs = it->second;
        }

        return new ChannelRef(obs, channelName);
    }

    DatafieldRefPtr StatechartContext::getDatafieldRef(const DataFieldIdentifier& datafieldIdentifier)
    {
        ChannelRefPtr channel = getChannelRef(datafieldIdentifier.getObserverName(), datafieldIdentifier.getChannelName());
        return new DatafieldRef(channel, datafieldIdentifier.getDataFieldName());
    }

    DatafieldRefPtr StatechartContext::getDatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName)
    {
        return new DatafieldRef(channelRef, datafieldName);
    }

    bool StatechartContext::setToplevelState(const StatePtr& newToplevelState, StringVariantContainerBaseMap startParameters)
    {
        newToplevelState->init(this, statechartManager.get());

        return statechartManager->setToplevelState(newToplevelState, startParameters);
    }

    void StatechartContext::setAutoEnterToplevelState(bool autoEnter)
    {
        autoEnterToplevelState = autoEnter;
    }

    void StatechartContext::startStatechart()
    {
        statechartManager->start(autoEnterToplevelState);
    }
}
