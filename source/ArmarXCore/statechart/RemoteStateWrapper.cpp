/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "RemoteStateOfferer.h"
#include "RemoteStateWrapper.h"

namespace armarx
{
    ////////////////////////////////////////////////////////////////////////////////
    //////////// RemoteStateWrapper
    ////////////////////////////////////////////////////////////////////////////////

    RemoteStateWrapper::RemoteStateWrapper(StatePtr realState, RemoteStateIceBasePrx  callerStatePrx, RemoteStateOffererIceBasePtr handlerPtr):
        callerStatePrx(callerStatePrx),
        realState(realState),
        handlerPtr(handlerPtr)
    {
        stateName = "remoteStateWrapper_of_" + realState->stateName;
        activeSubstate = realState;

        if (callerStatePrx)
        {
            StateIceBasePtr remoteParentStateLayout = callerStatePrx->getParentStateLayout();

            if (remoteParentStateLayout)
            {
                this->subStateList = remoteParentStateLayout->subStateList;
                this->transitions = remoteParentStateLayout->transitions;
                this->initState = remoteParentStateLayout->initState;
            }
        }
    }
    RemoteStateWrapper::RemoteStateWrapper(const RemoteStateWrapper& source):
        Shared(),
        Ice::Object(source),
        StateIceBase(source),
        Logging(source),
        StateBase(source),
        StateController(source),
        StateUtility(source),
        State(source)

    {
        realState = StatePtr::dynamicCast(source.realState->clone());

        if (source.realState && !realState)
        {
            throw exceptions::local::eNullPointerException("Could not cast from StateBasePtr to StatePtr");
        }

        callerStatePrx = source.callerStatePrx;
        handlerPtr = source.handlerPtr;
    }

    void
    RemoteStateWrapper::__processBufferedEvents()
    {
        ARMARX_LOG << eINFO << "calling remoteprocessbufferedevets" << "" << flush;

        //call remoteProcessBufferedEvents over ice
        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteProcessBufferedEvents();
        }
    }



    void RemoteStateWrapper::__processEvent(const EventPtr ev, bool buffered)
    {
        ARMARX_LOG << eINFO  <<  "entering processEvent() of Wrapper" << "" << flush;

        //call substatesFinished over ice
        if (!callerStatePrx._ptr)
        {
            throw UserException("Proxy wasnt set.");
        }

        if (callerStatePrx._ptr)
        {
            callerStatePrx->remoteProcessEvent(ev, buffered);
        }

        //        ARMARX_IMPORTANT << "processEvent DONE";

    }

    void RemoteStateWrapper::__enqueueEvent(const EventPtr event)
    {
        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteEnqueueEvent(event);
        }

    }

    void RemoteStateWrapper::__propagateUpdate(bool directionUp)
    {
        if (handlerPtr)
        {
            handlerPtr->notifySubscribers(realState);
        }
        else
        {
            throw exceptions::local::eNullPointerException("HandlerPtr is NULL");
        }

        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remotePropagateUpdate();
        }

        //        else
        //            ARMARX_WARNING << "callerStatePrx of '" << realState->globalStateIdentifier << "' is NULL" << flush;
    }

    unsigned int
    RemoteStateWrapper::__getUnbreakableBufferSize() const
    {
        ARMARX_FATAL << "__getUnbreakableBufferSize";

        if (callerStatePrx._ptr)
        {
            return callerStatePrx->getRemoteUnbreakableBufferSize();
        }
        else
        {
            return 0;
        }
    }

    bool RemoteStateWrapper::__getUnbreakableBufferStati() const
    {
        if (callerStatePrx._ptr)
        {
            return callerStatePrx->getRemoteUnbreakableBufferStati();
        }
        else
        {
            return true;
        }
    }


    void RemoteStateWrapper::__finalize(const EventPtr event)
    {
        //realState->baseOnExit();
        ARMARX_LOG << eINFO << "sending remote finalize " << realState._ptr << "" << flush;

        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteFinalize(StateUtilFunctions::getSetValues(realState->getOutputParameters()), event);
        }
    }

    StateBasePtr RemoteStateWrapper::clone() const
    {
        StateBasePtr result = new RemoteStateWrapper(*this);
        return result;
    }

    StateBasePtr RemoteStateWrapper::createEmptyCopy() const
    {
        assert(0);
        return new RemoteStateWrapper(NULL, callerStatePrx, handlerPtr);
    }
}
