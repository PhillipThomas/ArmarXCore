
#ifndef ARMARX_FILTERS_MEDIANFILTER_H
#define ARMARX_FILTERS_MEDIANFILTER_H
#include "DatafieldFilter.h"

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    namespace filters
    {

        /**
         * @class MedianFilter
         * @ingroup ObserverFilters
         * @brief The MedianFilter class provides an implementation
         *  for a median for datafields of type float, int and double.
         */
        class MedianFilter :
            virtual public MedianFilterBase,
            virtual public DatafieldFilter
        {
        public:
            MedianFilter(int windowSize = 11)
            {
                this->windowFilterSize = windowSize;
            }

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current&) const
            {
                if (dataHistory.size() == 0)
                {
                    return NULL;
                }

                VariantTypeId type = dataHistory.begin()->second->getType();

                if (type == VariantType::Float)
                {
                    auto values = SortVariants<float>(dataHistory);
                    return new Variant(values.at(values.size() / 2));
                }
                else if (type == VariantType::Double)
                {
                    auto values = SortVariants<double>(dataHistory);
                    return new Variant(values.at(values.size() / 2));
                }
                else if (type == VariantType::Int)
                {
                    auto values = SortVariants<int>(dataHistory);
                    return new Variant(values.at(values.size() / 2));
                }

                throw exceptions::user::UnsupportedTypeException(type);
            }

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current&) const
            {
                ParameterTypeList result;
                result.push_back(VariantType::Int);
                result.push_back(VariantType::Float);
                result.push_back(VariantType::Double);
                return result;
            }

            template <typename Type>
            static std::vector<Type> SortVariants(const TimeVariantBaseMap& map)
            {
                std::vector<Type> values;

                for (auto v : map)
                {
                    VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                    values.push_back(v2->get<Type>());
                }

                std::sort(values.begin(), values.end());
                return values;
            }


        };

    } // namespace Filters
} // namespace armarx

#endif // ARMARX_FILTERS_MEDIANFILTER_H
