
#ifndef ARMARX_FILTERS_GAUSSIANFILTER_H
#define ARMARX_FILTERS_GAUSSIANFILTER_H
#include "DatafieldFilter.h"

#include <ArmarXCore/interface/observers/Filters.h>

namespace armarx
{
    namespace filters
    {


        /**
         * @class  GaussianFilter
         * @ingroup ObserverFilters
         * @brief The GaussianFilter class provides a filter implemtentation
         * with gaussian weighted values for datafields of type float, int and double.
         */
        class GaussianFilter :
            public DatafieldFilter,
            public GaussianFilterBase
        {
        public:
            /**
             * @brief GaussianFilter
             * @param filterSizeInMs Width of the gauss function
             * @param windowSize size of the filter window
             */
            GaussianFilter(int filterSizeInMs = 200, int windowSize = 20)
            {
                this->filterSizeInMs = filterSizeInMs;
                this->windowFilterSize = windowSize;
            }


            // DatafieldFilterBase interface
        public:
            template <typename Type>
            Type calcGaussianFilteredValue(const TimeVariantBaseMap& map) const
            {
                const double sigma = filterSizeInMs / 2.5;
                //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

                double weightedSum = 0;
                double sumOfWeight = 0;


                const double sqrt2PI = sqrt(2 * M_PI);


                for (auto it = map.begin();
                     it != map.end();
                     it++
                    )
                {
                    double value;
                    value = VariantPtr::dynamicCast(it->second)->get<Type>();
                    double diff = 0.001 * (it->first - map.rbegin()->first);

                    double squared = diff * diff;
                    const double gaussValue = exp(-squared / (2 * sigma * sigma)) / (sigma * sqrt2PI);
                    sumOfWeight += gaussValue;
                    weightedSum += gaussValue * value;

                }

                double result;
                result = weightedSum / sumOfWeight;
                return result;
            }

            VariantBasePtr calculate(const Ice::Current&) const
            {
                if (dataHistory.size() == 0)
                {
                    return NULL;
                }

                VariantTypeId type = dataHistory.begin()->second->getType();

                if (type == VariantType::Float)
                {
                    return new Variant(calcGaussianFilteredValue<float>(dataHistory));
                }
                else if (type == VariantType::Double)
                {
                    return new Variant(calcGaussianFilteredValue<double>(dataHistory));
                }
                else if (type == VariantType::Int)
                {
                    return new Variant(calcGaussianFilteredValue<int>(dataHistory));
                }

                throw exceptions::user::UnsupportedTypeException(type);
            }

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current&) const
            {
                ParameterTypeList result;
                result.push_back(VariantType::Int);
                result.push_back(VariantType::Float);
                result.push_back(VariantType::Double);
                return result;
            }


        };

    } // namespace filters
} // namespace armarx

#endif // ARMARX_FILTERS_GAUSSIANFILTER_H
