/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_EVENT_H
#define _ARMARX_CORE_EVENT_H

#include <ArmarXCore/core/system/ImportExport.h>

#include <ArmarXCore/interface/observers/Event.h>
//#include <IceUtil/Handle.h>




namespace armarx
{
    class Variant;
    class Event;
    /**
     * Typedef of EventPtr as IceInternal::Handle<Event> for convenience.
     */
    typedef IceInternal::Handle<Event> EventPtr;

    /**
     * @class Event
     * @brief An Event is used to communicate between e.g. condition handlers and statecharts.
     * @ingroup ObserversGrp
     *
     * The receiver is always a statechart.
     * It contains a dictionary for additional data of any kind.
     */
    class ARMARXCORE_IMPORT_EXPORT Event :
        virtual public EventBase
    {
    public:
        Event();

        Event(std::string eventReceiverName, std::string eventName);


        EventPtr add(const std::string key, const Variant& value);
        EventPtr add(const std::string key, const VariantContainerBasePtr& valueContainer);

        Ice::ObjectPtr ice_clone() const;

        virtual EventPtr clone() const;
    };


}

#endif
