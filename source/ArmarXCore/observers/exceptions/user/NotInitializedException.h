/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_NOTINITIALIZEDEXCEPTIONTYPEEXCEPTION_H
#define _ARMARX_CORE_NOTINITIALIZEDEXCEPTIONTYPEEXCEPTION_H


#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace user
        {
            class NotInitializedException: public armarx::NotInitializedException
            {
            public:

                NotInitializedException(const std::string& fieldName = "UnknownField")
                {
                    this->fieldName = fieldName;
                }

                ~NotInitializedException() throw() { }

                virtual std::string ice_name() const
                {
                    return "armarx::exceptions::user::NotInitializedException";
                }

                // exception interface
                const char* what() const throw()
                {
                    return std::string(fieldName + " was not initialized").c_str();
                }
            };

        }
    }
}

#endif
