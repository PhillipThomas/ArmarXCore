/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke
* @date       2011 Humanoids Group, HIS, KIT
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_INVALIDTYPEEXCEPTION_H_
#define _ARMARX_CORE_INVALIDTYPEEXCEPTION_H_

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace user
        {
            class InvalidTypeException: public armarx::InvalidTypeException
            {
            public:

                InvalidTypeException(VariantTypeId typeId1, VariantTypeId typeId2)
                {
                    std::stringstream sstream;
                    sstream << "Type1: " << Variant::typeToString(typeId1) << " Type2: " << Variant::typeToString(typeId2);
                    reason = sstream.str();
                }
                InvalidTypeException(std::string typeId1, std::string typeId2)
                {
                    std::stringstream sstream;
                    sstream << "Type1: " << typeId1 << " Type2: " << typeId2;
                    reason = sstream.str();

                }

                ~InvalidTypeException() throw() { };

                virtual std::string ice_name() const
                {
                    return "armarx::exceptions::user::InvalidTypeException";
                };
            };
            class UnsupportedTypeException: public armarx::UnsupportedTypeException
            {
            public:

                UnsupportedTypeException(VariantTypeId typeId)
                {
                    std::stringstream sstream;
                    sstream <<  "Unsupported Type: " << Variant::typeToString(typeId);
                    reason = sstream.str();
                }
                UnsupportedTypeException(const std::string& typeId)
                {
                    std::stringstream sstream;
                    sstream <<  "Unsupported Type: " << typeId;
                    reason = sstream.str();

                }

                ~UnsupportedTypeException() throw() { };

                virtual std::string ice_name() const
                {
                    return "armarx::exceptions::user::UnsupportedTypeException";
                };
            };
        };
    };
};

#endif
