/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Tools
* @author     Kai Welke
* @date       2011 Humanoids Group, HIS, KIT
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_INVALIDDATAFIELDEXCEPTION_H_
#define _ARMARX_CORE_INVALIDDATAFIELDEXCEPTION_H_

#include <ArmarXCore/core/exceptions/Exception.h>

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            class InvalidDataFieldException: public armarx::LocalException
            {
            public:
                std::string channelName;
                std::string datafieldName;

                InvalidDataFieldException(std::string channelName, std::string datafieldName)
                    : channelName(channelName),
                      datafieldName(datafieldName)
                {
                    std::stringstream sstream;
                    sstream << "The dataField " << datafieldName << " is not valid for channel " << channelName << ".";
                    setReason(sstream.str());
                }

                ~InvalidDataFieldException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::InvalidDataFieldException";
                }
            };

            class IncompleteTypeException: public armarx::LocalException
            {
            public:
                int typeId;
                std::string foundType;

                IncompleteTypeException(int typeId, const std::string& foundTypeStr):
                    typeId(typeId),
                    foundType(foundTypeStr)
                {
                    std::stringstream sstream;
                    sstream << "Found an incomplete type with id " << typeId << " - found the following type: " << foundType;
                    setReason(sstream.str());
                }

                ~IncompleteTypeException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::IncompleteTypeException";
                }
            };
        };
    };
};

#endif
