#ifndef _ARMARX_DEBUGOBSERVER_H
#define _ARMARX_DEBUGOBSERVER_H

#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

namespace armarx
{

    /**
     * @defgroup Component-DebugObserver DebugObserver
     * @ingroup ObserversSub ArmarXCore-Components
     * Logging of frequently updated values to the console with printf, cout etc.
     * pollutes the console to an unreadable degree.
     * Therefore, the DebugObserver exists. The user can log data from anywhere
     * in the armarx framework by creating temporary observer channels on the DebugObserver.
     * These channels can be inspected in the armarx::ObserverGuiPlugin or, in the case of floats or ints,
     * in the armarx::ArmarXPlotter Widget.
     *
     * To interact with the DebugObserver, it need to be started since it is a seperate component (\ref DebugObserverApp).
     * Then the application needs to retrieve a proxy to this object (armarx::IceManager::getProxy(), \ref HowtoGetProxy).
     * With the proxy and the function setDebugDatafield() a channel and datafield can be created or
     * updated (if it already exists).
     * Unneeded channels or datafields can be removed with removeDebugChannel() resp. removeDebugDatafield().
     *
     * @class DebugObserver
     * @ingroup Component-DebugObserver
     * @brief The DebugObserver is a component for logging debug data, which is updated frequently.
     */
    class DebugObserver :
        public Observer,
        public DebugObserverInterface
    {
    public:
        DebugObserver();

        // framework hooks
        virtual std::string getDefaultName() const
        {
            return "DebugObserver";
        }
        void onInitObserver();
        void onConnectObserver();

        /**
         * @brief Creates or updates (if it already exists) a datafield in a channel.
         * @param channelName Name of the channel (anything the users likes)
         * @param datafieldName Name of the datafield (anything the users likes)
         * @param value A Variant that the user wants to log
         */
        void setDebugDatafield(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value, const Ice::Current& c = Ice::Current());
        void setDebugChannel(const std::string& channelName, const StringVariantBaseMap& valueMap, const Ice::Current& c = Ice::Current());
        /**
         * @brief Removes a datafield from the DebugObserver. Should be called to clean up the DebugObserver.
         * @param channelName
         * @param datafieldName
         */
        void removeDebugDatafield(const ::std::string& channelName, const ::std::string& datafieldName, const ::Ice::Current& = ::Ice::Current());
        /**
         * @brief Removes a channel and all its datafield it has.
         * @param channelName
         */
        void removeDebugChannel(const ::std::string& channelName, const ::Ice::Current& = ::Ice::Current());
        /**
         * @brief Removes all channels.
         */
        void removeAllChannels(const ::Ice::Current& = ::Ice::Current());
    };

}

#endif
