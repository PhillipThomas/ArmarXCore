/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::observers::ProfilerObserver
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerObserver.h"

#include "checks/ConditionCheckEquals.h"
#include "checks/ConditionCheckUpdated.h"


std::string armarx::ProfilerObserver::getDefaultName() const
{
    return "ProfilerObserver";
}


void armarx::ProfilerObserver::onInitObserver()
{
    usingTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
    //offeringTopic("PredictionTopic");
}


void armarx::ProfilerObserver::onConnectObserver()
{
    offerChannel("activeStateChanged", "Channel reporting the currently active state of statecharts with EnableProfiling property set");
    offerDataField("activeStateChanged", "activeState", VariantType::String, "Name of the currently active state");
    offerConditionCheck("equals", new ConditionCheckEquals());
    offerConditionCheck("updated", new ConditionCheckUpdated());
    setDataField("activeStateChanged", "activeState", "");
    //    StatechartListenerPrx? the other is defined in PnP
    //    predictionListenerPrx = getTopic<PredictionListenerPrx>("PredictionTopic");
}


void armarx::ProfilerObserver::reportNetworkTraffic(const std::string& id, const std::string& protocol, Ice::Int inBytes, Ice::Int outBytes, const Ice::Current&)
{
    try
    {
        if (!existsChannel(id))
        {
            offerChannel(id, "Network traffic");
        }

        Variant in;
        in.setInt(inBytes);
        offerOrUpdateDataField(id, protocol + "_inBytes", in, "Incoming network traffic");
        Variant out;
        out.setInt(outBytes);
        offerOrUpdateDataField(id, protocol + "_outBytes", out, "Outgoing network traffic");
        updateChannel(id);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportEvent(const armarx::ProfilerEvent&, const Ice::Current&)
{
    // ignore these events in the observer
}

void armarx::ProfilerObserver::reportProcessCpuUsage(const ProfilerProcessCpuUsage& process, const Ice::Current&)
{
    std::string id = process.processName + boost::lexical_cast<std::string>(process.processId);

    try
    {
        if (!existsChannel(id))
        {
            offerChannel(id, "Cpu Usage");
        }

        Variant in;
        in.setInt(process.cpuUsage);
        offerOrUpdateDataField(id, "Process Cpu Usage", in, "Incoming Cpu Usage");
        updateChannel(id);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName, const Ice::Current&)
{
    setDataField("activeStateChanged", "activeState", Variant(targetStateName));
    updateChannel("activeStateChanged");
    //    predictionListenerPrx->reportCurrentState(activeState);
}

void armarx::ProfilerObserver::reportEventList(const armarx::ProfilerEventList& events, const Ice::Current&)
{
    // ignore these events in the observer
}

void armarx::ProfilerObserver::reportStatechartTransitionList(const armarx::ProfilerTransitionList& transitions, const Ice::Current&)
{
    armarx::ProfilerTransition lastTransition = transitions.back();
    setDataField("activeStateChanged", "activeState", Variant(lastTransition.targetStateName));
    updateChannel("activeStateChanged");
}

void armarx::ProfilerObserver::reportProcessCpuUsageList(const ProfilerProcessCpuUsageList& processes, const Ice::Current&)
{
    reportProcessCpuUsage(processes.back());
}
