/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::units
 * @author     Erem Aksoy (eren dot aksoy at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LoggingExampleComponent.h"
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/VariantBase.h>

using namespace armarx;

//! [LoggingTutorial Component1]
void LoggingExampleComponent::onInitComponent()
{
    usingProxy("DebugObserver");
    ARMARX_INFO << " Logging Example Component is initiated" ;
    counter = 0;
    position = 0;
}

void LoggingExampleComponent::onConnectComponent()
{

    assignProxy(debugObserver, "DebugObserver");

    ARMARX_INFO << " Logging Example Component is connected" ;

    task = new PeriodicTask<LoggingExampleComponent>(this, &LoggingExampleComponent::run, 10, false, "LoggingExampleComponentTask");
    task->start();
}

void LoggingExampleComponent::onExitComponent()
{
    ARMARX_INFO << " Logging Example Component exits" ;

    if (task)
    {
        task->stop();
    }
}


PropertyDefinitionsPtr LoggingExampleComponent::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new LoggingExampleComponentPropertyDefinitions(getConfigIdentifier()));
}

void LoggingExampleComponent::run()
{
    // logging to console
    if (counter > 100)
    {
        ARMARX_WARNING << deactivateSpam(5) << " counter is more than 100" ;
    }

    if (counter > 1000)
    {
        ARMARX_ERROR << deactivateSpam(5) << " counter is more than 1000" ;
    }


    // logging to debug observed using separate channels
    debugObserver->setDebugDatafield("counterChannel", "counter", new Variant(counter));
    debugObserver->setDebugDatafield("positionChannel", "position", new Variant(position));


    // logging to debug observed via single channel using the map
    mapValues["positionX"] = new Variant(position);
    mapValues["positionY"] = new Variant(position * 2);
    mapValues["positionZ"] = new Variant(position / 5.0);
    debugObserver->setDebugChannel("mappedData", mapValues);

    // incrementing the dummy variables
    counter++;
    position += 0.1;
}

//! [LoggingTutorial Component1]
