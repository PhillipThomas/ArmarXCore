/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleUnitObserver.h"

#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>

armarx::ExampleUnitObserver::ExampleUnitObserver()
{
}

//! [ObserversDocumentation ObserverComponent4]
void armarx::ExampleUnitObserver::onInitObserver()
{
    usingTopic("PeriodicExampleValue");


    offerConditionCheck("larger", new ConditionCheckLarger());
    offerConditionCheck("smaller", new ConditionCheckSmaller());
}

void armarx::ExampleUnitObserver::onConnectObserver()
{
    offerChannel("exampleChannel", "A constand value periodically published by ExampleUnit");
    offerDataField("exampleChannel", "exampleDataFieldInt", VariantType::Int, "The value published by ExampleUnit (if it is in integer)");
    offerDataField("exampleChannel", "exampleDataFieldFloat", VariantType::Float, "Another field");
    offerDataField("exampleChannel", "exampleDataFieldType", VariantType::String, "The type of the value published by ExampleUnit");

}

void armarx::ExampleUnitObserver::reportPeriodicValue(const armarx::VariantBasePtr& value, const Ice::Current&)
{
    VariantPtr v = VariantPtr::dynamicCast(value);
    ARMARX_INFO << v;

    if (v->getType() == VariantType::Int)
    {
        setDataField("exampleChannel", "exampleDataFieldInt", v->getInt());
    }
    else if (v->getType() == VariantType::Float)
    {
        setDataField("exampleChannel", "exampleDataFieldFloat", v->getFloat());
    }

    setDataField("exampleChannel", "exampleDataFieldType", v->getTypeName());
    updateChannel("exampleChannel");
}
//! [ObserversDocumentation ObserverComponent4]

armarx::PropertyDefinitionsPtr armarx::ExampleUnitObserver::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ExampleUnitObserverPropertyDefinitions(getConfigIdentifier()));
}
