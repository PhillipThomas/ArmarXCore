/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::ParameterTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ArmarXManager.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/parameter/Parameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>

#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <ArmarXCore/observers/ObserverObjectFactories.h>

using namespace armarx;

BOOST_AUTO_TEST_CASE(VariantListSetAndGet)
{
    SingleTypeVariantListPtr lst = new SingleTypeVariantList(VariantType::Int);

    lst->addElement(new SingleVariant(2));
    lst->addVariant(2);

    BOOST_CHECK(lst->getElement<SingleVariant>(0)->get()->get<int>() == 2);
    BOOST_CHECK(lst->getVariant(0)->get<int>() == 2);

    BOOST_CHECK_THROW(lst->getVariant(0)->get<float>(), InvalidTypeException);
    BOOST_CHECK_THROW(lst->getElement<SingleTypeVariantList>(0), InvalidTypeException);
    BOOST_CHECK_THROW(lst->getVariant(2)->get<float>(), IndexOutOfBoundsException);
}


BOOST_AUTO_TEST_CASE(VariantMapSetAndGet)
{
    StringValueMapPtr lst = new StringValueMap(VariantType::String);

    lst->addElement("key", new SingleVariant("hello"));
    lst->addVariant("key2", "2");

    BOOST_CHECK(lst->getElement<SingleVariant>("key")->get()->get<std::string>() == "hello");
    BOOST_CHECK(lst->getVariant("key2")->get<std::string>() == "2");

    BOOST_CHECK_THROW(lst->getVariant("key")->get<float>(), InvalidTypeException);
    BOOST_CHECK_THROW(lst->getElement<SingleTypeVariantList>("key2"), InvalidTypeException);
    BOOST_CHECK_THROW(lst->getElement<SingleVariant>("noooo")->get()->get<float>(), IndexOutOfBoundsException);
}

BOOST_AUTO_TEST_CASE(ParameterVariantCopy)
{
    VariantPtr var = new Variant(2);

    ParameterPtr param = new VariantParameter();
    param->setVariant(var);

    ParameterPtr param2 = ParameterPtr::dynamicCast(param->clone());
    ParameterPtr param3 = param;

    VariantPtr rvar2 = VariantPtr::dynamicCast(param2->getVariant());
    VariantPtr rvar3 = VariantPtr::dynamicCast(param3->getVariant());

    BOOST_CHECK(rvar2->get<int>() == 2);
    BOOST_CHECK(rvar3->get<int>() == 2);
}

BOOST_AUTO_TEST_CASE(VariantListCreationFromString)
{
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    std::string containerStrId = SingleTypeVariantList::getTypePrefix();
    ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);
    Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(containerStrId);

    if (!objFac)
    {
        throw LocalException("Could not find ObjectFactory for string '" + containerStrId + "'");
    }


    SingleTypeVariantListPtr lst = SingleTypeVariantListPtr::dynamicCast(objFac->create(containerStrId));

    lst->setContainerType(VariantType::List(VariantType::Int).clone());
    lst->addElement(new SingleVariant(2));
    lst->addVariant(2);

    BOOST_CHECK(lst->getElement<SingleVariant>(0)->get()->get<int>() == 2);
    BOOST_CHECK(lst->getVariant(0)->get<int>() == 2);

    BOOST_CHECK_THROW(lst->getVariant(0)->get<float>(), InvalidTypeException);
    BOOST_CHECK_THROW(lst->getElement<SingleTypeVariantList>(0), InvalidTypeException);
}

BOOST_AUTO_TEST_CASE(VariantListMultiDimensional)
{
    SingleTypeVariantListPtr lst = new SingleTypeVariantList(VariantType::List(VariantType::Map(VariantType::Float)));
    SingleTypeVariantListPtr sublst = new SingleTypeVariantList(VariantType::Map(VariantType::Float));
    StringValueMapPtr map = new StringValueMap(VariantType::Float);
    map->addElement("left_shoulder_1", new SingleVariant(0.3f));
    sublst->addElement(map);
    lst->addElement(sublst);


    BOOST_CHECK(lst->getElement<SingleTypeVariantList>(0)->getElement<StringValueMap>(0)->getVariant("left_shoulder_1")->get<float>() == 0.3f);
    BOOST_CHECK_THROW(lst->getElement<SingleTypeVariantList>(0)->getElement<StringValueMap>(0)->getVariant("left_shoulder_1")->getString(), InvalidTypeException);

}

BOOST_AUTO_TEST_CASE(VariantListMultiDimensionalCopy)
{
    SingleTypeVariantListPtr lst = new SingleTypeVariantList(VariantType::List(VariantType::Map(VariantType::Float)));
    SingleTypeVariantListPtr sublst = new SingleTypeVariantList(VariantType::Map(VariantType::Float));
    StringValueMapPtr map = new StringValueMap(VariantType::Float);
    map->addVariant("left_shoulder_1", 0.3f);
    sublst->addElement(map);
    lst->addElement(sublst);

    SingleTypeVariantListPtr lstCopy = SingleTypeVariantListPtr::dynamicCast(lst->cloneContainer());
    BOOST_CHECK(lstCopy->getElement<SingleTypeVariantList>(0)->getElement<StringValueMap>(0)->getVariant("left_shoulder_1")->get<float>() == 0.3f);
}



BOOST_AUTO_TEST_CASE(VariantListMultiDimensionalWrongType)
{
    StringValueMapPtr map = new StringValueMap(VariantType::Float);
    BOOST_CHECK_THROW(map->addVariant("left_shoulder", "123"), exceptions::user::InvalidTypeException);
    SingleTypeVariantListPtr lst = new SingleTypeVariantList(VariantType::String);
    BOOST_CHECK_THROW(lst->addVariant(true), exceptions::user::InvalidTypeException);

}

//BOOST_AUTO_TEST_CASE(VariantListReadFromXML)
//{
//    std::string xmlStr = "<item0>0.3</item0><item1>0.1</item1>";
//    SingleTypeVariantListPtr lst = new SingleTypeVariantList(VariantType::Float);

//    BOOST_CHECK(lst->getElement<Variant>(0)->get<float>() == 0.3f);
//    BOOST_CHECK(lst->getElement<Variant>(1)->get<float>() == 0.1f);

//}

