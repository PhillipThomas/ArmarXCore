/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ROBOTAPI_EXAMPLE_UNIT_OBSERVER_H
#define _ARMARX_ROBOTAPI_EXAMPLE_UNIT_OBSERVER_H

#include <ArmarXCore/interface/observers/ExampleUnitObserverInterface.h>
#include <ArmarXCore/interface/observers/ExampleUnitInterface.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/core/application/properties/Properties.h>

namespace armarx
{
    class ExampleUnitObserverPropertyDefinitions : public ComponentPropertyDefinitions
    {
    public:
        ExampleUnitObserverPropertyDefinitions(std::string prefix) :
            ComponentPropertyDefinitions(prefix)
        {
            // No required properties
        }
    };

    //! [ObserversDocumentation ObserverComponent1]
    class ARMARXCORE_IMPORT_EXPORT ExampleUnitObserver :
        virtual public Observer,
        virtual public ExampleUnitObserverInterface
    {
    public:
        ExampleUnitObserver();

        virtual std::string getDefaultName() const
        {
            return "ExampleUnitObserver";
        }
        void onInitObserver();
        void onConnectObserver();

        void reportPeriodicValue(const VariantBasePtr& value, const ::Ice::Current& = ::Ice::Current());

        virtual PropertyDefinitionsPtr createPropertyDefinitions();
        using Observer::removeDatafield;
    };
    typedef IceInternal::Handle<ExampleUnitObserver> ExampleUnitObserverPtr;
    //! [ObserversDocumentation ObserverComponent1]
}

#endif

