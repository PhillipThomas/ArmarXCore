/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_OBSERVER_OBJECT_FACTORIES_H
#define _ARMARX_CORE_OBSERVER_OBJECT_FACTORIES_H

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/condition/Operations.h>
#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.h>
#include <ArmarXCore/interface/observers/Event.h>
#include <ArmarXCore/interface/observers/TermImplBase.h>
#include <ArmarXCore/interface/observers/ConditionCheckBase.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>
#include <Ice/ObjectFactory.h>
#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>

namespace armarx
{

    namespace ObjectFactories
    {

        /**
        * @class ObserverObjectFactories
        * @ingroup ObserversSub
        * @brief Factory that contains all Ice-Type implementations
        * and their Ice-Base-Class for registration with Ice.
        *
        */
        class ObserverObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories()
            {
                ObjectFactoryMap map;

                add<VariantBase, Variant>(map);
                add<VariantParameterBase, VariantParameter>(map);
                add<VariantListParameterBase, VariantListParameter>(map);
                add<DataFieldIdentifierBase, DataFieldIdentifier>(map);
                add<ChannelRefBase, ChannelRef>(map);
                add<DatafieldRefBase, DatafieldRef>(map);
                add<TimestampBase, TimestampVariant>(map);

                add<VariantContainerBase, ContainerDummy>(map);
                add<SingleVariantBase, SingleVariant>(map);
                add<SingleTypeVariantListBase, SingleTypeVariantList>(map);
                add<StringValueMapBase, StringValueMap>(map);
                add<ContainerType, ContainerTypeI>(map);


                // condition
                add<LiteralImplBase, LiteralImpl>(map);
                add<OperationAndBase, OperationAnd>(map);
                add<OperationOrBase, OperationOr>(map);
                add<OperationNotBase, OperationNot>(map);
                add<ConditionRootBase, ConditionRoot>(map);

                add<ConditionCheckBase, ConditionCheck>(map);
                add<EventBase, Event>(map);
                add<MedianFilterBase, filters::MedianFilter>(map);
                add<AverageFilterBase, filters::AverageFilter>(map);
                add<GaussianFilterBase, filters::GaussianFilter>(map);

                return map;
            }
        };
        const FactoryCollectionBaseCleanUp ObserverObjectFactoriesVar = FactoryCollectionBase::addToPreregistration(new ObserverObjectFactories());
    }

}

#endif
