/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Observer.h"
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/exceptions/local/InvalidChannelException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidCheckException.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace armarx::exceptions::local;

const std::string LAST_REFRESH_DELTA_CHANNEL = "_LastRefreshDelta";

// *******************************************************
// implementation of ObserverInterface
// *******************************************************
CheckIdentifier Observer::installCheck(const CheckConfiguration& configuration, const Ice::Current& c)
{
    // create condition check
    ConditionCheckPtr check;
    {
        boost::mutex::scoped_lock lock_checks(checksMutex);
        boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);
        check = createCheck(configuration);
    }
    ARMARX_CHECK_EXPRESSION(check);
    // insert into registry
    CheckIdentifier identifier;
    {
        boost::recursive_mutex::scoped_lock lock_conditions(channelsMutex);
        identifier = registerCheck(check);
    }

    ARMARX_VERBOSE << "Installed check " << identifier.uniqueId << flush;

    // perform initial check
    {
        boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);
        ARMARX_CHECK_EXPRESSION(configuration.dataFieldIdentifier);
        // retrieve channel
        ChannelRegistryEntry channel = channelRegistry[configuration.dataFieldIdentifier->channelName];

        // evaluate
        evaluateCheck(check, channel);
    }

    return identifier;
}

void Observer::removeCheck(const CheckIdentifier& id, const Ice::Current& c)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    // find channel
    ChannelRegistry::iterator iter = channelRegistry.find(id.channelName);

    if (iter == channelRegistry.end())
    {
        return;
    }

    // find condition
    ConditionCheckRegistry::iterator iterCheck = iter->second.conditionChecks.find(id.uniqueId);

    // remove condition
    if (iterCheck != iter->second.conditionChecks.end())
    {
        iter->second.conditionChecks.erase(iterCheck);
    }

    ARMARX_VERBOSE << "Removed check " << id.uniqueId << flush;
}

VariantBasePtr Observer::getDataField(const DataFieldIdentifierBasePtr& identifier, const Ice::Current& c)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (channelRegistry.find(identifier->channelName) == channelRegistry.end())
    {
        throw InvalidChannelException(identifier->channelName);
    }

    if (channelRegistry[identifier->channelName].dataFields.find(identifier->datafieldName) == channelRegistry[identifier->channelName].dataFields.end())
    {
        throw InvalidDataFieldException(identifier->channelName, identifier->datafieldName);
    }

    //    VariantPtr var = VariantPtr::dynamicCast(channelRegistry[identifier->channelName].dataFields[identifier->datafieldName].value);
    //    ARMARX_IMPORTANT << var->ice_ids() << "  - " << var->data->ice_ids();
    return channelRegistry[identifier->channelName].dataFields[identifier->datafieldName].value;
}

DatafieldRefPtr Observer::getDataFieldRef(const DataFieldIdentifierBasePtr& identifier) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (!existsChannel(identifier->channelName))
    {
        return NULL;
    }

    if (!existsDataField(identifier->channelName, identifier->datafieldName))
    {
        return NULL;
    }

    auto it = channelRegistry.find(identifier->channelName);
    auto itDF = it->second.dataFields.find(identifier->datafieldName);
    return DatafieldRefPtr::dynamicCast(itDF->second.identifier);
}

VariantBaseList Observer::getDataFields(const DataFieldIdentifierBaseList& identifiers, const Ice::Current& c)
{
    VariantBaseList result;

    DataFieldIdentifierBaseList::const_iterator iter = identifiers.begin();

    while (iter != identifiers.end())
    {
        result.push_back(getDataField(*iter, c));
        iter++;
    }

    return result;
}

ChannelRegistry Observer::getAvailableChannels(bool includeMetaChannels, const Ice::Current&)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (!includeMetaChannels)
    {
        ChannelRegistry result;
        result = channelRegistry;
        result.erase(LAST_REFRESH_DELTA_CHANNEL);
        return result;
    }
    else
    {
        return channelRegistry;
    }
}

StringConditionCheckMap Observer::getAvailableChecks(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(checksMutex);

    return availableChecks;
}

// *******************************************************
// offering of channels, datafield, and checks
// *******************************************************
void Observer::offerChannel(std::string channelName, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerChannel() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (channelRegistry.find(channelName) != channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    ChannelRegistryEntry channel;
    channel.name = channelName;
    channel.description = description;
    channel.initialized = false;

    std::pair<std::string, ChannelRegistryEntry> entry;
    entry.first = channelName;
    entry.second = channel;

    channelRegistry.insert(entry);
}

void Observer::offerDataFieldWithDefault(std::string channelName, std::string datafieldName, const Variant& defaultValue, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerDataFieldWithDefault() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    auto channelIt = channelRegistry.find(channelName);

    if (channelIt == channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    if (channelIt->second.dataFields.find(datafieldName) != channelIt->second.dataFields.end())
    {
        throw InvalidDataFieldException(channelName, datafieldName);
    }

    DataFieldRegistryEntry dataField;
    dataField.identifier = new DatafieldRef(this, channelName, datafieldName, false);
    dataField.description = description;
    dataField.value = new Variant();
    *dataField.value = defaultValue;

    std::pair<std::string, DataFieldRegistryEntry> entry;
    entry.first = datafieldName;
    entry.second = dataField;

    channelIt->second.dataFields.insert(entry);
}

void Observer::offerDataField(std::string channelName, std::string datafieldName, VariantTypeId type, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerDataField() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    auto channelIt = channelRegistry.find(channelName);

    if (channelIt == channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    if (channelIt->second.dataFields.find(datafieldName) != channelIt->second.dataFields.end())
    {
        throw InvalidDataFieldException(channelName, datafieldName);
    }

    DataFieldRegistryEntry dataField;
    dataField.identifier = new DatafieldRef(this, channelName, datafieldName, false);
    dataField.description = description;
    dataField.value = new Variant();
    dataField.value->setType(type);

    std::pair<std::string, DataFieldRegistryEntry> entry;
    entry.first = datafieldName;
    entry.second = dataField;

    channelRegistry[channelName].dataFields.insert(entry);
}

bool Observer::offerOrUpdateDataField(std::string channelName, std::string datafieldName, const Variant& value, const std::string& description)
{
    if (!existsDataField(channelName, datafieldName))
    {
        offerDataFieldWithDefault(channelName, datafieldName, value, description);
        return true;
    }
    else
    {
        setDataField(channelName, datafieldName, value);
        return false;
    }
}

void Observer::offerConditionCheck(std::string checkName, ConditionCheck* conditionCheck)
{
    boost::mutex::scoped_lock lock(checksMutex);

    if (availableChecks.find(checkName) != availableChecks.end())
    {
        throw InvalidCheckException(checkName);
    }

    std::pair<std::string, ConditionCheckPtr> entry;
    entry.first = checkName;
    entry.second = conditionCheck;

    availableChecks.insert(entry);
}

void Observer::removeChannel(std::string channelName)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator iter = channelRegistry.find(channelName);

    if (iter == channelRegistry.end())
    {
        return;
    }

    DataFieldIdentifierPtr id = new DataFieldIdentifier(getName(), LAST_REFRESH_DELTA_CHANNEL, channelName);
    removeDatafield(id);
    channelRegistry.erase(iter);
}

void Observer::removeDatafield(DataFieldIdentifierBasePtr id)
{
    {
        ScopedRecursiveLock lock(filterMutex);
        // Remove filter
        DataFieldIdentifierPtr idptr = DataFieldIdentifierPtr::dynamicCast(id);
        std::string idStr = idptr->getIdentifierStr();
        auto itFilter = filteredToOriginal.find(idStr);

        if (itFilter != filteredToOriginal.end())
        {
            DatafieldRefPtr refPtr = DatafieldRefPtr::dynamicCast(itFilter->second->original);
            auto range = orignalToFiltered.equal_range(refPtr->getDataFieldIdentifier()->getIdentifierStr());

            for (auto it = range.first; it != range.second; it++)
                if (it->second->filtered->getDataFieldIdentifier()->getIdentifierStr()
                    == idStr)
                {
                    orignalToFiltered.erase(it);
                    break;
                }

            filteredToOriginal.erase(itFilter);
        }
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(id->channelName);

    if (itChannel == channelRegistry.end())
    {
        return;
    }

    auto itDF = itChannel->second.dataFields.find(id->datafieldName);
    itChannel->second.dataFields.erase(itDF);




}

bool Observer::existsChannel(const std::string& channelName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    return channelRegistry.find(channelName) != channelRegistry.end();
}

bool Observer::existsDataField(const std::string& channelName, const std::string& datafieldName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    ChannelRegistry::const_iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        return false;
    }

    return itChannel->second.dataFields.find(datafieldName) != itChannel->second.dataFields.end();
}

// *******************************************************
// utility methods for sensordatalistener
// *******************************************************
void Observer::updateDatafieldFilter(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value)
{
    ScopedRecursiveLock lock(filterMutex);
    const std::string id = getName() + "." + channelName + "." + datafieldName;
    //    DatafieldRefPtr ref = new DatafieldRef(this, channelName, datafieldName);
    auto range = orignalToFiltered.equal_range(id);

    //IceUtil::Time start = IceUtil::Time::now();
    //bool found = false;
    for (auto it = range.first; it != range.second; it++)
    {
        it->second->filter->update(IceUtil::Time::now().toMicroSeconds(), value);
        setDataFieldFlatCopy(it->second->filtered->channelRef->channelName, it->second->filtered->datafieldName, VariantPtr::dynamicCast(it->second->filter->getValue()));
        //found = true;
    }

    /*if(found)
    {
        IceUtil::Time end = IceUtil::Time::now();
        IceUtil::Time duration = end - start;
        ARMARX_IMPORTANT << deactivateSpam(0.1f) << channelName << ":" << datafieldName << ": all filters calc microseconds: " << duration.toMicroSeconds();
    }*/
}



void Observer::setDataField(const std::string& channelName, const std::string& datafieldName, const Variant& value)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(datafieldName);

    if (itDF == itChannel->second.dataFields.end())
    {
        throw InvalidDataFieldException(channelName, datafieldName);
    }

    //    VariantPtr dataFieldValue = VariantPtr::dynamicCast(itDF->second.value);
    itDF->second.value = value.clone();
    updateDatafieldFilter(channelName, datafieldName, itDF->second.value);
    //*dataFieldValue = value;
}

void Observer::setDataFieldFlatCopy(const std::string& channelName, const std::string& datafieldName, const VariantPtr& value)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(datafieldName);

    if (itDF == itChannel->second.dataFields.end())
    {
        throw InvalidDataFieldException(channelName, datafieldName);
    }

    itDF->second.value = value;
    updateDatafieldFilter(channelName, datafieldName, itDF->second.value);
}

void Observer::setDataFieldsFlatCopy(const std::string& channelName, const StringVariantBaseMap& datafieldValues)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        throw InvalidChannelException(channelName);
    }

    for (const auto & elem : datafieldValues)
    {
        DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(elem.first);

        if (itDF == itChannel->second.dataFields.end())
        {
            throw InvalidDataFieldException(channelName, elem.first);
        }

        itDF->second.value = elem.second;
        updateDatafieldFilter(channelName, elem.first, itDF->second.value);
    }
}

void Observer::updateRefreshRateChannel(const std::string& channelName)
{
    auto& oldUpdateTime = channelUpdateTimestamps[channelName];
    auto now = IceUtil::Time::now();

    if (existsDataField(LAST_REFRESH_DELTA_CHANNEL, channelName))
    {
        setDataFieldFlatCopy(LAST_REFRESH_DELTA_CHANNEL, channelName, new Variant((now - oldUpdateTime).toMilliSecondsDouble()));
    }
    else
    {
        offerDataFieldWithDefault(LAST_REFRESH_DELTA_CHANNEL, channelName, 0.0, "Update delta of channel '" + channelName + "'");
    }

    oldUpdateTime = now;
}

void Observer::updateChannel(const std::string& channelName)
{
    boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);

    try
    {
        // check if channels exists
        ChannelRegistry::iterator iterChannel =  channelRegistry.find(channelName);

        if (iterChannel == channelRegistry.end())
        {
            throw InvalidChannelException(channelName);
        }

        updateRefreshRateChannel(channelName);

        // update initialized state
        DataFieldRegistry dataFields = iterChannel->second.dataFields;
        DataFieldRegistry::iterator iterDataFields = dataFields.begin();
        bool channelInitialized = true;

        while (iterDataFields != dataFields.end())
        {
            channelInitialized &= iterDataFields->second.value->getInitialized();
            iterDataFields++;
        }

        iterChannel->second.initialized = channelInitialized;

        // evaluate checks
        ConditionCheckRegistry::iterator iterChecks = iterChannel->second.conditionChecks.begin();

        while (iterChecks != iterChannel->second.conditionChecks.end())
        {
            try
            {
                evaluateCheck(ConditionCheckPtr::dynamicCast(iterChecks->second), iterChannel->second);
            }
            catch (...)
            {
                ARMARX_ERROR << "Evaluating condition for channel " << channelName << " failed!";
                handleExceptions();
            }

            iterChecks++;
        }
    }
    catch (...)
    {
        ARMARX_ERROR << "Upating channel " << channelName << " failed!";
        handleExceptions();
    }
}

// *******************************************************
// Component hooks
// *******************************************************
void Observer::onInitComponent()
{
    {
        boost::mutex::scoped_lock lock(idMutex);
        currentId = 0;
    }

    onInitObserver();

}

void Observer::onConnectComponent()
{
    offerChannel(LAST_REFRESH_DELTA_CHANNEL, "Metachannel with the last channel update deltas of all channels in milliseconds");

    // subclass init
    onConnectObserver();
    //    offerDataFieldWithDefault(channelName, LAST_REFRESH_DELTA_CHANNEL, 0.0, );
    //    channelUpdateTimestamps[channelName] = IceUtil::Time::now();

    metaTask = new PeriodicTask<Observer>(this, &Observer::metaUpdateTask, 50);
    metaTask->start();
}

void Observer::onExitComponent()
{
    onExitObserver();
    metaTask->stop();
}

void Observer::metaUpdateTask()
{
    updateChannel(LAST_REFRESH_DELTA_CHANNEL);
}

// *******************************************************
// private methods
// *******************************************************
int Observer::generateId()
{
    boost::mutex::scoped_lock lock(idMutex);

    return currentId++;
}

ConditionCheckPtr Observer::createCheck(const CheckConfiguration& configuration) const
{
    std::string checkName = configuration.checkName;

    // create check from elementary condition
    StringConditionCheckMap::const_iterator iterChecks = availableChecks.find(checkName);

    if (iterChecks == availableChecks.end())
    {
        std::string reason = "Invalid condition check \"" + checkName + "\" for observer \"" + getName() + "\".";
        throw InvalidConditionException(reason.c_str());
    }

    ARMARX_CHECK_EXPRESSION(iterChecks->second);
    ConditionCheckPtr check = ConditionCheckPtr::dynamicCast(iterChecks->second)->createInstance(configuration, channelRegistry);

    return check;
}

CheckIdentifier Observer::registerCheck(const ConditionCheckPtr& check)
{
    ARMARX_CHECK_EXPRESSION(check);
    ARMARX_CHECK_EXPRESSION(check->configuration.dataFieldIdentifier);
    // create identifier
    int id = generateId();
    CheckIdentifier identifier;
    identifier.uniqueId = id;
    identifier.channelName = check->configuration.dataFieldIdentifier->channelName;
    identifier.observerName = getName();

    // add to conditions list
    std::pair<int, ConditionCheckPtr> entry;
    entry.first = id;
    entry.second = check;
    channelRegistry[check->configuration.dataFieldIdentifier->channelName].conditionChecks.insert(entry);

    return identifier;
}

void Observer::evaluateCheck(const ConditionCheckPtr& check, const ChannelRegistryEntry& channel) const
{
    check->evaluateCondition(channel.dataFields);
}


DatafieldRefBasePtr Observer::createFilteredDatafield(const DatafieldFilterBasePtr& filter, const DatafieldRefBasePtr& datafieldRef, const Ice::Current& c)
{

    //    if( auto it = orignalToFiltered.find(datafieldRef) != orignalToFiltered.end())
    //    {
    //        return it->second.filtered;
    //    }
    ARMARX_CHECK_EXPRESSION(datafieldRef);
    std::string filteredName = datafieldRef->datafieldName + "_" + filter->ice_id();
    DatafieldRefPtr ref = DatafieldRefPtr::dynamicCast(datafieldRef);

    if (!filter->checkTypeSupport(ref->getDataField()->getType()))
    {
        auto types = filter->getSupportedTypes();
        std::string suppTypes = "supported types";

        for (auto t : types)
        {
            suppTypes += Variant::typeToString(t) + ", ";
        }

        ARMARX_WARNING << suppTypes;
        throw exceptions::user::UnsupportedTypeException(ref->getDataField()->getType());
    }

    int i = 1;

    while (existsDataField(datafieldRef->channelRef->channelName, filteredName))
    {
        ARMARX_IMPORTANT << "Checking if datafield " << filteredName << " exists";
        filteredName = datafieldRef->datafieldName + "_" + filter->ice_id() + "_" + ValueToString(i);
        i++;
    }

    // calculate initial value
    filter->update(IceUtil::Time::now().toMicroSeconds(), ref->getDataField());
    // create datefield for new filter
    offerDataFieldWithDefault(datafieldRef->channelRef->channelName, filteredName, *VariantPtr::dynamicCast(filter->getValue()), "Filtered value of " + ref->getDataFieldIdentifier()->getIdentifierStr());

    //create and store the refs for the filters
    ChannelRefPtr channel = ChannelRefPtr::dynamicCast(ref->channelRef);
    channel->refetchChannel();
    DatafieldRefPtr filteredRef = new DatafieldRef(ref->getChannelRef(), filteredName);
    FilterDataPtr data = new FilterData;
    data->filter = filter;
    data->original = ref;
    data->filtered = filteredRef;
    ScopedRecursiveLock lock(filterMutex);
    orignalToFiltered.insert(std::make_pair(ref->getDataFieldIdentifier()->getIdentifierStr(), data));
    filteredToOriginal[filteredRef->getDataFieldIdentifier()->getIdentifierStr()] = data;
    return filteredRef;
}




void Observer::removeFilteredDatafield(const DatafieldRefBasePtr& datafieldRef, const Ice::Current&)
{
    bool remove = true;
    DatafieldRefPtr ref;
    {
        ScopedRecursiveLock lock(filterMutex);
        ref = DatafieldRefPtr::dynamicCast(datafieldRef);
        const std::string idStr = ref->getDataFieldIdentifier()->getIdentifierStr();
        auto it = filteredToOriginal.find(idStr);
        remove = (it != filteredToOriginal.end());
    }

    if (remove && ref)
    {
        removeDatafield(ref->getDataFieldIdentifier());
    }
}
