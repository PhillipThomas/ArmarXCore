/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <Ice/Ice.h>
#include <cstdarg>

using namespace armarx;

// list of parameters version
LiteralImpl::LiteralImpl(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters) :
    installed(false)
{
    init(dataFieldIdentifierStr, checkName, checkParameters);
}

LiteralImpl::LiteralImpl(const DataFieldIdentifier& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters) :
    installed(false)
{
    init(dataFieldIdentifier.getIdentifierStr(), checkName, checkParameters);
}

LiteralImpl::LiteralImpl(const DataFieldIdentifierPtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters) :
    installed(false)
{
    init(dataFieldIdentifier->getIdentifierStr(), checkName, checkParameters);
}

LiteralImpl::LiteralImpl(const DatafieldRefBasePtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters)
{
    DatafieldRefPtr ref = DatafieldRefPtr::dynamicCast(dataFieldIdentifier);
    init(ref->getDataFieldIdentifier()->getIdentifierStr(), checkName, checkParameters);
}

void LiteralImpl::setValue(bool value, const Ice::Current& c)
{
    this->value = value;
    update();
}

void LiteralImpl::init(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters)
{
    type = eLiteral;

    DataFieldIdentifierPtr dataFieldIdentifier = new DataFieldIdentifier(dataFieldIdentifierStr);
    this->checkConfig.dataFieldIdentifier = dataFieldIdentifier;
    this->checkConfig.checkName = checkName;

    ParameterList::const_iterator iter = checkParameters.begin();

    while (iter != checkParameters.end())
    {
        this->checkConfig.checkParameters.push_back(VariantPtr(new Variant(*VariantPtr::dynamicCast(*iter))));
        iter++;
    }
}

void LiteralImpl::installCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy)
{
    boost::mutex::scoped_lock lock(accessLock);

    if (installed)
    {
        return;
    }

    // register object
    myProxy = adapter->addWithUUID(this);
    checkConfig.listener = LiteralImplBasePrx::uncheckedCast(myProxy);

    // add object to ice
    checkIdentifier = proxy->installCheck(checkConfig);

    installed = true;
}

void LiteralImpl::removeCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy)
{
    boost::mutex::scoped_lock lock(accessLock);

    if (!installed)
    {
        return;
    }

    // remove check
    proxy->removeCheck(checkIdentifier);

    // unregister object
    adapter->remove(myProxy->ice_getIdentity());

    installed = false;
}
