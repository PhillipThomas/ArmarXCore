/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_LITERALIMPL_H
#define _ARMARX_CORE_LITERALIMPL_H

#include <IceUtil/Handle.h>
#include <string>
#include <boost/thread/mutex.hpp>
#include <ArmarXCore/core/system/ImportExport.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXCore/observers/condition/TermImpl.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>


namespace armarx
{
    class LiteralImpl;

    /**
     * Typedef of LiteralImplPtr as IceInternal::Handle<LiteralImpl> for convenience.
     */
    typedef IceInternal::Handle<LiteralImpl> LiteralImplPtr;

    /**
     * @class LiteralImpl
     * @brief LiteralImpls are the basic elements for defining conditional expressions.
     * @ingroup Conditions
     *
     * LiteralImpl are leaves in the expression tree. Each LiteralImpl is associated to a ConditionCheck. The state of
     * the LiteralImpl corresponds to the state of the ConditionCheck.
     */
    class ARMARXCORE_IMPORT_EXPORT LiteralImpl :
        virtual public LiteralImplBase,
        virtual public TermImpl
    {
        friend class ConditionHandler;
        friend class Literal;
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;

    protected:
        /**
        * Creates an empty LiteralImpl. Required for Ice ObjectFactory
        */
        LiteralImpl()
        {
            type = eLiteral;
            installed = false;
        }

        /**
        * Creates a LiteralImpl using string as dataFieldIdentifier with multiple parameter. See also createParameterList().
        *
        * @param dataFieldIdentifierStr identifier of the datafield used by the condition in the form "observerName.channelName.datafieldName"
        * @param checkName name of the condition check
        * @param checkParameter parameter for the condition check
        */
        LiteralImpl(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters);

        /**
        * Creates a LiteralImpl using the DataFieldIdentifier type with multiple parameter. See also createParameterList()
        *
        * @param dataFieldIdentifier identifier of the datafield used by the condition
        * @param checkName name of the condition check
        * @param checkParameters list of check parameters
        */
        LiteralImpl(const DataFieldIdentifier& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters);

        /**
        * Creates a LiteralImpl using the DataFieldIdentifier type with multiple parameter. See also createParameterList()
        *
        * @param dataFieldIdentifier pointer to identifier of the datafield used by the condition
        * @param checkName name of the condition check
        * @param checkParameters list of check parameters
        */
        LiteralImpl(const DataFieldIdentifierPtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters);

        LiteralImpl(const DatafieldRefBasePtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters);

        void createInstance() {}
    public:
        /**
        * Retrieve check configuration. Inherited from LiteralImplBase
        *
        * @return configuration of check
        */
        CheckConfiguration getCheckConfiguration(const Ice::Current& c = ::Ice::Current())
        {
            return checkConfig;
        }

        /**
        * Set value of LiteralImpl
        *
        * @param value of LiteralImpl
        */
        void setValue(bool value, const Ice::Current& c = ::Ice::Current());

        /**
        * Reimplementation of the ice_clone method.
        *
        * @return clone of the object
        */
        virtual Ice::ObjectPtr ice_clone() const
        {
            LiteralImplPtr literal = new LiteralImpl();
            literal->type = this->type;
            literal->installed = this->installed;
            literal->checkIdentifier = this->checkIdentifier;
            literal->checkConfig = this->checkConfig;
            literal->myProxy = this->myProxy;

            return literal;
        }

        /**
         * output to stream. pure virtual.
         *
         * @param stream
         */
        virtual void output(std::ostream& out) const
        {
            DataFieldIdentifierPtr dataFieldIdentifier = DataFieldIdentifierPtr::dynamicCast(checkConfig.dataFieldIdentifier);

            out << checkConfig.checkName << "(";

            if (dataFieldIdentifier)
            {
                out << dataFieldIdentifier;
            }
            else
            {
                out << "NULL";
            }


            ParameterList::const_iterator iter = checkConfig.checkParameters.begin();

            while (iter != checkConfig.checkParameters.end())
            {
                if (iter == checkConfig.checkParameters.begin())
                {
                    out << ", ";
                }

                VariantPtr var = VariantPtr::dynamicCast(*iter);

                if (var)
                {
                    out << var;
                }
                else
                {
                    out << "NULL";
                }

                iter++;

                if (iter != checkConfig.checkParameters.end())
                {
                    out << ",";
                }
            }

            out << ")";
        }


        /**
        * stream operator for LiteralImpl
        */
        friend std::ostream& operator<<(std::ostream& stream, const LiteralImpl& rhs)
        {
            rhs.output(stream);

            return stream;
        }

        /**
        * stream operator for LiteralImplPtr
        */
        friend std::ostream& operator<<(std::ostream& stream, const LiteralImplPtr& rhs)
        {
            rhs->output(stream);

            return stream;
        }
    protected:
        void init(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters);
    private:
        void installCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy);
        void removeCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy);

        CheckIdentifier checkIdentifier;
        Ice::ObjectPrx  myProxy;
        bool installed;
        boost::mutex accessLock;
    };
}

#endif
