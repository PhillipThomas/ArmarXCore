/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_OBSERVER_H
#define _ARMARX_CORE_OBSERVER_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/observers/VariantBase.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/ConditionCheck.h>

#include <boost/thread/mutex.hpp>

namespace armarx
{

    class DatafieldRef;
    typedef IceInternal::Handle<DatafieldRef> DatafieldRefPtr;

    /**
      @class Observer
      @brief Baseclass for all ArmarX Observers
      @ingroup ObserversGrp

      A subclass of the observer should implement the two framework hooks onInitObserver and onConnectObserver.
      The onInitObserver method should register all channels, datafields, and available condition checks
      (see offerChannel, offerDattField, offerCheck).
      Further, a subclass should implement a SensorActorUnitListener Ice interface.
      Each interface method should correspond to one channel.
      The observer should map the actual sensor values to the observer superclass
      using the dataField method.
      Finally, after all values have been updated, the updateChannel method needs
      to be called in order to verify all currently active conditions and
      generate associated events.
     */
    class ARMARXCORE_IMPORT_EXPORT Observer :
        virtual public ObserverInterface,
        virtual public Component
    {
    public:
        /**
        * Installs a condition check with the observer
        *
        * @param configuration configuration of the check
        * @param listener literal of the conditional expression that is associated with this check
        * @param c ice context
        * @throw InvalidConditionException
        * @return identifier of installed condition as required for removal (see removeCondition)
        */
        CheckIdentifier installCheck(const CheckConfiguration& configuration, const Ice::Current& c = ::Ice::Current());

        /**
        * Removes a condition check from the observer. If the condition has already been removed, the function immidiately returns.
        *
        * @param id identifier of installed condition
        * @param c ice context
        */
        void removeCheck(const CheckIdentifier& id, const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve data field from observer
        * @param identifier Identifier of datafield
        * @param c ice context
        *
        * @return variant corresponding to the data field
        */
        VariantBasePtr getDataField(const DataFieldIdentifierBasePtr& identifier, const Ice::Current& c = ::Ice::Current());
        DatafieldRefPtr getDataFieldRef(const DataFieldIdentifierBasePtr& identifier) const;

        /**
        * Retrieve list of data field from observer
        * @param identifier list of identifiers of datafield
        * @param c ice context
        *
        * @return list of variants corresponding to the data field
        */
        VariantBaseList getDataFields(const DataFieldIdentifierBaseList& identifiers, const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve information on all sensory data channels available from the observer.
        * @param c ice context
        *
        * @return the ChannelRegistry contains information on each channel including its datafields and associated types and current values.
        */
        ChannelRegistry getAvailableChannels(bool includeMetaChannels, const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve list of available condition checks
        * @param c ice context
        *
        * @return list of available condition checks
        */
        StringConditionCheckMap getAvailableChecks(const Ice::Current& c = ::Ice::Current());

        bool existsChannel(const std::string& channelName, const Ice::Current& c = ::Ice::Current()) const;
        bool existsDataField(const std::string& channelName, const std::string& datafieldName, const Ice::Current& c = ::Ice::Current()) const;


    protected:
        /**
        * Offer a channel. Use this in an observer specialization.
        *
        * The channel is not initialized until updateChannel() was called once.
        * @param channelName name of the channel
        * @param description expressive description of the provided datafields
        * @throw InvalidChannelException
        * @see updateChannel()
        */
        void offerChannel(std::string channelName, std::string description);

        /**
        * Offer a datafield with default value. Use this in an observer specialization.
        *
        * @param channelName name of the channel
        * @param datafieldName name of the datafield
        * @param defaultValue defines the default value and the datatype for the field
        * @param description expressive description of the datafield
        * @throw InvalidChannelException
        * @throw InvalidDataFieldException
        */
        void offerDataFieldWithDefault(std::string channelName, std::string datafieldName, const Variant& defaultValue, std::string description);

        /**
        * Offer a datafield without default value. Use this in an observer specialization.
        *
        * @param channelName name of the channel
        * @param datafieldName name of the datafield
        * @param defaultValue defines the default value and the datatype for the field
        * @param description expressive description of the datafield
        * @throw InvalidChannelException
        * @throw InvalidDataFieldException
        */
        void offerDataField(std::string channelName, std::string datafieldName, VariantTypeId type, std::string description);
        bool offerOrUpdateDataField(std::string channelName, std::string datafieldName,  const Variant& value, const std::string& description);

        /**
        * Offer a condition check. Use this in an observer specialization.
        *
        * @param checkName name of the check
        * @param conditionCheck check to register under checkName
        * @throw InvalidCheckException
        */
        void offerConditionCheck(std::string checkName, ConditionCheck* conditionCheck);

        /**
        * set datafield with datafieldName and in channel channelName
        *
        * @param channelName name of the channel
        * @param datafieldName name of the datafield within channel
        * @param value value for datafield
        * @throw InvalidChannelException
        * @throw InvalidDataFieldException
        * @return reference to Variant associated with the dataField
        */
        void setDataField(const std::string& channelName, const std::string& datafieldName, const Variant& value);
        void setDataFieldFlatCopy(const std::string& channelName, const std::string& datafieldName, const VariantPtr& value);
        void setDataFieldsFlatCopy(const std::string& channelName, const StringVariantBaseMap& datafieldValues);

        /**
        * Update all conditiond for a channel. Call this from the sensorActorUnit listener implementation if new data is posted.
        *
        * @param channelName name of the channel to update
        * @throw InvalidChannelException
        */
        void updateChannel(const std::string& channelName);

        /**
        * Remove a channel. Use this in an observer specialization.
        *
        * @param channelName name of the channel
        */
        void removeChannel(std::string channelName);

        void removeDatafield(DataFieldIdentifierBasePtr id);

        void updateDatafieldFilter(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value);



        /**
        * Framework hook. Called once on initialization of the Observer.
        */
        virtual void onInitObserver() = 0;

        /**
        * Framework hook. Called on first run, after ice setup.
        */
        virtual void onConnectObserver() = 0;

        /**
        * Framework hook. Called on first run, after ice setup.
        */
        virtual void onExitObserver() {}

        void metaUpdateTask();




        mutable boost::recursive_mutex channelsMutex;

    private:
        // check handling
        ConditionCheckPtr createCheck(const CheckConfiguration& configuration) const;
        CheckIdentifier registerCheck(const ConditionCheckPtr& check);
        void evaluateCheck(const ConditionCheckPtr& check, const ChannelRegistryEntry& channel) const;

        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "Observer";
        }

        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();

        void updateRefreshRateChannel(const std::string& channelName);
        PeriodicTask<Observer>::pointer_type metaTask;


        // available checks and channels
        StringConditionCheckMap availableChecks;
        boost::mutex checksMutex;

        // available channels and data fields
        ChannelRegistry channelRegistry;

        // utility methods
        int generateId();
        int currentId;
        boost::mutex idMutex;

        struct FilterData : IceUtil::Shared
        {
            DatafieldFilterBasePtr filter;
            DatafieldRefPtr original;
            DatafieldRefPtr filtered;
        };
        typedef IceUtil::Handle<FilterData> FilterDataPtr;

        std::multimap<std::string, FilterDataPtr> orignalToFiltered;
        std::map<std::string, FilterDataPtr> filteredToOriginal;
        RecursiveMutex filterMutex;
        std::map<std::string, IceUtil::Time> channelUpdateTimestamps;


        // ObserverInterface interface
    public:
        /**
         * @brief This function creates a new datafield with new filter on the given datafield.
         * @param filter Configured filter object compatible with the datafieldRef (see \ref DatafieldFilter::getSupportedTypes())
         * @param datafieldRef Datafield for which a filtered datafield should be created
         * @return Returns a DatafieldRef to the new, filtered datafield.
         * @see \ref removeFilteredDatafield()
         */
        DatafieldRefBasePtr createFilteredDatafield(const DatafieldFilterBasePtr& filter, const DatafieldRefBasePtr& datafieldRef, const Ice::Current& c = Ice::Current());

        /**
         * @brief Removes a previously installed filter.
         * @param datafieldRef Datafield that was returned by createFilteredDatafield()
         * @see \ref createFilteredDatafield()
         */
        void removeFilteredDatafield(const DatafieldRefBasePtr& datafieldRef, const Ice::Current& c = Ice::Current());
    };


    namespace channels
    {
        //        class DatafieldRepresentationBase;
        //        typedef boost::shared_ptr<DatafieldRepresentationBase> DatafieldRepresentationBasePtr;
        //        typedef boost::shared_ptr<const DatafieldRepresentationBase> DatafieldRepresentationBaseConstPtr;
        //        struct DatafieldRepresentationBase {
        //            virtual const std::string & getDatafieldStr() const {return _observerStr+"."+_channelStr+"."+_datafieldStr;}
        //            static DatafieldRepresentationBasePtr create(const std::string& observer, const std::string& channel, const std::string& datafield )
        //            {
        //                return DatafieldRepresentationBasePtr(
        //                            new DatafieldRepresentationBase(observer, channel, datafield));
        //            }
        //        private:
        //            const std::string _channelStr;
        //            const std::string _observerStr;
        //            const std::string _datafieldStr;
        //            DatafieldRepresentationBase(){}
        //            DatafieldRepresentationBase(const std::string& observer, const std::string& channel, const std::string& datafield ) :
        //                _channelStr(channel),
        //                _observerStr(observer),
        //                _datafieldStr(datafield)
        //            {}
        //        };
    }


    /*#define ARMARX_CREATE_CHANNEL(OFFERER,NEWCHANNEL) namespace channels { namespace OFFERER { \
        const DatafieldRepresentationBaseConstPtr NEWCHANNEL =  DatafieldRepresentationBase::create(#NEWCHANNEL);\
        }}
    */

}

#endif
