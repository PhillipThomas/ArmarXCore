/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::observers::ProfilerObserver
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/interface/observers/ProfilerObserverInterface.h>

namespace armarx
{
    /**
     * @defgroup Component-ProfilerObserver ProfilerObserver
     * @ingroup ObserversSub ArmarXCore-Components
     * The ProfilerObserver class listens on the topic armarx::Profiler::PROFILER_TOPIC_NAME (defined in Core/interface/core/Profiler.ice) and provides the data via Channels.
     *
     * Network profiling can be enabled for any armarx::Application by setting the property armarx::NetworkStats to "1".
     * This property can either be set in ~/.armarx/default.cfg or on the commandline.
     *
     * The gathered data can for example be viewed with the Logger plugin of ArmarXGui.
     *
     * @class ProfilerObserver
     * @ingroup Component-ProfilerObserver
     */
    class ProfilerObserver :
        virtual public Observer,
        virtual public armarx::ProfilerObserverInterface
    {
    public:
        virtual std::string getDefaultName() const;
        void onInitObserver();
        void onConnectObserver();

        void reportNetworkTraffic(const std::string& id, const std::string& protocol, Ice::Int inBytes, Ice::Int outBytes, const Ice::Current& = ::Ice::Current());
        void reportEvent(const ProfilerEvent&, const Ice::Current& = ::Ice::Current());
        void reportStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName, const Ice::Current& = ::Ice::Current());
        void reportProcessCpuUsage(const ProfilerProcessCpuUsage& process, const Ice::Current& = ::Ice::Current());

        void reportEventList(const ProfilerEventList& events, const Ice::Current& = ::Ice::Current());
        void reportStatechartTransitionList(const ProfilerTransitionList& transitions, const Ice::Current& = ::Ice::Current());
        void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList& processes, const Ice::Current& = ::Ice::Current());
    };
}
