/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_CONDITIONHANDLER_H
#define _ARMARX_CORE_CONDITIONHANDLER_H

#include <vector>
#include <string>
#include <boost/thread/mutex.hpp>

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/application/properties/Properties.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>

#include <ArmarXCore/observers/condition/LiteralImpl.h>


namespace armarx
{
    /**
     * @class ConditionHandlerPropertyDefinitions
     * @brief
     */
    class ConditionHandlerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ConditionHandlerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Observers", "", "Comma seperated observer list");
            defineOptionalProperty<int>("HistoryLength", 1000, "Length of condition history kept by the conditionhandler");
        }
    };

    /**
     * @defgroup Component-ConditionHandler ConditionHandler
     * @ingroup ArmarXCore-Components
     * @brief This component allows installing distributed \ref armarx::Condition "conditions" on sensor data on multiple \ref armarx::Observer "Observers".
     *
     * The ConditionHandler manages the status of complex condition trees and distributes complex conditions \ref armarx::Literal "literals" to their designated observers.
     * Upon fulfillment of conditions an associated event the send to the given EventListenerInterfacePrx.
     * @see \ref Observers, armarx::ConditionCheck, armarx::ConditionRoot, armarx::Literal, armarx::Term
     *
     * @class ConditionHandler
     * @ingroup Component-ConditionHandler
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionHandler :
        virtual public ConditionHandlerInterface,
        virtual public Component
    {
    public:
        /**
        * Installs a condition
        *
        * @param listener Event listener
        * @param expression Expression to test for
        * @param e Event to generate when condition is fulfilled
        *
        * @throw InvalidConditionException
        *
        * @return Identifier of installed condition as required for removal (see removeCondition)
        */
        ConditionIdentifier installCondition(const EventListenerInterfacePrx& listener, const TermImplBasePtr& expression, const EventBasePtr& e, bool onlyFireOnce, const Ice::Current& c = ::Ice::Current());

        /**
        * Installs a condition
        *
        * @param listener Event listener
        * @param expression Expression to test for
        * @param e Event to generate when condition is fulfilled
        * @param desc Description of the condition for debugging and visualization purpose
        *
        * @throw InvalidConditionException
        *
        * @return Identifier of installed condition as required for removal (see removeCondition)
        */
        ConditionIdentifier installConditionWithDescription(const EventListenerInterfacePrx& listener, const TermImplBasePtr& expression, const EventBasePtr& e, const std::string& desc, bool onlyFireOnce, const Ice::Current& c = ::Ice::Current());

        /**
        * Removes a condition. If the condition has already been removed, the function immediately returns.
        *
        * @param id Identifier of the condition to remove
        */
        void removeCondition(const ConditionIdentifier& id, const Ice::Current& c = ::Ice::Current());


        /**
        * Retrieve the list of known observers as provided in the config file
        *
        * @return List of observer names
        */
        StringList getObserverNames(const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve the list of active conditions.
        *
        * @return The condition registry
        */
        ConditionRegistry getActiveConditions(const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve the list of conditions that have been registered in the past.
        * The length limit of the list can be controlled with the config file parameter HistoryLength.
        *
        * @return The condition registry for past conditions
        */
        ConditionRegistry getPastConditions(const Ice::Current& c = ::Ice::Current());

        ConditionRootBasePtr getCondition(Ice::Int id, const Ice::Current& c = ::Ice::Current());
        virtual std::string getDefaultName() const
        {
            return "ConditionHandler";
        }

    protected:
        /**
        * Framework hook. Called once on initialization of the ConditionInstaller.
        */
        virtual void onInitConditionHandler() { }

        /**
        * Framework hook. Called on first run, after ice setup.
        */
        virtual void onStartConditionHandler() { }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();

    private:
        // active checks and associated events
        boost::mutex idMutex;
        boost::mutex iceManagerMutex;
        boost::mutex conditionRegistryMutex;
        ConditionRegistry conditionRegistry;
        boost::mutex conditionHistoryMutex;
        ConditionRegistry conditionHistory;
        int historyLength;

        // utility methods for observers
        void useObservers(std::vector<std::string>& names);
        void preCacheObservers(std::vector<std::string>& names);
        ObserverInterfacePrx getObserver(std::string observerName);

        // utility methods for check handling
        std::vector<LiteralImplPtr> extractLiterals(const TermImplBasePtr& expression);
        void installChecks(std::vector<LiteralImplPtr>& literals, const Ice::Current& c);
        void removeChecks(std::vector<LiteralImplPtr>& literals, const Ice::Current& c);

        StringList observerNames;

        int generateId();
        int currentId;

        // ConditionHandlerInterface interface
    public:
    };
}

#endif
