/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_OBSERVERS_VARIANTLISTPARAMETER_H
#define _ARMARX_OBSERVERS_VARIANTLISTPARAMETER_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/parameter/Parameter.h>
#include <string>

namespace armarx
{
    class VariantListParameter;
    /**
     * Typedef of VariantListParameterPtr as IceInternal::Handle<VariantListParameter> for convenience.
     */
    typedef IceInternal::Handle<VariantListParameter> VariantListParameterPtr;

    /**
    */
    class ARMARXCORE_IMPORT_EXPORT VariantListParameter :
        virtual public Parameter,
        virtual public VariantListParameterBase
    {
    public:
        /**
        * Creates an empty Parameter. Required for Ice ObjectFactory
        */
        VariantListParameter()
        {
            type = eVariantListParam;
        }
        VariantListParameter(const VariantListParameter& source);
        Parameter& operator=(const Parameter& source);
        VariantListParameter(const SingleTypeVariantList& source);

        ParameterBasePtr clone(const Ice::Current& c = ::Ice::Current()) const;
        // setter
        void setVariantList(const SingleTypeVariantListBasePtr& variantList, const Ice::Current& c = ::Ice::Current());

        // getter
        VariantTypeId getVariantType(const Ice::Current& c = ::Ice::Current()) const;
        SingleTypeVariantListBasePtr getVariantList(const Ice::Current& c = ::Ice::Current()) const;

        bool validate(const Ice::Current& c) const;
    };
}

#endif
