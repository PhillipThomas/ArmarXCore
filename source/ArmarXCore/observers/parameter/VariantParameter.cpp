/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/parameter/VariantParameter.h>

using namespace armarx;

VariantParameter::VariantParameter(const Variant& variant)
{
    type = eVariantParam;
    this->variant = new Variant();
    *this->variant = variant;
}

VariantParameter::VariantParameter(const VariantParameter& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    ParameterBase(source),
    Parameter(source),
    VariantParameterBase(source)
{
    type = source.type;
    this->variant = new Variant();
    *VariantPtr::dynamicCast(this->variant) = *VariantPtr::dynamicCast(source.getVariant());
}

Parameter& VariantParameter::operator =(const Parameter& source)
{
    if (type != source.getParameterType())
    {
        throw InvalidTypeException();
    }

    type = source.getParameterType();

    if (!variant)
    {
        variant = new Variant();
    }

    *VariantPtr::dynamicCast(this->variant) = *VariantPtr::dynamicCast(source.getVariant());
    return *this;
}

ParameterBasePtr VariantParameter::clone(const Ice::Current& c) const
{
    return new VariantParameter(*this);
}


// setter
void VariantParameter::setVariant(const VariantBasePtr& variant, const Ice::Current& c)
{
    *(this->variant) = *variant;
}

VariantTypeId VariantParameter::getVariantType(const Ice::Current& c) const
{
    return variant->getType();
}

VariantBasePtr VariantParameter::getVariant(const Ice::Current& c) const
{
    return variant;
}

bool VariantParameter::validate(const Ice::Current& c) const
{
    return getVariant()->validate();
}

