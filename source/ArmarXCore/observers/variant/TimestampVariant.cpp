#include "TimestampVariant.h"

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

namespace armarx
{

    TimestampVariant::TimestampVariant()
    {
    }

    TimestampVariant::TimestampVariant(long timestamp)
    {
        this->timestamp = timestamp;
    }

    TimestampVariant::TimestampVariant(IceUtil::Time time)
    {
        this->timestamp = time.toMicroSeconds();
    }

    long TimestampVariant::getTimestamp()
    {
        return timestamp;
    }

    int TimestampVariant::readFromXML(const std::string& xmlData, const Ice::Current& c)
    {
        // TODO

        return 1;
    }

    std::string TimestampVariant::writeAsXML(const Ice::Current& c)
    {
        // TODO
        using namespace boost::property_tree;
        ptree pt;

#if BOOST_VERSION >= 105600
        boost::property_tree::xml_parser::xml_writer_settings<std::string> settings('\t', 1);
#else
        boost::property_tree::xml_parser::xml_writer_settings<char> settings('\t', 1);
#endif

        std::stringstream stream;
        xml_parser::write_xml(stream, pt, settings);
        return stream.str();
    }

    void TimestampVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setDouble("timestamp", timestamp);
    }

    void TimestampVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        timestamp = obj->getDouble("timestamp");
    }

}
