/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <boost/property_tree/xml_parser.hpp>

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;

StringValueMap::StringValueMap()
{
    typeContainer = VariantType::Map(VariantType::Invalid).clone();
}

StringValueMap::StringValueMap(VariantTypeId subType)
{
    this->typeContainer = VariantType::Map(subType).clone();
    //    type = Variant::addTypeName(getTypePrefix() + Variant::typeToString(subType));
}

armarx::StringValueMap::StringValueMap(const ContainerType& subType)
{
    this->typeContainer = VariantType::Map(subType).clone();
}

StringValueMap::StringValueMap(const StringValueMap& source) :
    IceUtil::Shared(source),
    VariantContainerBase(source),
    StringValueMapBase(source)
{
    *this = source;
}



StringValueMap& StringValueMap::operator =(const StringValueMap& source)
{

    this->typeContainer = ContainerTypePtr::dynamicCast(source.typeContainer->clone());
    elements.clear();
    StringVariantContainerBaseMap::const_iterator it = source.elements.begin();

    for (; it != source.elements.end(); it++)
    {
        elements[it->first] = it->second->cloneContainer();
    }

    return *this;
}

VariantContainerBasePtr StringValueMap::cloneContainer(const Ice::Current& c) const
{
    VariantContainerBasePtr result = new StringValueMap(*this);

    return result;
}

void StringValueMap::addElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (elements.find(key) != elements.end())
    {
        throw KeyAlreadyExistsException();
    }

    setElement(key, variantContainer->cloneContainer());
}

void StringValueMap::addVariant(const std::string& key, const Variant& variant)
{
    setElement(key, new SingleVariant(variant));
}

void StringValueMap::setElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (!VariantContainerType::compare(variantContainer->getContainerType(), getContainerType()->subType)
        && getContainerType()->subType->typeId != Variant::typeToString(VariantType::Invalid))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, variantContainer->getContainerType()->typeId);
    }

    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = variantContainer->getContainerType()->clone();
    }

    elements[key] = (variantContainer->cloneContainer());
}

void StringValueMap::clear(const Ice::Current& c)
{
    elements.clear();
}



VariantTypeId StringValueMap::getStaticType(const Ice::Current& c)
{
    return Variant::addTypeName(getTypePrefix());
}



int StringValueMap::getSize(const Ice::Current& c) const
{
    return int(elements.size());
}

bool StringValueMap::validateElements(const Ice::Current& c)
{
    StringVariantContainerBaseMap::iterator it = elements.begin();
    bool result = true;

    for (; it != elements.end(); it++)
    {
        result = result && it->second->validateElements();
    }

    return result;
}

VariantContainerBasePtr StringValueMap::getElementBase(const std::string& key, const Ice::Current& c) const
{
    StringVariantContainerBaseMap::const_iterator it = elements.find(key);

    if (it == elements.end())
    {
        throw IndexOutOfBoundsException();
    }

    return it->second;
}

VariantPtr StringValueMap::getVariant(const std::string& key) const
{
    VariantPtr ptr = getElement<SingleVariant>(key)->get();

    if (!ptr)
    {
        throw InvalidTypeException();
    }

    return ptr;
}

int StringValueMap::readFromXML(const std::string& xmlData, const Ice::Current& c)
{
    boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);

    std::stringstream currentItemXmlPathStr;
    int i = 0;
    currentItemXmlPathStr << "Item" << i;

    while (pt.get_child_optional(currentItemXmlPathStr.str()).is_initialized())
    {

        std::string key = pt.get_child(currentItemXmlPathStr.str()).get<std::string>("key");
        // get element type factory
        Ice::CommunicatorPtr  ic = c.adapter->getCommunicator();

        if (!ic)
        {
            throw LocalException("Ice::Communicator was not set in Ice::Current.adapter");
        }

        std::string typeStr =  getContainerType()->subType->typeId;

        //        ARMARX_IMPORTANT_S << "subtype: " << typeStr;
        VariantContainerBasePtr data;

        if (!getContainerType()->subType->subType) // must be variant
        {
            Variant var;
            var.setType(Variant::hashTypeName(getContainerType()->subType->typeId));
            data = new SingleVariant(var);
            data->readFromXML(pt.get_child(currentItemXmlPathStr.str()).get<std::string>("value"), c);
        }
        else
        {
            if (ic)
            {
                Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

                if (!objFac)
                {
                    throw LocalException("Could not find ObjectFactory for string '" + typeStr + "'");
                }

                data = VariantContainerBasePtr::dynamicCast(objFac->create(typeStr));
                data->setContainerType(getContainerType()->subType->clone());
            }
            else
            {
                ARMARX_WARNING_S << "could not load container" << flush;
                return 0;
            }

            //VariantDataClass data = element->getClass< armarx::VariantDataClass >();
            static Ice::Current c;

            if (!c.adapter)
            {
                c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to readFromXML so that a proxy can be retrieved in the elementiant
            }


            std::ostringstream stream;
            boost::property_tree::xml_parser::write_xml(stream, pt.get_child(currentItemXmlPathStr.str()).get_child("value"));

            data->readFromXML(stream.str(), c);
        }

        addElement(key, data);

        currentItemXmlPathStr.seekp(0);
        i++;
        currentItemXmlPathStr << "Item" << i;
    }

    return 1;
}

std::string StringValueMap::toString(const Ice::Current& c)
{
    std::stringstream ss;

    for (std::pair<std::string, VariantContainerBasePtr> element : elements)
    {
        ss << element.first << ": " << element.second->toString() << "\n";
    }

    return ss.str();


}

void StringValueMap::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr newObj = obj->createElement();

    StringVariantContainerBaseMap::const_iterator it = elements.begin();

    for (; it != elements.end(); it++)
    {
        newObj->setIceObject(it->first, it->second);
    }

    //    obj->setString("type", ice_id());
    obj->setElement("map", newObj);
}

void StringValueMap::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr map = obj->getElement("map");
    StringList keys = map->getElementNames();

    for (StringList::iterator it = keys.begin(); it != keys.end(); it++)
    {
        VariantContainerBasePtr c = VariantContainerBasePtr::dynamicCast(map->getIceObject(*it));

        if (c)
        {
            addElement(*it, c);
        }
        else
        {
            throw LocalException("Could not cast to VariantContainerBasePtr");
        }
    }
}


