/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <boost/algorithm/string.hpp>

using namespace armarx;

DataFieldIdentifier::DataFieldIdentifier(std::string dataFieldIdentifierStr)
{
    std::string tmpStr = dataFieldIdentifierStr + ".";

    std::vector<std::string> tokens;

    while (true)
    {
        size_t pos = tmpStr.find_first_of(".");

        if (pos == std::string::npos)
        {
            break;
        }

        std::string token = tmpStr.substr(0, pos);
        tmpStr = tmpStr.substr(pos + 1);

        tokens.push_back(token);
    }

    if (tokens.size() > 2)
    {
        observerName = tokens.at(0);
        channelName = tokens.at(1);
        datafieldName = tokens.at(2);
    }
}

DataFieldIdentifier::DataFieldIdentifier(std::string observerName, std::string channelName, std::string datafieldName)
{
    this->observerName = observerName;
    this->channelName = channelName;
    this->datafieldName = datafieldName;
}


std::string DataFieldIdentifier::getIdentifierStr() const
{
    return observerName + "." + channelName + "." + datafieldName;
}

bool DataFieldIdentifier::equals(const DataFieldIdentifier& dataFieldIdentifier)
{
    std::string thisStr = getIdentifierStr();
    std::string otherStr = dataFieldIdentifier.getIdentifierStr();

    return (thisStr.compare(otherStr) == 0);
}

bool DataFieldIdentifier::beginsWith(const DataFieldIdentifier& dataFieldIdentifier)
{
    std::string thisStr = getIdentifierStr();
    std::string otherStr = dataFieldIdentifier.getIdentifierStr();

    return (thisStr.compare(0, otherStr.length(), otherStr) == 0);
}

