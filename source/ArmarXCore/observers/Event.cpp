#include "Event.h"

#include <ArmarXCore/observers/parameter/Parameter.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>
//#include <ArmarXCore/observers/variant/Variant.h>

armarx::Event::Event(std::string eventReceiverName, std::string eventName)
{
    this->eventReceiverName = eventReceiverName;
    this->eventName = eventName;
}

armarx::EventPtr armarx::Event::add(const std::string key, const armarx::Variant& value)
{
    properties[key] = new SingleVariant(value);
    return this;
}

armarx::EventPtr armarx::Event::add(const std::string key, const armarx::VariantContainerBasePtr& valueContainer)
{
    properties[key] = valueContainer->cloneContainer();
    return this;
}

Ice::ObjectPtr armarx::Event::ice_clone() const
{
    EventPtr newEvent = new Event(*this);
    return newEvent;
}

armarx::EventPtr armarx::Event::clone() const
{
    EventPtr newEvent = new Event(*this);
    newEvent->properties.clear();

    for (StringVariantContainerBaseMap::const_iterator it = properties.begin(); it != properties.end(); it++)
    {
        newEvent->properties[it->first] = it->second->cloneContainer();
    }

    return newEvent;
}


armarx::Event::Event()
{
    eventReceiverName = "";
    eventName = "";
}
