/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::application::DebugObserver
 * @author     Manfred Kroehnert
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/statechart/xmlstates/StateGroupGenerator.h>

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        throw armarx::LocalException("Incorrect parameters. Expected: (<Statechargroupfile.scgxml> <statefile.xml> | context <Statechargroupfile.scgxml>) packagePath.");
    }

    std::string arg1(argv[1]);
    std::string statechartGroupXmlFilePath(argv[2]);
    std::string packagePath(argv[3]);

    if (arg1 == "context")
    {
        armarx::StatechartGroupGenerator::generateStatechartContextFile(statechartGroupXmlFilePath, packagePath);
    }
    else if (arg1 == "package")
    {
        armarx::StatechartGroupGenerator::generateStatechartFiles(statechartGroupXmlFilePath);
    }
    else
    {
        armarx::StatechartGroupGenerator::generateStateFile(arg1, statechartGroupXmlFilePath, packagePath);
    }
}
