/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::application::DebugObserver
 * @author     Manfred Kroehnert
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StatechartGroupDocGeneratorApp.h"
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    if (argc <= 1 || std::string(argv[1]) == "--print-options")
    {
        return 0;
    }

    auto list = armarx::StatechartGroupDocGenerator::FindAllStatechartGroupDefinitions(argv[1]);
    ARMARX_DEBUG_S << "Found " << list.size() << " Statechart Groups in " << argv[1];

    armarx::StatechartGroupDocGenerator generator;

    if (argc >= 3)
    {
        generator.setOutputpath(argv[2]);
    }

    generator.buildReferenceIndex(list);
    generator.generateDoxygenFiles(list);

    if (list.size() > 0)
    {
        ARMARX_DEBUG_S << "Generated " << list.size() << " files.";
    }
}
