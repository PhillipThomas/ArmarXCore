/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::StatechartPerfomanceTest
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_StatechartPerfomanceTest_StatechartPerfomanceTest_H
#define _ARMARX_COMPONENT_StatechartPerfomanceTest_StatechartPerfomanceTest_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

#include <IceUtil/Time.h>

//#define REMOTE

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT StatechartPerformanceTest :
#ifdef REMOTE
        virtual public RemoteStateOfferer
#else
        virtual public StatechartContext
#endif

    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "PerfTest";
        }
#ifdef REMOTE
        virtual void onInitRemoteStateOfferer();
#else
        virtual void onInitStatechart();
        virtual void onConnectStatechart();
        void onExitStatechart();

#endif
    };

    // Define Events before the first state they are used in
    DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
    DEFINEEVENT(EvNext)
    DEFINEEVENT(EvTimeout)

    // forward declare all substates
    struct StateRun;
    struct Statechart_StatechartPerfomanceTest : StateTemplate<Statechart_StatechartPerfomanceTest>
    {

        void defineState()
        {
            setUseRunFunction(false);
        }
        void defineParameters();
        void defineSubstates();
    };


    class StateRun : public StateTemplate<StateRun>
    {
    public:
        IceUtil::Time lastCall;
        int counter;

        ConditionIdentifier condId;
        void defineState()
        {
            setLocalMinimumLoggingLevel(eWARN);
            setUseRunFunction(false);
        }
        void defineParameters();
        void onEnter();
        void onExit();
    };


}

#endif
