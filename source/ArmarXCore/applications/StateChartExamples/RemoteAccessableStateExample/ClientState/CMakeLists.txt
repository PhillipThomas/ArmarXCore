
armarx_component_set_name(ClientState)


set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ArmarXCoreStatechart)

set(SOURCES main.cpp
    ClientState.cpp
    ClientState.h
    ClientStateApp.h
    )

armarx_add_component_executable("${SOURCES}")
