/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::RemoteAccessableStateExample::
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_TestCenter_TestCenter_H
#define _ARMARX_COMPONENT_TestCenter_TestCenter_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
//#include <interface/TestCenter/TestCenter.h>

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT ClientState :
        virtual public StatechartContext
    {
    public:

        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "ClientState";
        };
        virtual void onInitStatechart();
        virtual void onConnectStatechart();



    };

    //Event Definitions
    DEFINEEVENT(OuterTimer)

    struct StatechartClient : StateTemplate<StatechartClient>
    {
        void defineState()
        {
            //            setStateName("Client");
        }
        void defineParameters()
        {

            addToInput("x", VariantType::Float, true);
            addToInput("y", VariantType::Float, true);

            addToOutput("result", VariantType::Float, false);
        }

        void defineSubstates()
        {
            //add substates
            StateBasePtr remoteState = addRemoteState("add_x_to_y", "RemoteAccessableStateOfferer");
            setInitState(remoteState, createMapping()->mapFromParent("*", "*"));
            StatePtr finalSuccess = addState<SuccessState>("Success");
            StatePtr finalFailure = addState<FailureState>("Failure");

            // add transitions
            addTransition<OuterTimer>(getInitState(),
                                      finalFailure);
            addTransition<Success>(remoteState,
                                   finalSuccess);
        }
    };




}

#endif
