/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::TimeoutExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_TimeoutExample_TimeoutExample_H
#define _ARMARX_COMPONENT_TimeoutExample_TimeoutExample_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT TimeoutExample :
        virtual public StatechartContext
    {
    public:

        StatePtr statechart;

        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "TimeoutExample";
        };
        virtual void onInitStatechart();
        virtual void onConnectStatechart();
        void run();
    };
    struct stateInstallTimeout;
    DEFINEEVENT(Next)
    DEFINEEVENT(TimerExpired)
    struct TimeoutExampleStatechart : StateTemplate<TimeoutExampleStatechart>
    {
        void defineState()
        {
            //            setStateName("StatechartTimeoutexample");
        }
        void defineSubstates()
        {
            //add substates
            setInitState(addState<stateInstallTimeout>("InstallTimeout"));

            StateBasePtr finalSuccess = addState<SuccessState>("Success");
            StateBasePtr finalFailure = addState<FailureState>("Failure");

            // add transition
            addTransition<Next>(getInitState(),
                                getInitState());
            addTransition<TimerExpired>(getInitState(),
                                        finalFailure);
            addTransition<Success>(getInitState(),
                                   finalSuccess);

        }
    };


    struct DelayState;
    struct stateInstallTimeout : StateTemplate<stateInstallTimeout>
    {


        void onEnter()
        {

            setTimeoutEvent(5000, createEvent<TimerExpired>());
        }
        void onExit()
        {

        }
    };


}

#endif
