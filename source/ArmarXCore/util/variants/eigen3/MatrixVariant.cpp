#include "MatrixVariant.h"

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <Eigen/Geometry>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>


namespace armarx
{
    MatrixFloat::MatrixFloat()
    {
        rows = 0;
        cols = 0;
    }

    MatrixFloat::MatrixFloat(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->data.resize(rows * cols);
    }

    MatrixFloat::MatrixFloat(const Eigen::MatrixXf& m)
    {
        this->rows = m.rows();
        this->cols = m.cols();
        int i = 0;
        data = std::vector<float>(rows * cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                data[i] = m(row, col);
                i++;
            }
        }
    }


    MatrixFloat::MatrixFloat(int rows, int cols, const std::vector<float>& entries)
    {
        this->rows = rows;
        this->cols = cols;
        this->data = entries;
    }

    /*void MatrixFloat::setMatrix(int width, int height, const std::vector<float> &entries)
    {
        this->width = width;
        this->height = height;
        data = entries;
    }*/

    Eigen::MatrixXf MatrixFloat::toEigen() const
    {
        int i = 0;
        Eigen::MatrixXf m(rows, cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                m(row, col) = data[i];
                i++;
            }
        }

        return m;
    }

    std::vector<float> MatrixFloat::toVector() const
    {
        return data;
    }

    float& MatrixFloat::operator()(const int row, const int col)
    {
        return data.at(row + col * rows);
    }

    std::string MatrixFloat::toJsonRowMajor()
    {
        std::stringstream stream;
        stream << "[";

        for (int row = 0; row < rows; row++)
        {
            stream << (row > 0 ? ", [" : "[");

            for (int col = 0; col < cols; col++)
            {
                stream << (col > 0 ? ", " : "");
                stream << (*this)(row, col);
            }

            stream << "]";
        }

        stream << "]";
        return stream.str();
    }

    int MatrixFloat::readFromXML(const std::string& xmlData, const Ice::Current& c)
    {
        // TODO

        return 1;
    }

    std::string MatrixFloat::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        ptree pt;
        pt.add("rows", rows);
        pt.add("cols", cols);
        Eigen::MatrixXf m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << ",";
            }

            std::string str(ss.str());

            if (cols > 0)
            {
                str.erase(str.size() - 1);
            }

            pt.push_back(std::make_pair("", ptree(str)));
        }

#if BOOST_VERSION >= 105600
        boost::property_tree::xml_parser::xml_writer_settings<std::string> settings('\t', 1);
#else
        boost::property_tree::xml_parser::xml_writer_settings<char> settings('\t', 1);
#endif

        std::stringstream stream;
        xml_parser::write_xml(stream, pt, settings);
        return stream.str();
    }

    void MatrixFloat::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setInt("rows", rows);
        obj->setInt("cols", cols);
        StringList rowContent;
        Eigen::MatrixXf m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << (col < cols - 1 ? "," : "");
            }

            rowContent.push_back(ss.str());
        }

        obj->setStringArray("rowContent", rowContent);
    }

    void MatrixFloat::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        rows = obj->getInt("rows");
        cols = obj->getInt("cols");
        StringList rowContent;
        obj->getStringArray("rowContent", rowContent);

        if ((int)rowContent.size() != rows)
        {
            throw LocalException("unexcepted row count: ") << rowContent.size() << ", but expected " << rows;
        }

        data.resize(rows * cols);

        for (size_t row = 0; row < rowContent.size(); row++)
        {
            StringList values;
            boost::split(values, rowContent[row], boost::is_any_of(","));
            int col = 0;

            if ((int)values.size() != cols)
            {
                throw LocalException("unexcepted column count: ") << values.size() << ", but expected " << cols;
            }

            for (std::string v : values)
            {
                data.at(col * rows + row) = atof(v.c_str());
                col++;
            }
        }
    }





    MatrixDouble::MatrixDouble()
    {
        rows = 0;
        cols = 0;
    }

    MatrixDouble::MatrixDouble(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->data.resize(rows * cols);
    }

    MatrixDouble::MatrixDouble(const Eigen::MatrixXd& m)
    {
        this->rows = m.rows();
        this->cols = m.cols();
        int i = 0;
        data = std::vector<double>(rows * cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                data[i] = m(row, col);
                i++;
            }
        }
    }


    MatrixDouble::MatrixDouble(int rows, int cols, const std::vector<double>& entries)
    {
        this->rows = rows;
        this->cols = cols;
        this->data = entries;
    }

    /*void MatrixDouble::setMatrix(int width, int height, const std::vector<double> &entries)
    {
        this->width = width;
        this->height = height;
        data = entries;
    }*/

    Eigen::MatrixXd MatrixDouble::toEigen() const
    {
        int i = 0;
        Eigen::MatrixXd m(rows, cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                m(row, col) = data[i];
                i++;
            }
        }

        return m;
    }

    double& MatrixDouble::operator()(const int row, const int col)
    {
        return data.at(row + col * rows);
    }

    std::string MatrixDouble::toJsonRowMajor()
    {
        std::stringstream stream;
        stream << "[";

        for (int row = 0; row < rows; row++)
        {
            stream << (row > 0 ? ", [" : "[");

            for (int col = 0; col < cols; col++)
            {
                stream << (col > 0 ? ", " : "");
                stream << (*this)(row, col);
            }

            stream << "]";
        }

        stream << "]";
        return stream.str();
    }

    int MatrixDouble::readFromXML(const std::string& xmlData, const Ice::Current& c)
    {
        // TODO

        return 1;
    }

    std::string MatrixDouble::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        ptree pt;
        pt.add("rows", rows);
        pt.add("cols", cols);
        Eigen::MatrixXd m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << ",";
            }

            std::string str(ss.str());

            if (cols > 0)
            {
                str.erase(str.size() - 1);
            }

            pt.push_back(std::make_pair("", ptree(str)));
        }

#if BOOST_VERSION >= 105600
        boost::property_tree::xml_parser::xml_writer_settings<std::string> settings('\t', 1);
#else
        boost::property_tree::xml_parser::xml_writer_settings<char> settings('\t', 1);
#endif

        std::stringstream stream;
        xml_parser::write_xml(stream, pt, settings);
        return stream.str();
    }

    void MatrixDouble::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setInt("rows", rows);
        obj->setInt("cols", cols);
        StringList rowContent;
        Eigen::MatrixXd m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << (col < cols - 1 ? "," : "");
            }

            rowContent.push_back(ss.str());
        }

        obj->setStringArray("rowContent", rowContent);
    }

    void MatrixDouble::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        rows = obj->getInt("rows");
        cols = obj->getInt("cols");
        StringList rowContent;
        obj->getStringArray("rowContent", rowContent);

        if ((int)rowContent.size() != rows)
        {
            throw LocalException("unexcepted row count: ") << rowContent.size() << ", but expected " << rows;
        }

        data.resize(rows * cols);

        for (size_t row = 0; row < rowContent.size(); row++)
        {
            StringList values;
            boost::split(values, rowContent[row], boost::is_any_of(","));
            int col = 0;

            if ((int)values.size() != cols)
            {
                throw LocalException("unexcepted column count: ") << values.size() << ", but expected " << cols;
            }

            for (std::string v : values)
            {
                data.at(col * rows + row) = atof(v.c_str());
                col++;
            }
        }
    }
}
