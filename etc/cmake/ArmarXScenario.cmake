# Macros for generating ArmarX scenarios

set(CONFIG_DIR_RELATIVE "config")
set(DEPENDENCY_CONFIG_FILE "dependencies.cfg")
set(DEPENDENCY_REMOTE_CONFIG_FILE "dependencies.remote.cfg")

# Input Parameters:
# * SCENARIO_NAME:          Name of the scenario
# * SCENARIO_COMPONENTS:    List of Components included in scenario
#
# Optional 3rd Parameter:
# * SCENARIO_GLOBAL_CONFIG: Config file which gets added first to --Ice.Config of each Component
#
# Generates default config file for each entry in SCENARIO_COMPONENTS.
# Generates start-/stopScenario.sh scripts
macro(armarx_scenario _SCENARIO_NAME _SCENARIO_COMPONENTS)
    set(_GLOBAL_CONFIG "${ARGV2}")
    if (_GLOBAL_CONFIG)
        set(GLOBAL_CONFIG "${_GLOBAL_CONFIG}")
    endif()

    set(_SCENARIO_COMPONENT_CONFIG_FILES "")
    foreach(_SCENARIO_COMPONENT ${_SCENARIO_COMPONENTS})
        list(APPEND _SCENARIO_COMPONENT_CONFIG_FILES "${CONFIG_DIR_RELATIVE}/${_SCENARIO_COMPONENT}.cfg")
    endforeach()

    generate_scenario("${_SCENARIO_NAME}" "${_SCENARIO_COMPONENTS}" "${_SCENARIO_COMPONENT_CONFIG_FILES}" "${GLOBAL_CONFIG}")
endmacro()


# Input Parameters:
# * SCENARIO_NAME:          Name of the scenario
# * SCENARIO_CONFIGS:       List of Component configuration files included in scenario
#                           required format:
#                               config/ComponentName.optionalString.cfg
#
#   WARNING:    All configuration files must be specified with the config/ prefix and .cfg suffix.
#               Additionally the file itself must start with the component name followed by
#               an additional . prefixed string
#
# Optional 3rd Parameter:
# * SCENARIO_GLOBAL_CONFIG: Config file which gets added first to --Ice.Config of each Component
#
# Generates a start and stop command for every entry in SCENARIO_CONFIGS and creates the
# config files if necessary.
macro(armarx_scenario_from_configs _SCENARIO_NAME _SCENARIO_CONFIGS)
    set(_GLOBAL_CONFIG "${ARGV2}")
    if (_GLOBAL_CONFIG)
        set(GLOBAL_CONFIG "${_GLOBAL_CONFIG}")
    endif()

    set(VALID_CONFIGS "")
    # check validity of configuration files
    foreach(_SCENARIO_CONFIG ${_SCENARIO_CONFIGS})
        # match if string starts with config/ and ends with .cfg
        string(REGEX MATCH "^config/.*\\.cfg$" CONFIG_NAME "${_SCENARIO_CONFIG}")
        if (CONFIG_NAME)
            list(APPEND VALID_CONFIGS "${CONFIG_NAME}")
        else()
            message(STATUS "    Invalid config file: ${_SCENARIO_CONFIG}")
            message(STATUS "        Required Format: ${CONFIG_DIR_RELATIVE}/*.cfg")
        endif()
    endforeach()

    set(DETECTED_COMPONENTS "")
    # extract Component names from configuration file format which is
    # ComponentName[.OptionalSpecifier].cfg
    foreach(_SCENARIO_CONFIG ${VALID_CONFIGS})
        # REGEX matches the following parts: config/ (any character but .) (optional characters) .cfg
        # the first characters after config/ up to the first . are considered the ComponentName
        string(REGEX REPLACE "^config/([^.]*)(.*)?\\.cfg$" "\\1" CONFIG_NAME ${_SCENARIO_CONFIG})
        list(APPEND DETECTED_COMPONENTS "${CONFIG_NAME}")
    endforeach()

    generate_scenario("${_SCENARIO_NAME}" "${DETECTED_COMPONENTS}" "${VALID_CONFIGS}" "${GLOBAL_CONFIG}")
endmacro()


# generates the start-/stopScenario.sh scripts according to the given parameters
# * _SCENARIO_NAME:                 Name of the scenario
# * _SCENARIO_COMPONENTS_P:         Name of all Components to start
# * _SCENARIO_CONFIGURATIONS_P:     Name of configuration files for Components from previous parameter
# * _SCENARIO_GLOBAL_CONFIGURATION: Name of global configuration file (can be left specified as "")
#
# Each component gets written to the start-/stopScenario.sh scripts together with the
# according configuration file file.
# Component and config file are 'matched' by the list index of _SCENARIO_COMPONENTS_P and _SCENARIO_CONFIGURATIONS_P
macro(generate_scenario _SCENARIO_NAME _SCENARIO_COMPONENTS_P _SCENARIO_CONFIGURATIONS_P _SCENARIO_GLOBAL_CONFIGURATION)
    # copy lists: otherwise it will have a length of 0
    set(_SCENARIO_COMPONENTS ${_SCENARIO_COMPONENTS_P})
    set(_SCENARIO_CONFIGURATIONS ${_SCENARIO_CONFIGURATIONS_P})

    armarx_set_target("Scenario: ${_SCENARIO_NAME}")
    ARMARX_MESSAGE(STATUS "    Components:")
    if(${VERBOSE})
        printlist("        " "${_SCENARIO_COMPONENTS}")
    endif()
    # ARMARX_PROJECT_DEPENDENCIES is generated from depends_on_armarx_package() calls
    # in the toplevel CMakeLists.txt of the project
    ARMARX_MESSAGE(STATUS "    Dependencies:")
    if(${VERBOSE})
        printlist("        " "${ARMARX_PROJECT_DEPENDENCIES}")
    endif()

   if (NOT "${_SCENARIO_GLOBAL_CONFIGURATION}" STREQUAL "")
        # add comma at the end so we don't need to add it later on
        set(_SCENARIO_GLOBAL_CONFIG_WITH_COMMA "${_SCENARIO_GLOBAL_CONFIGURATION},")
        set(_SCENARIO_GLOBAL_CONFIG_WITH_COMMA_ABSOLUTE "${CMAKE_CURRENT_SOURCE_DIR}/${_SCENARIO_GLOBAL_CONFIG_WITH_COMMA}")
        ARMARX_MESSAGE(STATUS "    Global Configuration File:")
        ARMARX_MESSAGE(STATUS "        ${_SCENARIO_GLOBAL_CONFIGURATION}")
    endif()

    set(_SCENARIO_START_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/startScenario.sh")
    set(_SCENARIO_STOP_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/stopScenario.sh")
    # elements are added to this list in generate_icegrid_component_xml()
    set(ICEGRID_COMPONENT_XML_FILES "")

    generate_datapath_config()

    # only proceed if scenario components is not empty
    if(_SCENARIO_COMPONENTS)
        initialize_start_scenario("${_SCENARIO_START_SCRIPT}")
        initialize_stop_scenario("${_SCENARIO_STOP_SCRIPT}")

        # These variables are set in Installation.cmake and are not visible here.
        # Therefore, they get created in the local scope.
        set("${ARMARX_PROJECT_NAME}_EXECUTABLE" ${ARMARX_PROJECT_EXES})
        set("${ARMARX_PROJECT_NAME}_BINARY_DIR" "${ARMARX_BIN_DIR}")

        # use the index for enumeration since two lists must be iterated simultaneously
        list(LENGTH _SCENARIO_COMPONENTS COMPONENT_COUNT)
        math(EXPR COMPONENT_COUNT "${COMPONENT_COUNT} - 1")

        # Add dependency on ArmarXCore but not for ArmarXCore itself
        if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
            set(CURRENT_PROJECT_NAME ${PROJECT_NAME})
        endif()

        foreach(COMPONENT_INDEX RANGE ${COMPONENT_COUNT})
            list(GET _SCENARIO_COMPONENTS ${COMPONENT_INDEX} _SCENARIO_COMPONENT)
            list(GET _SCENARIO_CONFIGURATIONS ${COMPONENT_INDEX} _SCENARIO_CONFIG)
            set(_SCENARIO_COMPONENT_EXECUTABLE "${_SCENARIO_COMPONENT}Run")
            set(_SCENARIO_COMPONENT_CONFIG_FILE "${_SCENARIO_CONFIG}")
            set(_SCENARIO_COMPONENT_FOUND FALSE)
            set(_SCENARIO_COMPONENT_PACKAGE "")

            # detect if and in which package the executable exists
            foreach (DEPENDENT_PROJECT ${ARMARX_PROJECT_DEPENDENCIES} ${CURRENT_PROJECT_NAME})
                list(FIND "${${DEPENDENT_PROJECT}_EXECUTABLE}" "${_SCENARIO_COMPONENT_EXECUTABLE}" COMPONENT_IN_PROJECT)
                set(COMPONENT_PATH "${${DEPENDENT_PROJECT}_BINARY_DIR}/${_SCENARIO_COMPONENT_EXECUTABLE}")
                set(_SCENARIO_COMPONENT_BINARY_DIR "${${DEPENDENT_PROJECT}_BINARY_DIR}")

                if (COMPONENT_IN_PROJECT AND EXISTS "${COMPONENT_PATH}")
                    list(APPEND _SCENARIO_COMPONENT_PACKAGE ${DEPENDENT_PROJECT})
                    if (_SCENARIO_COMPONENT_FOUND)
                        #message(WARNING "    Component ${_SCENARIO_COMPONENT} is available in multiple ArmarX Packages: ${_SCENARIO_COMPONENT_PACKAGE}\n        ${COMPONENT_PATH} will be used.")
                    else()
                        set(_SCENARIO_COMPONENT_FOUND TRUE)
                        ARMARX_MESSAGE(STATUS "    Using Component at ${COMPONENT_PATH}")
                        generate_config("${COMPONENT_PATH}" "${CMAKE_CURRENT_SOURCE_DIR}/${_SCENARIO_COMPONENT_CONFIG_FILE}")
                        generate_scripts()
                        generate_icegrid_component_xml("${_SCENARIO_COMPONENT_BINARY_DIR}" "${DEPENDENT_PROJECT}" "${_SCENARIO_NAME}")
                    endif()
                endif()
            endforeach()

            if (NOT _SCENARIO_COMPONENT_FOUND)
                message(WARNING "    Component ${_SCENARIO_COMPONENT} could not be found in the list of ArmarX Package dependencies.\n    Did you forget to specify the correct Package or was ${_SCENARIO_COMPONENT} not build?")
            endif()
        endforeach()

        finalize_start_scenario("${_SCENARIO_START_SCRIPT}")
        generate_icegrid_application_xml("${_SCENARIO_NAME}")

        # make the scripts executable if platform is not windows
        if (NOT WIN32)
            execute_process(COMMAND chmod a+x "${_SCENARIO_START_SCRIPT}"
                            COMMAND chmod a+x "${_SCENARIO_STOP_SCRIPT}")
        endif()
    endif()
endmacro()


# This macro generates the ${DEPENDENCY_CONFIG_FILE}.
# It contains the single property ArmarX.ProjectDatapath.
# This property contains the list of *_DATA_DIR entries from all dependent projects separated by ';'.
macro(generate_datapath_config)
    file(REMOVE "${CONFIG_DIR_RELATIVE}/datapath.cfg")
    file(REMOVE "${CONFIG_DIR_RELATIVE}/datapath.remote.cfg")

    set(ARMARX_SCENARIO_DATA_DIRS "${ARMARX_PROJECT_DEPENDENT_DATA_DIRS}")
    configure_file("${ArmarXCore_TEMPLATES_DIR}/config/dependencies.cfg.in" "${PACKAGE_CONFIG_DIR}/${DEPENDENCY_CONFIG_FILE}" @ONLY)

    set(ARMARX_SCENARIO_DATA_DIRS "")
    foreach(DEPENDENT_PROJECT_NAME ${ARMARX_PROJECT_DEPENDENCIES} ${ARMARX_PROJECT_NAME})
        set(ARMARX_SCENARIO_DATA_DIRS "${ARMARX_SCENARIO_DATA_DIRS};${POSTSYNC_SHARE_DIR}/${DEPENDENT_PROJECT_NAME}/data")
    endforeach()
    configure_file("${ArmarXCore_TEMPLATES_DIR}/config/dependencies.cfg.in" "${PACKAGE_CONFIG_DIR}/${DEPENDENCY_REMOTE_CONFIG_FILE}"  @ONLY)
endmacro()


# generate default config file if it does not exist
# EXISTS check and generate_config() needs a full path in order to work,
# elsewhere we use relative paths
macro(generate_config COMPONENT_EXECUTABLE_PATH COMPONENT_CONFIG_FILE)
    if (NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${_SCENARIO_COMPONENT_CONFIG_FILE}")
        ARMARX_MESSAGE(STATUS "    Generating ${_SCENARIO_COMPONENT_CONFIG_FILE}")
        if(${COMPONENT_EXECUTABLE_PATH} MATCHES "(.+)Run$")
            ARMARX_MESSAGE(STATUS "${COMPONENT_EXECUTABLE_PATH} --print-options --options-format=config")
            file(WRITE "${COMPONENT_CONFIG_FILE}" "")
            execute_process(COMMAND "${COMPONENT_EXECUTABLE_PATH}" --print-options --options-format=config
                            OUTPUT_FILE "${COMPONENT_CONFIG_FILE}")
        else()
            message(STATUS "    ${COMPONENT_NAME}Run is not a valid ArmarX Component")
            message(STATUS "    Please note that the suffix 'Run' is appended automatically")
        endif()
    endif()
endmacro()


# All variables referenced in this macro are set within the generate_scenario() macro.
# It is possible to do this since the macro replacement takes place in the same context
# as generate_scenario().
macro(generate_scripts)
    # add entry to startScenario.sh
    # _SCENARIO_GLOBAL_CONFIG_WITH_COMMA is defined if macro is called with three parameters
    # the necessary comma for separating the two config files is added when setting the contents of the variable
    ARMARX_MESSAGE(STATUS "    Add ${_SCENARIO_COMPONENT} to startScenario.sh")
    append_to_start_scenario("${_SCENARIO_START_SCRIPT}" "${COMPONENT_PATH}" "${_SCENARIO_GLOBAL_CONFIG_WITH_COMMA}${_SCENARIO_COMPONENT_CONFIG_FILE}")

    # add entry to stopScenario.sh
    ARMARX_MESSAGE(STATUS "    Add ${_SCENARIO_COMPONENT} to stopScenario.sh")
    append_to_stop_scenario("${_SCENARIO_STOP_SCRIPT}" "${_SCENARIO_COMPONENT_EXECUTABLE}")
endmacro()


macro(generate_icegrid_component_xml _SCENARIO_COMPONENT_BINARY_DIR _COMPONENT_PACKAGE_NAME _SCENARIO_NAME)
    # REGEX matches the following parts: config/ (any character but .) (optional characters) .cfg
    # the first characters after config/ up to the first . are considered the ComponentName
    # the second part is a unique extension to differentiate configurations of the same component
    string(REGEX REPLACE "^config/([^.]*)(.*)?\\.cfg$" "\\1\\2" SCENARIO_EXTENDED_COMPONENT_NAME ${_SCENARIO_COMPONENT_CONFIG_FILE})
    string(REGEX REPLACE "^config/([^.]*)(.*)?\\.cfg$" "\\2" ARMARX_COMPONENT_ID ${_SCENARIO_COMPONENT_CONFIG_FILE})
    set(SCENARIO_COMPONENT_XML "${CMAKE_CURRENT_SOURCE_DIR}/icegrid/${SCENARIO_EXTENDED_COMPONENT_NAME}.icegrid.xml")
    list(APPEND ICEGRID_COMPONENT_XML_FILES "icegrid/${SCENARIO_EXTENDED_COMPONENT_NAME}.icegrid.xml")
    set(ARMARX_COMPONENT_NAME ${_SCENARIO_COMPONENT})

    # Ice.Config files are separated by comma
    if (NOT "${_SCENARIO_GLOBAL_CONFIG_WITH_COMMA}" STREQUAL "")
        set(ARMARX_COMPONENT_CONFIG "\${ARMARX_SCENARIO_DIR}/${_SCENARIO_GLOBAL_CONFIG_WITH_COMMA}\${ARMARX_SCENARIO_DIR}/${_SCENARIO_COMPONENT_CONFIG_FILE}")
    else()
        set(ARMARX_COMPONENT_CONFIG "\${ARMARX_SCENARIO_DIR}/${_SCENARIO_COMPONENT_CONFIG_FILE}")
    endif()

    set(PACKAGE_BIN_DIR_VAR_NAME "\${${_COMPONENT_PACKAGE_NAME}_BIN_DIR}")

    configure_file(${ArmarXCore_TEMPLATES_DIR}/ComponentTemplate/component.icegrid.xml.in ${SCENARIO_COMPONENT_XML} @ONLY NEWLINE_STYLE UNIX)
endmacro()


macro(generate_icegrid_application_xml _SCENARIO_NAME)
    ARMARX_MESSAGE(STATUS "    Generating IceGrid Deployment: ${_SCENARIO_NAME}.icegrid.xml")
    set(SCENARIO_APPLICATION_XML "${CMAKE_CURRENT_SOURCE_DIR}/${_SCENARIO_NAME}.icegrid.xml")
    set(SCENARIO_REMOTE_APPLICATION_XML "${CMAKE_CURRENT_SOURCE_DIR}/${_SCENARIO_NAME}.remote.icegrid.xml")
    set(ARMARX_SCENARIO_NAME "${_SCENARIO_NAME}")
    set(ARMARX_DEFAULT_NODE "NodeMain")

    # generate list of all component.icegrid.xml files to include in the application
    set(ARMARX_COMPONENT_XML_INCLUDE "\n")
    foreach(CURRENT_ICEGRID_COMPONENT_XML_FILE ${ICEGRID_COMPONENT_XML_FILES})
        set(ARMARX_COMPONENT_XML_INCLUDE "${ARMARX_COMPONENT_XML_INCLUDE}            <include file=\"${CURRENT_ICEGRID_COMPONENT_XML_FILE}\"/>\n")
    endforeach()

    #
    # local configuration
    #
    # generate a list of all local package binary directories
    set(ARMARX_PACKAGE_BIN_DIRS "\n")
    foreach (DEPENDENT_PROJECT ${ARMARX_PROJECT_DEPENDENCIES} ${CURRENT_PROJECT_NAME})
        set(ARMARX_PACKAGE_BIN_DIRS "${ARMARX_PACKAGE_BIN_DIRS}        <variable name=\"${DEPENDENT_PROJECT}_BIN_DIR\" value=\"${${DEPENDENT_PROJECT}_BINARY_DIR}\"/>\n")
    endforeach()
    # not required, linked with rpath
    #set(ArmarX_LD_LIB_PATH "\${LD_LIBRARY_PATH}")
    set(ARMARX_SCENARIO_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    set(ARMARX_SCENARIO_DEPENDENCY_CONFIG "${DEPENDENCY_CONFIG_FILE}")
    set(ARMARX_PACKAGE_CONFIG_DIR "${PACKAGE_CONFIG_DIR}")

    configure_file("${ArmarXCore_TEMPLATES_DIR}/ComponentTemplate/application.icegrid.xml.in" "${SCENARIO_APPLICATION_XML}" @ONLY NEWLINE_STYLE UNIX)

    #
    # sync configuration
    #
    # generate a list of all remote package binary directories
    set(ARMARX_PACKAGE_BIN_DIRS "\n")
    foreach (DEPENDENT_PROJECT ${ARMARX_PROJECT_DEPENDENCIES} ${CURRENT_PROJECT_NAME})
        set(ARMARX_PACKAGE_BIN_DIRS "${ARMARX_PACKAGE_BIN_DIRS}        <variable name=\"${DEPENDENT_PROJECT}_BIN_DIR\" value=\"${POSTSYNC_BIN_DIR}\"/>\n")
    endforeach()
    set(ArmarX_LD_LIB_PATH "${POSTSYNC_LIB_DIR}")

    # determin path to configuration files
    set(ARMARX_COMPONENT_BIN_DIR "${POSTSYNC_BIN_DIR}")
    if(_SCENARIO_GLOBAL_CONFIG_WITH_COMMA)
        set(_SCENARIO_GLOBAL_CONFIG_WITH_COMMA "${POSTSYNC_SHARE_DIR}/${ARMARX_PROJECT_NAME}/scenarios/${_SCENARIO_NAME}/${_SCENARIO_GLOBAL_CONFIG_WITH_COMMA}")
    endif()
    string(REGEX REPLACE "^.*/${ARMARX_PROJECT_NAME}/scenarios(/?.*/).*$" "\\1" SUBDIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    set(ARMARX_SCENARIO_DIR "${POSTSYNC_SHARE_DIR}/${ARMARX_PROJECT_NAME}/scenarios${SUBDIRECTORY}${_SCENARIO_NAME}")
    set(ARMARX_SCENARIO_DEPENDENCY_CONFIG "${DEPENDENCY_REMOTE_CONFIG_FILE}")
    set(ARMARX_PACKAGE_CONFIG_DIR "\${ARMARX_SYNC_DIR}/share/${PROJECT_NAME}/config")

    configure_file("${ArmarXCore_TEMPLATES_DIR}/ComponentTemplate/application.icegrid.xml.in" "${SCENARIO_REMOTE_APPLICATION_XML}" @ONLY NEWLINE_STYLE UNIX)
endmacro()


macro(initialize_start_scenario SCRIPT_FILENAME)
    file(WRITE  "${SCRIPT_FILENAME}" "#!/bin/bash\n")
    file(APPEND "${SCRIPT_FILENAME}" "\n")
    file(APPEND "${SCRIPT_FILENAME}" "# Components\n")
    file(APPEND "${SCRIPT_FILENAME}" "\n")
endmacro()

macro(append_to_start_scenario SCRIPT_FILENAME COMPONENT_EXECUTABLE_PATH COMPONENT_CONFIGURATION_FILE)
    file(APPEND "${SCRIPT_FILENAME}" "${COMPONENT_EXECUTABLE_PATH} --Ice.Config=${CMAKE_CURRENT_SOURCE_DIR}/${COMPONENT_CONFIGURATION_FILE} --ArmarX.DependenciesConfig=${PACKAGE_CONFIG_DIR}/${DEPENDENCY_CONFIG_FILE} &\n\n")
endmacro()

macro(finalize_start_scenario SCRIPT_FILENAME)
    file(APPEND "${SCRIPT_FILENAME}" "if [ \"\$1\" == \"-w\" ]; then\n")
    file(APPEND "${SCRIPT_FILENAME}" "for j in `jobs -p`; do\n")
    file(APPEND "${SCRIPT_FILENAME}" "wait \$j\n")
    file(APPEND "${SCRIPT_FILENAME}" "done\n")
    file(APPEND "${SCRIPT_FILENAME}" "fi\n")
endmacro()

macro(initialize_stop_scenario SCRIPT_FILENAME)
    file(WRITE  "${SCRIPT_FILENAME}" "#!/bin/bash\n")
    file(APPEND  "${SCRIPT_FILENAME}" "if test -z \"$1\"\n")
    file(APPEND  "${SCRIPT_FILENAME}" "then\n")
    file(APPEND  "${SCRIPT_FILENAME}" "    KILL_SIGNAL=2\n")
    file(APPEND  "${SCRIPT_FILENAME}" "else\n")
    file(APPEND  "${SCRIPT_FILENAME}" "    KILL_SIGNAL=\$1\n")
    file(APPEND  "${SCRIPT_FILENAME}" "fi\n\n")
    file(APPEND  "${SCRIPT_FILENAME}" "skill ()\n{\n")
    file(APPEND "${SCRIPT_FILENAME}" "    EXECUTABLE_STR=`ps aux | grep \"$1\" | grep -v grep | awk '{print $2}'`\n")
    file(APPEND "${SCRIPT_FILENAME}" "    if [ \${#EXECUTABLE_STR} == 0 ]\n")
    file(APPEND "${SCRIPT_FILENAME}" "    then\n")
    file(APPEND "${SCRIPT_FILENAME}" "        echo skipping $1\n")
    file(APPEND "${SCRIPT_FILENAME}" "    else\n")
    file(APPEND "${SCRIPT_FILENAME}" "        echo killing $1\n")
    file(APPEND "${SCRIPT_FILENAME}" "        kill -s $2 \${EXECUTABLE_STR}\n")
    file(APPEND "${SCRIPT_FILENAME}" "    fi\n")
    file(APPEND "${SCRIPT_FILENAME}" "}\n\n")
    file(APPEND "${SCRIPT_FILENAME}" "echo SENDING \${KILL_SIGNAL} TO ALL PROCESSES\n\n")
endmacro()


macro(append_to_stop_scenario SCRIPT_FILENAME COMPONENT_EXECUTABLE_NAME)
    file(APPEND "${SCRIPT_FILENAME}" "skill ${COMPONENT_EXECUTABLE_NAME} \${KILL_SIGNAL}\n\n")
endmacro()
