
find_package(CppCheck QUIET)

if(CppCheck_FOUND AND NOT ARMARX_OS_WIN)
    set(CppCheck_OUTPUT_FILE "${CMAKE_BINARY_DIR}/cppcheck.xml")
    set(CppCheck_ARGS --enable=all --inconclusive --xml --xml-version=2 ${PROJECT_SOURCECODE_DIR} 2> ${CppCheck_OUTPUT_FILE})
    add_custom_target(cppcheck
        COMMAND ${CppCheck_EXECUTABLE} ${CppCheck_ARGS}
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
        COMMENT "Generating cppcheck report for ${PROJECT_SOURCECODE_DIR}"
        VERBATIM)
    # to make the files readable by jenkins
    # https://wiki.jenkins-ci.org/display/JENKINS/Cppcheck+Plugin
    # SUB_DIR := PROJECT_SOURCECODE_DIR - JENKINS_WORKSPACE_DIR
    # sed -i 's%\(<location file="\)%\1SUB_DIR//%' cppcheck.xml
else()
    add_custom_target(cppcheck 
        COMMENT "cppcheck NOT found")
endif()
