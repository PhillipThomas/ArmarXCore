#!/bin/bash

usage() {
	cat << END
Usage:
	$(basename $0) ARMARX_USER_CODE_DIR ARMARX_PATCH_HOST ARMARX_PATCH_PORT
END
}

ARMARX_PATCH_ARGUMENTS=(${SSH_ORIGINAL_COMMAND})
if [ ${#ARMARX_PATCH_ARGUMENTS[@]} -ne 3 ]; then
        echo "Illegal number of parameters" >&2
        usage >&2
        exit 1
fi

mkdir -p ${ARMARX_PATCH_ARGUMENTS[0]}

icepatch2client -t --IcePatch2Client.Proxy="IcePatch2/server:tcp -h ${ARMARX_PATCH_ARGUMENTS[1]} -p ${ARMARX_PATCH_ARGUMENTS[2]}" "${ARMARX_PATCH_ARGUMENTS[0]}"
