#!/bin/bash

# set PORT to use for the IcePatchServer
[[ -z $ARMARX_PATCH_PORT ]] && ARMARX_PATCH_PORT="5555"

# try to read the directory which should be synchronized to the robot
[[ -z $ARMARX_DEPLOY_DIR ]] && read -p "ARMARX_DEPLOY_DIR = " ARMARX_DEPLOY_DIR

if [[ -z $ARMARX_USER_CODE_DIR ]]; then
	if [[ -z $USER ]]; then
		echo "You have set neither ARMARX_USER_CODE_DIR nor USER. Set ARMARX_USER_CODE_DIR and/or USER and run the script again."
		exit 2
	else
		ARMARX_USER_CODE_DIR="$USER"
	fi
fi
WHERE="$(dirname $0)"

if [ -f $HOME/.ssh/armarx-receive ]; then
	SSH_KEY="$HOME/.ssh/armarx-receive"
else
	SSH_KEY="$WHERE/armarx-receive"
fi
[[ $(stat -c %a "$SSH_KEY") == 400 ]] || {chmod 400 "$SSH_KEY" || exit}

# try to retreive the IP address which is used to communicate with ARMAR-IV
# it has to be in the range of 10.4.2.*
if which ip &>/dev/null; then
	ARMARX_PATCH_HOST="$(ip a | grep 'inet ' | grep '10.4.2.' | awk '{ print $2}' | cut -d'/' -f1)"
elif which ifconfig &> /dev/null; then
	ARMARX_PATCH_HOST="$(ifconfig | grep 'inet ' | grep '10.4.2.' | awk '{print $2}')"
else
	read -p "Enter your local ip address for communication with ARMAR: " ARMARX_PATCH_HOST
fi
[[ -z $ARMARX_PATCH_HOST ]] && read -p "Enter your local ip address for communication with ARMAR: " ARMARX_PATCH_HOST

ARMARX_PATCH_CLIENTS=("armar4-left" "armar4-right" "armar4-head")
ARMARX_PATCH_USER="armar-user"

# calculate checksums for all files to synchronize
icepatch2calc ${ARMARX_DEPLOY_DIR}

# start the icepatch2server locally
icepatch2server --IcePatch2.Endpoints="tcp -h ${ARMARX_PATCH_HOST} -p ${ARMARX_PATCH_PORT}" --IcePatch2.Directory=${ARMARX_DEPLOY_DIR} &

SERVER_PID="$!"

die() {
	kill $SERVER_PID
	exit $1
}

trap "die 3" SIGINT SIGTERM

for client in ${ARMARX_PATCH_CLIENTS[@]}; do
    echo "Starting IcePatch2 Client on host: ${client}"
	ssh -o UserKnownHostsFile="${WHERE}/known_hosts" -i "$SSH_KEY" -n ${ARMARX_PATCH_USER}@${client} "${ARMARX_USER_CODE_DIR} ${ARMARX_PATCH_HOST} ${ARMARX_PATCH_PORT}" &
	CLIENT_PIDS=("${CLIENT_PIDS[@]}" "$!")
done

wait $(printf "%s " ${CLIENT_PIDS[@]})
if [[ $? == 0 ]]; then
	kill ${SERVER_PID}
	echo "${ARMARX_DEPLOY_DIR} has finished syncronizing."
else
	echo "Something went wrong." 1>&2
	die 2
fi
