#!/bin/bash
find . -iname *.hpp -exec chmod a-x {} \;
find . -iname *.h -exec chmod a-x {} \;
find . -iname *.cpp -exec chmod a-x {} \;
find . -iname *.txt -exec chmod a-x {} \;
find . -iname *.dox -exec chmod a-x {} \;

