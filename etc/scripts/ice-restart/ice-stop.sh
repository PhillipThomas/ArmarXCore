#!/bin/bash

# import user defined variables
ARMARX_DEFAULTS_DIR="$HOME/.armarx/"
ARMARX_USER_VARIABLES_FILE="IceGridUserVariables.sh"

source ${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}

for SHUTDOWN_NAME in ${ICE_GRID_NODE2_NAME} ${ICE_GRID_NODE1_NAME} ${ICE_GRID_REGISTRY_NAME}
do
    echo "Shutting down node ${SHUTDOWN_NAME} running on: <${ICE_GRID_REGISTRY_HOST}:${ICE_GRID_REGISTRY_PORT}>"
    icegridadmin --Ice.Default.Locator="IceGrid/Locator:tcp -p ${ICE_GRID_REGISTRY_PORT} -h ${ICE_GRID_REGISTRY_HOST}" \
                 -u x -p y \
                 -e "node shutdown ${SHUTDOWN_NAME}"
done

sleep 2
killall icegridnode -9
killall icebox -9
killall icegridadmin -9
