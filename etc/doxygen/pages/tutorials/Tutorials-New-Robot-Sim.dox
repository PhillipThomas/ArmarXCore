/**
\page ArmarXCore-Tutorials-new-robot-sim New Robot Tutorial 1: Working with a new robot

\tableofcontents

This tutorial will guide you through creating a new package and starting the simulation with a new robot model.
You will need:
\li a working installation of ArmarX
\li a model of your robot in the simox xml format

As an example, this tutorial will use the model of youBot. The end goal will be starting a simulation with the new robot.

\section ArmarXCore-Tutorials-new-robot-sim-new-package Creating a new package
Since you'll need a place to start your scenarios, a new package will need to be created.
You can create a new package in your current folder by executing the following command:
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init YouBotTutorial --dir=.
\endverbatim

For more information about the ArmarX package tool, refer to \ref armarx-package.
This tutorial will refer to the new package as ${RobotPackage}, and to its path as ${RobotPackage_DIR}

\section ArmarXCore-Tutorials-new-robot-sim-setup-package Setting up the package
Begin by copying your robot definition file (```YouBot.xml```) and all necessary data, such as models, into ```${RobotPackage_DIR}/data/${RobotPackage}```.
Next, you'll need to adjust the ```${RobotPackage_DIR}/CMakeLists.txt``` in the package to use other ArmarX components.
Add these lines after the line starting with ```armarx_project(...)```
\verbatim
depends_on_armarx_package(MemoryX)
depends_on_armarx_package(RobotAPI)
depends_on_armarx_package(ArmarXSimulation)
\endverbatim
Afterwards, run cmake to ensure that your package finds all dependencies:
\verbatim
cd ${RobotPackage_DIR}/build
cmake ..
\endverbatim
<!-- cmake -DARMARX_ENABLE_DEPENDENCY_VERSION_CHECK=OFF ..

\warning You'll note that cmake is called with an additional argument to disable the version checking. See ??? for why this is necessary.
\warning If you were to leave the the version checking on, you'd need to ensure that your package has the same version number as the dependencies. -->

\section ArmarXCore-Tutorials-new-robot-sim-new-scenario Creating a new scenario
Now that the package is set up, a scenario is needed to execute the simulation. You can do that by using the package tool again:
\verbatim
cd ${RobotPackage_DIR}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package add scenario YouBotSimulation
\endverbatim
This will create an empty scenario that needs to be configured. The configuration is in ```${RobotPackage_DIR}/scenarios/YouBotSimulation/CMakeLists.txt```.
open the file with your favorite editor and make sure the following components are included:
\verbatim
set(SCENARIO_CONFIGS
        config/CommonStorage.cfg
        config/PriorKnowledge.cfg
        config/WorkingMemory.cfg

        config/SimulatorApp.cfg
        config/SimulatorViewerApp.cfg
)
\endverbatim
Make also sure to comment out the line ```armarx_scenario("YouBotSimulation" "${SCENARIO_COMPONENTS}")``` and to uncomment ```armarx_scenario_from_configs("YouBotSimulation" "${SCENARIO_CONFIGS}")```
The config files don't exist yet, but they'll be added to the ```config``` subfolder in the scenario after you rerun cmake.
You'll also notice that running cmake added two scripts - startScenario.sh and stopScenario.sh - which will be used to start/stop the scenario.
The only thing left is to configure each component.

You have to insert the following lines (uncomment or add if necessary):
<table>
        <tr><td>File</td><td>Lines to insert</td></tr>

<tr><td>CommonStorage.cfg</td><td><pre>
MemoryX.CommonStorage.MongoUser = ""
MemoryX.CommonStorage.MongoPassword = ""
</pre></td></tr>

<tr><td>PriorKnowledge.cfg</td><td><pre>
MemoryX.PriorKnowledge.ClassCollections = testdb.testCollection
</pre></td></tr>

<tr><td>WorkingMemory.cfg</td><td><pre>
#no configuration needed
</pre></td></tr>

<tr><td>SimulatorApp.cfg</td><td><pre>
ArmarX.Simulator.FixedTimeStepStepTimeMS = 1
ArmarX.Simulator.FixedTimeStepLoopNrSteps = 3
ArmarX.Simulator.FloorTexture = "SimulationX/images/floor.png"
ArmarX.Simulator.RobotFileName=YouBotTutorial/YouBot.xml
</pre></td></tr>

<tr><td>SimulatorViewerApp.cfg</td><td><pre>
ArmarX.SimulatorViewer_SimulationWindow.ShowBaseCoordSystem = false
ArmarX.SimulatorViewer_SimulationWindow.AntiAliasingRenderPasses = 4
ArmarX.SimulatorViewer_SimulationWindow.Camera.x = 0
ArmarX.SimulatorViewer_SimulationWindow.Camera.y = 4000
ArmarX.SimulatorViewer_SimulationWindow.Camera.z = 2500
ArmarX.SimulatorViewer_SimulationWindow.Camera.roll = 1.2
ArmarX.SimulatorViewer_SimulationWindow.Camera.yaw = 3.14
</pre></td></tr>
</table>
\note SimulatorApp.cfg specifies youBot as the robot, make sure you replace the filename with your robot

\section ArmarXCore-Tutorials-new-robot-sim-start-scenario Starting the scenario
Before continuing, make sure you've started ice and the database with the following commands:
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx start
${ArmarX_DIR}/MemoryX/build/bin/mongod.sh start
\endverbatim
Once done, you can start the scenario in the scenario folder with
\verbatim
./startScenario.sh
\endverbatim
Doing so should start the simulation, and show you the specified robot.
\image html Tutorials-new-robot-sim.png
Once you're done, stop the scenario with
\verbatim
./stopScenario.sh 9
\endverbatim
*/
