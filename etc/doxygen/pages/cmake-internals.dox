/**
\page cmake-internals CMake Internals


This document describes how the CMake based ArmarX build system works internally.
All necessary files are located in ${ArmarXCore_DIR}/etc/cmake.

\section cmake-internals-initialization ArmarX CMake: Bootup

    Each CMakeLists.txt file of an ArmarX Package usually starts with the following lines.

\verbatim
cmake_minimum_required(VERSION 2.8)

find_package("ArmarXCore" REQUIRED
    PATHS ....
)

include(${ArmarXCore_USE_FILE})
\endverbatim

    The first line specifies the required CMake version.
    The second line locates the whereabouts of the ArmarXCore Package which can either be
    a source build or an installed version (see the section about
    \ref cmake-internals-finding "Finding Packages" to see how this works).

    If the find_package script fails the configuration gets aborted due to the REQUIRED flag.
    If ArmarXCore has been found variables like ${ArmarXCore_CMAKE_DIR} are defined.
    For every other Package the variable is named ${${PackageName}_CMAKE_DIR}.

    Finally the file defined by ${ArmarXCore_USE_FILE} gets included
    which imports all necessary ArmarX macros and does all the basic setup needed to
    compile ArmarX Packages.
    What happens is described in more detail in the following sections.

    Each package must contain a file ${${PackageName}_DIR}/etc/cmake/Use${PackageName}.cmake.
    Package specific macros must be put into this file (or included).
    This file can be safely referred to as ${${PackageName}_USE_FILE} both for
    in-source builds and installed packages.
    When the armarx-package tool is used to create a new packages this file is created
    automatically.

\section cmake-internals-using-packages ArmarX CMake: Using Packages

    In order to ease using ArmarX Packages the following macro must be used in
    the top-level CMakeLists.txt file of the package:

    \verbatim
depends_on_armarx_package(PackageName)
    \endverbatim

    This line will do the following things

    \li find the package
    \li add ${PackageName}_INCLUDE_DIRS
    \li include ${PackageName}_USE_FILE (to make package specific macros available
    \li add ${PackageName}_CMAKE_DIR to CMAKE_MODULE_PATH
    \li setup dependencies to be used by 'make dependencies'

    'make dependencies' uses a CMake feature called 'External Project'.
    For details have a look at the following resources:

    \li Console command: cmake --help-module ExternalProject
    \li http://www.kitware.com/media/html/BuildingExternalProjectsWithCMake2.8.html

\section cmake-internals-finding Finding Packages produced by ArmarX CMake

    \li All generated binaries libraries and dependencies are exported into CMake scripts
        named ${PROJECT_NAME}Config.cmake, ${PROJECT_NAME}ConfigVersion.cmake, and
        ${PROJECT_NAME}LibraryDepends.cmake

    \li Prevent single libraries from being built by adding
        -DDISABLE_LIBRARY_${Library_Name}=TRUE as a commandline parameter to cmake
        or by setting these variables in ccmake or cmake-gui

*/
