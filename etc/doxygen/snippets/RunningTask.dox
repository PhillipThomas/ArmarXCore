#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

namespace armarx
{
    class RunningTaskExample :
        virtual public Component
    {
    public:
        virtual std::string getDefaultName()
        {
            return "RunningTaskExample";
        }
            
        virtual void onInitComponent()
        {
        }

        virtual void onConnectComponent()
        {
            // create a running task which uses exampleThreadMethod in this class as thread method
            // namespace needs to be specified with the method name
            exampleTask = new RunningTask<RunningTaskExample>(this, &RunningTaskExample::exampleThreadMethod);

            // start the thread
            exampleTask->start();
        }
            
        virtual void onDisconnectComponent()
        {
            // stop the thread and wait for join
            exampleTask->stop();
        }
            
        void exampleThreadMethod()
        {
            // do the work
            printf("Thread method\n");
                
            // shutdown is set if exampleTask->stop() is called.
            // running tasks need to handle the shutdown by themselves.
            // Here it is essentially not necessary but just serves as an example
            while(!exampleTask->isStopped())
            {
                usleep(10);
            }
        }

    private:
        // shared pointer to RunningTask
        RunningTask<RunningTaskExample>::pointer_type exampleTask;
    };

}
