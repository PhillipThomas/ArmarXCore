#!/usr/bin/env python2

##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Mirko Waechter (mirko dot waechter at kit dot edu)
# @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
# @date       2014, 2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import argparse

import ArmarXBuilder as ArmarXBuilder


def main():
    parser = argparse.ArgumentParser(description='ArmarX build script for building ArmarX packages with all its depedencies easily')
    parser.add_argument('package', nargs='*', default=['Armar3', 'Armar4'])
    parser.add_argument('-j', help='Number of cores to use for compilation. Number of cores of cpu is used of omitted.', default="0", dest="jobCount")
    parser.add_argument('--pull', '-p', help='Git pull all dependencies and target project before compilation', action="store_true", dest="pull")
    parser.add_argument('--stashnpull', '-sp', help='Git stash&&pull&&pop all dependencies and target project before compilation', action="store_true", dest="stashnpull")
    parser.add_argument('--cmake', '-c', help='Force cmake to run before compilation', action="store_true", dest="cmake")
    parser.add_argument('--ninja', '-n', help='Use ninja instead of make', action="store_true", dest="ninja")
    parser.add_argument('--nodefaultpath', help='Do not use default paths for the packages and always ask the user for the path in case of unknown packages', action="store_true", dest="nodefaultpath")

    args = parser.parse_args()

    generator = None
    if args.ninja:
        generator = ArmarXBuilder.ArmarXNinjaGenerator(args.jobCount)
    else:
        generator = ArmarXBuilder.ArmarXMakeGenerator(args.jobCount)

    builder = ArmarXBuilder.ArmarXBuilder(generator, args.cmake, args.pull, args.stashnpull, args.nodefaultpath)

    packages = args.package
    for package in packages:
        if not builder.buildPackageWithDependencies(package):
            exit(1)

if __name__ == "__main__":
    main()
