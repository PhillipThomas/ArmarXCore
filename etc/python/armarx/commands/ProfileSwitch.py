##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command
from ProfileCommand import ProfileCommand
from armarx.Profile import Profile
import argparse

def getProfiles(prefix, **kwargs):
    return Profile.getAvailableProfiles()

class ProfileSwitch(Command):

    commandName = "switch"

    requiredArgumentCount = 0

    requiresIce = False

    parser = argparse.ArgumentParser(description='Switch to a different armarx-ice profile')


    def __init__(self, configuration, profile):
        super(ProfileSwitch, self).__init__(configuration, profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('profile', nargs='?').completer = getProfiles


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        if not args.profile:
            profileCommand = ProfileCommand(self.configuration, self.profile)
            profileCommand.execute([])
            return

        profilename = args.profile
        profile = Profile(profilename)

        print "Switching to profile " + profilename

        # check if profile exists
        if not profile.exists():
            print "Profile does not exist: " + profilename
            print "Creating new profile from default template"
            if not profile.create():
                print "Error creating profile"
                return

        # check if current icegrid is running and warn user
        if self.iceHelper().isIceGridRunning(self.configuration):
            print "You are currently connected to: " + self.configuration.iceGridRegistryName
            print "Switch back to the <" + self.configuration.profilename + "> profile to connect to this instance again"

        # link new profile
        profile.link()

        # write new profile name to the ini file
        self.configuration.profilename = profilename


    @classmethod
    def getHelpString(cls):
        return "Switch configuration profiles (default.cfg, IceGrid synchronisation)"