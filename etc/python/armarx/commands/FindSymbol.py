##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'waechter'

import argparse
import os.path, os, subprocess
from subprocess import call

from armarx import ArmarXBuilder

from Command import Command, CommandType





class FindSymbol(Command):
    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        BOLD = '\033[1m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'

    builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False)
    commandName = "findSymbol"
    commandType = CommandType.Developer
    requiredArgumentCount = 0

    requiresIce = False

    parser = argparse.ArgumentParser(description='Finds a c++ symbol defined in an ArmarX package')


    def __init__(self, configuration, profile):
        super(FindSymbol, self).__init__(configuration, profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('--package', '-p', help="Toplevel ArmarX Package to search through. Dependencies are also searched.", dest='package', required = True).completer = ArmarXBuilder.getArmarXDefaultPackages
        subParser.add_argument('--symbol', '-s', help="Symbol name to search for. Can be part of the symbol name. Case-sensitive.", dest='symbol', required = True)



    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        packageName = args.package
        generator = None
        print "Searching for '" + args.symbol + "'"
        self.builder = ArmarXBuilder.ArmarXBuilder(generator, False, False, False, False)
        ret = self.builder.searchSymbolInPackageTree(packageName, args.symbol)
        if ret[0]:
            print ret[1]
        else:
            print "Could not find symbol '" + args.symbol + "'"


    @classmethod
    def getHelpString(cls):
        return "Find a symbol defined in an ArmarX Project"
