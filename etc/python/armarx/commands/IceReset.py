##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command
from IceStop import IceStop
from IceStart import IceStart

import os
import shutil

class IceReset(Command):

    commandName = "reset"

    requiredArgumentCount = 0

    requiresIce = False

    def __init__(self, configuration, profile):
        super(IceReset, self).__init__(configuration, profile)


    def execute(self, args):
        """
        stop IceGrid if it is running
        remove data directory of the IceGridRegistry node
        start IceGrid again
        """
        if self.iceHelper().isIceGridRunning(self.configuration):
            print "Stopping Ice"
            IceStop(self.configuration, self.profile).execute([])

        iceGridRegistryDataDir = os.path.join(self.configuration.iceGridNodeDirectory, self.configuration.iceGridRegistryNodeName, "registry")
        try:
            print "Removing all data dirs of Ice"
            shutil.rmtree(iceGridRegistryDataDir)
        except:
            print "could not remove IceGrid registry data directory: " + iceGridRegistryDataDir

        print "Starting Ice again"
        IceStart(self.configuration, self.profile).execute([])

    @classmethod
    def getHelpString(cls):
        return "stop IceGrid, delete IceGrid registry, start IceGrid"