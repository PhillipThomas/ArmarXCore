##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command

import argparse


class KillApplication(Command):

    commandName = "kill"

    requiredArgumentCount = 1

    parser = argparse.ArgumentParser(description='Kills an Ice application')

    def __init__(self, configuration, profile):
        super(KillApplication, self).__init__(configuration, profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):

        subParser.add_argument('app', nargs='+').completer = KillApplication.getIceApplications
        subParser.add_argument('--signal', '-s', help='Signal to send to process', default='SIGKILL', dest="signal")


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        signalString = args.signal

        self.iceHelper().sendSignalToApplications(args.app, signalString)


    @classmethod
    def getHelpString(cls):
        return "send SIGKILL to all programs running in the given application (pass SIGZZZ or signal number as a second parameter if you want to send something other then KILL)"

    @classmethod
    def getIceApplications(cls, prefix, **kwargs):
        return cls.iceHelper().getApplicationList()