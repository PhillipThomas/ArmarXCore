##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import os

class Configuration:
    armarxUserConfigurationDirectory = os.path.join(os.getenv("HOME"), ".armarx")
    # make sure $HOME/.armarx exists
    if not os.path.exists(armarxUserConfigurationDirectory):
        os.makedirs(armarxUserConfigurationDirectory, 0700)
    armarx_ini = os.path.join(armarxUserConfigurationDirectory, "armarx.ini")
    armarx_profiles_directory = os.path.join(armarxUserConfigurationDirectory, "profiles")
    armarxCoreDirectory = None #os.path.join(os.getenv("HOME"), "armarx", "Core")
    armarxCoreConfigurationDirectory = None #os.path.join(armarxCoreDirectory, "build", "config")
    profilename = "default"
    iceGridUsername = "user"
    iceGridPassword = "password"
    iceGridNodeNames = ["NodeMain"]
    iceGridRegistryNodeName = "NodeMain"
    iceGridRegistryName = "IceGrid/Registry"
    iceGridNodeDirectory = os.path.join(armarxUserConfigurationDirectory, "icegridnode_data")
    iceGridDefaultConfigurationFilename = "default.cfg"
    iceGridDefaultConfigurationFile = os.path.join(armarxUserConfigurationDirectory, iceGridDefaultConfigurationFilename)
    iceGridSyncVariablesFile = "icegrid-sync-variables.icegrid.xml"


    @classmethod
    def getIceGridConfigForProfile(cls, profilename):
        return os.path.join(cls.armarx_profiles_directory, profilename, cls.iceGridDefaultConfigurationFilename)


    @classmethod
    def setArmarXCoreDir(cls, coreDir, buildDir, includeDirs, configDir):
        cls.armarxCoreDirectory = coreDir
        cls.armarxCoreConfigurationDirectory = configDir
        cls.armarxCoreIncludeDirectories = includeDirs
