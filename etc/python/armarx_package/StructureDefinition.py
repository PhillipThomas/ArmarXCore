##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from xml.etree import ElementTree as ET
from DirectoryDefinition import DirectoryDefinition
from TemplateReplacementStrategy import TemplateReplacementStrategy

class StructureDefinition(object):

    ##
    # Constructor
    #
    # @param structureDefinitionFile  XML package structure definition file path
    #
    def __init__(self, structureDefinitionFile, templateReplacementStrategy=TemplateReplacementStrategy):
        self._structureDefinitionFile = structureDefinitionFile

        #=======================================================================
        # parse structure definition
        #=======================================================================
        tree = ET.parse(self.getStructureDefinitionFile())
        structureRoot = tree.getroot()

        #=======================================================================
        # build directory hierarchy from definition
        #=======================================================================
        packageRootNode = structureRoot.getchildren()[0]
        self._packageDirectoryDefinition = DirectoryDefinition(packageRootNode, templateReplacementStrategy=templateReplacementStrategy)

        self.templateReplacementStrategy = templateReplacementStrategy


    ##
    # Returns the structure definition file path
    #
    def getStructureDefinitionFile(self):
        return self._structureDefinitionFile


    ##
    # Returns the package directory object
    #
    def getPackageDirectoryDefinition(self):
        return self._packageDirectoryDefinition


    ##
    # Returns the component definitions
    #
    def getComponentDefinitions(self, recursive = False):
        return self.getPackageDirectoryDefinition().getComponentDefinitions(recursive)


