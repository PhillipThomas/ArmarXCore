##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
#

from tempfile import NamedTemporaryFile
from subprocess import call

class Manpage(object):

    def setProgName(self, progName):
        self.progName = progName

    def getProgName(self):
        return self.progName

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setSynopsis(self, synopsis):
        self.synopsis = synopsis

    def getSynopsis(self):
        return self.synopsis

    def setDescription(self, description):
        self.description = description

    def getDescription(self):
        return self.description

    def setBriefDescription(self, briefDescription):
        self.briefDescription = briefDescription

    def getBriefDescription(self):
        return self.briefDescription

    def setOptions(self, options):
        self.options = options

    def getOptions(self):
        return self.options

    def setExamples(self, examples):
        self.examples = examples

    def getExamples(self):
        return self.examples

    def setSeeAlsoSection(self, seeAlso):
        self.seeAlso = seeAlso

    def getSeeAlsoSection(self):
        return self.seeAlso

    def formatHeader(self, header):
        if header:
            return '.SH %s' % header.upper()
        else:
            return ''

    def getOutputPrefix(self):
        return self.outputPrefix

    def formatPage(self):
        content = '.TH %s 1 "29 April 2013"\n' % self.getProgName()

        content += '%s\n%s %s\n' % (self.formatHeader('Name'),
                                self.getProgName(),
                                self.getName())

        if self.getSynopsis():
            content += '%s\n%s' % (self.formatHeader('Synopsis'),
                                   self.getSynopsis())

        if self.getDescription():
            content += '%s\n%s' % (self.formatHeader('Description'),
                                   self.getDescription())

        if self.getOptions():
            content += self.getOptions()

        if self.getExamples():
            content += '%s\n%s' % (self.formatHeader('Examples'),
                                   self.getExamples())

        if self.getSeeAlsoSection():
            content += '%s\n%s' % (self.formatHeader('See also'),
                                   self.getSeeAlsoSection())

        return content


    def display(self):
        content = self.formatPage()

        tf = NamedTemporaryFile(prefix='armarx-package_' + self.name + '-',
                                delete=False)

        tf.write(content.encode('utf-8'))
        tf.close()
        call(['man', tf.name])
        tf.unlink(tf.name)

    name = 'Undefined'
    progName = None
    synopsis = None
    description = None
    briefDescription = None
    options = None
    examples = None
    seeAlso = None
    outputPrefix = ">"
