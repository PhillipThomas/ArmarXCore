##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from argparse import RawTextHelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.Structure import Structure
from armarx_package.TemplateReplacementStrategy import TemplateReplacementStrategy

from armarx_package.Exceptions import UndefinedComponentException

from armarx_package.Completers import PositionalCompleter
from argcomplete.completers import ChoicesCompleter

from armarx_package.Manpage import Manpage

class RemoveSubCmd(SubCommand):

    ##
    # remove sub-command constructor
    #
    def __init__(self, parserContainer):


        self.name = 'remove'

        self.manpage = Manpage()

        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
Removes a package element. The default behavior is removing an element
from the package structure and maintaining the element files within the
package (soft remove). This may be useful if you are using revision
control software such as apache's Subversion (SVN). A different scenario
would be moving an element from one package to another. The procedure
would include performing a soft remove on the element, moving the element
to a desired package and join the element to it. For more details on
joining elements to packages check out 'add' sub-command.

Nevertheless, if you know what you are doing and insist on removing the
element entirely, use the --force option to delete the element's files
as well.

To maintain an overview of the soft removed elements you may run
'status -all' which will list all components within a package including
the removed ones. For more details on that check out the 'status'
sub-command.
''')

        self.manpage.setBriefDescription('Removes a package element.')
        self.manpage.setExamples('''
.B 1.
Perform a soft remove on a package element

    $ cd /path/to/workspace/PickAndPlace
    $ armarx-package remove armarx-object MotionPlaner
    $ %s MotionPlaner element has been removed from the package.
    $ %s Note: The component is no longer a part of the package yet the files still remain in:
    $ %s /path/to/workspace/PickAndPlace/source/PickAndPlace/armarx-objects/MotionPlaner

.B 2.
Remove an element entirely from a package. This is irreversible!

    $ cd /path/to/workspace/PickAndPlace
    $ armarx-package remove armarx-object MotionPlaner
    $ %s Deleting element files ...
    $ %s MotionPlaner element has been removed completely from the package.
''' % (u'>',
       u'>',
       u'>',
       u'>',
       u'>'))

        self.parser = parserContainer.add_parser(
                               self.name,
                               help="Removes an existing package component",
                               formatter_class=RawTextHelpFormatter,
                               description=self.manpage.getBriefDescription(),
                               epilog='Checkout \'armarx-package help remove\' for more details and examples!')

        self.parser.add_argument("elementCategory", help="ArmarX Package element category")\
            .completer = ChoicesCompleter(self.getCategoryList())

        self.parser.add_argument("elementName", help="Name of the element which should be removed.")\
            .completer = PositionalCompleter("elementName")

        self.parser.add_argument("-f", "--force", action='store_true', help="Force element removal")

        self.addTemplateReplacementStrategyArgument()

        self.parser.set_defaults(func = self.execute)

        self.addGlobalArguments()


    def execute(self, args):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), "package-structure.xml")

        templateReplacementStrategy = TemplateReplacementStrategy.getStrategyForComponentType(args.elementCategory, args.strategy)

        # create a package structure object for an existing package
        structure = Structure.CreateStructureFromPackage(structureDefinitionFilePath, args.dir, templateReplacementStrategy)
        try:
            # validate package structure before creating the component
            structure.validate()

            component = structure.getComponent(args.elementCategory, args.elementName)

            component.remove(args.force)

            if args.force:
                print self.outputPrefix, args.elementName, "element has been removed completely from the package."
            else:
                print self.outputPrefix, args.elementName, "element has been removed from the package."

                print self.outputPrefix, "Note: The element is no longer a part of the package yet the files still remain in:"

                for directory in component.getDirectories():
                    print self.outputPrefix, "  ", directory.getPath()

        except UndefinedComponentException as unknownCompExc:
            self.handleException(args, unknownCompExc)

            self.printAvailableCategories(
                    structure.getComponentDefinitions(True))

        except Exception as exc:
            self.handleException(args, exc)
