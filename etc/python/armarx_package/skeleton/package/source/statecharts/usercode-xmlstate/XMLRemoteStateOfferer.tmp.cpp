/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::@LIBRARY_NAME@::@LIBRARY_NAME@RemoteStateOfferer
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@LIBRARY_NAME@RemoteStateOfferer.h"

using namespace armarx;
using namespace @LIBRARY_NAME@;

// DO NOT EDIT NEXT LINE
@LIBRARY_NAME@RemoteStateOfferer::SubClassRegistry @LIBRARY_NAME@RemoteStateOfferer::Registry(@LIBRARY_NAME@RemoteStateOfferer::GetName(), &@LIBRARY_NAME@RemoteStateOfferer::CreateInstance);



@LIBRARY_NAME@RemoteStateOfferer::@LIBRARY_NAME@RemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < @LIBRARY_NAME@StatechartContext > (reader)
{
}

void @LIBRARY_NAME@RemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void @LIBRARY_NAME@RemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void @LIBRARY_NAME@RemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string @LIBRARY_NAME@RemoteStateOfferer::GetName()
{
    return "@LIBRARY_NAME@RemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr @LIBRARY_NAME@RemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new @LIBRARY_NAME@RemoteStateOfferer(reader));
}



