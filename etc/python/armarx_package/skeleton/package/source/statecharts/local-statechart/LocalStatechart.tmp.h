/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::statechart::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_STATECHART_@PACKAGE_NAME@_@COMPONENT_NAME@_H
#define _ARMARX_STATECHART_@PACKAGE_NAME@_@COMPONENT_NAME@_H

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    /**
     * @class @COMPONENT_NAME@
     */
    class @COMPONENT_NAME@ :
        virtual public StatechartContext
    {
    public:
        /**
         * @see StatechartContext::onInitStatechart()
         */
        virtual void onInitStatechart();

        /**
         * @see StatechartContext::onConnectStatechart()
         */
        virtual void onConnectStatechart();

        /**
         * @see ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const
        {
            return "@COMPONENT_NAME@";
        }
    };

    // Define Events before the first state they are used in
    // e.g.
    // DEFINEEVENT(EvInit)

    /**
     * Top-level state
     */
    struct Statechart@COMPONENT_NAME@: StateTemplate < Statechart@COMPONENT_NAME@ >
    {
        void defineState();
        void defineParameters();
        void defineSubstates();
        void onEnter();
        void onExit();
    };
}

#endif
