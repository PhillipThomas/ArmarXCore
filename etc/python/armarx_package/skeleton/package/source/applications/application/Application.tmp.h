/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::application::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_@PACKAGE_NAME@_@COMPONENT_NAME@_H
#define _ARMARX_APPLICATION_@PACKAGE_NAME@_@COMPONENT_NAME@_H


// #include <@PACKAGE_NAME@/components/@MyComponent@/@MyComponent@.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class @COMPONENT_NAME@App
     * @brief A brief description
     *
     * Detailed Description
     */
    class @COMPONENT_NAME@App :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject( armarx::Component::create<@COMPONENT_NAME@>(properties) );
        }
    };
}

#endif
